package security;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import static common.Common.User.*;
import dao.UserDao;
import service.UserService;

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler{

	@Autowired
	private UserService userService; 
	
	@Autowired
	private UserDao userDao;
	
	private static Logger logger = Logger.getLogger(LoginSuccessHandler.class);
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		
		logger.info("functionCall : onAuthenticationSuccess");
		
		Map<String, Object> argMap = new HashMap<>();
		
		argMap.put(USER_ID, authentication.getName());
		Map<String, Object> user = userDao.selectUser(argMap);
		
		BigInteger user_idx_temp = (BigInteger)user.get(USER_IDX);
		int user_idx = user_idx_temp.intValue();
		
		int status = 1;
		
		if(userService.insertUserLog(request, user_idx, status) > 0) {
			System.out.println("call : projectList");
			response.sendRedirect("/project/projectList");
		}else{
			response.sendRedirect("/user/loginForm");
		};
	}
}