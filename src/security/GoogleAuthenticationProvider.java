package security;

import static common.Common.User.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import model.Role;
import service.UserService;


@Component
public class GoogleAuthenticationProvider implements AuthenticationProvider {

	private static Logger logger = Logger.getLogger("GoogleAuthenticationProvider.java");
	
	@Autowired
	private UserService userService;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException{
		logger.info("functionCall : google_authenticationProvider");
		
		String user_id = authentication.getName();
		String user_pw = (String) authentication.getCredentials();
		
		UsernamePasswordAuthenticationToken authToken = null;
		
		Map<String, Object> map = new HashMap<>();
		map.put(USER_ID, user_id);
		
		Map<String, Object> googleUser = userService.getUser(map);
		
		boolean result = user_pw.equals((String)googleUser.get(USER_PW));
		
		if(result) {
			
			BigInteger user_idx_temp = (BigInteger)googleUser.get(USER_IDX);
			int user_idx = user_idx_temp.intValue();
			map.put(USER_IDX, user_idx);
			
			List<String> authList = userService.getUserAuths(map);
			logger.info("authList : " + authList);
			
			List<Role> authorities = new ArrayList<Role>();
			for(String auth : authList) {
				authorities.add(new Role(auth));
			}
			logger.info("authorities : " + authorities);
			
			authToken = new UsernamePasswordAuthenticationToken(user_id, user_pw, authorities);
		}
		logger.info("authToken : " + authToken);
		
		return authToken;
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
}
