package security;

import static common.Common.User.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import model.Role;
import service.UserService;


@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static Logger logger = Logger.getLogger("CustomAuthenticationProvider.java");
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder encoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException{
		logger.info("functionCall : authenticationProvider");
		
		String user_id = authentication.getName();
		String user_pw = (String) authentication.getCredentials();
		String encodedPassword = encoder.encode(user_pw);
		
		UsernamePasswordAuthenticationToken authToken = null;
		
		Map<String, Object> map = new HashMap<>();
			
			map.put(USER_ID, user_id);
			
			Map<String, Object> user = userService.getUser(map);
			if(user == null) {
				return authToken;
			}
			if((int)user.get(STATUS) != 1) {
				return authToken;
			}
			
			boolean result = encoder.matches(user_pw, (String)user.get(USER_PW));
			
			if(result) {
				
				BigInteger user_idx_temp = (BigInteger)user.get(USER_IDX);
				int user_idx = user_idx_temp.intValue();
				map.put(USER_IDX, user_idx);
				
				List<String> authList = userService.getUserAuths(map);
				logger.info("authList : " + authList);
				
				List<Role> authorities = new ArrayList<Role>();
				for(String auth : authList) {
					authorities.add(new Role(auth));
				}
				logger.info("authorities : " + authorities);
				
				authToken = new UsernamePasswordAuthenticationToken(user_id, encodedPassword, authorities);
				
			}
			logger.info("authToken : " + authToken);
			
			return authToken;
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}
}
