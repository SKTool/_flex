package common;

public class Constants {
	public static class Project {
		public static final String PROJECT_IDX = "project_idx";
		public static final String PROJECT_TITLE = "project_title";
		public static final String PROJECT_WIKI = "project_wiki";
		public static final String PROJECT_START = "project_start";
		public static final String PROJECT_END = "project_end";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}
	
	public static class ProjectMember {
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
		public static final String BELL_ACTIVE = "bell_active";
		public static final String AUTH_IDX = "auth_idx";
		public static final String USER_IDX = "user_idx";
		public static final String PROJECT_IDX = "project_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}
	
	public static class Auth {
		public static final String AUTH_IDX = "auth_idx";
		public static final String AUTH_NAME = "auth_name";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}

	public static class Schedule {
		public static final String PROJECT_IDX = "project_idx";
		public static final String SCHEDULE_IDX = "schedule_idx";
		public static final String SCHEDULE_NAME = "schedule_name";
		public static final String SCHEDULE_START = "schedule_start";
		public static final String SCHEDULE_END = "schedule_end";
		public static final String SCHEDULE_COLOR = "schedule_color";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}

	public static class Qna {
		public static final String QNA_IDX = "qna_idx";
		public static final String QNA_PW = "qna_pw";
		public static final String QNA_CONTENT = "qna_content";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String USER_IDX = "user_idx";
	}

	public static class QnaAnswer {
		public static final String ADMIN_IDX = "admin_idx";
		public static final String QNA_IDX = "qna_idx";
		public static final String QNA_ANSWER_IDX = "qna_answer_idx";
		public static final String QNA_ANSWER_CONTENT = "qna_answer_content";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}

	public static class AdminBoard {
		public static final String ADMIN_IDX = "admin_idx";
		public static final String ADMIN_BOARD_IDX = "admin_board_idx";
		public static final String ADMIN_BOARD_TITLE = "admin_board_title";
		public static final String ADMIN_BOARD_CONTENT = "admin_board_content";
		public static final String ADMIN_BOARD_READ_COUNT = "admin_board_read_count";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}

	public static class UserBell {
		public static final String USER_BELL_IDX = "user_bell_idx";
		public static final String USER_BELL_CONTENT = "user_bell_content";
		public static final String USER_BELL_TYPE = "user_bell_type";
		public static final String USER_BELL_OCC_DATE = "user_bell_occ_date";
		public static final String USER_IDX = "user_idx";
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}
	
	public static class MsgBoard{
		public static final String MSGBOARD_IDX = "msgboard_idx";
		public static final String MSGBOARD_TITLE = "msgboard_title";
		public static final String MSGBOARD_CONTENT = "msgboard_content";
		public static final String MSGBOARD_FILE = "msgboard_file";
		public static final String MSGBOARD_READ_COUNT = "msgboard_read_count";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String USER_IDX = "user_idx";
	}
	
	public static class MsgBoardReply{
		public static final String MSGBOARD_REPLY_IDX = "msgboard_reply_idx";
		public static final String MSGBOARD_REPLY_CONTENT = "msgboard_reply_content";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String USER_IDX = "user_idx";
		public static final String MSGBOARD_IDX = "msgboard_idx";
	}
	
	public static class Recruitment{
		public static final String RECRUITMENT_IDX = "recruitment_idx";
		public static final String RECRUITMENT_TITLE = "recruitment_title";
		public static final String RECRUITMENT_CONTENT = "recruitment_content";
		public static final String RECRUITMENT_PERSONNEL = "recruitment_personnel";
		public static final String RECRUITMENT_PW= "recruitment_pw";
		public static final String RECRUITMENT_READ_COUNT = "recruitment_read_count";
		public static final String RECRUITMENT_TERM = "recruitment_term";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String USER_IDX = "user_idx";
	}
	
	
	public static class Chat{
		public static final String CHAT_IDX = "chat_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String PROJECT_IDX = "project_idx";
	}
	
	public static class ChatMember{
		public static final String CHAT_MEMBER_IDX = "chat_member_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String CHAT_IDX = "chat_idx";
		public static final String USER_IDX = "user_idx";
	}
	
	public static class ChatContent{
		public static final String CHAT_CONTENT_IDX = "chat_content_idx";
		public static final String CHAT_CONTENT_CONTENT = "chat_content_content";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String CHAT_MEMBER_IDX = "chat_member_idx";
	}
	
	public static class PlatFormList{
		public static final String PLATFORM_LIST_IDX = "platform_list_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String RECRUITMENT_IDX = "recruitment_idx";
		public static final String PLATFORM_IDX = "platform_idx";
	}
	
	public static class JobGroupList{
		public static final String JOB_GROUP_LIST_IDX = "job_group_list_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
		public static final String RECRUITMENT_IDX = "recruitment_idx";
		public static final String JOB_GROUP_IDX = "job_group_idx";
	}
	
	public static class Git {
		public static final String GIT_IDX = "git_idx";
		public static final String GIT_REPO = "git_repo";
		public static final String GIT_USER = "git_user";
		public static final String GIT_ID = "git_id";
		public static final String GIT_TOKEN = "git_token";
		public static final String GIT_ISSUES_COUNT = "git_issues_count";
		public static final String GIT_PUSH_COUNT = "git_push_count";
		public static final String GIT_PULL_REQUESTS_COUNT = "git_pull_requests_count";
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUS = "status";
	}

}
