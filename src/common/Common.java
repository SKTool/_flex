package common;

public class Common {

	public static class Chat {
		// common field
		public static final String IN_D = "in_d";
		public static final String R_D = "r_d";
		public static final String STATE = "state";

		// chat_member
		public static final String CHAT_MEMBER_IDX = "chat_member_idx";

		// chat
		public static final String CHAT_IDX = "chat_idx";
		public static final String CHAT_HEAD_COUNT = "chat_head_count";
		public static final String CHAT_TITLE = "chat_title";
		public static final String CHAT_CONTENT = "chat_content";

		// content
		public static final String CONTENT_IDX = "content_idx";
		public static final String USER_IDX = "user_idx";
		public static final String CONTENT = "content";

		// file
		public static final String FILE_IDX = "file_idx";
		public static final String FILE_URL = "file_url";

		// pic_video
		public static final String PIC_VIDEO_IDX = "pic_video_idx";
		public static final String PIC_VIDEO_URL = "pic_video_url";
	}

	public static class Todo {
		public static final String TO_DO_IDX = "to_do_idx";
		public static final String TO_DO_CONTENT = "to_do_content";
		public static final String TO_DO_STATUS = "to_do_status";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUE = "statue";
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
	}

	public static class Bug {
		public static final String BUG_IDX = "bug_idx";
		public static final String OCC_DATE = "occ_date";
		public static final String BUG_TITLE = "bug_title";
		public static final String BUG_CONTENT = "bug_content";
		public static final String BUG_WHERE = "bug_where";
		public static final String BUG_CRITICAL = "bug_critical";
		public static final int LOW = 1;
		public static final int NORMAL = 2;
		public static final int HIGH = 3;
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUE = "statue";
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
	}

	public static class BugReply {
		public static final String BUG_REPLY_IDX = "bug_reply_idx";
		public static final String BUG_REPLY_CONTENT = "bug_reply_content";
		public static final String IN_DATE = "in_date";
		public static final String M_DATE = "m_date";
		public static final String STATUE = "statue";
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
		public static final String BUG_IDX = "bug_idx";
	}

	public static class User {
		public static final String USER_IDX = "user_idx";
		public static final String USER_ID = "user_id";
		public static final String USER_PW = "user_pw";
		public static final String USER_PIC = "user_pic";
		public static final String USER_FILE = "user_file";
		public static final String USER_NAME = "user_name";
		public static final String USER_TEL = "user_tel";
		public static final String STATUS = "status";
		public static final String ETC = "etc";
		public static final String USER = "user";
		public static final String MSG = "msg";
		public static final String FILE_PATH = "C:\\temp\\";
		public static final String RESULT = "result";
		
		public static final String USER_PW_OLD = "user_pw_old";
		public static final String USER_PW_NEW = "user_pw_new";
		
		public static final String GOOGLE = "google";
		public static final String GOOGLE_USER_IDX = "google_user_idx";
		public static final String GOOGLE_USER_ID = "google_user_id";
		public static final String GOOGLE_USER_EMAIL = "google_user_email";
		public static final String GOOGLE_USER_NAME = "google_user_name";
		public static final String GOOGLE_USER_PIC = "google_user_pic";

		public static final String USER_AUTH = "user_auth";
		public static final String USER_AUTH_IDX = "user_auth_idx";

		public static final String TRUE = "true";
		public static final String FALSE = "false";

		public static final String USER_LOG_IP = "user_log_ip";

		public static final int DEFAULT = 0;
		public static final int SUCCESS = 1;
		public static final int FAIL = -1;
		public static final int ALREADY_DONE = 2;
		public static final int INAPPROPRIATE_ACCESS = -2;
		public static final int CERTIFICATION_NEEDED = -2;
		public static final int PASSWORD_WRONG = -2;
		public static final int CHECKED = 1;
		
		
		public static final String PROJECT_MEMBER_IDX = "project_member_idx";
		public static final String PROJECT_IDX = "project_idx";
		public static final String PROJECT_END = "project_end";
		public static final String BELL_ACTIVE = "bell_active";
		public static final String FILE_NAME = "file_name";
	}

}
