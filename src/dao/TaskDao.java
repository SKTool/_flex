package dao;

import java.util.List;
import java.util.Map;

public interface TaskDao {
	//Task
	public int insertTask(Map<String,Object> param);
	public int updateTask(Map<String,Object> param);
	public int updateTaskStatus(Map<String,Object> param);
	public int deleteTask(int task_idx);
	public Map<String,Object> selectOneTask(int task_idx);
	public List<Map<String,Object>> selectKanban();
	public List<Map<String,Object>> selectByTaskMember(int task_idx);
	public List<Map<String,Object>> selectByTaskTag(int task_idx);
	public List<Map<String,Object>> selectAllTask();
	public List<Map<String,Object>> selectAllTaskByProject(int project_idx);
	public List<Map<String,Object>> fastSelectMember(int project_idx);
	public List<Map<String,Object>> fastSelectTaskTag();
	public List<Map<String,Object>> fastSelectViewMember(Map<String,Object> param);
	public List<Map<String,Object>> fastSelectViewTag(int task_idx);
	
	//TaskMember
	public int insertTaskMember(Map<String,Object> param);
	public int updateTaskMember(Map<String,Object> param);
	public int deleteTaskMember(int task_idx);
	public Map<String,Object> selectOneTaskMember(int task_member_idx);
	public List<Map<String,Object>> selectAllTaskMember();
	public List<Map<String, Object>> selectTasksByProjectMemberIdx(Map<String, Object> map);
	
	//TaskTagList
	public int insertTaskTagList(Map<String,Object> param);
	public int updateTaskTagList(Map<String,Object> param);
	public int deleteTaskTagList(int task_idx);
	public Map<String,Object> selectOneTaskTagList(int task_tag_list_idx);
	public List<Map<String,Object>> selectAllTaskTagList();
	

}
