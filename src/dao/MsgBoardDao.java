package dao;

import java.util.List;
import java.util.Map;

public interface MsgBoardDao {
	//MsgBoard Dao
	public int insertMsgBoard(Map<String,Object> params);
	public int updateMsgBoard(Map<String,Object> params);
	public int deleteMsgBoard(int msgboard_idx);
	public Map<String,Object> selectOneMsgBoard(int msgboard_idx);
	public int selectTotalCount(Map<String,Object> params);
	public List<Map<String,Object>> msgBoardList(Map<String,Object> params);
	public int plusReadCount(int msgboard_idx);
	
	
	//MsgBoard_Reply Dao
	public int insertMsgBoardReply(Map<String,Object> params);
	public int updateMsgBoardReply(Map<String,Object> params);
	public int deleteMsgBoardReply(int msgboard_Reply_idx);
	public Map<String,Object> selectOneMsgBoardReply(int msgboard_reply_idx);
	public List<Map<String,Object>> selectAllMsgBoardReply();
	public List<Map<String,Object>> selectByMsgBoardIdx(int msgboard_idx);
	
	
	
	/////////////////////////////////////////////////////////////////////////////
	
	public List<Map<String, Object>> selectAllMsgBoard(Map<String, Object> params);	
	public int selectTotalCountMsgBoard(Map<String, Object> param);
	

	// 대형
	public Map<String, Object> selectCountMsgboard();
	public List<Map<String, Object>> selectQuarterDate();	
	
	
}