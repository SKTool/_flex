package dao;

import java.util.List;
import java.util.Map;

public interface TodoDao {
	//Todo
	public int insertTodo(Map<String,Object> param);
	public int updateTodo(Map<String,Object> param);
	public int updateStatusTodo(Map<String,Object> param);
	public int deleteTodo(int to_do_idx);
	public Map<String,Object> selectOneTodo(int to_do_idx);
	public List<Map<String,Object>> selectAllTodo();
	public List<Map<String,Object>> selectMyTodo(int user_idx);
	public int selectMyProjectMemberIdx(Map<String,Object> param);
}
