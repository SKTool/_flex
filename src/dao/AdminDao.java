package dao;

import java.util.List;
import java.util.Map;

public interface AdminDao {

	public int insertAdminBoard(Map<String, Object> param);
	public int updateAdminBoard(Map<String, Object> param);
	public int updateAdminBoardReadCount(int admin_board_idx);
	public int deleteAdminBoard(int admin_board_idx);
	public Map<String, Object> selectOneAdminBoard(int admin_board_idx);
	public List<Map<String, Object>> selectAllFaq();	
	public List<Map<String, Object>> selectAllAdminBoard(Map<String, Object> params);	
	public int selectTotalCountAdminBoard(Map<String, Object> param);
	
	

	// 대형
	public Map<String, Object> selectCountAdmin();
	public List<Map<String, Object>> selectQuarterDate();	
	
		
}




