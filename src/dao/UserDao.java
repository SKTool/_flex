package dao;

import java.util.List;
import java.util.Map;

public interface UserDao {
//	user
	public int insertUser(Map<String, Object> map);
	public int updateUser(Map<String, Object> map);
	public int deleteUser(Map<String, Object> map);
	public Map<String, Object> selectUser(Map<String, Object> map);
	public int insertUserLog(Map<String, Object> map);
	public int uploadFiletoProject(Map<String, Object> map);
	public List<Map<String, Object>> selectFilesByProjectMemberIdx(Map<String, Object> map);	
	public List<Map<String, Object>> selectGitCommitCounts(Map<String, Object> map);
 //  auth
	public int insertUserAuthList(Map<String, Object> map);
	public int updateUserAuthList(Map<String, Object> map);
	public List<String> selectUserAuthList(Map<String, Object> map);
	
//	googleUser
	public int insertGoogleUser(Map<String, Object> map);
	public int updateGoogleUser(Map<String, Object> map);
	
	// 대형
	public Map<String, Object> selectCountUser();
	public List<Map<String, Object>> selectQuarterDate();	
	public List<Map<String, Object>> selectListLimit(Map<String, Object> map);


//프로젝트 멤버 검색용
	public List<Map<String, Object>> selectAllUser();
}
