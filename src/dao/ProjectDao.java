package dao;

import java.util.List;
import java.util.Map;

public interface ProjectDao {

	// Project
	public int insertProject(Map<String, Object> param);
	public int updateProject(Map<String, Object> param);
	public int deleteProject(int project_idx);
	public Map<String, Object> checkWriteAuth(Map<String, Object> param);
	public Map<String, Object> selectOneProject(int project_idx);
	public List<Map<String, Object>> selectAllProject();
	public List<Map<String, Object>> selectAllMyProject(int user_idx);

	
	// ProjectMember
	public int insertProjectMember(Map<String, Object> param);
	public int updateProjectMember(Map<String, Object> param);
	public int updateProjectMemberAuth(Map<String, Object> param);
	public int deleteProjectMember(int project_member_idx);
	public Map<String, Object> selectOneProjectMember(int project_idx);
	public List<Map<String, Object>> selectOneProjectMemberJoinUser(int project_idx);
	public List<Map<String, Object>> selectAllProjectMember();
	public Map<String, Object> selectOneProjectMemberIdxForBro(Map<String, Object> param);
	
	

	//보민
	public int inviteMember(Map<String, Object> map);
	public List<Map<String,Object>> projectMemberList(int project_idx);
	public Map<String, Object> selectProjectMemberByUserIdx(Map<String, Object> map);
	
	//수
	public List<Map<String, Object>> selectProjectsForMyPage(Map<String, Object> map);
	
	// 대형
	public Map<String, Object> selectCountProject();
	public List<Map<String, Object>> selectQuarterDate();
	public List<Map<String, Object>> selectListLimit(Map<String, Object> map);
	public Map<String, Object> selectCountProjectMember();
	public Map<String, Object> selectDutyCycleGit();
	
	
	
	
	
}
