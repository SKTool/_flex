package dao;

import java.util.List;
import java.util.Map;

import javax.print.DocFlavor.STRING;

public interface RecruitmentDao {
	
	//recruitmentDao
	public int insertRecruitment(Map<String,Object> params);
	public int updateRecruitment(Map<String,Object> params);
	public int deleteRecruitment(int recruitment_idx);
	public Map<String,Object> selectOneRecruitment(int recruitment_idx);
	public int selectTotalCount(Map<String,Object> params);
	public List<Map<String,Object>> recruitmentList(Map<String,Object> params);
	public int plusReadCount(int recruitment_idx);
	
	
	//platformListDao
	public Map<String,Object> selectOnePlatFormList(int platform_list_idx);
	public List<Map<String,Object>> selectAllPlatFormList();
	public int insertPlatformList(Map<String, Object> map);
	public int updatePlatformList(Map<String, Object> map);
	public List<Map<String, Object>> selectPlatformList(Map<String, Object> map);
	public int deletePlatformList(Map<String,Object> map);
	
 	//jobgrouplistDao
	public Map<String,Object> selectOneJobGroupList(int job_group_list);
	public List<Map<String,Object>> selectAllJobGroupList();
	
	
	public List<Map<String, Object>> selectAllRecruitment(Map<String, Object> params);	
	public int selectTotalCountRecruitment(Map<String, Object> param);
	public List<String> selectAllByrecruitment(int recruitment_idx);

	// 대형
	public Map<String, Object> selectCountRecruitment();
	public List<Map<String, Object>> selectQuarterDate();	
	
	
	
}
