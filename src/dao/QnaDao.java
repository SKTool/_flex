package dao;

import java.util.List;
import java.util.Map;

public interface QnaDao {
	
	//QnaDao
	public int insertQna(Map<String, Object> param);
	public int updateQna(Map<String, Object> param);
	public int deleteQna(int qna_idx);
	public Map<String, Object> selectOneQna(int qna_idx);
	public List<Map<String, Object>> selectAllQna(Map<String, Object> params);	
	public int selectTotalCountQna(Map<String, Object> param);
	
	
	//QnaAnswerDao
	public int insertQnaAnswer(Map<String, Object> param);
	public int updateQnaAnswer(Map<String, Object> param);
	public int deleteQnaAnswer(int qna_answer_idx);
	public Map<String, Object> selectOneQnaAnswer(int qna_answer_idx);
	public Map<String, Object> selectOneQnaBoardWithAnswer(int qna_idx);
	public List<Map<String, Object>> selectAllQnaAnswer();	
	

	// 대형
	public Map<String, Object> selectCountQna();
	public List<Map<String, Object>> selectQuarterDate();	
	
	
}






