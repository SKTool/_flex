package dao;

import java.util.List;
import java.util.Map;

public interface ChatDao {
	
	//chatDao
	public int insertChat(Map<String,Object> params);
	public int updateChat(Map<String,Object> params);
	public int deleteChat(int chat_idx);
	public Map<String,Object> selectOneChat(int chat_idx);
	public List<Map<String,Object>> selectAllChat();
	
	//chat_memberDao
	public int insertChatMember(Map<String,Object> params);
	public int updateChatMember(Map<String,Object> params);
	public int deleteChatMember(int chat_idx);
	public Map<String,Object> selectOneChatMember(int chat_idx);
	public List<Map<String,Object>> selectAllChatMember();
	
	//chat_contentDao
	public int insertChatContent(Map<String,Object> params);
	public int updateChatContent(Map<String,Object> params);
	public int deleteChatContent(int chat_idx);
	public Map<String,Object> selectOneChatContent(int chat_idx);
	public List<Map<String,Object>> selectAllChatContent();
	

}
