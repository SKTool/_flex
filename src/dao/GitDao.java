package dao;

import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Map;

public interface GitDao {
	public int insertGit(Map<String, Object>map) throws SQLIntegrityConstraintViolationException;
	public int updateGit(int gitIdx);
	public Map<String, Object> selectOneGit(int gitIdx);
	public List<Map<String, Object>> selectAllGit();
	public int deleteOneGit(int gitIdx);
	public int deleteAllGit();

	public Map<String, Object> selectOneGitByProjectMemberIdx(int projectMemberIdx);
	public int deleteOneGitBygitIdAndProjectMemberIdx( Map<String, Object> map );
}
