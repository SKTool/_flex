package dao;

import java.util.List;
import java.util.Map;

public interface ScheduleDao {
	public int insertSchedule(Map<String, Object> param);
	public int updateSchedule(Map<String, Object> param);
	public int updateScheduleById(Map<String, Object> param);
	public int deleteSchedule(String id);
	public Map<String, Object> selectOneSchedule(int schedule_idx);
	public List<Map<String, Object>> selectOneScheduleByprojectIdx(int project_idx);
	public Map<String, Object> selectOneScheduleById(String id);
	public List<Map<String, Object>> selectAllSchedule();	
}






