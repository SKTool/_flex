package dao;

import java.util.List;
import java.util.Map;

public interface GitRestDao {

	public List<Map<String, Object>> selectAllBranchesByProjectMemberIdx(int projectMemberIdx);
	public List<Map<String, Object>> selectAllCommitsByBranch(int projectMemberIdx, String branch);
	
	// Issue
	public List<Map<String, Object>> selectAllIssueOpen(int projectMemberIdx, String labels);
	public List<Map<String, Object>> selectAllIssueClosed(int projectMemberIdx, String labels);

	// Issue Comment
	public List<Map<String, Object>> selectAllIssueComment(int projectMemberIdx, int issueNumber);
	
	// Issue Labels
	public List<Map<String, Object>> selectAllLabelsIssue(int projectMemberIdx) throws Exception;


}
