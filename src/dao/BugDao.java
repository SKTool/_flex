package dao;

import java.util.List;
import java.util.Map;

public interface BugDao {
	//Bug
	public int insertBug(Map<String,Object> param);
	public int updateBug(Map<String,Object> param);
	public int deleteBug(int bug_idx);
	public Map<String,Object> selectOneBug(int bug_idx);
	public List<Map<String,Object>> selectAllBugByProjectIdx(int project_idx);
	public List<Map<String, Object>> selectAllBugList(Map<String, Object> params);
	public int selectTotalCountBugList(Map<String, Object> param);
	public int selectMyBugMemberIdx(Map<String,Object> param);
	//수
	public List<Map<String,Object>> selectBugsByProjectMemberIdx(Map<String,Object> map);

	//BugReply
	public int insertBugReply(Map<String,Object> param);
	public int updateBugReply(Map<String,Object> param);
	public int deleteBugReply(int bug_reply_idx);
	public Map<String,Object> selectOneBugReply(int bug_reply_idx);
	public List<Map<String,Object>> selectByBugIdx(int bug_idx);
	

}
