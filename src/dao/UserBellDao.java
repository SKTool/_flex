package dao;

import java.util.List;
import java.util.Map;

public interface UserBellDao {
	int insertUserBell(Map<String, Object> map);
	int updateUserBell(Map<String, Object> map);
	Map<String, Object> selectOneUserBell(int idx);
	List<Map<String, Object>> selectAllUserBell();
	int deleteOneUserBell(int idx);
	int deleteAllUserBell();

	// 커스텀 dao
	List<Map<String, Object>> selectAllUserBellByUserIdx(Map<String, Object> map); 
	int updateStatusInUserBell(Map<String, Object> map);
}
