package controller.version;

import java.math.BigInteger;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import common.Constants.Git;
import service.UserService;
import service.version.VersionService;

@Controller
@RequestMapping("/version/commit")
public class VersionCommitController {
	private static Logger logger = Logger.getLogger("VersionController.java");
	
	@Autowired
	VersionService versionService;

	@RequestMapping(value="", method=RequestMethod.GET)
	public String viewCommitList(@RequestParam(value="branch", defaultValue="master") String branch, Model model, HttpSession session) {
		logger.info("GET /version 200");

		// 임시 데이터 삭제
//		session.setAttribute(Git.PROJECT_MEMBER_IDX, 1);

		
		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		boolean hasGitInProjectMember = versionService.hasGitInProjectMember(projectMemberIdx);
		List<Map<String, Object>> commits = null;
		List<String> branches = null;
		if ( hasGitInProjectMember == true ) {
			commits = versionService.getCommitsByBranch(projectMemberIdx, branch);
			branches = versionService.getBranches(projectMemberIdx);
		}

		logger.info("input session value " + projectMemberIdx);
		
		model.addAttribute(
				"hasGitInProjectMember",
				hasGitInProjectMember );

		model.addAttribute(
				"branches", branches );

		model.addAttribute(
				"selectedBranch", branch );

		model.addAttribute(
				"commits", commits );

		

		return "version/commitList";
	}
	
	@RequestMapping(value="/saveGitIdPw", method=RequestMethod.POST)
	public String saveGitIdPw (
			@RequestParam("git-id") String gitId, 
			@RequestParam("git-pw") String gitToken, 
			@RequestParam("repository") String gitRepoUser,
			HttpSession session ) {
		
		
		logger.info("== saveGitIdPw 파라미터 값  ==");
		logger.info(gitId);
		logger.info(gitToken);
		logger.info(gitRepoUser);
		
		StringTokenizer strToken = new StringTokenizer(gitRepoUser, "/");
		String gitUser = strToken.nextToken();
		String gitRepo = strToken.nextToken();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Git.GIT_ID, gitId);
		map.put(Git.GIT_TOKEN, gitToken);
		map.put(Git.GIT_USER, gitUser);
		map.put(Git.GIT_REPO, gitRepo);

		map.put(Git.PROJECT_MEMBER_IDX, ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue() );
		
		logger.info("== Service 전달 값 (map) ==");
		logger.info(map);
		versionService.insertGit(map);


		return "redirect:/version/commit";
	}
	
	@ResponseBody
	@RequestMapping(value="/removeGitBygitIdAndProjectMemberIdx", method=RequestMethod.POST)
	public boolean removeGitBygitIdAndProjectMemberIdx(Principal principal, HttpSession session) {
		String userId = principal.getName();
		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("user_id", userId);
		map.put("project_member_idx", projectMemberIdx);
		return versionService.deleteGit(map);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/searchBranch", method=RequestMethod.POST)
	public List<String> searchBranch(@RequestParam(value="branch") String branch, HttpSession session) {
		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		return versionService.getBranchesForSearch(projectMemberIdx, branch);
	}
	
	
	
	

}
