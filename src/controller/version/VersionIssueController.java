package controller.version;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.annotation.RequestScope;

import common.Constants.Git;
import service.version.VersionService;

@Controller
@RequestMapping("/version/issue")
public class VersionIssueController {
	private static Logger logger = Logger.getLogger("VersionController.java");
	
	@Autowired
	VersionService versionService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public String viewIssueList() {
		
		return "version/issueList";
	}
//	
	@ResponseBody
	@RequestMapping(value="/selectLabels", method=RequestMethod.POST)
	public List<Map<String, Object>> getLabels(HttpSession session) {
		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		List<Map<String, Object>> labels = versionService.getLabelsIssue(projectMemberIdx);
		return labels;
	}

	@ResponseBody
	@RequestMapping(value="/getIssues/{type}", method=RequestMethod.POST)
	public List<Map<String, Object>> getIssues(
			HttpSession session,
			@RequestParam(value="labels", required=false) String labels,
			@PathVariable("type") String type ) {

		logger.info("getIssues 200 POST" + type);
		logger.info("Labels Value" + labels);
		
		// 임시 섹션 값
//		session.setAttribute(Git.PROJECT_MEMBER_IDX, 1);

		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();

		List<Map<String, Object>> issues = new ArrayList<Map<String, Object>>();

		if (labels != null) {

			logger.info("status labels 로 검색");
			if (type.equals("open")) 
				issues = versionService.getIssueOpen(projectMemberIdx, labels);
			else 
				issues = versionService.getIssueClosed(projectMemberIdx, labels);
			
		} else {

			logger.info("status 로 만 검색");
			if (type.equals("open")) 
				issues = versionService.getIssueOpen(projectMemberIdx, labels);
			else 
				issues = versionService.getIssueClosed(projectMemberIdx, labels);
			
		}
		
	
		
		return issues;
	}
	
	
	@ResponseBody
	@RequestMapping(value="/comment", method=RequestMethod.POST)
	public List<Map<String, Object>> getComment(@RequestParam("number") int number, HttpSession session) {

		List<Map<String, Object>> comments = new ArrayList<Map<String, Object>>();
		
		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		System.out.println(number);
		comments = versionService.getIssueComment(projectMemberIdx, number);
		System.out.println(comments);
		
		return comments;
	}
	
	@ResponseBody
	@RequestMapping("/sendComment")
	public int sendToRepoIssue(
			HttpSession session,
			@RequestParam("number") int number, 
			@RequestParam("comment") String comment ) {

		int projectMemberIdx = ((BigInteger)session.getAttribute(Git.PROJECT_MEMBER_IDX)).intValue();
		int isSuccess = 0;
		
		try {
			if (versionService.sendCommentIssue(number, comment, projectMemberIdx)) {
				isSuccess = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return isSuccess;
	}
	
	
	

	
}





