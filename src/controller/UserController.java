package controller;

import static common.Common.User.*;

import java.io.File;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	private static Logger logger = Logger.getLogger("UserController.java");

	@Autowired
	private UserService userService;

	// 회원 가입 화면
	@RequestMapping(value = "/join", method = RequestMethod.GET)
	public String join(Model model) {
		logger.info("pageCall : join ");
		model.addAttribute("content", "/jsp/main/join.jsp");
		model.addAttribute("headerJSP", "/jsp/include/header/header_main.jsp");
		return "main";
	}
//	// 회원 가입 화면
//	@RequestMapping(value = "/join", method = RequestMethod.GET)
//	public String join() {
//		logger.info("pageCall : join ");
//		return "join";
//	}
	
	// 회원가입
	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public String join(@RequestParam Map<String, Object> map, HttpServletRequest request, RedirectAttributes ra,
			@RequestParam(value = USER_PIC, required = false) MultipartFile user_pic,
			@RequestParam(value = USER_FILE, required = false) MultipartFile user_file) {
		logger.info("functionCall : join");
		
		ra.addFlashAttribute("result_join", userService.join(map, user_pic, user_file, request));
		
		return "redirect:loginForm";
	}
	
	// 이메일체크 -> 로그인 화면
	@RequestMapping(value = "/emailCheck", method = RequestMethod.GET)
	public String emailCheck(@RequestParam Map<String, Object> map, RedirectAttributes ra) {
		logger.info("functionCall : emailCheck");
		ra.addFlashAttribute(RESULT, userService.emailCheck(map));

		return "redirect:loginForm";
	}

	// 이메일 재전송
	@ResponseBody
	@RequestMapping(value = "/resendMail", method = RequestMethod.POST)
	public int resendMail(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : resendMail / map : " + map);
		return userService.resendMail(map);
	}
	
//	// 로그인 화면
//	@RequestMapping(value = "/loginForm", method=RequestMethod.GET)
//	public String loginForm(Principal principal, Model model) {
//		logger.info("pageCall : loginForm");
//		if(principal != null) {
//			model.addAttribute("principal", principal.getName());
//		}
//		return "login";
//	}
	
	@RequestMapping(value = "/loginForm", method=RequestMethod.GET)
	public String loginForm(Principal principal, Model model) {
		logger.info("pageCall : login");
		if(principal != null) {
			model.addAttribute("principal", principal.getName());
		}
		model.addAttribute("content", "/jsp/main/login.jsp");
		model.addAttribute("headerJSP", "/jsp/include/header/header_main.jsp");
		return "main";
	}
	
	// 로그아웃
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request,HttpServletResponse response) {
		logger.info("functionCall : logout");
		userService.logout(request, response);
		return "redirect:/index/main?logout=true";
	}
	
//	// 회원 아이디, 비밀번호 찾기 화면.
//	@RequestMapping(value = "/idPwInquiry", method = RequestMethod.GET)
//	public String idPwInquiry() {
//		logger.info("pageCall : idPwInquiry");
//		return "/user/idPwInquiry";
//	}
	
	// 아이디찾기
	@ResponseBody
	@RequestMapping(value = "/idInquiry", method = RequestMethod.POST)
	public Map<String, Object> IdInquiry(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : idInquiry");
		return userService.findId(map);
	}
	
	// 비밀번호찾기
	@ResponseBody
	@RequestMapping(value = "/pwInquiry", method = RequestMethod.POST)
	public int PwInquiry(@RequestParam Map<String, Object> map, HttpServletRequest request) {
		logger.info("functionCall : pwInquiry");
		return userService.findPw(map, request);
	}

//	// 찾은 아이디 리스트 화면.
//	@RequestMapping(value = "/viewIdList", method = RequestMethod.GET)
//	public String viewIdList() {
//		logger.info("pageCall : viewIdList");
//		return "/viewIdList";
//	}
	
	// 아이디체크
	@ResponseBody
	@RequestMapping(value = "/userIdCheck", method = RequestMethod.POST)
	public int userIdCheck(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : userIdCheck");
		return userService.userIdCheck(map);
	}
	
	// 전화번호체크
	@ResponseBody
	@RequestMapping(value = "/userTelCheck", method = RequestMethod.POST)
	public int userTelCheck(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : userTelCheck");
		return userService.userTelCheck(map);
	}

	// 유저 프로필 화면
	@RequestMapping(value = "/userProfile", method = RequestMethod.POST)
	public String userProfile(@RequestParam Map<String, Object> map, Model model, HttpSession session, Principal principal) {
		logger.info("pageCall : userProfile");
		
		map.put(USER_IDX, userService.getUserIdxById(principal.getName()));
		model.addAttribute(USER, userService.getUser(map));
//		model.addAttribute("content", "/jsp/user/userProfile.jsp");
//		model.addAttribute("headerJSP", "/jsp/include/header/header.jsp");
		
		return "/user/userProfile";
	}

	// 이미지가져오기
	@ResponseBody
	@RequestMapping("/image")
	public byte[] getImage(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : image");
		return userService.getImage(map);
	}
	
	// 기본이미지로 
	@ResponseBody
	@RequestMapping(value = "/defaultPic", method = RequestMethod.POST)
	public int defaultPic(@RequestParam Map<String, Object> map) {
		return userService.defaultPic(map);
	}
	
	// 유저 프로필 수정 화면
	@RequestMapping(value = "/modifyProfile", method = RequestMethod.POST)
	public String modifyProfile(@RequestParam Map<String, Object> map, Model model, Principal principal) {
		logger.info("pageCall : modifyProfile");
		
		model.addAttribute(USER, userService.getUser(map));
		model.addAttribute("content", "/jsp/user/modifyProfile.jsp");
		return "main";
	}

	// 회원정보수정
	@ResponseBody
	@RequestMapping(value = "/modifyUser", method = RequestMethod.POST)
	public int modifyUser(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : modifyUser / map : " + map);
		return userService.modifyUser(map);
	}

	// 회원아이디수정
	@ResponseBody
	@RequestMapping(value = "/modifyUserId", method = RequestMethod.POST)
	public int modifyUserId(@RequestParam Map<String, Object> map, HttpServletRequest request) {
		logger.info("functionCall : modifyUserId");
		return userService.sendMailForUserIdModification(map, request);
	}

	// 회원전화번호수정
	@ResponseBody
	@RequestMapping(value = "/modifyUserTel", method = RequestMethod.POST)
	public int modifyUserTel(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : modifyUserTel");
		return userService.modifyUserTel(map);
	}

	// 프로필사진바꾸기
	@ResponseBody
	@RequestMapping(value = "/modifyPic", method = RequestMethod.POST)
	public int modifyPic(@RequestParam Map<String, Object> map, @RequestParam(value = "user_pic") MultipartFile user_pic) {
		logger.info("functionCall : modifyPic / map : " + map);
		return userService.modifyUserPic(map, user_pic);
	}
	
	// 포트폴리오 파일바꾸기
	@ResponseBody
	@RequestMapping(value = "/modifyFile", method = RequestMethod.POST)
	public int modifyFile(@RequestParam Map<String, Object> map, @RequestParam(value = "user_file") MultipartFile user_file) {
		logger.info("functionCall : modifyFile / map : " + map);
		return userService.modifyUserFile(map, user_file);
	}

	// 회원 비밀번호 수정 화면
	@RequestMapping(value = "/modifyUserPwForm", method = RequestMethod.GET)
	public String modifyUserPw(@RequestParam Map<String, Object> map, Model model) {
		logger.info("pageCall : modifyUserPwForm");
		model.addAttribute(USER, map);
		return "/user/modifyUserPw";
	}

	// 회원비밀번호수정
	@ResponseBody
	@RequestMapping(value = "/modifyUserPw", method = RequestMethod.POST)
	public int modifyUserPw(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : modifyUserPw");
		logger.info("modifyUserPw map : " + map);
		return userService.modifyUserPw(map);
	}

	// 회원비밀번호수정
	@ResponseBody
	@RequestMapping(value = "/modifyUserPwFromMail", method = RequestMethod.POST)
	public int modifyUserPwFromMail(@RequestParam Map<String, Object> map) {
		logger.info("functionCall : modifyUserPwFromMail");
		return userService.modifyUserPwFromMail(map);
	}

//	// 비밀번호 체크 화면
//	@RequestMapping(value = "/pwCheckForm", method = RequestMethod.POST)
//	public String pwCheckForm(@RequestParam Map<String, Object> map, Model model) {
//		logger.info("pageCall : pwCheckForm");
//		model.addAttribute(USER, userService.getUser(map));
//
//		return "/user/pwCheck";
//	}

	// 비밀번호 체크 구글계정
	@ResponseBody
	@RequestMapping(value = "/pwCheck", method = RequestMethod.POST)
	public int pwCheck(@RequestParam Map<String, Object> map, Model model) {
		logger.info("functionCall : pwCheck");
		
		return userService.pwCheck(map);
	}
	
	// 비밀번호 체크 일반계정
	@ResponseBody
	@RequestMapping(value = "/userPwCheck", method = RequestMethod.POST)
	public int userPwCheck(@RequestParam Map<String, Object> map, Model model) {
		logger.info("functionCall : pwCheck");
		
		return userService.userPwCheck(map);
	}

	// 계정 삭제 
	@ResponseBody
	@RequestMapping(value = "/removeAccount", method = RequestMethod.POST)
	public int removeAccount(@RequestParam Map<String, Object> map, RedirectAttributes ra, HttpServletRequest request, HttpServletResponse response) {
		logger.info("functionCall : removeAccount / map : " + map);

		return userService.removeAccount(map, request, response);
	}
	
	// 구글 계정 연결 해제 
	@RequestMapping(value = "/removeGoogleAccount", method = RequestMethod.POST)
	public String removeGoogleAccount(@RequestParam Map<String, Object> map, RedirectAttributes ra, HttpServletRequest request, HttpServletResponse response) {
		logger.info("functionCall : removeGoogleAccount");
		ra.addFlashAttribute(RESULT, userService.removeGoogleAccount(map, request, response));
		
		return "redirect:/index/main";
	}
	
	//프로젝트리스트화면 >프로젝트 리스트 화면으로 이동하도록 수정 ***
	@RequestMapping("/projectList")
	public String projectList(Principal principal, Model model) {
		logger.info("pageCall : projectList / principal : " + principal.getName());
		model.addAttribute(USER_ID, principal.getName());
		return "redirect:/project/projectList";
	}
	
	// 파일업로드화면 
	@RequestMapping("/fileUpload")
	public String fileUpload(@RequestParam Map<String, Object> map, Model model) {
		logger.info("pageCall : fileUpload");
//		model.addAttribute(PROJECT_MEMBER_IDX, "3");
		return "/fileUpload/fileUpload";
	}
	
	// 파일업로드
	@ResponseBody
	@RequestMapping(value = "uploadProjectFile", method = RequestMethod.POST)
    public int uploadProjectFile(@RequestParam Map<String, Object> map, MultipartHttpServletRequest files) {
		logger.info("functionCall : fileUpload");
        return userService.filesUpload(map, files);
    }
	
	@ResponseBody
	@RequestMapping(value = "getFiles", method = RequestMethod.POST)
	public List<Map<String, Object>> getFiles(@RequestParam Map<String, Object> map){
		logger.info("functionCall : getFiles");
		return userService.getFiles(map);
	}
	
	@RequestMapping(value = "/downloadFiles", method = RequestMethod.GET)
    public ModelAndView reDocumentDown(@RequestParam(value="file_name") String file_name) {
		logger.info("functionCall : downloadFiles");
        File downFile = new File(FILE_PATH + file_name);
        return new ModelAndView("downloadView", "downloadFile", downFile);
    }
	
	@ResponseBody
	@RequestMapping(value = "/getAllTasks", method = RequestMethod.POST)
	public List<Map<String, Object>> getAllTasks(@RequestParam Map<String, Object> map){
		logger.info("functionCall : getAllTasks");
		return userService.getAllTasks(map);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getBugCounts", method = RequestMethod.POST)
	public List<Integer> getBugCounts(@RequestParam Map<String, Object> map){
		logger.info("functionCall : getBugCounts");
		return userService.getBugCounts(map);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getProjectsForMyPage", method = RequestMethod.POST)
	public List<Map<String, Object>> getProjectForMyPage(@RequestParam Map<String, Object> map){
		logger.info("functionCall : getProjectsForMyPage");
		List<Map<String, Object>> result = userService.getProjectsForMyPage(map);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/alterBellActive", method = RequestMethod.POST)
	public int alterBellActive(@RequestParam Map<String, Object> map){
		logger.info("functionCall : alterBellActive");
		return userService.alterBellActive(map);
	}
	
	@ResponseBody
	@RequestMapping(value = "/leaveProject", method = RequestMethod.POST)
	public int leaveProject(@RequestParam Map<String, Object> map){
		logger.info("functionCall : leaveProject");
		return userService.leaveProject(map);
	}
	
	@ResponseBody
	@RequestMapping(value = "/dDay", method = RequestMethod.POST)
	public String dDay(@RequestParam Map<String, Object> map){
		logger.info("functionCall : dDay");
		return userService.getDDay(map);
	}
	
	@RequestMapping("/chart") // project_idx 수정 해야됨
	public String chart(Principal principal, Model model,@RequestParam Map<String, Object> map) {
		logger.info("pageCall : chart");
//		model.addAttribute("project_member_idx", "3");
		return "/chart/chart";
	}
	
	@ResponseBody
	@RequestMapping(value = "getUser",method = RequestMethod.POST)
	public Map<String, Object> getUser(Principal principal){
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String, Object> argMap = new HashMap<>();
		argMap.put(USER_IDX, user_idx);
		
		return userService.getUser(argMap);
	}
	
	@ResponseBody
	@RequestMapping(value = "/getGitCommitCounts", method = RequestMethod.POST)
	public List<Map<String, Object>> getGitCommitCounts(@RequestParam Map<String, Object> map){
		logger.info("pageCall : getGitCommitCounts map : " + map);
		return userService.getGitCommitCounts(map);
	}
	
	@ResponseBody
	@RequestMapping(value = "/sendInvitationMail", method = RequestMethod.POST)
	public int sendInvitationMail(Principal principal, @RequestParam Map<String, Object> map ) {
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String, Object> argMap = new HashMap<>();
		argMap.put(USER_IDX, user_idx);
		Map<String, Object> user = userService.getUser(argMap);
		String toAddress = (String)map.get("toAddress");
		String fromId = (String)user.get(USER_NAME);
		return userService.sendInvitationMail(fromId, toAddress);
	}
	

	@ResponseBody
	@RequestMapping(value = "/sendMailWithFile", method = RequestMethod.POST)
	public int testFile(@RequestParam Map<String, Object> map, Principal principal){
		logger.info("functionCall : sendMailWithFile map : " + map);
		
		int user_idx = userService.getUserIdxById(principal.getName());
		map.put("user_idx", user_idx);
		
		return userService.sendMailWithFile(map);
	}
	

}