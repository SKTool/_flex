package controller;


import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpSession;

import static common.Common.User.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;

@Controller
@RequestMapping("/google")
public class GoogleController {
	
	private static Logger logger = Logger.getLogger("GoogleController.java");
	
	@Autowired
	private UserService userService;
	
	// 구글 로그인 체크
	@ResponseBody
	@RequestMapping("/googleUserCheck")
	public int googleUserCheck(@RequestParam Map<String, Object> map, HttpSession session) {
		logger.info("functionCall : googleUserCheck / map : " + map);
		return userService.googleUserCheck(map, session);
	}
}
