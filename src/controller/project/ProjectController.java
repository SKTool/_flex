package controller.project;



import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import service.UserService;
import service.project.ProjectService;

@Controller
@RequestMapping("/project")
public class ProjectController {

	private static Logger logger = Logger.getLogger("ProjectController.java");
	
	@Autowired
	private ProjectService projectService;

	
	@Autowired
	private UserService userService;
	
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	private String insertProject(@RequestParam Map<String, Object> param, Principal principal, Model model) {	
		param.put("user_idx", userService.getUserIdxById(principal.getName()));			
		boolean result = projectService.insertProject(param);
		logger.info("pageCall : insertProject, principal : " + principal + "result: " + result);
		return "redirect:/project/projectList";
	}
	
	@RequestMapping(value = "/projectList")
	public String projectList(Model model, Principal principal, HttpSession session) {
		logger.info("pageCall : projectList, principal : "+principal);
		int user_idx = userService.getUserIdxById(principal.getName());		
		Map<String, Object> umap = new HashMap<>();
		umap.put("user_idx", user_idx);
		if(session!=null){
			session.removeAttribute("project_idx");
			session.removeAttribute("project_member_idx");
		}
		model.addAttribute("user_name", userService.getUser(umap).get("user_name"));
		model.addAttribute("user_pic", userService.getUser(umap).get("user_pic"));
		model.addAttribute("userList", userService.selectAllUser());
		model.addAttribute("projectList", projectService.selectAllMyProject(user_idx));
		model.addAttribute("allProjectList", projectService.selectAllProject());
		
		return "project/projectList";
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete", method=RequestMethod.GET)
	public boolean deleteProject(Model model, @RequestParam(value = "num") int project_idx) {
		boolean result = projectService.deleteProject(project_idx);	
		logger.info("pageCall : deleteProject, result : "+result);
		return result;
	}

	@ResponseBody
	@RequestMapping(value = "/view")
	public List<Map<String, Object>>  projectView(@RequestParam(value = "num") int project_idx) {
		logger.info("pageCall : projectView, project_idx : "+project_idx);
		List<Map<String, Object>> project = projectService.selectOneProjectMemberJoinUser(project_idx);
		project.add(projectService.selectOneProject(project_idx));
		return project;
	}
	
	
	@RequestMapping(value="/wiki", method=RequestMethod.GET)
	private String projectaWiki(@RequestParam(value = "num") int project_idx, Model model, Principal principal, HttpServletRequest request) {	
		logger.info("pageCall : projectaWiki, project_idx : "+project_idx);
		HttpSession session = request.getSession();
		session.setAttribute("project_idx", project_idx);		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("project_idx", project_idx);
		param.put("user_idx", userService.getUserIdxById(principal.getName()));
		projectService.selectOneProjectMemberIdxForBro(param);
		session.setAttribute("project_member_idx", projectService.selectOneProjectMemberIdxForBro(param).get("project_member_idx"));
		model.addAttribute("projectMember", projectService.selectOneProjectMemberJoinUser(project_idx));
		model.addAttribute("project",projectService.selectOneProject(project_idx));
		return "project/projectWiki";
	}
	
	@ResponseBody
	@RequestMapping(value="/modify", method=RequestMethod.POST)
	public int modifyProject(@RequestParam Map<String, Object> param, Principal principal, Model model) {	
		logger.info("pageCall : modifyProject");
		System.out.println(param);
		int user_idx = userService.getUserIdxById(principal.getName());	
		boolean result = projectService.updateProject(param);
		if(result) {
			return 1;
		}else {
			return -1;
		}
	}
	
	@RequestMapping(value="/projectwiki", method=RequestMethod.GET)
	private String wiki(Model model, Principal principal, HttpServletRequest request) {	
		logger.info("pageCall : projectaWiki");
		HttpSession session = request.getSession();
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("project_idx", getByProjectIdx(request));
		param.put("user_idx", userService.getUserIdxById(principal.getName()));
		projectService.selectOneProjectMemberIdxForBro(param);
		session.setAttribute("project_member_idx", projectService.selectOneProjectMemberIdxForBro(param).get("project_member_idx"));
		model.addAttribute("projectMember", projectService.selectOneProjectMemberJoinUser((int)session.getAttribute("project_idx")));
		model.addAttribute("project",projectService.selectOneProject((int)session.getAttribute("project_idx")));
		return "project/projectWiki";
	}
	
	
	
	@RequestMapping(value="/modifywiki", method=RequestMethod.POST)
	private String modifyWiki(@RequestParam Map<String, Object> param, Principal principal, Model model,HttpServletRequest request) {	
		logger.info("pageCall : modifyWiki");
		System.out.println(param);
		int user_idx = userService.getUserIdxById(principal.getName());	
		projectService.updateProject(param);
		int project_idx = getByProjectIdx(request);
		System.out.println(project_idx);
		return "redirect:/project/wiki"+"?num="+project_idx;
	}
	
	@RequestMapping(value="/modifywiki", method=RequestMethod.GET)
	private String modifyWikiForm(@RequestParam(value = "num") int project_idx, Principal principal, Model model) {	
		logger.info("pageCall : modifyWikiForm");
		int user_idx = userService.getUserIdxById(principal.getName());	
		model.addAttribute("projectMember", projectService.selectOneProjectMemberJoinUser(project_idx));
		model.addAttribute("project",projectService.selectOneProject(project_idx));
		return "project/projectWikiModify";
	}
	
	
	//프로젝트 인덱스 뽑아줌. 
	@RequestMapping
	private int getByProjectIdx(HttpServletRequest request) {
		HttpSession session = request.getSession();
		int project_idx = (int)session.getAttribute("project_idx");
		logger.info("pageCall : getByProjectIdx, project_idx : "+project_idx);
		return project_idx;
	}
	
	
	//관리자만 사용
	@RequestMapping(value = "/allProjectList")
	public String projectList(Model model) {
		model.addAttribute("projectList", projectService.selectAllProject());
		return "allProjectList";
	}
	
	
}
