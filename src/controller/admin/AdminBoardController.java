package controller.admin;

import org.apache.log4j.Logger;

import static common.Common.User.USER_ID;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import dao.AdminDao;

import service.UserService;
import service.admin.AdminBoardService;

@Controller
@RequestMapping("/adminBoard")
public class AdminBoardController {
	
	private static Logger logger = Logger.getLogger("AdminBoardController.java");

	@Autowired
	private AdminBoardService adminBoardService;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/adminBoardList")
	public String adminBoardList(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
		logger.info("pageCall : adminBoardList");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		model.addAllAttributes(adminBoardService.getViewDataAdminBoard(params));
		return "admin/adminBoardList";
	}

	@RequestMapping(value = "/write", method = RequestMethod.GET)
	public String adminBoardWriteForm() {
		logger.info("pageCall : adminBoardWriteForm");
		return "admin/adminBoardWrite";
	}

	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public String adminBoardWrite(@RequestParam Map<String, Object> params, Model model, Principal principal) {
		logger.info("pageCall : adminBoardWrite");
		params.put("user_idx", userService.getUserIdxById(principal.getName()));
		adminBoardService.insertAdminBoard(params);	
		System.out.println(params.get("status"));
		if(params.get("status").equals(1)) {
			return "redirect:/adminBoard/adminBoardList";	
		}if(params.get("status").equals(2)) {
			return "redirect:/qna/faqList";					
		}
		return "redirect:/adminBoard/adminBoardList";
	}

	@RequestMapping(value = "/view")
	public String adminBoardView(@RequestParam(value = "num") int admin_board_idx, Model model) {
		logger.info("pageCall : adminBoardView");
		model.addAttribute("adminBoard", adminBoardService.adminBoardReadCount(admin_board_idx));
		return "admin/adminBoardView";
	}

	@RequestMapping(value = "/modify", method = RequestMethod.GET)
	public String adminBoardModifyForm(@RequestParam(value = "num") int admin_board_idx, Model model) {
		logger.info("pageCall : adminBoardModifyForm");
		model.addAttribute("adminBoard", adminBoardService.selectOneAdminBoard(admin_board_idx));	
		return "admin/adminBoardModify";
	}

	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public String adminBoardModify( Model model, @RequestParam Map<String, Object> params, Principal principal) {
		logger.info("pageCall : adminBoardModify");
		params.put("user_idx", userService.getUserIdxById(principal.getName()));
		adminBoardService.updateAdminBoard(params);
		return "redirect:/adminBoard/adminBoardList";
	}

	@ResponseBody
	@RequestMapping(value = "/delete")
	public boolean adminBoardDelete(@RequestParam(value = "num") int admin_board_idx, Model model) {
		logger.info("pageCall : adminBoardDelete");
		boolean result = adminBoardService.deleteAdminBoard(admin_board_idx);	
		return result;
	}
}
