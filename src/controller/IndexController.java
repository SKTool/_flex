package controller;


import java.security.Principal;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/index")
public class IndexController {
	
	private static Logger logger = Logger.getLogger("IndexController.java");
	
	//메인페이지 
	@RequestMapping("/main")
	public String main(Model model) {
		logger.info("pageCall : main");
		model.addAttribute("content", "/jsp/main/project_main.jsp");
		model.addAttribute("headerJSP", "/jsp/include/header/header_main.jsp");
		return "main";
	}
	
	// 비정상접근
	@RequestMapping(value = "/abnormalAccess", method = RequestMethod.GET)
	public String abnormalAccess() {
		logger.info("pageCall : abnormalAccess");
		return "/main/abnormalAccess";
	}
}