package controller.userBell;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import service.UserService;
import service.userBell.UserBellService;
import service.userBell.UserBellServiceImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( locations = {"file:WebContent/WEB-INF/spring/servlet-context.xml", "file:WebContent/WEB-INF/spring/root-context.xml"})
@WebAppConfiguration
public class TestClassTest {

	// mockito InjectMocks
	@InjectMocks
	private UserBellController testClass;

	@Autowired
	private UserBellServiceImpl userBellService;

	@Autowired
	private UserService userService;

	@Autowired
	SimpMessagingTemplate template;
	
	//spring test web servlet library
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(testClass).build();
		testClass.userBellService = userBellService;
		testClass.template = template;
		testClass.userService = userService;
	}

	
	private String jsonData() {

    	String bodyjson = new String();
		File file = null;
    	FileInputStream fis = null;
		try {
							
//			file = new File("/home/com/Documents/bitWorkspace/copied_flex/flex/src/controller/userBell/push.json");
//			file = new File("C:\\Users\\bitcamp\\Desktop\\FlexWS\\copy_flex\\src\\controller\\userBell\\push.json");
			file = new File("C:\\Users\\snow_\\Documents\\bitcampWorkspace\\flex\\src\\controller\\userBell\\push.json");
			
//			file = new File("C:\\Users\\bitcamp\\Desktop\\FlexWS\\copy_flex\\src\\controller\\userBell\\issue.json");
//			file = new File("C:\\Users\\bitcamp\\Desktop\\FlexWS\\copy_flex\\src\\controller\\userBell\\issue.json");
			fis = new FileInputStream(file);
			int read;
			while((read = fis.read()) != -1) {
				bodyjson += Character.toString((char)read);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		
		System.out.println(bodyjson);
		return bodyjson;
	}
	
	
	@Test
	public void testTestClass() throws Exception {
//		when(testClass.test()).thenReturn("10");

		mockMvc.perform(post("/userBell/alertUserBell?project_member_idx=9&user_id=t01.experiment@gmail.com").header("x-github-event", "push").content(  jsonData()  ))
					.andDo(print())
					.andExpect(status().isOk());


//        verify(testClass).test();
//        verifyNoMoreInteractions(testClass);	
	}

	
	



	

	

	
	
	
	
	

}
