package controller.userBell;

import static common.Constants.*;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.userBell.UserBellService;

@Controller
@RequestMapping("/userBell")
public class UserBellController {
	Logger loggerBody = Logger.getLogger("log4jBody");

	@Autowired
	SimpMessagingTemplate template;

	@Autowired
	UserBellService userBellService;

	@Autowired
	UserService userService;
	
	@ResponseBody
	@RequestMapping(value="/alertSmallRow", method=RequestMethod.POST) 
	public List<Map<String, Object>> headerBellRow(Principal principal) {
		System.out.println("alertSmallRow");
		Map<String, Object> map = new HashMap<String, Object>();
		int userIdx = userService.getUserIdxById(principal.getName());
		map.put(UserBell.USER_IDX, userIdx);

		return userBellService.loadAlerts(map);
	}
	
	
	@ResponseBody
	@RequestMapping(value="/gitAlerts", method=RequestMethod.POST)
	public List<Map<String, Object>> userBellGitAlerts(Principal principal) {
		Map<String, Object> map = new HashMap<String, Object>();

		// TODO  예외처리 ?
		loggerBody.info(principal.toString());
		if(principal != null) {
//			map.put(UserBell.USER_IDX, principal.getName());
			
			map.put(UserBell.USER_IDX, userService.getUserIdxById(principal.getName()));
			loggerBody.info(principal.toString());
			System.out.println("================");
			
			System.out.println(principal.toString());
		}

		List<Map<String, Object>> list = userBellService.loadAlerts(map);
		System.out.println(list);
		
		return list;
	}
	@ResponseBody
	@RequestMapping(value="/gitDeletedAlerts", method=RequestMethod.POST)
	public List<Map<String, Object>> userBellGitDeletedAlerts(Principal principal) {
		Map<String, Object> map = new HashMap<String, Object>();

		// TODO  예외처리 ?
		loggerBody.info(principal.toString());
		if(principal != null) {
			map.put(UserBell.USER_IDX, userService.getUserIdxById(principal.getName()));
			loggerBody.info(principal.toString());
		}
		

		return userBellService.loadDeletedAlerts(map);
	}

// 페이지 을 합치면서 페이지 변경으로 ajax 로 변경
//	@RequestMapping(value="", method=RequestMethod.GET)
//	public String userBellGETRequest(Model model) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put(UserBell.USER_IDX, 1);
//		// TODO  user_idx 파라미터 
////		System.out.println(userBellService.loadAlerts(map));
//		model.addAttribute("gitAlerts", userBellService.loadAlerts(map));
//		model.addAttribute("gitDeletedAlerts", userBellService.loadDeletedAlerts(map));
//
//		return "userBell/userBell";
//	}

	@ResponseBody
	@RequestMapping(value="/alertUserBell", method=RequestMethod.POST)
	public String alertGit(
			@RequestHeader Map<String, Object> headers
			, @RequestBody String body
			, @RequestParam(value="project_member_idx") String project_member_idx 
			, @RequestParam(value="user_id") String user_id) {

		int userIdx = userService.getUserIdxById(user_id);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(UserBell.USER_IDX, userIdx);
		loggerBody.info(user_id);
		loggerBody.info(project_member_idx);

		if (userBellService.isActiveInProjectMember(project_member_idx)) {
			map.put(UserBell.PROJECT_MEMBER_IDX, project_member_idx);
			userBellService.alertEventHandler(headers, body, map);
		}

		String total = userBellService.loadAlertsConvertedJson(map);

		if (total != null) {
			noticeGitEvent(total, user_id);
		}

		return total;
	}

	public void noticeGitEvent(String data, String user_id) {
		this.template.convertAndSend("/alert/msg/" + user_id, data);
	}	

	@ResponseBody
	@RequestMapping(value="/removeGitMsgUserBell",method=RequestMethod.POST)
	public int deleteUserBell(@RequestParam Map<String, Object> map) {
		System.out.println("map: " + map);
		return userBellService.updateStatusToZero_InUserBell(map);

	}

	@ResponseBody
	@RequestMapping(value="/recoveryGitMsgUserBell",method=RequestMethod.POST)
	public int recoveryUserBell(@RequestParam Map<String, Object> map) {
		System.out.println(map);
		return userBellService.updateStatusToNotZero_InUserBell(map);

	}
	
//	@MessageMapping("/hello/{project_member_idx}/{target_project_member_idx}")
//	@SendTo("/alert/msg/{project_member_idx}/{user_idx}")
//	public String stompTest(
//			String message
//			, @DestinationVariable(value="project_member_idx") String project_member_idx
//			, @DestinationVariable(value="target_project_member_idx") String target_project_member_idx
//			) {
//		System.out.println("메시지 받았씁니다. : " + message);
//		
//		
//		return message;
//	}
	
	
//	@ResponseBody
//	@RequestMapping("/test2") 
//	public void test2() {
//		noticeGitEvent("11", "1234", "1234");
////		return "stomp";
//	}
}
