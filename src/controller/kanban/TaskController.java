package controller.kanban;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.kanban.ITaskService;
import service.project.ProjectService;

@Controller
@RequestMapping("/task")
public class TaskController {
	@Autowired
	private ITaskService taskService;
	@Autowired
	private ProjectService projectService;
	
	@RequestMapping(value = "/write", method = RequestMethod.GET)
	public String kanbanForm() {
		return "kanban/kanban";
	}
	@ResponseBody
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public boolean writeTask(@RequestParam Map<String, Object> task, HttpServletRequest request) {
		System.out.println(task.put("project_idx", projectService.getByProjectIdx(request)));
		if (taskService.insertTaskMember(task)) {
			return true;
		} else {
			return false;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public boolean modifyTask(@RequestParam Map<String, Object> task) {
		if (taskService.modifyTaskMember(task)) {
			return true;
		} else {
			return false;
		}
	}	
	
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public boolean deleteTask(@RequestParam int task_idx) {
		if(taskService.deleteTask(task_idx)) {
			return true;
		} else {
			return false;
		}
	}		
	
	// Task 불러오기
	@ResponseBody
	@RequestMapping(value = "/selectTask", method = RequestMethod.POST)
	public List<Map<String, Object>> selectProjectTask(@RequestParam int project_idx) {
		return taskService.getTaskListByProject(project_idx);
	}
	
	//fastSelect Tag,Member 불러오기
	@ResponseBody
	@RequestMapping(value = "/fastSelectMember/{projectIdx}", method = RequestMethod.GET)
	public List<Map<String,Object>> fastSelectMember( @PathVariable("projectIdx") int projectIdx ){
		List<Map<String,Object>> taskMembers =  taskService.fastSelectMember(projectIdx);	
		return taskMembers;
	}
	@ResponseBody
	@RequestMapping(value = "/fastSelectTag", method = RequestMethod.GET)
	public List<Map<String,Object>> fastSelectTag(){
		List<Map<String,Object>> taskTag = taskService.fastSelectTag();
		return taskTag;
	}	
	
	//fastselect에서 선택한 Tag,Member 불러오기
	@ResponseBody
	@RequestMapping(value = "/fastSelectViewMember", method = RequestMethod.GET)
	public List<Map<String,Object>> fastSelectViewMember(@RequestParam Map<String,Object> param){
		List<Map<String,Object>> taskViewMembers =  taskService.fastSelectViewMember(param);
		return taskViewMembers;
	}
	@ResponseBody
	@RequestMapping(value = "/fastSelectViewTag", method = RequestMethod.GET)
	public List<Map<String,Object>> fastSelectViewTag(@RequestParam int task_idx){
		List<Map<String,Object>> taskViewTag = taskService.fastSelectViewTag(task_idx);
		return taskViewTag;
	}		

}
