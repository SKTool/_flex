package controller.bug;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.bug.BugReplyService;
import service.bug.BugService;
import service.project.ProjectService;

@Controller
@RequestMapping("/bugReply")
public class BugReplyController {
	@Autowired
	private BugService bugService;
	@Autowired
	private BugReplyService bugReplyService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;
	
	public int selectMyBugMemberIdx(HttpServletRequest request, Principal principal){
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String,Object> param = new HashMap<>();
		param.put("project_idx", projectService.getByProjectIdx(request));
		param.put("user_idx", user_idx);
		return bugService.selectMyBugMemberIdx(param);
	}
	
	@ResponseBody
	@RequestMapping(value="/write", method = RequestMethod.POST)
	public boolean writeBugReply(@RequestParam Map<String,Object> bugReply, HttpServletRequest request, Principal principal) {
		bugReply.put("project_member_idx", (int)selectMyBugMemberIdx(request, principal));
		return bugReplyService.writeReply(bugReply);
	}
	@ResponseBody
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public boolean deleteBugReply(@RequestParam Map<String,Object> bugReply) {
		System.out.println("삭제:" + bugReply);
		return bugReplyService.deleteReply(bugReply);
		
	}
	@ResponseBody
	@RequestMapping(value="/all", method=RequestMethod.POST)
	public List<Map<String,Object>> bugReplyList(@RequestParam int bug_idx) {
		System.out.println(bug_idx);
		List<Map<String,Object>> bugReplyList = bugReplyService.getReply(bug_idx);
		System.out.println(bugReplyList);
		return bugReplyList;	
	}
	
}
