package controller.bug;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.bug.BugService;
import service.project.ProjectService;
import service.todo.TodoService;

@Controller
@RequestMapping("/bugTracking")
public class BugController {
	@Autowired
	private BugService bugService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;	
	
	public int selectMyBugMemberIdx(HttpServletRequest request, Principal principal){
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String,Object> param = new HashMap<>();
		param.put("project_idx", projectService.getByProjectIdx(request));
		param.put("user_idx", user_idx);
		return bugService.selectMyBugMemberIdx(param);
	}
	
	

	@RequestMapping(value = "/write", method = RequestMethod.GET)
	public String bugForm() {
		return "bugTracking/bugTracking";
	}
	// 버그 게시글 쓰기요청(/write, method=post)
	@ResponseBody
	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public int writeBug(@RequestParam Map<String, Object> bug, HttpServletRequest request, Principal principal) {
		bug.put("project_member_idx", (int)selectMyBugMemberIdx(request, principal));
		System.out.println("writeBug map : " + bug);
		return bugService.writeBug(bug);
	}

	// 버그 게시글 목록(/all)
	@ResponseBody
	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public List<Map<String, Object>> listBug(HttpServletRequest request) {
		int project_idx = projectService.getByProjectIdx(request);
		return bugService.getAllBugList(project_idx);
	}
	
	// 버그 게시글 상세보기(/view)
	public String viewBug() {
		return null;
	}

	// 버그 게시글 수정요청(/modify, method=post)
	@ResponseBody
	@RequestMapping(value = "/modify", method = RequestMethod.POST)
	public int modifyBug(@RequestParam Map<String,Object> bug) {
		System.out.println("넘어가냐!!!");
		return bugService.modifyBug(bug);
	}

	// 버그 게시글 삭제요청(/delete)
	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public boolean deleteBug(int bug_idx) {
		System.out.println("게시글 번호 : " + bug_idx);
		return bugService.deleteBug(bug_idx);
	}
	
	@RequestMapping(value = "/bugList")
	   public List<Map<String, Object>> bugList(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
	      Map<String, Object> params = new HashMap<String, Object>();
	      params.put("page", page);
//	      model.addAllAttributes(bugService.getViewDataBugList(params));
	      System.out.println(params);
	      return bugService.selectAllBugList(params);
	   }

}
