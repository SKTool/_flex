package controller.recruitment;

import java.security.Principal;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import common.Constants;
import service.UserService;
import service.recruitment.RecruitmentService;

@Controller
@RequestMapping("/recruitment")
public class RecruitmentController {

	@Autowired
	private RecruitmentService service;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/recruitmentList")
	public String recruitmentList(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
//		logger.info("pageCall : adminBoardList");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		model.addAllAttributes(service.getViewDataRecruitment(params));
		
		return "recruitment/recruitmentList";
	}
	
	@RequestMapping(value="/recruitmentWrite",method=RequestMethod.GET)
	public String recruitmentWriteForm() {
		return "recruitment/recruitmentWrite";
	}
	
	@RequestMapping(value="/recruitmentWrite",method=RequestMethod.POST)
	public String recruitmentWrite(Model model, @RequestParam Map<String,Object> recruitment,Principal principal){
		String url = "recruitmentList";
		String msg = null;
		recruitment.put(Constants.MsgBoard.USER_IDX, 1);
		if(service.insertRecruitment(recruitment)) {
			msg = "작성완료";
		}else {
			msg = "작성실패";
		}
		model.addAttribute("url",url);
		model.addAttribute("msg",msg);
		return "result";
	}
	
	@RequestMapping(value="/recruitmentModify",method=RequestMethod.GET)
	public String recruitmentModifyForm(Model model,int recruitment_idx) {
		model.addAttribute("recruitment",service.recruitmentByIdx(recruitment_idx));
		return "recruitment/recruitmentModify";
	}
	
	@RequestMapping(value="/recruitmentModify",method=RequestMethod.POST)
	public String recruitmentModify(Model model, @RequestParam Map<String,Object> recruitment, Principal principal) {
		String url = "recruitmentView?recruitment_idx="+recruitment.get(Constants.Recruitment.RECRUITMENT_IDX);
		String msg = null;
		
		recruitment.put(Constants.MsgBoard.USER_IDX, userService.getUserIdxById(principal.getName()));
		
		if(service.updateRecruitment(recruitment)) {
			msg = "모집 공고 수정에 성공하였습니다.";
		}else {
			msg = "모집 공고 수정에 실패하였습니다.";
		}
		model.addAttribute("url",url);
		model.addAttribute("msg",msg);
		return "result";
	}
	
	@RequestMapping(value="/recruitmentDelete")
	public String recruitmentDelete(int recruitment_idx,Model model) {
		String url="recruitmentList";
		String msg = null;
		if(service.deleteRecruitment(recruitment_idx)) {
			model.addAttribute("msg","모집 공고 삭제에 성공하였습니다.");
		}else {
			model.addAttribute("msg","모집 공고 삭제에 실패하였습니다.");
		}
		model.addAttribute("url",url);
		return "result";
	}
	
	@RequestMapping("/recruitmentView")
	public String msgBoardView(Model model, @RequestParam(value = "recruitment_idx") int recruitment_idx) {
		model.addAttribute("recruitment",service.readRecruitment(recruitment_idx));
		return "recruitment/recruitmentView";
	}
	
	@ResponseBody
	@RequestMapping(value = "/getPlatform", method = RequestMethod.POST)
	public List<Map<String, Object>> getPlatform(@RequestParam Map<String, Object> map){
		return service.getPlatform(map);
	}
	
	
}