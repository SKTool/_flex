package controller.faq;

import java.security.Principal;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.admin.AdminBoardService;
import service.faq.QnaBoardService;

@Controller
@RequestMapping("/qna")
public class QnaBoardController {

	private static Logger logger = Logger.getLogger("QnaBoardController.java");
	
	@Autowired
	private QnaBoardService qnaService;
	@Autowired
	private AdminBoardService adminBoardService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/write", method = RequestMethod.POST)
	public String qnaBoardWrite(@RequestParam Map<String, Object> params, Model model, Principal principal) {
		logger.info("pageCall : qnaBoardWrite");
		params.put("user_idx", userService.getUserIdxById(principal.getName()));
		qnaService.insertQnaBoard(params);
		return "redirect:/qna/faqList";
	}
	

	@RequestMapping(value = "/modify", method = RequestMethod.GET)
	public String qnaBoardModify(@RequestParam(value = "num") int qna_idx, Model model, Principal principal) {
		logger.info("pageCall : qnaBoardModify");
		model.addAttribute("OneQna", qnaService.selectOneQnaBoard(qna_idx));	
		return "/qna/faqList";
	} 
	
	@ResponseBody
	@RequestMapping(value = "/delete")
	public boolean qnaBoardDelete(@RequestParam(value = "num") int qna_idx, Model model) {
		logger.info("pageCall : qnaBoardDelete");
		boolean result = qnaService.deleteQnaBoard(qna_idx);	
		return result;
	}
	

	@RequestMapping(value = "/checkPass",  method = RequestMethod.POST)
	public String deleteQna(int qna_idx, Model model, String qna_pw) {
		if(qnaService.selectOneQnaBoard(qna_idx).get("qna_pw").equals(qna_pw)) {
			qnaService.deleteQnaBoard(qna_idx);
		}
		return "redirect:/qna/faqList";
	}
	
	@RequestMapping(value = "/faqList")
	public String faqList(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
		model.addAttribute("FaqList", adminBoardService.selectAllFaq());
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		model.addAllAttributes(qnaService.getViewDataQna(params));
		return "qna/faqList";
	}

	@RequestMapping(value = "/writeAnswer")
	public String writeAnswerForm(@RequestParam(value = "num") int qna_idx, Model model) {
		model.addAttribute("qna", qnaService.selectOneQnaBoard(qna_idx));
		return "qna/writeQnaAnswer";
	}
	
	@RequestMapping(value = "/writeAnswer", method = RequestMethod.POST)
	public String writeAnswer(@RequestParam Map<String, Object> params, Model model, Principal principal) {
		params.put("user_idx", userService.getUserIdxById(principal.getName()));
		qnaService.insertQnaAnswer(params);
		return "redirect:/qna/faqList";
	}
	
	@RequestMapping(value = "/modifyAnswer", method = RequestMethod.GET)
	public String modifyAnswerForm(@RequestParam(value = "num") int qna_idx, Model model) {
		logger.info("pageCall : modifyAnswerForm");
		model.addAttribute("qna", qnaService.selectOneQnaBoardWithAnswer(qna_idx));	
		return "qna/modifyQnaAnswer";
	}

	@RequestMapping(value = "/modifyAnswer", method = RequestMethod.POST)
	public String modifyAnswer( Model model, @RequestParam Map<String, Object> params, Principal principal) {
		logger.info("pageCall : modifyAnswer");
		params.put("user_idx", userService.getUserIdxById(principal.getName()));
		qnaService.updateQnaAnswer(params);
		return "redirect:/qna/faqList";
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteAnswer")
	public boolean deleteAnswer(@RequestParam(value = "num") int qna_answer_idx, Model model) {
		logger.info("pageCall : qnaBoardDelete");
		boolean result = qnaService.deleteQnaAnswer(qna_answer_idx);	
		return result;
	}
	
}
