package controller.adminPage;

import static org.hamcrest.CoreMatchers.instanceOf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import dao.UserDao;
import service.UserService;
import service.adminPage.AdminPageService;
import service.project.ProjectService;

@Controller
@RequestMapping("/adminPage")
public class AdminPageController {
	
	@Autowired
	AdminPageService adminPageService;
	
	@Autowired
	ProjectService projectService;
	
	@Autowired
	UserDao userDao;

	@RequestMapping(value="/project", method=RequestMethod.GET)
	public String project( @RequestParam Map<String, Object> map , Model model) {
		System.out.println("projectList");

		return "adminPage/adminProjectPage";
	}

	@RequestMapping(value="", method=RequestMethod.GET)
	public String bashboard( @RequestParam Map<String, Object> map , Model model) {
		System.out.println("bashboard");

		Map<String, Object> projectCount = adminPageService.countProject();
		Map<String, Object> userCount = adminPageService.countUser();
		Map<String, Object> recruitmentCount = adminPageService.countRecruitment();
		Map<String, Object> msgBoardCount = adminPageService.countMsgBoard();
		Map<String, Object> qnaBoardCount = adminPageService.countQna();
		Map<String, Object> adminCount = adminPageService.countAdmin();

		List<Map<String, Object>> projectQuarter = adminPageService.QuarterProject();
		List<Map<String, Object>> userQuarter = adminPageService.QuarterUser();
		List<Map<String, Object>> recruitmentQuarter = adminPageService.QuarterRecruitment();
		List<Map<String, Object>> msgBoardQuarter = adminPageService.QuarterMsgBoard();
		List<Map<String, Object>> qnaQuarter = adminPageService.QuarterQna();
		List<Map<String, Object>> adminQuarter = adminPageService.QuarterAdmin(); 

		Map<String, Object> onOffGitCount  = adminPageService.getDutyCycleGit();

		
		model.addAttribute("projectCount", projectCount);
		model.addAttribute("userCount", userCount);
		model.addAttribute("recruitmentCount", recruitmentCount);
		model.addAttribute("msgBoardCount", msgBoardCount);
		model.addAttribute("qnaBoardCount", qnaBoardCount);
		model.addAttribute("adminCount", adminCount);

		model.addAttribute("projectQuarter", projectQuarter);
		model.addAttribute("userQuarter", userQuarter);
		model.addAttribute("recruitmentQuarter", recruitmentQuarter);
		model.addAttribute("msgBoardQuarter", msgBoardQuarter);
		model.addAttribute("qnaQuarter", qnaQuarter);
		model.addAttribute("adminQuarter", adminQuarter);

		model.addAttribute("onOffGitCount", onOffGitCount);

		
		return "adminPage/adminPage";
	}
	
	@ResponseBody
	@RequestMapping(value="/projectListOnlyfive", method=RequestMethod.POST) 
	public List<Map<String, Object>> projectListOnlyfive() {
		return adminPageService.getAllProjectListOnlyfive();
	}
	
	@ResponseBody
	@RequestMapping(value="/projectList", method=RequestMethod.POST) 
	public Map<String, Object> projectList( @RequestParam Map<String, Object> map) {
		return adminPageService.getAllProjectList(map);
	}
	
	@ResponseBody
	@RequestMapping(value="/deleteProject", method=RequestMethod.POST) 
	public int deleteProject( @RequestParam Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper(); // create once, reuse
		List<String> value = new ArrayList<String>();
		int isSuccess = 0;

		try {
			value = mapper.readValue(map.get("list").toString(), List.class);
			isSuccess = 1;

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(value);
		if (!value.isEmpty()) {
			Iterator<String> iterm = value.iterator();
			
			while( iterm.hasNext() ) {
				System.out.println(value);
				projectService.deleteProject(Integer.parseInt(iterm.next()));
			}
		}

		return isSuccess;
	}


	@RequestMapping(value="/member", method=RequestMethod.GET)
	public String member( @RequestParam Map<String, Object> map , Model model) {
		return "adminPage/adminMemberPage";
	}

	@ResponseBody
	@RequestMapping(value="/memberList", method=RequestMethod.POST) 
	public Map<String, Object> memberList( @RequestParam Map<String, Object> map) {
		return adminPageService.getAllMemberList(map);
	}

	@ResponseBody
	@RequestMapping(value="/deleteMember", method=RequestMethod.POST) 
	public int deleteMember( @RequestParam Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper(); // create once, reuse
		List<String> value = new ArrayList<String>();
		Map<String, Object> param = new HashMap<String, Object>();
		int isSuccess = 0;

		try {
			value = mapper.readValue(map.get("list").toString(), List.class);
			isSuccess = 1;

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(value);
		if (!value.isEmpty()) {
			Iterator<String> iterm = value.iterator();
			
			while( iterm.hasNext() ) {
				System.out.println(value);
				param.put("user_idx",(Integer.parseInt(iterm.next())));
				userDao.deleteUser(param);
			}
		}

		return isSuccess;
	}

}









