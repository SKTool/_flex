package controller.projectMember;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.bug.BugService;
import service.project.ProjectService;
import service.projectMember.ProjectMemberService;

@Controller
@RequestMapping("/projectMember")
public class ProjectMemberController {
	
	@Autowired
	private ProjectMemberService projectMemberService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private BugService bugService;

	@RequestMapping(value = "/write", method = RequestMethod.GET)
	public String ProjectMemberForm() {
		return "projectMember/memberList";
	}
	
	@ResponseBody
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public boolean registerProjectMember(@RequestParam Map<String,Object> projectMember, HttpServletRequest request, Principal principal) {
		System.out.println("registerController");
		
		projectMember.put("project_idx", projectService.getByProjectIdx(request));
		
		int from_idx = userService.getUserIdxById(principal.getName());
		Map<String, Object> argMap = new HashMap<>();
		argMap.put("user_idx", from_idx);
		Map<String, Object> user = userService.getUser(argMap);
		projectMember.put("fromId", (String)user.get("user_id"));
//		poject_idx, auth, email
		return projectMemberService.registerProjectMember(projectMember);
	}
	@ResponseBody
	@RequestMapping(value = "/all", method=RequestMethod.POST)
	public List<Map<String, Object>> listMember(HttpServletRequest request) {
		return projectMemberService.memberList(projectService.getByProjectIdx(request));
	}
	@ResponseBody
	@RequestMapping(value ="/modify", method=RequestMethod.POST)
	public int modifyProjectMemberAuth(@RequestParam Map<String,Object> projectMember, Principal principal, HttpSession session) {
		Map<String,Object> param = new HashMap<>();
		System.out.println("무엇이넘어올까?:" + projectMember);
		param.put("user_idx", userService.getUserIdxById(principal.getName()));
		param.put("project_idx", session.getAttribute("project_idx"));
		int projectIdx = bugService.selectMyBugMemberIdx(param);
		projectMember.put("my_project_member_idx", projectIdx);
		return projectMemberService.modifyProjectMemberAuth(projectMember);
		
	}
	@ResponseBody
	@RequestMapping(value = "/delete", method=RequestMethod.POST)
	public int deleteProjectMember(@RequestParam int project_member_idx) {
		return projectMemberService.deleteProjectMember(project_member_idx);	
	}
	
	
	@RequestMapping(value = "/emailCheck", method=RequestMethod.GET)
	public String deleteProjectMember(@RequestParam Map<String, Object> map) {
		System.out.println("map : " + map);
		projectMemberService.insertMember(map);
		
		return "redirect:/index/main";
	}
	
}
