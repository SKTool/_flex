package controller.schedule;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import service.UserService;
import service.project.ProjectService;
import service.schedule.ScheduleService;

@RequestMapping("/schedule")
@Controller
public class Schedulecontroller {

	@Autowired
	private ScheduleService scheduleService;

	@Autowired
	private ProjectService projectService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/schedule", method = RequestMethod.GET)
	public String schedule(Model model,HttpServletRequest request) {
		Gson gson = new Gson();
		HttpSession session = request.getSession();
		List<Map<String, Object>> scheduleList = scheduleService.selectOneScheduleByprojectIdx((int) session.getAttribute("project_idx"));
		String sList = gson.toJson(scheduleList);
		model.addAttribute("scheduleList",sList);
		System.out.println(sList);
		return "schedule/schedule";
	}
	
	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String createSchedules(@RequestParam Map<String, Object> calendar, Model model, Principal principal,HttpServletRequest request) {
		HttpSession session = request.getSession();
		calendar.put("project_idx", session.getAttribute("project_idx"));
		calendar.put("user_idx", userService.getUserIdxById(principal.getName()));
		System.out.println(calendar);
		if(scheduleService.insertSchedule(calendar)) {
			System.out.println("일정 등록 성공");			
		}else{
			System.out.println("일정 등록 실패");	
		};
		return "redirect:/schedule/schedule";
	}
	
	
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateSchedules(@RequestParam Map<String, Object> calendar, Model model, Principal principal,HttpServletRequest request) {
		calendar.put("user_idx", userService.getUserIdxById(principal.getName()));
		System.out.println(calendar);
		if(scheduleService.updateSchedule(calendar)) {
			System.out.println("일정 수정 성공");			
		}else{
			System.out.println("일정 수정 실패");	
		};
		return "redirect:/schedule/schedule";
	}
	

	@RequestMapping(value = "/gantt", method = RequestMethod.GET)
	public String toast(Model model, HttpServletRequest request) {
		Gson gson = new Gson();
		HttpSession session = request.getSession();
		List<Map<String, Object>> scheduleList = scheduleService.selectOneScheduleByprojectIdx((int)session.getAttribute("project_idx"));
		String ganttList = gson.toJson(scheduleList);
		if (scheduleList != null) {
	        // With 4 indent spaces
			model.addAttribute("ganttList", ganttList);
	    } 
		return "schedule/gantt";
	}

	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public boolean scheduleDelete(@RequestParam(value = "id") String id) {
		if (scheduleService.deleteSchedule(id)) {
			return true;
		}
		return false;
	}
	

}
