package controller.msgBoard;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.msgboard.MsgBoardService;


@Controller
@RequestMapping("/reply")
public class MsgBoardReplyController {
	@Autowired
	private MsgBoardService service;
	
	@ResponseBody
	@RequestMapping(value="",method =RequestMethod.POST)
	public boolean MsgBoardReplyInsert(
			@RequestParam Map<String, Object> params) {
		return service.insertMsgBoardReply(params);
	}  
	
	@ResponseBody
	@RequestMapping(value = "/{msgboard_reply_idx}",method=RequestMethod.POST)
	public boolean MsgBoardRplyModify(
			@RequestParam Map<String, Object> params, @PathVariable("msgboard_reply_idx") String msgboard_reply_idx) {
		params.put("msgboard_reply_idx", msgboard_reply_idx);
		return service.updateMsgBoardReply(params);
	}
	
	@ResponseBody
	@RequestMapping(value="/msgboardReplyDelete",method=RequestMethod.POST)
	public boolean MsgBoardRplyDelete(
			@RequestParam Map<String, Object> params) {
		return service.deleteMsgBoardReply(params);
	}
	
	@ResponseBody
	@RequestMapping(value="/all/{msgboard_idx}",method=RequestMethod.GET)
	public ResponseEntity<List<Map<String, Object>>> MsgBoardRplyList(
			@PathVariable("msgboard_idx") int msgboard_idx){
		ResponseEntity<List<Map<String, Object>>> entity = null;
		try{
			List<Map<String, Object>> replyList
			= service.msgBoardReplyList(msgboard_idx);
			entity = new ResponseEntity<List<Map<String,Object>>>(replyList, HttpStatus.OK);
		}catch (Exception e) {
			entity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
}
