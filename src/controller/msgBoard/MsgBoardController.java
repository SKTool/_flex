package controller.msgBoard;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import common.Constants;
import service.UserService;
import service.msgboard.MsgBoardService;

@Controller
@RequestMapping("/msgboard")
public class MsgBoardController {

	@Autowired
	private MsgBoardService service;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/msgboardList")
	public String msgBoardList(Model model, @RequestParam(value = "page", defaultValue = "1") int page) {
//		logger.info("pageCall : adminBoardList");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("page", page);
		model.addAllAttributes(service.getViewDataMsgBoard(params));
		return "msgboard/msgboardList";
	}
	
	@RequestMapping(value="/msgboardWrite", method=RequestMethod.GET)
	public String msgBoardWriteForm() {
		return "msgboard/msgboardWrite";
	}
	
	@RequestMapping(value="/msgboardWrite", method=RequestMethod.POST)
	public String msgBoardWrite(Model model, @RequestParam Map<String,Object> msgboard, MultipartFile file) {
		String msg = null;
		String url = "msgboardList";
		
		// TODO idx spring security 
		msgboard.put(Constants.MsgBoard.USER_IDX, 1);
		
		if(service.insertMsgBoard(msgboard)) { //, file 파라미터 추가
			msg = "작성완료";
		}else {
			msg = "작성실패";
		}
		model.addAttribute("msg",msg);
		model.addAttribute("url",url);
		return "result";
	}
	
	@RequestMapping(value="/msgboardModify",method=RequestMethod.GET)
	public String msgBoardModifyForm(Model model, int msgboard_idx) {
		model.addAttribute("msgboard",service.getMsgBoardByIdx(msgboard_idx));
		return "msgboard/msgboardModify";
	}
	
	@RequestMapping(value="/msgboardModify",method=RequestMethod.POST)
	public String msgBoardModify(Model model,@RequestParam Map<String,Object> msgboard) {
		String url = "msgboardView?msgboard_idx="+msgboard.get(Constants.MsgBoard.MSGBOARD_IDX);
		String msg = null;
		if(service.updateMsgBoard(msgboard)) {
			msg = "게시글 수정을 완료하였습니다.";
		}else {
			msg = "게시글 수정에 실패였습니다.";
		}
		model.addAttribute("url",url);
		model.addAttribute("msg",msg);
		return "result";
	}
	  
	@RequestMapping(value="/msgboardDelete")
	public String msgBoardDelete(int msgboard_idx,Model model) {
		String url = "msgboardList";
		String msg = null;
		if(service.deleteMsgBoard(msgboard_idx)) {
			msg = "게시글 삭제를 완료하였습니다.";
		}else {
			msg = "게시글 삭제에 실패하였습니다.";
		}
		model.addAttribute("url",url);
		model.addAttribute("msg",msg);
		return "result";
	}
	
	@RequestMapping(value="/msgboardView")
	public String msgBoardView(Model model, @RequestParam(value = "msgboard_idx") int msgboard_idx, Principal principal) {
		model.addAttribute("msgboard",service.readMsgBoard(msgboard_idx));

		int userIdx = userService.getUserIdxById(principal.getName());
		model.addAttribute("userIdx",userIdx);

		return "msgboard/msgboardView";
	}
}