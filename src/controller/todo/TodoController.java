package controller.todo;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import service.UserService;
import service.project.ProjectService;
import service.todo.TodoService;

@Controller
@RequestMapping("/todo")
public class TodoController {
	@Autowired
	private TodoService todoService;
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectService projectService;
	
	public int selectMyProjectMemberIdx(HttpServletRequest request, Principal principal){
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String,Object> param = new HashMap<>();
		param.put("project_idx", projectService.getByProjectIdx(request));
		param.put("user_idx", user_idx);
		return todoService.selectMyProjectMemberIdx(param);
	}
	@RequestMapping(value = "/write", method = RequestMethod.GET)
	public String todoForm() {
		return "todo/todo";
	}
	@ResponseBody
	@RequestMapping(value ="/write", method = RequestMethod.POST)
	public Map<String,Object> writeTodo(@RequestParam Map<String,Object> todo, HttpServletRequest request, Principal principal) {
		int user_idx = userService.getUserIdxById(principal.getName());
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("project_idx", projectService.getByProjectIdx(request));
		param.put("user_idx", user_idx);
		todo.put("project_member_idx", (int)selectMyProjectMemberIdx(request, principal));
		return todoService.writeTodo(todo);
	}
	
	@ResponseBody
	@RequestMapping(value ="/modify", method = RequestMethod.POST)
	public Map<String, Object> modifyTodo(@RequestParam Map<String, Object> todo) {
		return todoService.modifyTodo(todo);
	}
	@ResponseBody
	@RequestMapping(value ="/delete", method = RequestMethod.POST)
	public boolean deleteTodo(@RequestParam Map<String, Object> todo) {
		return todoService.deleteTodo(todo);
	}
	
	@ResponseBody
	@RequestMapping(value ="/checked", method = RequestMethod.GET)
	public int checkTodo(@RequestParam Map<String, Object> todo) {
		System.out.println("checked 된.. todo.. : " + todo );
		int a = todoService.checkTodo(todo);
		return a;
	}
	
	@ResponseBody
	@RequestMapping(value ="/all", method = RequestMethod.POST)
	public List<Map<String,Object>> listTodo(Principal principal){
		int user_idx = userService.getUserIdxById(principal.getName());
		System.out.println(todoService.getUserIdxTodoList(user_idx));
		return todoService.getUserIdxTodoList(user_idx);
	}
}
