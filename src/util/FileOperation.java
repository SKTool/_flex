package util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import common.Common;

//파일업로드 후 파일명 반환
public class FileOperation{
	
	public static String upload(MultipartFile file) throws IOException{
		
		String fileName = null;
		if(file != null || !((String)file.getOriginalFilename()).equals("")) {
			
			String root_path = Common.User.FILE_PATH;
			String date_temp = new Date().getTime() + "";
			String fileName_temp = file.getOriginalFilename();
			
			FileCopyUtils.copy(file.getBytes(), new File(root_path + date_temp + "_" + fileName_temp));
			
			fileName = date_temp + "_" + fileName_temp;
			
		}
		return fileName;
	}
}
