package util;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.File;
import java.util.Properties;

public class JavaMailWithFile {
	
  private static final String senderEmail = "forprojectflex@gmail.com";
  private static final String senderPassword = "dkdlxl1!";
  public final static String host_name = "http://localhost:9080";
  
  public static void sendMail(String to, String title, String content, File file) throws MessagingException {
      System.out.println("Sending email to " + to);
      	String contentForm1;
		String contentForm2;
		String contentForm3;
		
		contentForm1 ="<div style='background-color: #f9f9f9;'>"
			         +"<div style='margin: auto; width: 80%; border-top: 5px solid #00004d; border-left: 1px solid #dddddd; "
			         +"            border-right: 1px solid #dddddd; border-bottom: 5px solid #00004d; padding: 50px; background-color: #fff;'>"
			         +"    <h2 style='text-align: center; margin-bottom: 80px;'>"; 
		contentForm2 ="</h2> <div align = \"center\">";
		contentForm3 ="<br> <br> "
			         +"<div style='text-align:center;'>"
			         +"</div>"
			         +"</div> </div>";
		
		content = contentForm1 + title + contentForm2 + content;
		String content_bottom = contentForm3;
      Session session = createSession();

      //create message using session
      MimeMessage message = new MimeMessage(session);
      prepareEmailMessage(message, to, title, content, content_bottom, file);

      //sending message
      Transport.send(message);
      System.out.println("Done");
  }

  private static void prepareEmailMessage(MimeMessage message, String to, String title, String msg,String msg_bottom, File file)
          throws MessagingException {
      message.setFrom(new InternetAddress(senderEmail));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
      message.setSubject(title);
      
      MimeBodyPart mbp1 = new MimeBodyPart();
      mbp1.setContent(msg, "text/html; charset=utf-8");
      
      
      MimeBodyPart mbp2 = new MimeBodyPart();
      FileDataSource fds = new FileDataSource(file.getAbsolutePath());
      mbp2.setDataHandler(new DataHandler(fds));
      mbp2.setFileName(fds.getName());
      
      
      
      Multipart mp = new MimeMultipart();
      mp.addBodyPart(mbp1);
      mp.addBodyPart(mbp2);
      
      message.setContent(mp);
      
  }

  private static Session createSession() {
      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");//Outgoing server requires authentication
      props.put("mail.smtp.starttls.enable", "true");//TLS must be activated
      props.put("mail.smtp.host", "smtp.gmail.com"); //Outgoing server (SMTP) - change it to your SMTP server
      props.put("mail.smtp.port", "587");//Outgoing port

      Session session = Session.getInstance(props, new javax.mail.Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(senderEmail, senderPassword);
          }
      });
      return session;
  }

  
}