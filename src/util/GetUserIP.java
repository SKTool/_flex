package util;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

public class GetUserIP {
	
	private static Logger logger = Logger.getLogger(GetUserIP.class);
	
	public static String from(HttpServletRequest request) {
		
		String ip = request.getHeader("X-FORWARDED-FOR");
		String from = "X-FORWARDED-FOR";
		
		if(ip == null) {
			ip = request.getHeader("Proxy-Client-IP");
			from = "Proxy-Client-IP";
		}
		
		if(ip == null) {
			ip = request.getHeader("WL-Proxy-Client-IP");
			from = "WL-Proxy-Client-IP";
		}
		
		if(ip == null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
			from = "HTTP_CLIENT_IP";
		}
		
		if(ip == null) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			from = "HTTP_X_FORWARDED_FOR";
		}
		
		if(ip == null) {
			ip = request.getRemoteAddr();
			from = "";
		}
		
		logger.info("ip from " + from + " : " + ip);
		return ip;
	}
}