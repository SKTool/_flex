package util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class JavaMail {
	
  private static final String senderEmail = "forprojectflex@gmail.com";
  private static final String senderPassword = "dkdlxl1!";
  public final static String host_name = "http://localhost:9080";
  
  public static void sendMail(String to, String title, String content) throws MessagingException {
      System.out.println("Sending email to " + to);
      	String contentForm1;
		String contentForm2;
		String contentForm3;
		
		contentForm1 ="<div style='background-color: #f9f9f9;'>"
			         +"<div style='margin: auto; width: 80%; border-top: 5px solid #00004d; border-left: 1px solid #dddddd; "
			         +"            border-right: 1px solid #dddddd; border-bottom: 5px solid #00004d; padding: 50px; background-color: #fff;'>"
			         +"    <h2 style='text-align: center; margin-bottom: 80px;'>"; 
		contentForm2 ="</h2> <div align = \"center\">";
		contentForm3 ="<br> <br> "
			         +"<div style='text-align:center;'>"
			         +"</div>"
			         +"</div> </div>";
		
		content = contentForm1 + title + contentForm2 + content + contentForm3;
      
      Session session = createSession();

      //create message using session
      MimeMessage message = new MimeMessage(session);
      prepareEmailMessage(message, to, title, content);

      //sending message
      Transport.send(message);
      System.out.println("Done");
  }

  private static void prepareEmailMessage(MimeMessage message, String to, String title, String html)
          throws MessagingException {
      message.setContent(html, "text/html; charset=utf-8");
      message.setFrom(new InternetAddress(senderEmail));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
      message.setSubject(title);
  }

  private static Session createSession() {
      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");//Outgoing server requires authentication
      props.put("mail.smtp.starttls.enable", "true");//TLS must be activated
      props.put("mail.smtp.host", "smtp.gmail.com"); //Outgoing server (SMTP) - change it to your SMTP server
      props.put("mail.smtp.port", "587");//Outgoing port

      Session session = Session.getInstance(props, new javax.mail.Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(senderEmail, senderPassword);
          }
      });
      return session;
  }

  
}