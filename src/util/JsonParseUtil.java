package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonParseUtil {
	private ObjectMapper mapper;

	public JsonParseUtil() {
		mapper = new ObjectMapper();
	}

	public String parseJsonWithKeyword(String json, String keyword) throws IOException {
		JsonNode node = null;
		node = mapper.readTree(json);
		node = node.get(keyword);

		String result = null;

		if (node != null) {
			result = node.toString();
		}

		return result; 
	}
	public Object convertToObject(String json) throws IOException {
		Map<String, Object> commitsMap = new HashMap<String, Object>(); 
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		Object obj = null;

		if (json.charAt(0) == '[') {
			 list = mapper.readValue(json, new TypeReference<List<Map<String, Object>>>(){});
//			commitsMap = list.get(0);
			 obj = list;
		} else {
			commitsMap = mapper.readValue(json, new TypeReference<Map<String, Object>>(){});
			obj = commitsMap;
		}
//		return commitsMap;
		return obj;
	}
	
	
	public String convertToString(Object map) throws JsonProcessingException {
		return mapper.writeValueAsString(map);
	}

	

}
