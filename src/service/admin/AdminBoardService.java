package service.admin;

import java.util.List;
import java.util.Map;

public interface AdminBoardService {

	public boolean insertAdminBoard(Map<String, Object> params);
	public boolean updateAdminBoard(Map<String, Object> params);
	public boolean deleteAdminBoard(int admin_board_idx);
	public Map<String, Object> adminBoardReadCount(int admin_board_idx);
	public Map<String, Object> selectOneAdminBoard(int admin_board_idx);
	public List<Map<String, Object>> selectAllAdminBoard(Map<String, Object> params);
//	public List<Map<String, Object>> selectAllFaq(Map<String, Object> params);	
	public List<Map<String, Object>> selectAllFaq();	

	public Map<String, Object> getViewDataAdminBoard(Map<String, Object> params);
//	public Map<String, Object> getViewDataFaq(Map<String, Object> params);

}
