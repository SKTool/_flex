package service.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.AdminDao;

@Service("adminBoardService")
public class AdminBoardServiceImp implements AdminBoardService{

	@Autowired
	private AdminDao adminDao;

	private static final int NUM_OF_BOARD_PER_PAGE=10;
	private static final int NUM_OF_NAVI_PAGE=10;

	//페이징 처리하기 위한 함수
	private int getFirstRow(int page) {
		int result  = ((page-1)*NUM_OF_BOARD_PER_PAGE);
//		System.out.println("firstrow : " + result);
		return result;
	}
	private int getEndRow(int page) {
		int result =  ((page-1) + 1)*NUM_OF_BOARD_PER_PAGE ;
//		System.out.println("endrow : " + result);
		
		return result;
	}
	private int getStartPage(int page) {
		int result  = (((page-1)/NUM_OF_BOARD_PER_PAGE)*NUM_OF_BOARD_PER_PAGE)+1;
		
		return result;
	}
	private int getEndPage(int page) {
		int result = getStartPage(page)+9;
		return result;
	}
	
	private int getTotalPageAdminBoard(Map<String, Object> params) {
		int totalCount = adminDao.selectTotalCountAdminBoard(params);
		int totalPage = ((totalCount-1)/NUM_OF_BOARD_PER_PAGE)+1;
		return totalPage;
	}
	
//	private int getTotalPageFaq(Map<String, Object> params) {
//		int totalCount = adminDao.selectTotalCountFaq(params);
//		int totalPage = (totalCount-1)/NUM_OF_BOARD_PER_PAGE+1;
//		return totalPage;
//	}
	
	public Map<String, Object> getViewDataAdminBoard(Map<String, Object> params) {
		//startPage, endPage, totalPage, boardList  반환
		int page = (int)params.get("page");
		Map<String, Object> daoParam = new HashMap<String,Object>();
		daoParam.put("firstRow", getFirstRow(page));
		params.put("firstRow", getFirstRow(page));
		daoParam.put("endRow", getEndRow(page));
		params.put("endRow", getEndRow(page));
		
		Map<String, Object> viewData = new HashMap<String,Object>();
		List<Map<String, Object>> adminBoardList = selectAllAdminBoard(daoParam);
//		viewData.put("daoParam", daoParam); 
		viewData.put("adminBoardList", adminBoardList);
		viewData.put("startPage", getStartPage(page));
		viewData.put("endPage", getEndPage(page));
		viewData.put("totalPage", getTotalPageAdminBoard(daoParam));
		viewData.put("page", page);
		
		return viewData;
	}
	
//	public Map<String, Object> getViewDataFaq(Map<String, Object> params) {
//		//startPage, endPage, totalPage, boardList  반환
//		int page = (int)params.get("page");
//		Map<String, Object> daoParam = new HashMap<String,Object>();
//		daoParam.put("firstRow", getFirstRow(page));
//		params.put("firstRow", getFirstRow(page));
//		daoParam.put("endRow", getEndRow(page));
//		params.put("endRow", getEndRow(page));
//		
//		Map<String, Object> viewData = new HashMap<String,Object>();
//		List<Map<String, Object>> FaqList = selectAllFaq(params);
//		viewData.put("daoParam", daoParam); 
//		viewData.put("FaqList", FaqList);
//		viewData.put("startPage", getStartPage(page));
//		viewData.put("endPage", getEndPage(page));
//		viewData.put("totalPage", getTotalPageFaq(daoParam));
//		viewData.put("page", page);
//		return viewData;
//	}
		
	@Override
	public boolean insertAdminBoard(Map<String, Object> params) {
		if(adminDao.insertAdminBoard(params)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateAdminBoard(Map<String, Object> params) {
		if(adminDao.updateAdminBoard(params)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteAdminBoard(int admin_board_idx) {
		if(adminDao.deleteAdminBoard(admin_board_idx)>0) {
			return true;
		}
		return false;
	}

	@Override
	public Map<String, Object> adminBoardReadCount(int admin_board_idx) {
		adminDao.updateAdminBoardReadCount(admin_board_idx);
		return adminDao.selectOneAdminBoard(admin_board_idx);
	}

	@Override
	public Map<String, Object> selectOneAdminBoard(int admin_board_idx) {
		return adminDao.selectOneAdminBoard(admin_board_idx);
	}

	@Override
	public List<Map<String, Object>> selectAllAdminBoard(Map<String, Object> params) {
		return adminDao.selectAllAdminBoard(params);
	}
//	@Override
//	public List<Map<String, Object>> selectAllFaq(Map<String, Object> params) {
//		return adminDao.selectAllFaq(params);
//	}
	
	@Override
	public List<Map<String, Object>> selectAllFaq() {
		return adminDao.selectAllFaq();
	}

}
