package service.faq;

import java.util.List;
import java.util.Map;

public interface QnaBoardService {

	public boolean insertQnaBoard(Map<String, Object> params);
	public boolean updateQnaBoard(Map<String, Object> params);
	public boolean deleteQnaBoard(int qna_idx);
	public Map<String, Object> selectOneQnaBoard(int qna_idx);
	public List<Map<String, Object>> selectAllQna(Map<String, Object> params);
	public Map<String, Object> getViewDataQna(Map<String, Object> params);

	
	public boolean insertQnaAnswer(Map<String, Object> param);
	public boolean updateQnaAnswer(Map<String, Object> param);
	public boolean deleteQnaAnswer(int qna_answer_idx);
	public Map<String, Object> selectOneQnaAnswer(int qna_answer_idx);
	public Map<String, Object> selectOneQnaBoardWithAnswer(int qna_idx);
	public List<Map<String, Object>> selectAllQnaAnswer();	

}
