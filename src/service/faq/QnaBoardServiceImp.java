package service.faq;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.AdminDao;
import dao.QnaDao;


@Service("qnaBoardService")
public class QnaBoardServiceImp implements QnaBoardService{
	
	@Autowired
	private QnaDao qnaDao;
	
	
	private static final int NUM_OF_BOARD_PER_PAGE=10;
	private static final int NUM_OF_NAVI_PAGE=10;

	//�럹�씠吏� 泥섎━�븯湲� �쐞�븳 �븿�닔
	private int getFirstRow(int page) {
		int result  = ((page-1)*NUM_OF_BOARD_PER_PAGE);
		return result;//1 //11
	}
	private int getEndRow(int page) {
		int result =  ((page-1) + 1)*NUM_OF_BOARD_PER_PAGE ;
		return result;//10//20
	}
	private int getStartPage(int page) {
		int result  = ((page-1)/NUM_OF_BOARD_PER_PAGE)*NUM_OF_BOARD_PER_PAGE+1;
		return result;//1//2
	}
	private int getEndPage(int page) {
		int result = getStartPage(page)+9;
		return result;//10
	}
	
	private int getTotalPageQnaBoard(Map<String, Object> params) {
		int totalCount =  qnaDao.selectTotalCountQna(params) ;
		int totalPage = ((totalCount-1)/NUM_OF_BOARD_PER_PAGE)+1;
		return totalPage;
	}
	
	public Map<String, Object> getViewDataQna(Map<String, Object> params) {
		//startPage, endPage, totalPage, boardList  諛섑솚
		int page = (int)params.get("page");
		Map<String, Object> daoParam = new HashMap<String,Object>();
		daoParam.put("firstRow", getFirstRow(page));
		params.put("firstRow", getFirstRow(page));
		daoParam.put("endRow", getEndRow(page));
		params.put("endRow", getEndRow(page));
				
		Map<String, Object> viewData = new HashMap<String,Object>();
		List<Map<String, Object>> QnaList = selectAllQna(params);
//		List<Map<String, Object>> QnaAnswer = selectAllQnaAnswer();
//		viewData.put("QnaAnswer", QnaAnswer);
		viewData.put("daoParam", daoParam); 
		viewData.put("QnaList", QnaList);
		viewData.put("startPage", getStartPage(page));
		viewData.put("endPage", getEndPage(page));
		viewData.put("totalPage", getTotalPageQnaBoard(daoParam));
		viewData.put("page", page);	
		return viewData;
	}	
	
	@Override
	public boolean insertQnaBoard(Map<String, Object> params) {
		if(qnaDao.insertQna(params)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateQnaBoard(Map<String, Object> params) {
		if(qnaDao.updateQna(params)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteQnaBoard(int qna_idx) {
		if(qnaDao.deleteQna(qna_idx)>0) {
			return true;
		}
		return false;
	}

	@Override
	public Map<String, Object> selectOneQnaBoard(int qna_idx) {
		return qnaDao.selectOneQna(qna_idx);
	}

	@Override
	public List<Map<String, Object>> selectAllQna(Map<String, Object> params) {
		return qnaDao.selectAllQna(params);
	}
	@Override
	public boolean insertQnaAnswer(Map<String, Object> param) {
		if(qnaDao.insertQnaAnswer(param)>0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean updateQnaAnswer(Map<String, Object> param) {
		if(qnaDao.updateQnaAnswer(param)>0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean deleteQnaAnswer(int qna_answer_idx) {
		if(qnaDao.deleteQnaAnswer(qna_answer_idx)>0) {
			return true;
		}
		return false;
	}
	@Override
	public Map<String, Object> selectOneQnaAnswer(int qna_answer_idx) {
		return qnaDao.selectOneQnaAnswer(qna_answer_idx);
	}
	@Override
	public List<Map<String, Object>> selectAllQnaAnswer() {
		return qnaDao.selectAllQnaAnswer();
	}
	public Map<String, Object> selectOneQnaBoardWithAnswer(int qna_idx){
		return qnaDao.selectOneQnaBoardWithAnswer(qna_idx);
	};

	

}
