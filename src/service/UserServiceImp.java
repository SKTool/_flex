package service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import static common.Common.User.*;

import dao.BugDao;
import dao.ProjectDao;
import dao.TaskDao;
import dao.UserDao;
import util.FileOperation;
import util.GetUserIP;
import util.JavaMail;
import util.JavaMailWithFile;

@Service
public class UserServiceImp implements UserService{
	
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private TaskDao taskDao;
	
	@Autowired
	private BugDao bugDao;
	
	@Autowired
	private ProjectDao projectDao;
	
	@Autowired
	private PasswordEncoder encoder;
	
	private static Logger logger = Logger.getLogger("UserService.java");
	
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public int join(Map<String, Object> map,MultipartFile user_pic, MultipartFile user_file, HttpServletRequest request) {
		logger.info("serviceCall : join");
		int user_idx;
		int result = DEFAULT;
		
		Map<String, Object> argMap;
		
		map.put(STATUS, DEFAULT);
		map.put(USER_PIC, "blank_profile.png");
		
		String uuid = UUID.randomUUID().toString().replace("-", "");
		
		map.put(ETC, uuid);
		map.put(USER_PW, encoder.encode((String)map.get(USER_PW)));
		
		userDao.insertUser(map);
		
		user_idx = Integer.parseInt(map.get(USER_IDX) + "");
		
		try {
			
			JavaMail.sendMail((String)map.get(USER_ID), "회원 가입 인증", 
					"<p><b>인증</b></p>"
						+ "<div> 이름 : " + map.get("user_name") + "</div>"
						+ "<div> 이메일 : " + map.get("user_id") + "</div><br/>"
						+ "<div>"
						+ "<a href = '"
						+ JavaMail.host_name
						+ request.getContextPath() + "/user/emailCheck?etc="
						+ uuid
						+ "&user_idx="
						+ user_idx
						+ "&user_id="
						+ (String)map.get(USER_ID)
						+ "'>"
						+ "인증하기"
						+ "</a>"
						+ "</div><br/>"
					);
		} catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			result = FAIL;
			return result;
		} 
		
		if(user_idx > 0) {
			
			argMap = new HashMap<>();
			argMap.put(USER_IDX, user_idx);	
			argMap.put(USER_AUTH_IDX, 2);
			
			if((userDao.insertUserAuthList(argMap)) > 0) {
				result = SUCCESS;
			}
		}else {
			result = FAIL;
		}
		
		return result;
	}
	
	@Override
	public int loginCheck(Map<String, Object> map, HttpSession session) {
		logger.info("serviceCall : loginCheck");
		int result;
		
		Map<String, Object> user = userDao.selectUser(map);
		
		if(user != null) {
			
			if((int)user.get(STATUS) == CHECKED) {
				
				if(encoder.matches((String)map.get(USER_PW),(String)user.get(USER_PW))) {
					
					session.setAttribute(USER_IDX, user.get(USER_IDX));
					result = SUCCESS;
					
				}else {
					result = FAIL;
				}
			}else {
				result = CERTIFICATION_NEEDED;
			}
		}else {
			result = FAIL;
		}
		
		return result;
	}
	
	@Override
	public int pwInquiryChk(Map<String, Object> map) {
		logger.info("serviceCall : pwInquiryChk");
		Map<String, Object> user;
		
		if((user = userDao.selectUser(map)) != null) {
			
			if(user.get(USER_NAME).equals(map.get(USER_NAME))) {
				
				if(user.get(USER_TEL).equals(map.get(USER_TEL))) {
					
					BigInteger user_idx_temp = (BigInteger)user.get(USER_IDX); 
					int user_idx = user_idx_temp.intValue();
					return user_idx;
					
				}
			}
		};
		
		return FAIL;
	}

	@Override
	public Map<String, Object> findId(Map<String, Object> map) {
		logger.info("serviceCall : findId");
		int result = DEFAULT;
		
		Map<String ,Object> resultMap = new HashMap<>();
		Map<String, Object> user_id = getIdByTelName(map);
		
		if((int)user_id.get(RESULT) == FAIL) {
			
			result = FAIL;
			
		}else if((int)user_id.get(RESULT) == SUCCESS) {
			
			String user_id_temp = (String)user_id.get(USER_ID);
			
			user_id_temp = user_id_temp.substring(0, user_id_temp.lastIndexOf("@")-4) 
				    + "****" 
				    + user_id_temp.substring(user_id_temp.lastIndexOf("@"), user_id_temp.length());
			
			result = SUCCESS;
			resultMap.put(USER_ID, user_id_temp);
		}
		
		resultMap.put(RESULT, result);
		
		return resultMap;
	}

	@Override
	public int findPw(Map<String, Object> map, HttpServletRequest request) {
		logger.info("serviceCall : findPw");
		Map<String, Object> argMap;
		int result = DEFAULT;
		int user_idx = pwInquiryChk(map);
		if(user_idx != -1) {
			
			String uuid = UUID.randomUUID().toString().replace("-", "");
			
			argMap = new HashMap<>();
			argMap.put(USER_IDX, user_idx);
			argMap.put(ETC, uuid);
			if(userDao.updateUser(argMap) > 0) {
				try {
					
					JavaMail.sendMail((String)map.get("user_id"), "비밀번호 변경", 
							"<p><b>인증</b></p>"
									+ "<div> 이름 : " + map.get("user_name") + "</div>"
									+ "<div> 이메일 : " + map.get("user_id") + "</div><br/>"
									+ "<div>"
									+ "<a href = '"
									+ JavaMail.host_name
									+ request.getContextPath() + "/user/modifyUserPwForm?uuid="
									+ uuid
									+ "&user_id="
									+ map.get(USER_ID)
									+ "&user_idx="
									+ user_idx
									+ "'>"
									+ "비밀번호 변경 페이지로"
									+ "</a>"
									+ "</div><br/>"
							);
				} catch (MessagingException e) {
					e.printStackTrace();
				}
				
				result = SUCCESS;
				
			}else {
				result = FAIL;
			}
			
		}else {
			result = FAIL;
		}
		
		return result;
	}

	@Override
	public Map<String, Object> getIdByTelName(Map<String, Object> map) {
		logger.info("serviceCall : getIdByTelName");
		Map<String, Object> resultMap = new HashMap<>();
		Map<String, Object> user = userDao.selectUser(map);
		
		if(user.get(USER_NAME).equals(map.get(USER_NAME))) {
			
			resultMap.put(USER_ID, (String)user.get(USER_ID));
			resultMap.put(RESULT, SUCCESS);
			
		}else {
			
			resultMap.put(RESULT, FAIL);
			
		}
		
		return resultMap;
	}

	@Override
	public boolean getPwByIdTel(Map<String, Object> map) {
		logger.info("serviceCall : getPwByIdTel");
		Map<String, Object> user = userDao.selectUser(map);
		
		if(user != null) {
			
			if(user.get(USER_TEL).equals(map.get(USER_TEL))) {
				
				return true;
			}else {
				
				return false;
			}
		}else {
			
			return false;
		}
	}
	
	@Override
	public int emailCheck(Map<String, Object> map) {
		logger.info("serviceCall : emailCheck");
		int result = DEFAULT;
		
		Map<String, Object> argMap;
		Map<String, Object> result_map = userDao.selectUser(map);
		
		String temp_etc = map.get(ETC) + "";
		String real_etc = result_map.get(ETC) + "";
		
		if ("".equals(real_etc)) {
			
			result = ALREADY_DONE;

		} else {

			if (temp_etc.equals(real_etc)) {
				
				argMap = new HashMap<>();
				argMap.put(USER_IDX, map.get(USER_IDX));
				argMap.put(STATUS, CHECKED);
				argMap.put(ETC, "");
				int result_int = userDao.updateUser(argMap);
				
				if (result_int > 0) {
					result = SUCCESS;
				} else {
					result = FAIL;
				}
			} else {
				result = INAPPROPRIATE_ACCESS;
			}
		}
		
		return result;
	}
	
	@Override
	public int userIdCheck(Map<String, Object> map) {
		logger.info("serviceCall : userIdCheck");
		int result = DEFAULT;
		
		if(map.get(USER_ID) != null || (String)map.get(USER_ID) != "") {
			
			if(userDao.selectUser(map) == null) {
				result = SUCCESS;
			}else {
				result = FAIL;
			}
		}
		
		return result;
	}
	
	@Override
	public int userTelCheck(Map<String, Object> map) {
		logger.info("serviceCall : userTelCheck");
		int result = DEFAULT;
		
		if(map.get(USER_TEL) != null || (String)map.get(USER_TEL) != "") {
			
			if(userDao.selectUser(map) == null) {
				result = SUCCESS;
			}else {
				result = FAIL;
			}
		}
		
		return result;
	}

	@Override
	public Map<String, Object> getUser(Map<String, Object> map) {
		logger.info("serviceCall : getUser");
		return userDao.selectUser(map);
	}

	@Override
	public int modifyUser(Map<String, Object> map) {
		logger.info("serviceCall : modifyUser");
		int result_update = userDao.updateUser(map);
		int result = DEFAULT;
		
		if(result_update > 0) {
			result = SUCCESS;
		}else{
			result = FAIL;
		}
		
		return result;
	}
	
	@Override
	public int sendMailForUserIdModification(Map<String, Object> map, HttpServletRequest request) {
		logger.info("serviceCall : sendMailForUserIdModification");
		int result = DEFAULT;
		Map<String, Object> argMap;
		
		argMap = new HashMap<>();
		argMap.put(USER_IDX, map.get(USER_IDX));
		argMap.put(USER_ID, map.get(USER_ID));
		int result_update = userDao.updateUser(argMap);
		
		if(result_update > 0) {
			
			String uuid = UUID.randomUUID().toString().replace("-", "");
			
			argMap = new HashMap<>();
			argMap.put(USER_IDX, map.get(USER_IDX));
			argMap.put(ETC, uuid);
			argMap.put(STATUS, "0");
			userDao.updateUser(argMap);
			
			map = userDao.selectUser(map);
			
			int user_idx = Integer.parseInt(map.get(USER_IDX) + "");
			
			try {
				
				JavaMail.sendMail((String)map.get(USER_ID), "이메일 변경 인증", 
						"<p><b>인증</b></p>"
							+ "<div> 이름 : " + map.get(USER_NAME) + "</div>"
							+ "<div> 이메일 : " + map.get(USER_ID) + "</div><br/>"
							+ "<div>"
							+ "<a href = '"
							+ JavaMail.host_name
							+ request.getContextPath() + "/user/emailCheck?etc="
							+ uuid
							+ "&user_idx="
							+ user_idx
							+ "&user_id="
							+ (String)map.get(USER_ID)
							+ "'>"
							+ "인증하기"
							+ "</a>"
							+ "</div><br/>"
						);
				result = SUCCESS;
				
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}else {
			result = FAIL;
		}
		
		return result;
	};
	
	@Override
	public int modifyUserTel(Map<String, Object> map) {
		logger.info("serviceCall : modifyUserTel");
		Map<String, Object> user = userDao.selectUser(map);
		Map<String, Object> argMap;
		int result = DEFAULT;
		
		if(user != null) {
			
			argMap = new HashMap<>();
			argMap.put(USER_TEL, map.get(USER_TEL));
			
			if(userDao.selectUser(argMap) != null) {
				result = FAIL;
			}else {
				
				argMap.put(USER_IDX, map.get(USER_IDX));
				int result_update = userDao.updateUser(map);
				
				if(result_update > 0) {
					result = SUCCESS;
				}else {
					result = FAIL;
				}
			}
		}
		
		return result;
	};
	
	@Override
	public int modifyUserPw(Map<String, Object> map) {
		logger.info("serviceCall : modifyUserPw");
		int result = DEFAULT;
		Map<String, Object> argMap;
		Map<String, Object> user = userDao.selectUser(map);
		
		String user_pw = (String)user.get(USER_PW);
		
		if(encoder.matches((String)map.get(USER_PW_OLD), user_pw)){
			
			argMap = new HashMap<>();
			argMap.put(USER_IDX, map.get(USER_IDX));
			argMap.put(USER_PW, encoder.encode((String)map.get(USER_PW_NEW)));
			int temp_result = userDao.updateUser(argMap);
			
			if(temp_result > 0) {
				result = SUCCESS;
			}else {
				result = FAIL;
			}
		}
		
		return result;
	};
	
	@Override
	public int modifyUserPwFromMail(Map<String, Object> map) {
		logger.info("serviceCall : modifyUserPwFromMail");
		int result = DEFAULT;
		Map<String, Object> user;
		Map<String, Object> argMap;
		if((user = userDao.selectUser(map)) != null) {

			System.out.println("user : " + user);

			if(((String)user.get(ETC)).equals((String)map.get("uuid"))) {
				System.out.println("유유아이디확인");
				
				argMap = new HashMap<>();
				argMap.put(USER_IDX, map.get(USER_IDX));
				argMap.put(USER_PW, encoder.encode((String)map.get(USER_PW)));
				System.out.println("argMap : " + argMap);
				if(userDao.updateUser(argMap) > 0) {
					System.out.println("업데이트유저성공");
					result = SUCCESS;
				}else {
					System.out.println("업데이트유저실패");
					result = FAIL;
				}
			}else{
				System.out.println("유유아이디틀림");
				result = FAIL;
			}
		}else{
			System.out.println("그런유저없음");
			result = FAIL;
		}
		return result;
	};
	
	
	@Override
	public byte[] getImage(Map<String, Object> map) {
		logger.info("serviceCall : getImage");
		File file = new File(FILE_PATH + (String)map.get(USER_PIC));
		InputStream in = null;
		
		try {
			
			in = new FileInputStream(file);
			
			return IOUtils.toByteArray(in);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(in != null) {in.close();};
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return null;
	}
	
	@Override
	public int defaultPic(Map<String, Object> map) {
		logger.info("serviceCall : defaultPic");
		int result = DEFAULT;
		Map<String, Object> argMap = new HashMap<>();
		argMap.put(USER_IDX, map.get(USER_IDX));
		argMap.put(USER_ID, map.get(USER_ID));
		argMap.put(USER_PIC, "blank_profile.png");
		
		if(userDao.updateUser(argMap) > 0) {
			result = SUCCESS;
		}else {
			result = FAIL;
		}
		return result;
	}
	@Override
	public int modifyUserPic(Map<String, Object> map, MultipartFile user_pic) {
		logger.info("serviceCall : modifyUserPic");
		int result = DEFAULT;
		Map<String, Object> argMap;
		
		argMap = new HashMap<>();
		try {
			argMap.put(USER_PIC, FileOperation.upload(user_pic));
		} catch (IOException e) {
			e.printStackTrace();
			result = FAIL;
			return result;
		}
		
		argMap.put(USER_IDX, map.get(USER_IDX));
		int result_update = userDao.updateUser(argMap);
		
		if(result_update > 0) {
			result = SUCCESS;
		}else {
			result = FAIL;
		}
		
		return result;
	}
	
	@Override
	public int modifyUserFile(Map<String, Object> map, MultipartFile user_file) {
		logger.info("serviceCall : modifyUserFile");
		int result = DEFAULT;
		Map<String, Object> argMap;
		
		argMap = new HashMap<>();
		try {
			argMap.put(USER_FILE, FileOperation.upload(user_file));
		} catch (IOException e) {
			e.printStackTrace();
			result = FAIL;
			return result;
		}
		
		argMap.put(USER_IDX, map.get(USER_IDX));
		int result_update = userDao.updateUser(argMap);
		
		if(result_update > 0) {
			result = SUCCESS;
		}else {
			result = FAIL;
		}
		
		return result;
	}
	@Override
	public int removeAccount(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response) {
		logger.info("serviceCall : removeAccount");
		int result = FAIL;
		
		Map<String, Object> user = userDao.selectUser(map);
		BigInteger temp = (BigInteger)user.get(USER_IDX);
		String user_idx_from_db = Integer.toString(temp.intValue());
		String user_idx = (String)map.get(USER_IDX);
		
		if(user_idx != null || user_idx != ""){
			
			if(user_idx_from_db.equals(user_idx)) {
				
				if(encoder.matches((String)map.get(USER_PW),(String)user.get(USER_PW))) {
					
					userDao.deleteUser(map);
					logout(request, response);
					result = SUCCESS;
				}
			}
		}
		
		return result;
	}
	
	@Override
	public int removeGoogleAccount(Map<String, Object> map,  HttpServletRequest request, HttpServletResponse response) {
		logger.info("serviceCall : removeGoogleAccount");
		
		int result = DEFAULT;
		Map<String, Object> googleUser; 
		if((googleUser = userDao.selectUser(map)) != null) {
			if( ((String)map.get(USER_PW)).equals( (String)googleUser.get(USER_PW) ) ){
				
				if( userDao.deleteUser(map) > 0 ) {
					result = SUCCESS;
					logout(request, response);
				}else {
					result = FAIL;
				}
			}else {
				result = FAIL;
			}
		}else {
			result = FAIL;
		}
		
		return result;
	}

	@Override
	public int resendMail(Map<String, Object> map) {
		logger.info("serviceCall : resendMail");
		int result = DEFAULT;
		
		Map<String, Object> user;
		if((user = userDao.selectUser(map)) != null) {
			
			if(encoder.matches((String)map.get(USER_PW), (String)user.get(USER_PW))){
				
				if((int)user.get(STATUS) != 1) {
					
					String uuid = UUID.randomUUID().toString().replace("-", "");
					user.put(ETC, uuid);
					user.put(STATUS, "0");
					
					int result_update = userDao.updateUser(user);
					BigInteger user_idx_temp = (BigInteger)user.get(USER_IDX); 
					int user_idx = user_idx_temp.intValue();
					
					if(result_update > 0) {
						
						try {
							
							JavaMail.sendMail((String)user.get(USER_ID), "이메일 인증",
									"<p><b>인증</b></p>"
											+ "<div> 이름 : " + user.get(USER_NAME) + "</div>"
											+ "<div> 이메일 : " + user.get(USER_ID) + "</div><br/>"
											+ "<div>"
											+ "<a href = '"
											+ JavaMail.host_name
											+ "/user/emailCheck?etc="
											+ uuid
											+ "&user_idx="
											+ user_idx
											+ "&user_id="
											+ (String)map.get(USER_ID)
											+ "'>"
											+ "인증하기"
											+ "</a>"
											+ "</div><br/>"
									);
						}catch(Exception e) {
							e.printStackTrace();
							result = FAIL;
						}
						
						result = SUCCESS;
					}else {
						result = FAIL; 
					}
				}else {
					result = FAIL;
				}
			}else {
				result = FAIL;
			}
		}else {
			result = FAIL;
		}
		return result;
	}

	@Override
	public List<String> getUserAuths(Map<String, Object> map) {
		logger.info("serviceCall : getUserAuths");
		return userDao.selectUserAuthList(map);
	}
	
	@Override
	public void logout(HttpServletRequest request,HttpServletResponse response) {
		logger.info("serviceCall : logout");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		request.getSession().removeAttribute(GOOGLE);
		
		if(auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
	}

	@Override
	public int getUserIdxById(String user_id) {
		logger.info("serviceCall : getUserIdxById");
		Map<String, Object> map = new HashMap<>();
		map.put(USER_ID, user_id);
		Map<String, Object> user = userDao.selectUser(map);
		BigInteger user_idx = (BigInteger)user.get(USER_IDX);
		
		return user_idx.intValue();
	}

	@Override
	public int googleUserCheck(Map<String, Object> map, HttpSession session) {
		logger.info("serviceCall : googleUserCheck / map : " + map);
		
		int result = FAIL;
		
		if(userDao.selectUser(map) == null) {
			
			if(userDao.insertGoogleUser(map) > 0) {
				
				map.put(USER_AUTH_IDX, 2);
				if(userDao.insertUserAuthList(map) > 0) {
					result = SUCCESS;
				}else {
					result = FAIL;
				}
			}else {
				result = FAIL;
			}
		}else {
			result = SUCCESS;
		}
		session.setAttribute(GOOGLE, TRUE);
		return result;
	}

	@Override
	public int pwCheck(Map<String, Object> map) {
		logger.info("serviceCall : pwCheck");
		int result = DEFAULT;
		Map<String, Object> user;
		if((user = userDao.selectUser(map)) != null) {
			if(((String)user.get(USER_PW)).equals((String)map.get(USER_PW))) {
				result = SUCCESS;
			}else {
				result = FAIL;
			}
		}else {
			result = FAIL;
		}
		return result;
	}
	
	@Override
	public int userPwCheck(Map<String, Object> map) {
		logger.info("serviceCall : userPwCheck");
		int result = DEFAULT;
		Map<String, Object> user;
		if((user = userDao.selectUser(map)) != null) {
			if(encoder.matches((String)map.get(USER_PW), (String)user.get(USER_PW))) {
				result = SUCCESS;
			}else {
				result = FAIL;
			}
		}else {
			result = FAIL;
		}
		return result;
	}

	@Override
	public int insertUserLog(HttpServletRequest request, int user_idx, int status) {
		logger.info("serviceCall : insertUserLog");
		int result = DEFAULT;
		String ip_temp = GetUserIP.from(request);
		Map<String, Object> argMap = new HashMap<>();
		
		argMap.put(USER_LOG_IP, ip_temp);
		argMap.put(USER_IDX, user_idx);
		argMap.put(STATUS, status);
		
		if(userDao.insertUserLog(argMap) > 0) {
			result = SUCCESS;
		}else {
			result = FAIL;
		}
		
		return result;
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public int filesUpload(Map<String, Object> map, MultipartHttpServletRequest files) {
		logger.info("serviceCall : filesUpload");
		List<MultipartFile> fileList = files.getFiles("file");
		int result = SUCCESS;
		 
		Map<String, Object> argMap;
		    
		   for (MultipartFile mf : fileList) {
		       try {
		    	   argMap = new HashMap<>();
		    	   argMap.put(FILE_NAME, FileOperation.upload(mf));
		    	   argMap.put(PROJECT_MEMBER_IDX,map.get(PROJECT_MEMBER_IDX));
		    	   userDao.uploadFiletoProject(argMap);
		       } catch (IllegalStateException e) {
		    	   TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		    	   result = FAIL;
		    	   e.printStackTrace();
		       } catch (IOException e) {
		    	   TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		    	   result = FAIL;
		    	   e.printStackTrace();
		       }
		   }
		   return result;
	}

	@Override
	public List<Map<String, Object>> getFiles(Map<String, Object> map) {
		logger.info("serviceCall : getFiles");
		return userDao.selectFilesByProjectMemberIdx(map);
	}

	@Override
	public List<Map<String, Object>> getAllTasks(Map<String, Object> map) {
		logger.info("serviceCall : getAllTasks");
		List<Map<String, Object>> result_temp = taskDao.selectTasksByProjectMemberIdx(map);
		List<Map<String, Object>> result = new ArrayList<>();
		for(Map<String, Object> task : result_temp) {
			if(task != null) {
				result.add(task);
			}
		}
		return result;
	}

	@Override
	public List<Integer> getBugCounts(Map<String, Object> map) {
		logger.info("serviceCall : getBugCounts");
		List<Map<String, Object>> bugs = bugDao.selectBugsByProjectMemberIdx(map);
		
		List<String> months = new ArrayList<>();
		List<String> result_temp = new ArrayList<>();
		
		Calendar cal = Calendar.getInstance();
		
		
		int thisYear = cal.get(Calendar.YEAR);
		int lastYear = cal.get(Calendar.YEAR)-1;
		int thisMonth = cal.get(Calendar.MONTH)+1;
				
		for(int j = 0; j < thisMonth; j++) {
			
			String temp = Integer.toString(thisYear) + Integer.toString(thisMonth-j);
			months.add(temp);
			if(j == 10) {
				break;
			}
		}
		
		if(months.size() < 10) {
			int leftDate = 10 - thisMonth;
			for(int j = 0; j < leftDate; j++) {
				String temp = Integer.toString(lastYear) + Integer.toString(12-j);
				months.add(temp);
			}
		}
		
		for(int i = 0; i < bugs.size(); i++) {
			int year = Integer.parseInt(((String)bugs.get(i).get("occ_date")).substring(0, 4));
			int month = Integer.parseInt(((String)bugs.get(i).get("occ_date")).substring(5, 7));
			String occ_date = Integer.toString(year) + Integer.toString(month);
			for(int j = 0; j < months.size(); j++) {
				if(occ_date.equals(months.get(j))) {
					result_temp.add(occ_date);
				}
			}
		}
		
		List<Integer> result = new ArrayList<>();
		int count = 0;
		
		for(int i = 0; i < months.size(); i++) {
			for(int j = 0; j < result_temp.size(); j++) {
				if(months.get(i).equals(result_temp.get(j))) {
					count++;
				}
			}
			result.add(count);
			count = 0;
		}
		
		return result;
	}

	@Override
	public List<Map<String, Object>> getProjectsForMyPage(Map<String, Object> map) {
		logger.info("serviceCall : getProjectsForMyPage");
		List<Map<String, Object>> result = projectDao.selectProjectsForMyPage(map);
		return result;
	}

	@Override
	public int alterBellActive(Map<String, Object> map) {
		logger.info("serviceCall : alterBellActive");
		Map<String, Object> argMap = new HashMap<>();
		argMap.put(PROJECT_MEMBER_IDX, map.get(PROJECT_MEMBER_IDX));
		argMap.put(BELL_ACTIVE, map.get(BELL_ACTIVE));
		
		int updateVal = projectDao.updateProjectMember(argMap);
		int result = DEFAULT;
		if(updateVal > 0) {
			result = SUCCESS;
		}else {
			result = FAIL;
		}
		return result;
	}

	@Override
	public int leaveProject(Map<String, Object> map) {
		logger.info("serviceCall : leaveProject");
		int project_member_idx = Integer.parseInt((String)map.get(PROJECT_MEMBER_IDX));
		int deleteVal = projectDao.deleteProjectMember(project_member_idx);
		int result = 0;
		if(deleteVal > 0) {
			result = 1;
		}else {
			result = -1;
		}
		
		return result;
	}

	@Override
	public String getDDay(Map<String, Object> map) {
		logger.info("serviceCall : getDDay");
		int project_member_idx = Integer.parseInt((String)map.get(PROJECT_MEMBER_IDX));
		Map<String, Object> project_member = projectDao.selectOneProjectMember(project_member_idx);
		BigInteger project_idx_temp = (BigInteger)project_member.get(PROJECT_IDX);
		int project_idx = project_idx_temp.intValue();
		Map<String, Object> project = projectDao.selectOneProject(project_idx);
		String project_end = ((String)project.get(PROJECT_END));
		String project_year = project_end.substring(0, 4);
		String project_month = project_end.substring(5, 7);
		String project_date = project_end.substring(8, 10);
		
		Calendar today = Calendar.getInstance();
		Calendar dDay = Calendar.getInstance();	
		
		dDay.set(Integer.parseInt(project_year), Integer.parseInt(project_month)-1, Integer.parseInt(project_date));
		
		long dDay_millis = dDay.getTimeInMillis() / (24*60*60*1000);
		long today_millis = today.getTimeInMillis() / (24*60*60*1000);
		
		int resultVal = (int)(today_millis - dDay_millis); 
		
		String result = null;
		if(resultVal > 0) {
			result = "-" + resultVal;
		}else {
			result = "+" + resultVal;
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> getGitCommitCounts(Map<String, Object> map) {
		return userDao.selectGitCommitCounts(map);
	}

	@Override
	public Map<String, Object> checkUser(Map<String, Object> map) {
		return userDao.selectUser(map);
	}

	@Override
	public int sendInvitationMail(String fromName, String toAddress) {
	   
	   int result = DEFAULT;
	   try {
	      
	      JavaMail.sendMail( toAddress, "Flex로 초대합니다.", 
	            "<p><b>Flex에서 함께 스마트하게 협업하세요. </b></p>"
	               + "<div> 초대한 사람 : " + fromName + "</div><br/>"
	               + "<div>"
	               + "<a href = '"
	               + JavaMail.host_name
	               + "/index/main"
	               + "'>"
	               + "이동하기"
	               + "</a>"
	               + "</div><br/>"
	            );
	      result = SUCCESS;
	      
	   } catch (MessagingException e) {
	      e.printStackTrace();
	      result = FAIL;
	   }
	   return result;
	}

	@Override
	public int sendMailWithFile(Map<String, Object> map) {
		
		int result = DEFAULT;
		Map<String, Object> user = userDao.selectUser(map); 
		map.put("fromAddress", (String)user.get("user_id"));
		String user_file = (String)user.get("user_file");
		if(user_file != null) {
			user_file = (user_file).trim();
		}
		if(user_file != null && !user_file.equals("")) {
			System.out.println("in");
			File file = new File(FILE_PATH + user_file);
			InputStream in = null;
			
			map.put("user_idx", (String)map.get("toIdx"));
			user = userDao.selectUser(map);
			
			map.put("toAddress", (String)user.get("user_id"));
			
			try {
				
				in = new FileInputStream(file);
				
				byte[] fileBytes = IOUtils.toByteArray(in);
				file.createNewFile();
				FileOutputStream fos = new FileOutputStream(file);
				fos.write(fileBytes);
				fos.close();
				
				JavaMailWithFile.sendMail( (String)map.get("toAddress"), "모집 참여 지원서", 
						"<p><b>지원한 사용자의 이메일, 포트폴리오</b></p>"
								+ "<div> 지원자 이메일 : " + (String)map.get("fromAddress") + "</div><br/>"
								+ "<div>"
								+ "</div><br/>"
								, file);
				result = SUCCESS;
				
			} catch (MessagingException e) {
				e.printStackTrace();
				result = FAIL;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else {
			result = FAIL;
		}
		
		return result;
	}

	
	@Override
	public List<Map<String, Object>> selectAllUser() {
		return userDao.selectAllUser();
	}
	
}
