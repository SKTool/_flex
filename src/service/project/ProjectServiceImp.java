package service.project;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestMapping;

import dao.ProjectDao;

@Service("projectService")
public class ProjectServiceImp implements ProjectService{

	@Autowired
	private ProjectDao projectDao;	
	
	@Transactional
	@Override
	public boolean insertProject(Map<String, Object> param) {
		try {
			projectDao.insertProject(param);
			Map<String, Object> projectMember = new HashMap<String, Object>();
			projectMember.put("user_idx", param.get("user_idx"));			
			System.out.println("프로젝트 생성!");

			if (projectDao.insertProjectMember(projectMember)> 0) {
				System.out.println("프로젝트 멤버 생성!");
				return true;
			} else {
				return false;
			}
		}catch(Exception e) {
			//예외를 별도로 발생시키지 않고 현재 적용된 Aspect에 의한 트랙잭션을 rollback 시킴
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return false;
		}
	}

	@Override
	public boolean updateProject(Map<String, Object> param) {
		if(projectDao.updateProject(param)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteProject(int project_idx) {
		if(projectDao.deleteProject(project_idx)>0) {
			return true;
		}
		return false;
	}

	@Override
	public Map<String, Object> selectOneProject(int project_idx) {
		return projectDao.selectOneProject(project_idx);
	}

	
	@Override
	public List<Map<String, Object>> selectAllProject() {
		return projectDao.selectAllProject();
	}

	@Override
	public List<Map<String, Object>> selectAllMyProject(int user_idx) {
		return projectDao.selectAllMyProject(user_idx);
	}

	public Map<String, Object> checkWriteAuth(Map<String, Object> param){
		return projectDao.checkWriteAuth(param);
	};

	
	
	//프로젝트 멤버
	
	@Override
	public boolean insertProjectMember(Map<String, Object> param) {
		if(projectDao.insertProjectMember(param)>0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateProjectMember(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteProjectMember(int project_member_idx) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Map<String, Object>> selectOneProjectMemberJoinUser(int project_idx) {
		return projectDao.selectOneProjectMemberJoinUser(project_idx);
	}

	public Map<String, Object> selectOneProjectMember(int project_idx){
		return projectDao.selectOneProjectMember(project_idx);
	};
	
	@Override
	public List<Map<String, Object>> selectAllProjectMember() {
		return projectDao.selectAllProjectMember();
	}
	public Map<String, Object> selectOneProjectMemberIdxForBro(Map<String, Object> param){
		return projectDao.selectOneProjectMemberIdxForBro(param);
	};
	

	@Override
	public int getByProjectIdx(HttpServletRequest request) {
		HttpSession session = request.getSession();
		int project_idx = (int)session.getAttribute("project_idx");
		return project_idx;
	}

}
