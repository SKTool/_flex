package service.project;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface ProjectService {
	public boolean insertProject(Map<String, Object> param);
	public boolean updateProject(Map<String, Object> param);
	public boolean deleteProject(int project_idx);
	public Map<String, Object>  selectOneProject(int project_idx);
	public List<Map<String, Object>> selectAllProject();
	public List<Map<String, Object>> selectAllMyProject(int user_idx);
	public Map<String, Object> checkWriteAuth(Map<String, Object> param);
	
	public boolean insertProjectMember(Map<String, Object> param);
	public boolean updateProjectMember(Map<String, Object> param);
	public boolean deleteProjectMember(int project_member_idx);
	public List<Map<String, Object>> selectOneProjectMemberJoinUser(int project_idx);
	public Map<String, Object> selectOneProjectMember(int project_idx);
	public Map<String, Object> selectOneProjectMemberIdxForBro(Map<String, Object> param);
	
	public List<Map<String, Object>> selectAllProjectMember();
	public int getByProjectIdx(HttpServletRequest request);
}
