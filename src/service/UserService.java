package service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public interface UserService {
	
	public int join(Map<String, Object> map,MultipartFile user_pic, MultipartFile user_file, HttpServletRequest request);
	public int loginCheck(Map<String, Object> map, HttpSession session);
	public int pwInquiryChk(Map<String, Object> map);
	public Map<String, Object> findId(Map<String, Object> map);
	public int findPw(Map<String, Object> map, HttpServletRequest request);
	public Map<String, Object> getIdByTelName(Map<String, Object> map);
	public boolean getPwByIdTel(Map<String, Object> map);
	public int emailCheck(Map<String, Object> map);
	public int userIdCheck(Map<String, Object> map);
	public int userTelCheck(Map<String, Object> map);
	public Map<String, Object> getUser(Map<String, Object> map);
	public int modifyUser(Map<String, Object> map);
	public int sendMailForUserIdModification(Map<String, Object> map, HttpServletRequest request);
	public int modifyUserTel(Map<String, Object> map);
	public int modifyUserPw(Map<String, Object> map);
	public int modifyUserPwFromMail(Map<String, Object> map);
	public byte[] getImage(Map<String, Object> map);
	public int defaultPic(Map<String, Object> map);
	public int modifyUserPic(Map<String, Object> map, MultipartFile user_pic);
	public int modifyUserFile(Map<String, Object> map, MultipartFile user_pic);
	public int removeAccount(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response);
	public int removeGoogleAccount(Map<String, Object> map, HttpServletRequest request, HttpServletResponse response);
	public int resendMail(Map<String, Object> map);
	public List<String> getUserAuths(Map<String, Object> map);
	public void logout(HttpServletRequest request,HttpServletResponse response);
	public int getUserIdxById(String user_id);
	public int googleUserCheck(Map<String, Object> map, HttpSession session);
	public int pwCheck(Map<String, Object> map);
	public int userPwCheck(Map<String, Object> map);
	public int insertUserLog(HttpServletRequest request, int user_idx, int status);
	public int filesUpload(Map<String, Object> map, MultipartHttpServletRequest files);
	public List<Map<String, Object>> getFiles(Map<String, Object> map);
	public List<Map<String, Object>> getAllTasks(Map<String, Object> map);
	public List<Integer> getBugCounts(Map<String, Object> map);
	public List<Map<String, Object>> getProjectsForMyPage(Map<String, Object> map);
	public int alterBellActive(Map<String, Object> map);
	public int leaveProject(Map<String, Object> map);
	public String getDDay(Map<String, Object> map);
	public List<Map<String, Object>> getGitCommitCounts(Map<String, Object> map);
	public Map<String, Object> checkUser(Map<String, Object> map);
	public int sendInvitationMail(String fromName, String toAddress);
	public int sendMailWithFile(Map<String, Object> map);
	public List<Map<String, Object>> selectAllUser();

}

