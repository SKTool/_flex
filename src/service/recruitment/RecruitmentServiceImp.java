package service.recruitment;

import static common.Common.User.DEFAULT;
import static common.Common.User.ETC;
import static common.Common.User.FAIL;
import static common.Common.User.STATUS;
import static common.Common.User.USER_ID;
import static common.Common.User.USER_IDX;
import static common.Common.User.USER_PIC;
import static common.Common.User.USER_PW;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import dao.RecruitmentDao;
import util.JavaMail;

@Service
public class RecruitmentServiceImp implements RecruitmentService{

	@Autowired
	private RecruitmentDao dao;
	
	private static final int NUM_OF_BOARD_PER_PAGE=10;
	
	@Override
	public List<Map<String, Object>> recruitmentList(Map<String, Object> params) {
		return dao.recruitmentList(params);
	}

	@Override
	public Map<String, Object> recruitmentByIdx(int recruitment_idx) {
		return dao.selectOneRecruitment(recruitment_idx);
	}

	@Override
	public boolean insertRecruitment(Map<String, Object> params) {
		try{
			int result = dao.insertRecruitment(params);
			if(result>0) {
				int recruitmentIdx = Integer.parseInt(params.get("recruitment_idx").toString());
				Map<String,Object> rec = new HashMap<String,Object>();
				rec.put("recruitment_idx",recruitmentIdx);
				if(params.get("web") != null) {
					rec.put("platform_idx",params.get("web"));
					dao.insertPlatformList(rec);
				}
				if(params.get("linux") != null) {
					rec.put("platform_idx",params.get("linux"));
					dao.insertPlatformList(rec);
				}
				if(params.get("window") != null) {
					rec.put("platform_idx",params.get("window"));
					dao.insertPlatformList(rec);
				}
				if(params.get("ios") != null) {
					rec.put("platform_idx",params.get("ios"));
					dao.insertPlatformList(rec);
				}
				if(params.get("android") != null) {
					rec.put("platform_idx",params.get("android"));
					dao.insertPlatformList(rec);
				}
				System.out.println(params);
				return true;
			}else {
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateRecruitment(Map<String, Object> params) {
		try{
			int result = dao.updateRecruitment(params);
			dao.deletePlatformList(params);
			if(result>0) {
				
				int recruitmentIdx = Integer.parseInt(params.get("recruitment_idx").toString());
				Map<String,Object> rec = new HashMap<String,Object>();
				rec.put("recruitment_idx",recruitmentIdx);
				
				if(params.get("web") != null) {
					rec.put("platform_idx",params.get("web"));
					dao.insertPlatformList(rec);
				}
				if(params.get("linux") != null) {
					rec.put("platform_idx",params.get("linux"));
					dao.insertPlatformList(rec);
				}
				if(params.get("window") != null) {
					rec.put("platform_idx",params.get("window"));
					dao.insertPlatformList(rec);
				}
				if(params.get("ios") != null) {
					rec.put("platform_idx",params.get("ios"));
					dao.insertPlatformList(rec);
				}
				if(params.get("android") != null) {
					rec.put("platform_idx",params.get("android"));
					dao.insertPlatformList(rec);
				}
				System.out.println(params);
				return true;
			}else {
				return false;
			}
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Map<String, Object> readRecruitment(int recruitment_idx) {
		dao.plusReadCount(recruitment_idx);
		return dao.selectOneRecruitment(recruitment_idx);
	}


	@Override
	public boolean deleteRecruitment(int recruitment_idx) {
		if(dao.deleteRecruitment(recruitment_idx)>0) {
			return true;
		}
		return false;
	}
	
	private int getFirstRow(int page) {
		int result  = ((page-1)*NUM_OF_BOARD_PER_PAGE);
		return result;
	}
	private int getEndRow(int page) {
		int result =  ((page-1) + 1)*NUM_OF_BOARD_PER_PAGE ;
		
		return result;
	}
	private int getStartPage(int page) {
		int result  = (((page-1)/NUM_OF_BOARD_PER_PAGE)*NUM_OF_BOARD_PER_PAGE)+1;
		
		return result;
	}
	private int getEndPage(int page) {
		int result = getStartPage(page)+9;
		return result;
	}
	
	private int getTotalPageRecruitment(Map<String, Object> params) {
		int totalCount = dao.selectTotalCountRecruitment(params);
		int totalPage = ((totalCount-1)/NUM_OF_BOARD_PER_PAGE)+1;
		return totalPage;
	}
	
	public Map<String, Object> getViewDataRecruitment(Map<String, Object> params) {
		//startPage, endPage, totalPage, boardList  반환
		int page = (int)params.get("page");
		Map<String, Object> daoParam = new HashMap<String,Object>();
		daoParam.put("firstRow", getFirstRow(page));
		params.put("firstRow", getFirstRow(page));
		daoParam.put("endRow", getEndRow(page));
		params.put("endRow", getEndRow(page));
		
		Map<String, Object> viewData = new HashMap<String,Object>();
		List<Map<String, Object>> recruitmentList = selectAllRecruitment(daoParam);
//		viewData.put("daoParam", daoParam); 
		viewData.put("recruitmentList", recruitmentList);
		viewData.put("startPage", getStartPage(page));
		viewData.put("endPage", getEndPage(page));
		viewData.put("totalPage", getTotalPageRecruitment(daoParam));
		viewData.put("page", page);
		
		return viewData;
	}

	@Override
	   public List<Map<String, Object>> selectAllRecruitment(Map<String, Object> params) {
	      List<Map<String, Object>> reqcruitmentList = dao.selectAllRecruitment(params);
	      for(Map<String, Object> m:reqcruitmentList) {
	    	 int idx = ((BigInteger) m.get("recruitment_idx")).intValue();
	    	 List<String> tmpPlatformList = dao.selectAllByrecruitment(idx);
	    	 m.put("platformList", tmpPlatformList);
	      }
		return reqcruitmentList;
	   }

	@Override
	public List<Map<String, Object>> getPlatform(Map<String, Object> map) {
		return dao.selectPlatformList(map);
	}

	
	
	
	
	
	
//	@Override
//	public int recruitmentSend(Map<String, Object> map, HttpServletRequest request) {
//
//		int user_idx;
//		int result = DEFAULT;
//		
//		Map<String, Object> argMap;
//		
//		map.put(STATUS, DEFAULT);
//		String uuid = UUID.randomUUID().toString().replace("-", "");
//		
//		map.put(ETC, uuid);
//		user_idx = Integer.parseInt(map.get(USER_IDX) + "");
//		
//		try {
//			
//			JavaMail.sendMail((String)map.get(USER_ID), "PortFolio", 
//					"<p><b>인증</b></p>"
//						+ "<div> 제목 : " + map.get("recruitment_title") + "</div>"
//						+ "<div> 이메일 : " + map.get("user_id") + "</div><br/>"
//						+ "<div>"
//						+ "<a href = '"
//						+ JavaMail.host_name
//						+ request.getContextPath() + "/user/emailCheck?etc="
//						+ uuid
//						+ "&user_idx="
//						+ user_idx
//						+ "&user_id="
//						+ (String)map.get(USER_ID)
//						+ "'>"
//						+ "인증하기"
//						+ "</a>"
//						+ "</div><br/>"
//					);
//		} catch (Exception e) {
//			e.printStackTrace();
//			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//			result = FAIL;
//			return result;
//		}
//		return result; 
//	}
}