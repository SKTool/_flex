package service.recruitment;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface RecruitmentService {
	public List<Map<String,Object>> recruitmentList(Map<String,Object> params);
	public Map<String,Object> recruitmentByIdx(int recruitment_idx);
	public boolean insertRecruitment(Map<String,Object> params);
	public Map<String,Object> readRecruitment(int recruitment_idx);
	public boolean updateRecruitment(Map<String,Object> params);
	public boolean deleteRecruitment(int recruitment_idx);
	public List<Map<String, Object>> getPlatform(Map<String, Object> map);
	
	public List<Map<String, Object>> selectAllRecruitment(Map<String, Object> params);

	public Map<String, Object> getViewDataRecruitment(Map<String, Object> params);
	
//	public int recruitmentSend(Map<String,Object> map,HttpServletRequest req);
	
}
