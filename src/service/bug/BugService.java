package service.bug;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.BugDao;

@Service
public class BugService implements IBugService {
	@Autowired
	private BugDao bugDao;

	private static final int NUM_OF_BOARD_PER_PAGE = 10;
	private static final int NUM_OF_NAVI_PAGE = 10;

	// bug_idx를 받아 하나의 게시글 조회
	@Override
	public Map<String, Object> getBugByNum(int bug_idx) {
		return bugDao.selectOneBug(bug_idx);
	}

	// 버그 게시글 등록
	@Override
	public int writeBug(Map<String, Object> params) {
		int result = 0;
		if (bugDao.insertBug(params) > 0) {
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	// 버그 게시글 수정
	@Override
	public int modifyBug(Map<String, Object> params) {
		System.out.println("Service Modify : " + params);
		int result = 0;
		if (bugDao.updateBug(params) > 0) {
			result = 1;
		} else {
			result = -1;
		}
		return result;
	}

	// 버그 게시글 삭제
	@Override
	public boolean deleteBug(int bug_idx) {
		if (bugDao.deleteBug(bug_idx) > 0) {
			return true;
		} else {
			return false;
		}
	}

	// 전체 버그 게시글 가져오기
	@Override
	public List<Map<String, Object>> getAllBugList(int project_idx) {
		return bugDao.selectAllBugByProjectIdx(project_idx);
	}

	// 페이징 처리하기 위한 함수
	private int getFirstRow(int page) {
		int result = ((page - 1) * NUM_OF_BOARD_PER_PAGE) + 1;
		return result;
	}
	private int getEndRow(int page) {
		int result = ((page - 1) + 1) * NUM_OF_BOARD_PER_PAGE;
		return result;
	}

	private int getStartPage(int page) {
		int result = (((page - 1) / NUM_OF_BOARD_PER_PAGE) * NUM_OF_BOARD_PER_PAGE) + 1;
		return result;
	}
	private int getEndPage(int page) {
		int result = getStartPage(page) + 9;
		return result;
	}
	private int getTotalPageBugList(Map<String, Object> params) {
		int totalCount = bugDao.selectTotalCountBugList(params);
		int totalPage = ((totalCount - 1) / NUM_OF_BOARD_PER_PAGE) + 1;
		return totalPage;
	}

	public Map<String, Object> getViewDataBugList(Map<String, Object> params) {
		// startPage, endPage, totalPage, boardList 반환
		int page = (int) params.get("page");
		Map<String, Object> daoParam = new HashMap<String, Object>();
		daoParam.put("firstRow", getFirstRow(page));
		params.put("firstRow", getFirstRow(page));
		daoParam.put("endRow", getEndRow(page));
		params.put("endRow", getEndRow(page));

		Map<String, Object> viewData = new HashMap<String, Object>();
		List<Map<String, Object>> bugList = selectAllBugList(daoParam); 
		viewData.put("adminBoardList", bugList);
		viewData.put("startPage", getStartPage(page));
		viewData.put("endPage", getEndPage(page));
		viewData.put("totalPage", getTotalPageBugList(daoParam));
		viewData.put("page", page);
		return viewData;
	}

	@Override
	public List<Map<String, Object>> selectAllBugList(Map<String, Object> params) {
		return bugDao.selectAllBugList(params);
	}

	@Override
	public int selectMyBugMemberIdx(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return bugDao.selectMyBugMemberIdx(param);
	}
}
