package service.bug;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import dao.BugDao;

@Service
public class BugReplyService implements IBugReplyService {
	@Autowired
	private BugDao bugReplyDao;
	
	@Override
	public boolean writeReply(Map<String, Object> params) {
		if(bugReplyDao.insertBugReply(params)>0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean modifyReply(Map<String, Object> params) {
		if(bugReplyDao.updateBugReply(params)>0) {
			return true;
		} else {
			return false;
		}	
	}

	@Override
	public boolean deleteReply(Map<String, Object> params) {
		System.out.println("서비스 : " + params);
		String strReplyIdx = (String)params.get("bug_reply_idx");
		int bug_reply_idx = Integer.parseInt(strReplyIdx);
		if(bugReplyDao.deleteBugReply(bug_reply_idx)>0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<Map<String, Object>> getReply(int bug_idx) {
		return bugReplyDao.selectByBugIdx(bug_idx);
	}

}
