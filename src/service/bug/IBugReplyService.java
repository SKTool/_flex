package service.bug;

import java.util.List;
import java.util.Map;

public interface IBugReplyService {
	//댓글 등록
	public boolean writeReply(Map<String,Object> params);
	//댓글 수정
	public boolean modifyReply(Map<String,Object> params);
	//댓글 삭제
	public boolean deleteReply(Map<String,Object> params);
	//댓글 목록
	public List<Map<String,Object>> getReply(int bug_idx);
}
