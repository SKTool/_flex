package service.bug;

import java.util.List;
import java.util.Map;

public interface IBugService {
	//버그 게시글 하나 가져오기 
	public Map<String,Object> getBugByNum(int bug_idx);
	//버그 게시글 작성
	public int writeBug(Map<String,Object> params);
	//버그 게시글 수정
	public int modifyBug(Map<String,Object> params);
	//버그 게시글 삭제
	public boolean deleteBug(int bug_idx);
	//전체 버그 게시글 가져오기
	public List<Map<String,Object>> getAllBugList(int project_idx);
	public List<Map<String, Object>> selectAllBugList(Map<String, Object> params);
	public int selectMyBugMemberIdx(Map<String,Object> param);
}
