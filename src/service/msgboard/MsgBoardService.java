package service.msgboard;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.View;

public interface MsgBoardService {
	
	//msgboard service
	public List<Map<String,Object>> msgBoardList(Map<String,Object> params);
	public Map<String,Object> getMsgBoardByIdx(int msgboard_idx);
	public Map<String,Object> readMsgBoard(int msgboard_idx);
	public boolean insertMsgBoard(Map<String,Object> params);
	public boolean updateMsgBoard(Map<String,Object> params);
	public boolean deleteMsgBoard(int msgboard_idx);
	
	//_reply service
	public boolean insertMsgBoardReply(Map<String,Object> params);
	public boolean updateMsgBoardReply(Map<String,Object> params);
	public boolean deleteMsgBoardReply(Map<String,Object> params);
	public List<Map<String,Object>> msgBoardReplyList(int msgboard_reply_idx);
	
	//////////////////////////////////////////////////////////////////////////
	public List<Map<String, Object>> selectAllMsgBoard(Map<String, Object> params);
	public Map<String, Object> getViewDataMsgBoard(Map<String, Object> params);
	
}
