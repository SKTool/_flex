package service.msgboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.MsgBoardDao;

@Service
public class MsgBoardServiceImp implements MsgBoardService {

	@Autowired
	private MsgBoardDao dao;
	private static final int NUM_OF_BOARD_PER_PAGE=10;

	@Override
	public List<Map<String, Object>> msgBoardList(Map<String, Object> params) {
		return dao.msgBoardList(params);
	}

	@Override
	public Map<String, Object> getMsgBoardByIdx(int msgboard_idx) {
		return dao.selectOneMsgBoard(msgboard_idx);
	}
	
	@Override
	public boolean insertMsgBoard(Map<String, Object> params) {
		try {
			int result = dao.insertMsgBoard(params);
			if (result > 0) {
//				int msgBoardIdx = ((BigInteger)params.get("msgboard_idx")).intValue();
				int msgBoardIdx = Integer.parseInt(params.get("msgboard_idx").toString());
				Map<String, Object> fileParam = new HashMap<String, Object>();
				fileParam.put("msgboard_idx", msgBoardIdx);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Map<String, Object> readMsgBoard(int msgboard_idx) {
		dao.plusReadCount(msgboard_idx);
		return dao.selectOneMsgBoard(msgboard_idx);
	}

	@Override
	public boolean updateMsgBoard(Map<String, Object> params) {
		if (dao.updateMsgBoard(params) > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean deleteMsgBoard(int msgboard_idx) {
		if (dao.deleteMsgBoard(msgboard_idx) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
		@Override
		public boolean insertMsgBoardReply(Map<String, Object> params) {
			if (dao.insertMsgBoardReply(params) > 0) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public boolean updateMsgBoardReply(Map<String, Object> params) {
			if(dao.updateMsgBoardReply(params)>0) {
				return true;
			}else {
				return false;
			}			
		}

		@Override
		public boolean deleteMsgBoardReply(Map<String,Object> params) {
			String msgboard_idx = (String)params.get("msgboard_reply_idx");
			int msg = Integer.parseInt(msgboard_idx);
			if(dao.deleteMsgBoardReply(msg)>0) {
				return true;
			}else {
				return false;
			}
		}

		@Override
		public List<Map<String, Object>> msgBoardReplyList(int msgboard_idx) {
			return dao.selectByMsgBoardIdx(msgboard_idx);
		}
	
	@Override
	public Map<String, Object> getViewDataMsgBoard(Map<String, Object> params) {
		int page = (int)params.get("page");
		Map<String, Object> daoParam = new HashMap<String,Object>();
		daoParam.put("firstRow", getFirstRow(page));
		params.put("firstRow", getFirstRow(page));
		daoParam.put("endRow", getEndRow(page));
		params.put("endRow", getEndRow(page));
		
		Map<String, Object> viewData = new HashMap<String,Object>();
		List<Map<String, Object>> msgboardList = selectAllMsgBoard(daoParam);
		viewData.put("msgboardList", msgboardList);
		viewData.put("startPage", getStartPage(page));
		viewData.put("endPage", getEndPage(page));
		viewData.put("totalPage", getTotalPageMsgBoard(daoParam));
		viewData.put("page", page);
		
		return viewData;
	}
	
		private int getFirstRow(int page) {
			int result  = ((page-1)*NUM_OF_BOARD_PER_PAGE);
			return result;
		}
		private int getEndRow(int page) {
			int result =  ((page-1) + 1)*NUM_OF_BOARD_PER_PAGE ;
			
			return result;
		}
		private int getStartPage(int page) {
			int result  = (((page-1)/NUM_OF_BOARD_PER_PAGE)*NUM_OF_BOARD_PER_PAGE)+1;
			
			return result;
		}
		private int getEndPage(int page) {
			int result = getStartPage(page)+9;
			return result;
		}
		
		private int getTotalPageMsgBoard(Map<String, Object> params) {
			int totalCount = dao.selectTotalCountMsgBoard(params);
			int totalPage = ((totalCount-1)/NUM_OF_BOARD_PER_PAGE)+1;
			return totalPage;
		}

		@Override
		   public List<Map<String, Object>> selectAllMsgBoard(Map<String, Object> params) {
		      return dao.selectAllMsgBoard(params);
		   }
}
