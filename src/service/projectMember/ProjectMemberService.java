package service.projectMember;

import static common.Common.User.FAIL;
import static common.Common.User.USER_ID;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import common.Common;
import dao.ProjectDao;
import dao.UserDao;
import service.UserService;
import util.JavaMail;

@Service
public class ProjectMemberService implements IProjectMemberService {
	@Autowired
	private ProjectDao projectDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserService userService;

	@Override
	public boolean registerProjectMember(Map<String, Object> params) {
		System.out.println("resterMember");
		boolean result = false;
		String user_id = (String)params.get("user_id");
		Map<String, Object> argMap = new HashMap<>();
		argMap.put("user_id", user_id);
		Map<String, Object> user = userService.checkUser(argMap);
//		유저가 가입자일때
		if(user != null) {
			BigInteger user_idx_temp = (BigInteger)user.get("user_idx");
			int user_idx = user_idx_temp.intValue();
			argMap.put("user_idx", user_idx);
			argMap.put("project_idx", params.get("project_idx"));
			
			Map<String, Object> user_member = projectDao.selectProjectMemberByUserIdx(argMap);
//			유저가 멤버로 등록되어있지 않을때
			if(user_member == null) {
				try {
					JavaMail.sendMail((String)params.get("user_id"), "초대", 
							"<p><b>프로젝트 맴버로 초대합니다</b></p>"
									+ "<div> 초대한 사람 이메일  : " + params.get("fromId") + "</div>"
									+ "<div>"
									+ "<a href = '"
									+ JavaMail.host_name
									+ "/projectMember/emailCheck?project_idx="
									+ params.get("project_idx")
									+ "&project_auth_idx="
									+ params.get("project_auth_idx")
									+ "&user_id="
									+ (String)params.get("user_id")
									+ "&user_idx="
									+ user_idx
									+ "'>"
									+ "수락하기"
									+ "</a>"
									+ "</div><br/>"
							);
					result = true;
				} catch (Exception e) {
					e.printStackTrace();
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				} 
			}
		}
		
		return result;
	}

	
//		
//		System.out.println("이건가:" + params);
//		String userId = (String)params.get("user_id");
//		
//		Map<String,Object> projectMember = new HashMap<String,Object>();
//		projectMember.put("user_id", userId);
//		params.put("user_idx", userDao.selectUser(projectMember).get("user_idx"));
//		System.out.println("param : " + params);
//		if(userDao.selectUser(projectMember).get("user_id").equals(userId)) {
//			if(projectDao.insertProjectMember(params)>0) {
//				System.out.println("들어갔는데?");
//				result = true;
//			} else {
//				result = false;
//			}
//		}

	@Override
	public List<Map<String, Object>> memberList(int project_idx) {
		return projectDao.projectMemberList(project_idx);
	}


	@Override
	public int deleteProjectMember(int project_member_idx) {
		return projectDao.deleteProjectMember(project_member_idx);
	}


	@Override
	public int modifyProjectMemberAuth(Map<String, Object> param) {
		System.out.println("서비스에서 넘어오는 param : " + param);
		
		
		param.put("project_auth_idx", 1);
		System.out.println("바뀌는 param : " + param);
		int update = projectDao.updateProjectMemberAuth(param);
		if(update > 0) {
			System.out.println("in");
			Map<String, Object> argMap = new HashMap<>();
			argMap.put("project_member_idx", param.get("my_project_member_idx"));
			argMap.put("project_auth_idx", 2);
			System.out.println("argMap : " + argMap);
			int update2 = projectDao.updateProjectMemberAuth(argMap);
			
			
			if(update2 > 0) {
				System.out.println("in2");
				return 1;
			} else {
				return -1;
			}
		}  else {
			return -1;
		}
	}


	@Override
	public int insertMember(Map<String, Object> map) {
		return projectDao.inviteMember(map);
	}
}
