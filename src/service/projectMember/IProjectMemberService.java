package service.projectMember;

import java.util.List;
import java.util.Map;

public interface IProjectMemberService {
	//프로젝트에 멤버 추가
	public boolean registerProjectMember(Map<String,Object> params);
	//프로젝트에 추가된 멤버 리스트 띄우기
	public List<Map<String,Object>> memberList(int project_idx);
	//프로젝트에 멤버 삭제
	public int deleteProjectMember(int project_member_idx);
	//프로젝트 권한 변경
	public int modifyProjectMemberAuth(Map<String,Object> param);
	
	public int insertMember(Map<String, Object> map);
}
