package service.schedule;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.ScheduleDao;

@Service("scheduleService")
public class ScheduleServiceImp implements ScheduleService {

	@Autowired
	private ScheduleDao scheduleDao;
	
	@Override
	public boolean insertSchedule(Map<String, Object> param) {
		if(scheduleDao.insertSchedule(param)>0) {
			System.out.println("insertSchedule 진입");
			return true;
		}
		return false;
	}

	@Override
	public boolean updateSchedule(Map<String, Object> param) {
		System.out.println("param : " + param);
		if(scheduleDao.updateScheduleById(param)>0) {
			System.out.println("updateSchedule 진입");
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteSchedule(String id) {
		if(scheduleDao.deleteSchedule(id)>0) {
			System.out.println("deleteSchedule");
			return true;
		} 
		
		return false;
	}

	@Override
	public Map<String, Object> selectOneSchedule(int schedule_idx) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> selectOneScheduleById(String id) {	
		return scheduleDao.selectOneScheduleById(id);
	}
	public List<Map<String, Object>>  selectOneScheduleByprojectIdx(int project_idx){
		return scheduleDao.selectOneScheduleByprojectIdx(project_idx);
	};

	
	@Override
	public List<Map<String, Object>> selectAllSchedule() {
		return scheduleDao.selectAllSchedule();
	}

}
