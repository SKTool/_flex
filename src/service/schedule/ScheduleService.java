package service.schedule;

import java.util.List;
import java.util.Map;

public interface ScheduleService {
	public boolean insertSchedule(Map<String, Object> param);
	public boolean updateSchedule(Map<String, Object> param);
	public boolean deleteSchedule(String id);
	public Map<String, Object> selectOneSchedule(int schedule_idx);
	public List<Map<String, Object>>  selectOneScheduleByprojectIdx(int project_idx);
	public Map<String, Object> selectOneScheduleById(String id);
	public List<Map<String, Object>> selectAllSchedule();	
}
