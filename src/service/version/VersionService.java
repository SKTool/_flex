package service.version;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

public interface VersionService {

	void insertGit(Map<String, Object> map);
	boolean hasGitInProjectMember(int projectMemberIdx);
	public List<String> getBranches(int projectMemberIdx);
	List<Map<String, Object>> getCommitsByBranch(int projectMemberIdx, String branch);
	
	public List<String> getBranchesForSearch(int projectMemberIdx, String branch);
	public boolean deleteGit(Map<String, Object> map);

	
// issue	
	
	public List<Map<String, Object>> getIssueOpen(int projectMemberIdx, String labels); 
	public List<Map<String, Object>> getIssueClosed(int projectMemberIdx, String labels); 
	
// issue comment
	
	public List<Map<String, Object>> getIssueComment(int projectMemberIdx, int issueNumber); 

// issue Labels 
	
	public List<Map<String, Object>> getLabelsIssue(int projectMemberIdx);

// send comment to issue

	public boolean sendCommentIssue(int number, String comment, int projectMemberIdx)   throws Exception; 
	
	
}
