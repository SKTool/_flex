package service.version;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Constants.Git;
import dao.GitDao;
import dao.GitRestDao;

@Component
public class GitRestDaoImpl implements GitRestDao {
	private static Logger logger = Logger.getLogger("GitRestDaoImpl.java");
	
	@Autowired
	GitDao gitDao;

	private ObjectMapper bodyMapper = new ObjectMapper();

	private String basedURL;
	private Map<String, Object> map;
	private String id;
	private String token;
	private String user;
	private String repo;
	
	@Override
	public List<Map<String, Object>> selectAllBranchesByProjectMemberIdx(int projectMemberidx) {
		System.out.println("GitRestDaoImpl.selectAllBranchesByProjectMemberIdx");
		String AuthorationToken = connectGitHub(projectMemberidx);
		List<Map<String, Object>> bodyList = null;
		
		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches";
		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine = null;

			while( (inputLine = br.readLine()) != null ) {
//				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}

	@Override
	public List<Map<String, Object>> selectAllCommitsByBranch(int projectMemberIdx, String branch) {
		System.out.println("GitRestDaoImpl.selectAllCommitsByBranch");
		String AuthorationToken = connectGitHub(projectMemberIdx);
		List<Map<String, Object>> bodyList = null;
		
		
//		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches/"+branch+"?since=2017-02-10T00:19:02Z";
		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/commits?sha="+branch;

		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine;

			while( (inputLine = br.readLine()) != null ) {
				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}

	private String connectGitHub(int projectMemberidx) {
		map = gitDao.selectOneGitByProjectMemberIdx(projectMemberidx);

		id = map.get(Git.GIT_ID).toString();
		token = map.get(Git.GIT_TOKEN).toString();
		user = map.get(Git.GIT_USER).toString();
		repo = map.get(Git.GIT_REPO).toString();


		Encoder encoder = Base64.getEncoder();
//		Decoder decoder = Base64.getDecoder();
		
		String AuthorationToken = new String( encoder.encode((id+":"+token).getBytes()) );
		return AuthorationToken;
	}

// Issue	
	
	

	@Override
	public List<Map<String, Object>> selectAllIssueOpen(int projectMemberIdx, String labels) {
		System.out.println("GitRestDaoImpl.selectAllIssueOpen");
		String AuthorationToken = connectGitHub(projectMemberIdx);
		List<Map<String, Object>> bodyList = null;
		
//		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches/"+branch+"?since=2017-02-10T00:19:02Z";
		if (labels != null) {
			basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues?state=open&labels="+labels;
			
		} else {
			basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues?state=open";
			
		}

		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine;

			while( (inputLine = br.readLine()) != null ) {
				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}

	@Override
	public List<Map<String, Object>> selectAllIssueClosed(int projectMemberIdx, String labels) {
		System.out.println("GitRestDaoImpl.selectAllIssueClosed");
		String AuthorationToken = connectGitHub(projectMemberIdx);
		List<Map<String, Object>> bodyList = null;
		
//		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches/"+branch+"?since=2017-02-10T00:19:02Z";
		if ( labels != null) {
			basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues?state=closed&labels="+labels;
		} else {
			basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues?state=closed";
		}


		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine;

			while( (inputLine = br.readLine()) != null ) {
				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}

// Issue Comment
	@Override
	public List<Map<String, Object>> selectAllIssueComment(int projectMemberIdx, int issueNumber) {
		System.out.println("GitRestDaoImpl.selectAllIssueComment");
		String AuthorationToken = connectGitHub(projectMemberIdx);
		List<Map<String, Object>> bodyList = null;
		
//		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches/"+branch+"?since=2017-02-10T00:19:02Z";
		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues/"+issueNumber+"/comments";

		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine;

			while( (inputLine = br.readLine()) != null ) {
				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}


// Issue Labels

	@Override
	public List<Map<String, Object>> selectAllLabelsIssue(int projectMemberIdx) {
		System.out.println("GitRestDaoImpl.selectAllIssueComment");
		String AuthorationToken = connectGitHub(projectMemberIdx);
		List<Map<String, Object>> bodyList = null;
		
//		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/branches/"+branch+"?since=2017-02-10T00:19:02Z";
		basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/labels";
		try {
			URL url = new URL(basedURL);
			
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);

			int responseCode = connection.getResponseCode();
			
			BufferedReader br = null;
			if ( responseCode == 200 ) {
				logger.info("github 연결에 성공");
				br = new BufferedReader(
						new InputStreamReader(connection.getInputStream()));
			}
			
			StringBuffer sb = new StringBuffer();
			String inputLine;

			while( (inputLine = br.readLine()) != null ) {
				logger.info("bodyJson line 내용 : " + inputLine);
				sb.append(inputLine);
			}

			br.close();
			
			String bodyJson = sb.toString();
			logger.info("bodyJson 내용 : " + bodyJson);
			
			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return bodyList;
	}
	
	
	
	
	
	
	
	

}
