package service.version;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import java.util.Base64.Encoder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.Constants.Git;
import dao.GitDao;
import dao.GitRestDao;

@Service
public class VersionServiceImpl implements VersionService {

	@Autowired
	GitDao gitDao;

	@Autowired
	GitRestDao gitRestDao;

	@Override
	public void insertGit(Map<String, Object> map) {
		try {
			gitDao.insertGit(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Override
	public boolean deleteGit(Map<String, Object> map) {
		map.put("git_id", gitDao.selectOneGitByProjectMemberIdx((int)map.get("project_member_idx")).get("git_id"));
		boolean isGit = false;
		if (gitDao.deleteOneGitBygitIdAndProjectMemberIdx(map) > 0) {
			isGit = true;
		}
		return isGit;
	}

	@Override
	public boolean hasGitInProjectMember(int projectMemberIdx) {
		boolean isGit = false;
		if ( gitDao.selectOneGitByProjectMemberIdx(projectMemberIdx) != null ) {
			isGit = true;
		}
		return isGit;
	}
	
	@Override
	public List<String> getBranches(int projectMemberIdx) {
		List<Map<String, Object>> dataList = gitRestDao.selectAllBranchesByProjectMemberIdx(projectMemberIdx);
		List<String> names = new ArrayList<String>();
		
		Iterator<Map<String, Object>> iter = dataList.iterator();
		while ( iter.hasNext() ) {
			names.add(iter.next().get("name").toString());
		}

		return names;
	}

	@Override
	public List<Map<String, Object>> getCommitsByBranch(int projectMemberIdx, String branch) {
		List<Map<String, Object>> commits = gitRestDao.selectAllCommitsByBranch(projectMemberIdx, branch);
//		System.out.println("-Map--------------------------------------------------------");
//		System.out.println(commits);
//		System.out.println("END-Map--------------------------------------------------------");

		List<Map<String,Object>> commitList = new ArrayList<Map<String,Object>>();
		Iterator<Map<String, Object>> iter = commits.iterator();
		
		
		Map<String, Object> git =  gitDao.selectOneGitByProjectMemberIdx(projectMemberIdx);
		String user = git.get(Git.GIT_USER).toString();
		String repo = git.get(Git.GIT_REPO).toString();
		
		
		String previousDate = "";
		String date = "";
		Map<String, Object> commit;
		while ( iter.hasNext() ) {
			commit = new HashMap<String, Object>();
			Map<String, Object> tempIter = iter.next();

			commit.put("name", ((Map)((Map)((Map)tempIter.get("commit"))).get("committer")).get("name"));

			date = ((Map)((Map)((Map)tempIter.get("commit"))).get("committer")).get("date").toString();
			if (!previousDate.equals(date.substring(0, 10))) {
				commit.put("date", date);
				previousDate = date.substring(0, 10);
			} else {
				commit.put("date", null);
			}
			commit.put("time", date);

			if ( ((Map)tempIter.get("commit")).get("message").toString().length() >= 40 ) {
				commit.put("title", ((Map)tempIter.get("commit")).get("message").toString().substring(0, 40) + " …");
			} else {
				commit.put("title", ((Map)tempIter.get("commit")).get("message").toString());
			}

			commit.put("message", ((Map)tempIter.get("commit")).get("message"));

			commit.put("sha", tempIter.get("sha").toString());
			commit.put("short_sha", tempIter.get("sha").toString().substring(0, 7));

			commit.put("commit_url", "https://github.com/"+user+"/"+repo+"/commit/"+tempIter.get("sha").toString());
			commit.put("tree_url", "https://github.com/"+user+"/"+repo+"/tree/"+tempIter.get("sha").toString());
			commitList.add(commit);

		}
		return commitList;
	}

	@Override
	public List<String> getBranchesForSearch(int projectMemberIdx, String branch) {
		List<Map<String, Object>> dataList = gitRestDao.selectAllBranchesByProjectMemberIdx(projectMemberIdx);
		List<String> names = new ArrayList<String>();
		
		Iterator<Map<String, Object>> iter = dataList.iterator();
		while ( iter.hasNext() ) {
			names.add(iter.next().get("name").toString());
		}
		
		List<String> selectedNames = new ArrayList<String>();
		Iterator<String> listiter = names.iterator();
		while ( listiter.hasNext() ) {
			String sN = listiter.next();
			if ( sN.contains(branch) ) {
				selectedNames.add(sN);
			}
		}
		return selectedNames;
	}


	@Override
	public List<Map<String, Object>> getIssueOpen(int projectMemberIdx, String labels) {
		List<Map<String, Object>> issuesOpen = gitRestDao.selectAllIssueOpen(projectMemberIdx,  labels);
		
		List<Map<String, Object>> issues = parseIssues(issuesOpen);

		return issues;
	}


	@Override
	public List<Map<String, Object>> getIssueClosed(int projectMemberIdx, String labels) {
		List<Map<String, Object>> issuesClosed = gitRestDao.selectAllIssueClosed(projectMemberIdx,  labels);

		List<Map<String, Object>> issues = parseIssues(issuesClosed);

		return issues;
	}


	private List<Map<String, Object>> parseIssues(List<Map<String, Object>> issuesClosed) {
		Iterator<Map<String, Object>> iter = issuesClosed.iterator();
		List<Map<String, Object>> issues = new ArrayList<Map<String, Object>>();
		Map<String, Object> issue = new HashMap<String, Object>();
		Map<String, Object> issueMap = null;

		while ( iter.hasNext() ) {
			issueMap = new HashMap<String, Object>();
			issue = iter.next();
			issueMap.put("title",issue.get("title"));
			issueMap.put("login",((Map)issue.get("user")).get("login"));
			issueMap.put("number",issue.get("number"));
			issueMap.put("body",issue.get("body"));
			issueMap.put("labels",issue.get("labels"));
			issueMap.put("state",issue.get("state"));
			issueMap.put("comments",issue.get("comments"));
			issueMap.put("updated_at",issue.get("updated_at"));
			issues.add(issueMap);
		}
		return issues;
	}


	@Override
	public List<Map<String, Object>> getIssueComment(int projectMemberIdx, int issueNumber) {
		List<Map<String, Object>> issuesComment = gitRestDao.selectAllIssueComment(projectMemberIdx, issueNumber);
		
		Iterator<Map<String, Object>> iter = issuesComment.iterator();
		List<Map<String, Object>> comments = new ArrayList<Map<String, Object>>();
		Map<String, Object> comment = new HashMap<String, Object>();
		Map<String, Object> commentMap = null;
		
		while ( iter.hasNext() ) {
			commentMap = new HashMap<String, Object>();
			comment = iter.next();

			System.out.println("issue");
			System.out.println(comment);

			commentMap.put("login",((Map)comment.get("user")).get("login"));
			commentMap.put("body",comment.get("body"));
			commentMap.put("updated_at",comment.get("updated_at"));
			comments.add(commentMap);
		}

		return comments;
	}


	@Override
	public List<Map<String, Object>> getLabelsIssue(int projectMemberIdx) {
		List<Map<String, Object>> issuesLabels;
		try {
			issuesLabels = gitRestDao.selectAllLabelsIssue(projectMemberIdx);
		} catch (Exception e) {
			issuesLabels = null;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		Iterator<Map<String, Object>> iter = issuesLabels.iterator();
//		List<Map<String, Object>> labels = new ArrayList<Map<String, Object>>();
//		Map<String, Object> label = new HashMap<String, Object>();
//		Map<String, Object> labelMap = null;
		
//		while ( iter.hasNext() ) {
//			labelMap = new HashMap<String, Object>();
//			label = iter.next();
//
//			labelMap.put("login",((Map)label.get("user")).get("login"));
//			labelMap.put("body",label.get("body"));
//			labelMap.put("updated_at",label.get("updated_at"));
//			labels.add(labelMap);
//		}
		
		System.out.println(issuesLabels);
		
		return issuesLabels;
	}


	//send
	@Override
	public boolean sendCommentIssue(int number, String comment, int projectMemberIdx)  throws Exception{
		
		boolean isSuccess = false;
		Map<String, Object> map = gitDao.selectOneGitByProjectMemberIdx(projectMemberIdx);

		String id = map.get(Git.GIT_ID).toString();
		String token = map.get(Git.GIT_TOKEN).toString();
		String user = map.get(Git.GIT_USER).toString();
		String repo = map.get(Git.GIT_REPO).toString();

		Encoder encoder = Base64.getEncoder();
//		Decoder decoder = Base64.getDecoder();
		
		String AuthorationToken = new String( encoder.encode((id+":"+token).getBytes()) );
	
		
		System.out.println(AuthorationToken);
		System.out.println(user);
		System.out.println(repo);
		System.out.println(number);
		String basedURL = "https://api.github.com/repos/"+user+"/"+repo+"/issues/"+number+"/comments";
		System.out.println("==================================================================");
		System.out.println(basedURL);
//		String param = "{\"body\":\""+comment+"\"}";
		  
		URL url = new URL(basedURL);
//		String commentJson = "\""+comment+"\"";
		

		
		
//		basedURL
		URL obj = new URL(basedURL);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Authorization", "Basic "+AuthorationToken);
		con.setRequestProperty("Content-Type","application/json");
		

//		String postJsonData = "{\"body\":\"bbbbbbbbaToTOTOTO\"}";
//		String postJsonData = "{\"body\":\""+comment+"\"}";

		Map<String, Object> mapTemp = new HashMap<String, Object>();
		mapTemp.put("body", comment);

		ObjectMapper mapper = new ObjectMapper();
		String postJsonData = mapper.writeValueAsString(mapTemp);

		
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(postJsonData);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		System.out.println("nSending 'POST' request to URL : " + url);
		System.out.println("Post Data : " + postJsonData);
		System.out.println("Response Code : " + responseCode);
		

		if ( responseCode == 201 ) {
			isSuccess = true;
		}
		BufferedReader in = new BufferedReader(
			  new InputStreamReader(con.getInputStream()));
		String output;
		StringBuffer response = new StringBuffer();

		while ((output = in.readLine()) != null) {
			System.out.println("false");
			response.append(output);
		}

		in.close();

		//printing result from response
		System.out.println(response.toString());
//
//		try {
//			URL url = new URL(basedURL);
//			
//			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
//			connection.setRequestMethod("POST");
//			connection.setRequestProperty("Authorization", "Basic " + AuthorationToken);
//
//			int responseCode = connection.getResponseCode();
//			
//			BufferedReader br = null;
//			if ( responseCode == 200 ) {
//				isSuccess = true;
//				br = new BufferedReader(
//						new InputStreamReader(connection.getInputStream()));
//			}
//			StringBuffer sb = new StringBuffer();
//			String inputLine;
//
//			while( (inputLine = br.readLine()) != null ) {
//				sb.append(inputLine);
//			}
//
//			br.close();
//			
//			String bodyJson = sb.toString();
//			
//			bodyList = bodyMapper.readValue(bodyJson, new TypeReference<List<Map<String, Object>>>(){});
//
//		} catch (MalformedURLException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return bodyList;
		
		 return isSuccess; 
		  
		
	}

}
