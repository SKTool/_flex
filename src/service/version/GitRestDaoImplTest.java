package service.version;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import dao.GitRestDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value= {"file:WebContent/WEB-INF/spring/root-context.xml", "file:WebContent/WEB-INF/spring/servlet-context.xml"})
@WebAppConfiguration
public class GitRestDaoImplTest {
	
	@Autowired
	GitRestDao gitRestDao;
	
	@Ignore
	@Test
	public void test() {
		assertNotNull(gitRestDao.selectAllBranchesByProjectMemberIdx(1)) ;
		assertNotNull(gitRestDao.selectAllCommitsByBranch(1, "PLMDebug")) ;
		
	}
	
	@Ignore
	@Test
	public void getIssues() {
//		assertNotNull(gitRestDao.selectAllIssueOpen(1));
//		assertNotNull(gitRestDao.selectAllIssueClosed(1));
		
	}
	
	@Test
	public void getLabels() throws Exception {
		assertNotNull(gitRestDao.selectAllLabelsIssue(1));
		
	}
			

}
