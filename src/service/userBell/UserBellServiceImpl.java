package service.userBell;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import common.Constants.Project;
import common.Constants.ProjectMember;
import common.Constants.UserBell;
import dao.ProjectDao;
import dao.UserBellDao;
import service.userBell.gitEvent.GitEventIssue;
import service.userBell.gitEvent.GitEventPullRequest;
import service.userBell.gitEvent.GitEventPush;
import util.JsonParseUtil;

@Service
public class UserBellServiceImpl implements UserBellService {
	Logger loggerBody = Logger.getLogger("log4jBody");

	@Autowired
	UserBellDao userBellDao;
	
	@Autowired
	ProjectDao projectDao;
	

	JsonParseUtil parseUtil = new JsonParseUtil();


	@Override
	public String alertEventHandler(Map<String, Object> headers, String body, Map<String ,Object> map) {

		loggerBody.info("alertEventHandler");

		Map<String, Object> result = new HashMap<String, Object>(); 
		String eventType = headers.get("x-github-event").toString();

		ResponseEvent gitEvent = null;
		

		if (eventType.equals("push")) {
			gitEvent = new GitEventPush();
		} else if (eventType.equals("issues")) {
			gitEvent = new GitEventIssue();
		}  else if (eventType.equals("pull_request")) {
			gitEvent = new GitEventPullRequest();
		}

		String jsonString = gitEvent.writeGitEvent(body);

		// user_bell 데이터 입력
//		Map<String, Object> userBell = new HashMap<String, Object>();
//		userBell.put(UserBell.USER_BELL_CONTENT, jsonString);
		map.put(UserBell.USER_BELL_CONTENT, jsonString);

		// TODO 값 수정 
//		userBell.put(UserBell.USER_IDX, 1);
//		userBell.put(UserBell.USER_IDX, map.get(arg0));
		// TODO 값 수정 
//		userBell.put(UserBell.PROJECT_MEMBER_IDX, 2);
		map.put(UserBell.USER_BELL_TYPE, 1);
		userBellDao.insertUserBell(map);
		

		return jsonString;
	}



	@Override
	public int addAlerts(Map<String, Object> map) {
		return 0;
	}

	@Override
	public int removeAlerts(int userBellIdx) {
		return userBellDao.deleteOneUserBell(userBellIdx);
	}


	@Override
	public List<Map<String, Object>> loadAlerts(Map<String, Object> map) {
//		map.put(UserBell.USER_BELL_TYPE, 1);

		map.put(UserBell.STATUS, 1);
		List<Map<String, Object>> userBelles
			= userBellDao.selectAllUserBellByUserIdx(map);

		return userBelles;
	}
	
	@Override
	public String loadAlertsConvertedJson(Map<String, Object> map) {
		String json = new String();
		try {
			json = parseUtil.convertToString(loadAlerts(map));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public List<Map<String, Object>> loadDeletedAlerts(Map<String, Object> map) {
		map.put(UserBell.STATUS, 0);
		List<Map<String, Object>> userBelles
			= userBellDao.selectAllUserBellByUserIdx(map);

		return userBelles;
	}

	@Override 
	public int updateStatusToZero_InUserBell(Map<String, Object> map) {
		map.put(UserBell.STATUS, 0);
		return userBellDao.updateStatusInUserBell(map);
		
	}


	@Override 
	public int updateStatusToNotZero_InUserBell(Map<String, Object> map) {
		map.put(UserBell.STATUS, 1);
		return userBellDao.updateStatusInUserBell(map);
		
	}

	@Override
	public boolean isActiveInProjectMember(String projectMemberIdx) {
		boolean isActive = false;
		if ( (int)(projectDao.selectOneProjectMember(Integer.parseInt(projectMemberIdx)).get(ProjectMember.BELL_ACTIVE)) == 1 ) {
			isActive = true;
		}
		return isActive;
	}





	
}
