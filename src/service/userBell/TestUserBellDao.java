package service.userBell;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import static common.Constants.UserBell.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dao.UserBellDao;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:WebContent/WEB-INF/spring/root-context.xml")
public class TestUserBellDao {

	@Autowired
	UserBellDao userBellDao;
	private Map<String, Object> map;

	@Before
	public void setup() {
		userBellDao.deleteAllUserBell();
		map = new HashMap<String, Object>();
		map.put(USER_BELL_IDX, 1);
		map.put(USER_BELL_CONTENT, "content");
		map.put(USER_BELL_OCC_DATE, "0214");
		map.put(IN_DATE, "0214");
		map.put(M_DATE, "0215");
		map.put(STATUS, 0);
		map.put(USER_IDX, 1);
		map.put(PROJECT_MEMBER_IDX, 1);
	}

	@After
	public void end() {
		userBellDao.deleteAllUserBell();
	}

	@Test
	public void testInsertUserBell() {
		assertEquals(1, userBellDao.insertUserBell(map));
	}

	@Test
	public void testDeleteOneUserBell() {
		userBellDao.insertUserBell(map);
		assertEquals(1, userBellDao.deleteOneUserBell(1));
	}
	
	@Test
	public void testUpdateUserBell() {

		Map<String, Object> changedMap = new HashMap<String, Object>();
		changedMap.put(USER_BELL_IDX, 1);
		changedMap.put(USER_BELL_CONTENT, "content");
		changedMap.put(USER_BELL_OCC_DATE, "0214");
		changedMap.put(IN_DATE, "0214");
		changedMap.put(M_DATE, "0215");
		changedMap.put(STATUS, 0);
		changedMap.put(USER_IDX, 1);

		userBellDao.insertUserBell(map);
		assertEquals(1, userBellDao.updateUserBell(changedMap));
	}

	@Test
	public void testSeleteOneUserBell() {
		userBellDao.insertUserBell(map);
		assertNotNull(userBellDao.selectOneUserBell(1));
	}

	@Test
	public void testSeleteAllUserBell() {
		userBellDao.insertUserBell(map);
		map.put(USER_BELL_IDX, 2);
		map.put(STATUS, 1);
		userBellDao.insertUserBell(map);
		map.put(USER_BELL_IDX, 4);
		map.put(STATUS, 0);
		userBellDao.insertUserBell(map);
		map.put(USER_BELL_IDX, 5);
		map.put(STATUS, 1);
		userBellDao.insertUserBell(map);
		System.out.println(userBellDao.selectAllUserBell());
		assertNotNull(userBellDao.selectAllUserBell());
	}


}
