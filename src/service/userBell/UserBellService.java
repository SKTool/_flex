package service.userBell;

import java.util.List;
import java.util.Map;

public interface UserBellService {
	
	// 작성중 ( 삭제 예정  )
	public int addAlerts(Map<String, Object> map);
	
	public int removeAlerts(int userBellIdx);
//	public List<Map<String, Object>> loadAlerts(int userIdx);
	public List<Map<String, Object>> loadAlerts(Map<String, Object> map);
	public String loadAlertsConvertedJson(Map<String, Object> map);
	public List<Map<String, Object>> loadDeletedAlerts(Map<String, Object> map);


	public String alertEventHandler(Map<String, Object> headers, String body, Map<String ,Object> map);
//	public String alertEventHandler(Map<String, Object> headers, String body); 
	
	
	
	public int updateStatusToZero_InUserBell(Map<String, Object> map);
	public int updateStatusToNotZero_InUserBell(Map<String, Object> map);
	

	public boolean isActiveInProjectMember(String projectMemberIdx);
}
