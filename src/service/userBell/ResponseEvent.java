package service.userBell;

import java.io.IOException;
import java.util.Map;

import service.userBell.gitEvent.GitEventIssue;
import service.userBell.gitEvent.GitEventPullRequest;
import service.userBell.gitEvent.GitEventPush;
import util.JsonParseUtil;

public abstract class ResponseEvent {

	public static ResponseEvent GitEventFactory(String body) {
		JsonParseUtil userBellUtil = new JsonParseUtil();
		ResponseEvent ge = null;

		try {
			if (userBellUtil.parseJsonWithKeyword(body, "commits") != null ) {
				ge = new GitEventPush();
			} else if (userBellUtil.parseJsonWithKeyword(body, "issue") != null) {
				ge = new GitEventIssue();
			} else if (userBellUtil.parseJsonWithKeyword(body, "pull_request") != null) {
				ge = new GitEventPullRequest();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ge;
	}

	abstract public String writeGitEvent(String body);

}
