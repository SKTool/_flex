package service.userBell.gitEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import service.userBell.ResponseEvent;
import util.JsonParseUtil;

public class GitEventIssue extends ResponseEvent {
	JsonParseUtil parseUtil;
	private Map<String, Object> jsonMap;

	public GitEventIssue() {
		parseUtil = new JsonParseUtil();
		jsonMap = new HashMap<String, Object>();

	}

	@Override
	public String writeGitEvent(String body) {
		 Map<String, Object> jsonMap = new HashMap<String, Object>();
		 Map<String, Object> returnMap = new HashMap<String, Object>();

		try {
			jsonMap = (Map)parseUtil.convertToObject(parseUtil.parseJsonWithKeyword(body, "issue"));

			
			returnMap.put("eventType","issue");
			returnMap.put("title", jsonMap.get("title"));
			returnMap.put("message", jsonMap.get("body"));
			returnMap.put("timestamp", jsonMap.get("created_at"));
			returnMap.put("email", ((Map)jsonMap.get("user")).get("login"));
			
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String json = new String();
		try {
			json = parseUtil.convertToString(returnMap);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json; 
	}

}
