package service.userBell.gitEvent;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import service.userBell.ResponseEvent;
import util.JsonParseUtil;

public class GitEventPush extends ResponseEvent {
	JsonParseUtil parseUtil;
	
	public GitEventPush() {
		parseUtil = new JsonParseUtil();
	}

	@Override
	public String writeGitEvent(String body) {
		 Map<String, Object> jsonMap = new HashMap<String, Object>();
		 Map<String, Object> returnMap = new HashMap<String, Object>();

		try {
			jsonMap = (Map)((List)parseUtil.convertToObject(parseUtil.parseJsonWithKeyword(body, "commits") )).get(0);
			
			String title = "";

			if ( jsonMap.get("message").toString().length() > 14 ) {
				title = jsonMap.get("message").toString().substring(0, 10);
				
			}

			returnMap.put( "eventType", "push" );
			returnMap.put( "title", title );
			returnMap.put( "message", jsonMap.get("message") );
			returnMap.put( "timestamp", jsonMap.get("timestamp") );
			returnMap.put("email", ((Map)jsonMap.get("committer")).get("name") );
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String json = new String();
		try {
			json = parseUtil.convertToString(returnMap);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json; 
	}

}
