package service.adminPage;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.AdminDao;
import dao.MsgBoardDao;
import dao.ProjectDao;
import dao.QnaDao;
import dao.RecruitmentDao;
import dao.UserDao;

@Service
public class AdminPageService {
	@Autowired
	ProjectDao projectDao;

	@Autowired
	UserDao userDao;

	@Autowired
	RecruitmentDao recruitmentDao;

	@Autowired
	QnaDao qnaDao;
	
	@Autowired
	AdminDao adminDao;

	@Autowired
	MsgBoardDao msgBoardDao;

	private static final int NUM_OF_BOARD_PER_PAGE = 10;

	public Map<String, Object> countProject() {
		return 	projectDao.selectCountProject();
	}
	public List<Map<String, Object>> QuarterProject() {
		return 	projectDao.selectQuarterDate();
	}


	public Map<String, Object> countUser() {
		return 	userDao.selectCountUser();
	}
	public List<Map<String, Object>> QuarterUser() {
		return 	userDao.selectQuarterDate();
	}	

	
	public Map<String, Object> countRecruitment() {
		return 	recruitmentDao.selectCountRecruitment();
	}
	public List<Map<String, Object>> QuarterRecruitment() {
		return 	recruitmentDao.selectQuarterDate();
	}


	public Map<String, Object> countMsgBoard() {
		return 	msgBoardDao.selectCountMsgboard();
	}
	public List<Map<String, Object>> QuarterMsgBoard() {
		return 	msgBoardDao.selectQuarterDate();
	}	
	
	public Map<String, Object> countAdmin() {
		return 	adminDao.selectCountAdmin();
	}
	public List<Map<String, Object>> QuarterAdmin() {
		return 	adminDao.selectQuarterDate();
	}	
	
	public Map<String, Object> countQna() {
		return qnaDao.selectCountQna();
	}
	public List<Map<String, Object>> QuarterQna() {
		return 	qnaDao.selectQuarterDate();
	}	
	
	

	public List<Map<String, Object>> getAllProjectListOnlyfive() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start_limit", 0);
		map.put("end_limit", 5);

		return projectDao.selectListLimit(map);
	}
	
	
	
	public int getTotalPage(int totalCount) {
//		int totalCount = ;
		int totalPage = (totalCount-1)/NUM_OF_BOARD_PER_PAGE + 1;

		return totalPage;
	}

	//1 11 21 31     2-1*10 + 1
	//10 20 30 40     ((10-1)/10 + 1)*10 
	public int getFirstRow(int page) {
		int result = (page - 1) * NUM_OF_BOARD_PER_PAGE;
		return result;
	}
	public int getEndRow(int page) {
		int result = getFirstRow(page) + 10;
		return result;
	}
	private int getStartPage(int page) {
		int result  = page-1;
		return result;
	}
	private int getEndPage(int page) {
		int result = getStartPage(page)+10;
		return result;
	}
	
	
	
	public Map<String, Object> getAllProjectList(Map<String, Object> map) {
		int page = Integer.parseInt(map.get("page").toString());
		
		map.put("start_limit", getFirstRow(page));
		map.put("end_limit", getEndRow(page));
		System.out.println("map " + map);

		Map<String, Object> daoParam = new HashMap<String, Object>();
		daoParam.put("totalPage", getTotalPage(((Long)projectDao.selectCountProject().get("projectCount")).intValue()));
		daoParam.put("startPage", getStartPage(page));
		daoParam.put("endPage", getEndPage(page));
		daoParam.put("projectList", projectDao.selectListLimit(map));
		System.out.println("daoParam " + daoParam);

		return daoParam;
	}
	
	
	public Map<String, Object> getAllMemberList(Map<String, Object> map) {
		int page = Integer.parseInt(map.get("page").toString());
		
		map.put("start_limit", getFirstRow(page));
		map.put("end_limit", getEndRow(page));
		System.out.println("map " + map);

		Map<String, Object> daoParam = new HashMap<String, Object>();
		daoParam.put("totalPage", getTotalPage(((Long)userDao.selectCountUser().get("userCount")).intValue()));
		daoParam.put("startPage", getStartPage(page));
		daoParam.put("endPage", getEndPage(page));
		daoParam.put("projectList", userDao.selectListLimit(map));
		System.out.println("daoParam " + daoParam);

		return daoParam;
	}	
	
	

	
	public Map<String, Object> getDutyCycleGit() {
		int offGitCount = ((Long)projectDao.selectDutyCycleGit().get("gitOffCount")).intValue();
		int projectMemberCount = ((Long)projectDao.selectCountProjectMember().get("projectMemberCount")).intValue();
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("offGitCount", offGitCount);
		map.put("projectMemberCount", projectMemberCount);

		return map;
	}
	
}
