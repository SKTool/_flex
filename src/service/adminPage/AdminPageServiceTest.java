package service.adminPage;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:WebContent/WEB-INF/spring/root-context.xml")
public class AdminPageServiceTest {
	
	@Autowired
	AdminPageService service;
	
	@Test
	public void test() {
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", 1);

		System.out.println("test");
		System.out.println(service.getAllProjectList(map));
		System.out.println(service.getAllProjectList(map).get("projectList"));
		System.out.println("end test");
		
	}

}
