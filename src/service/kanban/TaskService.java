	package service.kanban;
	import java.util.ArrayList;
	import java.util.HashMap;
	import java.util.LinkedHashMap;
	import java.util.List;
	import java.util.Map;
	import java.util.StringTokenizer;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;
	import org.springframework.transaction.annotation.Transactional;
	import org.springframework.transaction.interceptor.TransactionAspectSupport;
	import dao.TaskDao;
	
	@Service
	public class TaskService implements ITaskService {
		@Autowired
		private TaskDao taskDao;
	
		@Override
		public boolean writeTask(Map<String, Object> params) {
			if (taskDao.insertTask(params) > 0) {
				return true;
			} else {
				return false;
			}
		}
	
		@Override
		public boolean modifyTask(Map<String, Object> params) {
			if (taskDao.updateTask(params) > 0) {
				return true;
			} else {
				return false;
			}
		}
	
		@Override
		public boolean deleteTask(int task_idx) {
			if (taskDao.deleteTask(task_idx) > 0) {
				taskDao.deleteTaskMember(task_idx);
				taskDao.deleteTaskTagList(task_idx);
				return true;
			} else {
				return false;
			}
		}
	
		@Override
		public Map<String, Object> getTaskList(int task_idx) {
			return taskDao.selectOneTask(task_idx);
		}
	
		@Override
		public List<Map<String, Object>> getAllTaskList() {
			return taskDao.selectAllTask();
		}
	
		@Transactional
		@Override
		public boolean insertTaskMember(Map<String, Object> params) {
	
			try {
				int result = taskDao.insertTask(params);
				Map<String, Object> taskMember = new HashMap<String, Object>();
				Map<String, Object> taskTagList = new HashMap<String, Object>();
				taskMember.put("task_idx", params.get("task_idx"));
				taskTagList.put("task_idx", params.get("task_idx"));
	
				String projectMemberIdx = (String) params.get("project_member_idx");
				StringTokenizer st = new StringTokenizer(projectMemberIdx, ",");
	
				String taskTagIdx = (String) params.get("task_tag_idx");
				StringTokenizer st2 = new StringTokenizer(taskTagIdx, ",");
	
				int countTokens = st.countTokens();
				int countTokens2 = st2.countTokens();
				if (result > 0) {
					for (int i = 0; i < countTokens; i++) {
						String data = st.nextToken();
						int dataIdx = Integer.parseInt(data);
						taskMember.put("project_member_idx", dataIdx);
						taskDao.insertTaskMember(taskMember);
					}
	
					for (int i = 0; i < countTokens2; i++) {
						String data = st2.nextToken();
						int dataIdx = Integer.parseInt(data);
						taskTagList.put("task_tag_idx", dataIdx);
						taskDao.insertTaskTagList(taskTagList);
					}
	
				}
			} catch (Exception e) {
				// 예외를 별도로 발생시키지 않고 현재 적용된 Aspect에 의한 트랙잭션을 rollback 시킴
				e.printStackTrace();
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return false;
			}
			return true;
		}
	
		@Override
		public boolean modifyTaskMember(Map<String, Object> params) {
			int result = taskDao.updateTask(params);
			System.out.println("modifyService : " + params);
			Map<String, Object> modifyTaskMember = new HashMap<String, Object>();
			Map<String, Object> modifyTaskTagList = new HashMap<String, Object>();
	
			modifyTaskMember.put("task_idx", params.get("task_idx"));
			modifyTaskTagList.put("task_idx", params.get("task_idx"));
	
			String strTaskIdx = (String) params.get("task_idx");
			int taskIdx = Integer.parseInt(strTaskIdx);
	
			List<Map<String, Object>> taskTagList = taskDao.selectByTaskTag(taskIdx);
			List<Map<String, Object>> taskMember = taskDao.selectByTaskMember(taskIdx);
	
			String modifyMemberIdx = (String) params.get("task_member_idx");
			StringTokenizer memberSt = new StringTokenizer(modifyMemberIdx, ",");
			String modifyTagIdx = (String) params.get("task_tag_idx");
			StringTokenizer tagSt = new StringTokenizer(modifyTagIdx, ",");
			int countMember = memberSt.countTokens();
			int countTag = tagSt.countTokens();
	
			if (result > 0) {
				if(memberSt.countTokens() > 0) {
					taskDao.deleteTaskMember(taskIdx);
				}
				for (int i = 0; i < countMember; i++) {
					String data = memberSt.nextToken();
					int dataIdx = Integer.parseInt(data);
					System.out.println("수정하려는 멤버 index : " + dataIdx);
					modifyTaskMember.put("project_member_idx", dataIdx);
					System.out.println(modifyTaskMember);
					taskDao.insertTaskMember(modifyTaskMember);
				}
				if(tagSt.countTokens() > 0) {
					taskDao.deleteTaskTagList(taskIdx);
				}	
				for (int j = 0; j < countTag; j++) {
					String data = tagSt.nextToken();
					int dataIdx = Integer.parseInt(data);
					System.out.println("수정하려는 태그 index : " + dataIdx);
					modifyTaskTagList.put("task_tag_idx", dataIdx);
					taskDao.insertTaskTagList(modifyTaskTagList);
				}
			}
			return true;
		}
	
		@Override
		public boolean deleteTaskMember(int task_idx) {
			if (taskDao.deleteTaskMember(task_idx) > 0) {
				return true;
			} else {
				return false;
			}
		}
	
		@Override
		public boolean insertTaskTagList(Map<String, Object> params) {
			return false;
		}
	
		@Override
		public boolean deleteTaskTagList(Map<String, Object> params) {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public List<Map<String, Object>> selectByTaskMember(int task_idx) {
			// TODO Auto-generated method stub
			return taskDao.selectByTaskMember(task_idx);
		}
	
		@Override
		public List<Map<String, Object>> selectByTaskTag(int task_idx) {
			// TODO Auto-generated method stub
			return taskDao.selectByTaskTag(task_idx);
		}
	
		@Override
		public List<Map<String, Object>> getTaskListByProject(int project_idx) {
			// TODO Auto-generated method stub
			List<Map<String, Object>> taskList = taskDao.selectAllTaskByProject(project_idx);
	
			Map<String, Object> taskTag = new HashMap<String, Object>();
			Map<String, Object> taskMember = new HashMap<String, Object>();
	
	//		System.out.println("몇개 : " + taskList.size());
			for (int i = 0; i < taskList.size(); i++) {
	//			System.out.println("게시글 번호 : " + taskList.get(i).get("task_idx").toString());
				int a = Integer.parseInt(taskList.get(i).get("task_idx").toString());
				List<Map<String, Object>> tags = taskDao.selectByTaskTag(a);
				taskList.get(i).put("tags", tags);
				List<Map<String, Object>> members = taskDao.selectByTaskMember(a);
				taskList.get(i).put("members", members);
			}
			return taskList;
		}
	
		@Override
		public List<Map<String, Object>> fastSelectMember(int project_idx) {
			List<Map<String, Object>> task = taskDao.fastSelectMember(project_idx);
			List<Map<String, Object>> realTask = new ArrayList<>();
	
			for (Map<String, Object> taskMember : task) {
				Map<String, Object> tm = new LinkedHashMap<>();
				tm.put("text", taskMember.get("user_name"));
				tm.put("value", taskMember.get("project_member_idx"));
				realTask.add(tm);
			}
			return realTask;
		}
	
		@Override
		public List<Map<String, Object>> fastSelectTag() {
			List<Map<String, Object>> tag = taskDao.fastSelectTaskTag();
			List<Map<String, Object>> realTag = new ArrayList<>();
	
			for (Map<String, Object> taskTag : tag) {
				Map<String, Object> tt = new LinkedHashMap<>();
				tt.put("text", taskTag.get("task_tag_name"));
				tt.put("value", taskTag.get("task_tag_idx"));
				realTag.add(tt);
			}
			return realTag;
		}
	
		@Override
		public List<Map<String, Object>> fastSelectViewMember(Map<String, Object> params) {
			List<Map<String, Object>> viewMember = taskDao.fastSelectViewMember(params);
			List<Map<String, Object>> realViewMember = new ArrayList<>();
			for (Map<String, Object> viewTaskMember : viewMember) {
				Map<String, Object> vtm = new LinkedHashMap<>();
				vtm.put("text", viewTaskMember.get("user_name"));
				vtm.put("value", viewTaskMember.get("project_member_idx"));
				realViewMember.add(vtm);
			}
			return realViewMember;
		}
	
		@Override
		public List<Map<String, Object>> fastSelectViewTag(int task_idx) {
			List<Map<String, Object>> viewTag = taskDao.fastSelectViewTag(task_idx);
			List<Map<String, Object>> realViewTag = new ArrayList<>();
			for (Map<String, Object> viewTaskTag : viewTag) {
				Map<String, Object> vtt = new LinkedHashMap<>();
				vtt.put("text", viewTaskTag.get("task_tag_name"));
				vtt.put("value", viewTaskTag.get("task_tag_idx"));
				realViewTag.add(vtt);
			}
			return realViewTag;
		}
	
		@Override
		public boolean modifyTaskTagList(Map<String, Object> params) {
			// TODO Auto-generated method stub
			return false;
		}
	}
