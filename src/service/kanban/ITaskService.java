package service.kanban;

import java.util.List;
import java.util.Map;

public interface ITaskService {
	//task 추가
	public boolean writeTask(Map<String,Object> params);
	//task 수정
	public boolean modifyTask(Map<String,Object> params);
	//task 삭제
	public boolean deleteTask(int task_idx);
	//멤버 번호 가져오기
	public List<Map<String,Object>> selectByTaskMember(int task_idx);
	//태그 번호 가져오기
	public List<Map<String,Object>> selectByTaskTag(int task_idx);
	//전체 task 가져오기
	public List<Map<String,Object>> getAllTaskList();
	//task 하나 가져오기
	public Map<String,Object> getTaskList(int task_idx);
	public List<Map<String,Object>> getTaskListByProject(int project_idx);
	public List<Map<String,Object>> fastSelectMember(int project_idx);
	public List<Map<String,Object>> fastSelectTag();
	public List<Map<String,Object>> fastSelectViewMember(Map<String,Object> params);
	public List<Map<String,Object>> fastSelectViewTag(int task_idx);
	
	//taskMember 추가
	public boolean insertTaskMember(Map<String,Object> params);
	//taskMember 수정
	public boolean modifyTaskMember(Map<String,Object> params);
	//taskMember 삭제
	public boolean deleteTaskMember(int task_idx);
	//taskTagList 추가
	public boolean insertTaskTagList(Map<String,Object> params);
	//taskTagList 수정
	public boolean modifyTaskTagList(Map<String,Object> params);
	//taskTagList 삭제
	public boolean deleteTaskTagList(Map<String,Object> params);
}
