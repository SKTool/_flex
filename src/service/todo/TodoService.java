package service.todo;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.Common;
import dao.TodoDao;

@Service
public class TodoService implements ITodoService {
	@Autowired
	private TodoDao todoDao;
	
	@Override
	public Map<String,Object> writeTodo(Map<String, Object> params) {
		if(todoDao.insertTodo(params)>0) {
			return params;
		} else {
			return params;
		}
	}

	@Override
	public Map<String,Object> modifyTodo(Map<String, Object> params) {
		if(todoDao.updateTodo(params)>0) {
			return params;
		} else {
			return params;
		}
	}
	@Override
	public boolean deleteTodo(Map<String, Object> params) {
		String strTodoIdx = (String)params.get("to_do_idx");
		int to_do_idx = Integer.parseInt(strTodoIdx);
		Map<String,Object> todo = todoDao.selectOneTodo(to_do_idx);
		if(todoDao.deleteTodo(to_do_idx)>0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<Map<String, Object>> getAllTodoList() {
		return todoDao.selectAllTodo();
	}

	@Override
	public int checkTodo(Map<String, Object> params) {
		String strTodoStatus = (String)params.get("to_do_status");
		String strTodoIdx = (String) params.get("to_do_idx");
		int to_do_status = Integer.parseInt(strTodoStatus);
		int to_do_idx = Integer.parseInt(strTodoIdx);
		Map<String,Object> todo = todoDao.selectOneTodo(to_do_idx);
		if(todo.get(Common.Todo.TO_DO_IDX).toString().equals(strTodoIdx)) {
			System.out.println("넘어오는 status : " + to_do_status);
			System.out.println("넘어오는 todo : " + params);
			if(to_do_status == 0) {
				todoDao.updateStatusTodo(params);
			} else if(to_do_status == 1) {
				todoDao.updateStatusTodo(params);
			}
		}
		return to_do_status;
	}

	@Override
	public List<Map<String, Object>> getUserIdxTodoList(int user_idx) {
		return todoDao.selectMyTodo(user_idx);
	}

	@Override
	public int selectMyProjectMemberIdx(Map<String, Object> param) {
		System.out.println(todoDao.selectMyProjectMemberIdx(param));
		return todoDao.selectMyProjectMemberIdx(param);
	}
}
