package service.todo;

import java.util.List;
import java.util.Map;

public interface ITodoService {
	//todo 작성
	public Map<String,Object> writeTodo(Map<String,Object> params);
	//todo 수정
	public Map<String,Object> modifyTodo(Map<String,Object> params);
	//todo 삭제
	public boolean deleteTodo(Map<String,Object> params);
	//todo 상태값 변경
	public int checkTodo(Map<String,Object> params);
	//모든 todo 가져오기
	public List<Map<String,Object>> getAllTodoList();
	//특정 user_idx에 대한 todo 가져오기
	public List<Map<String,Object>> getUserIdxTodoList(int user_idx);
	public int selectMyProjectMemberIdx(Map<String,Object> param);
}
