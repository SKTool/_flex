<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<meta charset="UTF-8">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<title>Flex</title>
<script type="text/javascript">
	$(function(){
		createFileList();
		
		$('#searchBar').on('keyup', function(){
			if(event.keyCode === 8){
				$('#searchBar').val("");
			}
			if($('#searchBar').val() != ''){
				console.log($('#searchBar').val());
					
				var fileListFromTable = $('#fileListTable').find('td[name="nameTd"]');
				var fileList = [];
				
				for(var i = 0; i < fileListFromTable.length; i++){
					if(fileListFromTable[i].innerHTML.includes($('#searchBar').val())){
						fileList.push(fileListFromTable[i].innerHTML);
					}
				}
				
				console.log(fileList);
				createSearchedFileList(fileList);
			}else{
				createFileList();
			}
		})
		
		$('#upload').on('click', function(){
			alert('1');
			var form_temp = document.getElementById('uploadFileForm');
			form_temp.method  = 'POST';
			form_temp.enctype = 'multipart/form-data';
			var formData = new FormData(form_temp);
			
			$.ajax({
				
				url 		: '/user/uploadProjectFile',
				type 		: 'POST',
				data 		: formData,
				cache 		: false,
				processData : false,
				contentType : false,
				success : function(result){
					location.reload();
				}
			});
		})
		
		$('#download').on('click', function(){
			if($('input:checkbox[name="fileCheckbox"]:checked').length === 0){
				swal.fire({
					type : 'warning',
					text : '파일을 선택해 주세요'
				}).then(()=>{
					return false;
				})
			}
			
			var checkboxes = $('#fileListTable').find('input[type=checkbox]');
			for(var i = 0; i < checkboxes.length; i++){
				
				if(checkboxes[i].checked){
					var checkboxId = checkboxes[i].getAttribute("id");
					var fileNameTdId = checkboxId.replace('checkbox', 'fileNameTd');
					$('<iframe src = "/user/downloadFiles?file_name=' + $('#'+fileNameTdId).val() + '" style = "display :none;">').appendTo($('body'));
				}
			}
		})
	})
	function createFileList(){
		$.ajax({
			url : '/user/getFiles',
			type : 'post',
			data : {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : '${project_member_idx}'},
			success : function(fileList){
				$('#fileListTable tr:gt(0)').remove();
				for(var i = 0; i < fileList.length; i++){
					var nameArr = (fileList[i].file_name).split('_'); 
					var name = nameArr[1];
					if(nameArr.length > 2){
						name += ('_' + nameArr[2]);
					}
					var tr = $('<tr>');
					tr.append($('<td name = "nameTd">').text((JSON.stringify(name)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').text((JSON.stringify(fileList[i].in_date)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').text((JSON.stringify(fileList[i].user_id)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').append($('<div class="custom-control custom-checkbox"><input type = "checkbox" class="custom-control-input" id = "checkbox' + i + '" name = "fileCheckbox"><label class="custom-control-label" for="checkbox' + i + '"></label></div>')));
					tr.append($('<td>').append($('<input type = "hidden"  id = "fileNameTd'+ i +'" value = "' + fileList[i].file_name + '">')));
					tr.appendTo($('#fileListTable'));
				}
			}
		});       	
	}
	function createSearchedFileList(searchedFileList){
		var searchedFileList = searchedFileList;
		$.ajax({
			url : '/user/getFiles',
			type : 'post',
			data : {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : '${project_member_idx}'},
			success : function(fileList){
				$('#fileListTable tr:gt(0)').remove();
				for(var i = 0; i < fileList.length; i++){
					var nameArr = (fileList[i].file_name).split('_'); 
					var name = nameArr[1];
					if(nameArr.length > 2){
						name += ('_' + nameArr[2]);
					}
					var check = 0;
					for(var k = 0; k < searchedFileList.length; k++){
						if(searchedFileList[k] === name){
							check++;  
						}
					}
					if(check === 0){
						continue;
					}
					
					var tr = $('<tr>');
					tr.append($('<td name = "nameTd">').text((JSON.stringify(name)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').text((JSON.stringify(fileList[i].in_date)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').text((JSON.stringify(fileList[i].user_id)).replace('\"', '').replace('\"', '')));
					tr.append($('<td>').append($('<div class="custom-control custom-checkbox"><input type = "checkbox" class="custom-control-input" id = "checkbox' + i + '" name = "fileCheckbox"><label class="custom-control-label" for="checkbox' + i + '"></label></div>')));
					tr.append($('<td>').append($('<input type = "hidden"  id = "fileNameTd'+ i +'" value = "' + fileList[i].file_name + '">')));
					tr.appendTo($('#fileListTable'));
				}
			}
		});
	}
</script>
</head>
<body>
	<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
	
    <div id="session" class="container-fluid" style="padding:0;">
        <div class="row">

            <div id="session01" class="col-sm-12 col-md-4 col-lg-3 col-xl-2 bd-sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
            </div>

            <main id="session02" class="col-sm-12 col-md-8 col-lg-9 col-xl-10" style = "background : #f1f1f1;">
            	<div>
            		<h2 style = "margin-left : 5%;">파일공유</h2>
            		<hr>
            		<br>
            	</div> 
				<div class = "container card" id = "fileListDiv" align = "center" style = "width : 90%; height : 40%; margin-left : 5%; border : 1px solid black; ">
					<div class="card-header input-group mb-3">
    					<div class="input-group-prepend">
     						<span class="input-group-text">검색</span>
   						</div>
 						<input type="text" id = "searchBar" class="form-control" placeholder="search">
					</div>
					<div class="card-body" style = "overflow:scroll;">
						<table class = "table" id = "fileListTable" style = "text-align : center; width : 100%;	">
							<tr>
								<th>파일명</th>
								<th>저장된 날짜</th>
								<th>작성자</th>
								<th>check</th>
							</tr>
						</table>
					</div>
				</div>
				<div align = "right" style = "width : 100%; padding-right : 5%;"> 
					<br>
				 	<button type="button" class="btn btn-outline-info"  data-toggle="modal" data-target="#filUploadModal">파일업로드</button>
				 	<input type = "button" class="btn btn-outline-info" id = "download" value = "파일다운로드">
				</div> 
            </main>
        </div>
    </div>
<div class="modal" id="filUploadModal" style = "margin-top : 10%;">
	<div class="modal-dialog">
		<div class="modal-content">
		
		<div class="modal-header">
		<h4 class="modal-title">업로드</h4>
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		
		<div class="modal-body">
		  	<form id = "uploadFileForm">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<input type="hidden" name="project_member_idx" value="${project_member_idx}">
				<input multiple="multiple" type="file" name="file" value = "파일업로드" />
			</form>
		</div>
		
		<div class="modal-footer">
			<button type="button" class="btn btn-outline-info" id = "upload" data-dismiss="modal">업로드하기</button>
			<button type="button" class="btn btn-outline-danger" id = "upload" data-dismiss="modal">Close</button>
		</div>
		
		</div>
	</div>
</div>
</body>
</html>