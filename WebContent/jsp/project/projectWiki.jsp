<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>


<script type="text/javascript">
	$(function() {
		$('.summernote').summernote({
			lang : 'ko-KR',
			width : 1350,
			height : 300, // 기본 높이값
			focus : true
		// 페이지가 열릴때 포커스를 지정
		});
	});
</script>

<title>Flex</title>

</head>
<body>
	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>
			<!-- End sidebar -->

			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">
				<div class="rounded"
					style="background: white; width: 94%; margin-left: 3%; margin-top: 2%;">


					<div>
						<br> <br>
						<h2 style="width: 95%; margin-left: 5%; color: gray;">WIKI</h2>
						<p style="margin-left: 5%; color: lightgray; font-size: 0.9em;">공동
							작업으로 프로젝트 위키를 만들고 유지 관리합니다.</p>
									<hr style="width: 95%; margin-left: 5%; margin-right: 5%;">
					</div>
				
					<div style="width: 90%; text-align: left;">

						<div>
							<div>
								<button class="btn btn-info btn-m" type="button"
									id="modifyButton" style="float: right;"
									onclick="location.href='/project/modifywiki?num=${project.project_idx}'">수정</button>

								<div class="project_wiki"
									style="margin: 15px; padding: 10px; width: 95%; margin-left: 5%;">
									${project.project_wiki}</div>
							</div>
							<br>

							<div>
								<br> <br>
								<h2 style="width: 95%; margin-left: 5%; color: gray;">
									Project Member <i class="fas fa-users"></i>
								</h2>
								<p style="margin-left: 5%; color: lightgray; font-size: 0.9em;">프로젝트를
									함께 진행하는 멤버들입니다.</p>
									<hr style="width: 95%; margin-left: 5%; margin-right: 5%;">
								<div class="project"
									style="display: inline-block; white-space: normal; margin: 10px; padding: 10px; width: 95%; margin-left: 5%;  ">
									<c:forEach items="${projectMember}" var="wiki" varStatus="i">

										<div class="wikiMember"
											style="text-align: center; display: inline-block; white-space: normal; width: 150px; height: 120px; margin: 7px;">
											<div class="panel panel-primary rounded-circle"
												style="margin: 7px; padding: 3px; padding-top: 5px; display: inline-block;">
												<img src="/user/image?user_pic=${wiki.user_pic}"
													style="border-radius: 50%; width: 80px; height: 80px;">
											</div>
											<div class="panel panel-primary"
												style="margin: 7px; padding: 3px; padding-top: 5px; display: inline-block; text-align: center;">
												${wiki.user_name} <br>
												<c:if test="${wiki.project_auth_idx eq 1}">
												[Project Master]
												</c:if>
												<c:if test="${wiki.project_auth_idx eq 2}">
												[Member]
												</c:if>
												<c:if test="${wiki.project_auth_idx eq 3}">
												[Guest]
												</c:if>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>

						</div>
					</div>
				</div>

			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->
</body>
</html>