<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id"
	content="674931549888-nfb6kriv0lrjpmrup2cgp2ktq2cs20ik.apps.googleusercontent.com">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="/css/semantic.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="/js/semantic.min.js"></script>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<link href="https://fonts.googleapis.com/css?family=Raleway:200"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<script>
   $(function(){
      
      var form = $('<form action = "/user/userProfile" method = "post">');
      var input = $('<input type = "hidden" name = "user_id" value = "${principal}">');
      var submit = $('<input type = "submit" id = "submitIt" style = "display:none;">');
      var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
      input.appendTo(form);
      submit.appendTo(form);
      token.appendTo(form);
      form.appendTo($("body"));
      
      $('#userProfile').on('click', function(){
         $('#submitIt').click();
      })
      
      $('#logout').on('click', function(){   
         $('#logoutIframe').attr('src',"https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:9080/index/main?logout=true");
         $('#logoutIframe').on('load', function(){
            location.href='/user/logout';
         })
      })  
   });
   
     Kakao.init('8111651fb06f9bc9197c2be46f23cab0');
    function plusFriendChat() {
      Kakao.PlusFriend.chat({
        plusFriendId: '_rrxoLj' // 플러스친구 홈 URL에 명시된 id로 설정합니다.
      });
    }
</script>
<style type="text/css">
.project:hover {
	box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0
		rgba(0, 0, 0, 0.1);
}

.project:active {
	box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0
		rgba(0, 0, 0, 0.1);
	transform: translateY(4px);
}

.projectbox:hover {
	box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0
		rgba(0, 0, 0, 0.1);
	transform: translateY(4px);
}

.project-button-box:hover {
	background-color: #001133;
	color: white;
	box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0
		rgba(0, 0, 0, 0.1);
	transform: translateY(5px);
}

.project-button-box {
	background: white;
	color: #001133;
}
</style>
<meta charset="UTF-8">
<title>main</title>

</head>
<body>
	<!-- 권한 확인 -->
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<sec:authentication property="principal" var="admin" />
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_USER')">
		<sec:authentication property="principal" var="user" />
	</sec:authorize>

	<!--현재 로그인 된 회원 확인하기 위한 값  -->
	<sec:authentication property="name" var="login_user_name" />

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">


		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">


<!-- 						<a href="javascript:void plusFriendChat()"> <img -->
<!-- 							src="https://developers.kakao.com/assets/img/about/logos/plusfriend/consult_small_yellow_pc.png" /> -->
<!-- 						</a> -->

			<!-- Main -->
			<div class="main" style="background: #f7f7f7;">
				<div
					style="height: 230px; background-color: #001133; ertical-align: middle; text-align: center; color: white;">
					<br> <br> <span
						style="font-family: 'Raleway', sans-serif; vertical-align: middle; font-weight: 100%; font-size: 50px;">Project</span><span></span>

				</div>

				<!-- 				<div class="filter" -->
				<!-- 					style="padding-right: 15rem; padding-left: 15rem;"> -->
				<!-- 					<span class="m m-search"></span> <input type="text" -->
				<!-- 						ng-model="searchies" ng-change="onQueryChange(searchies)" -->
				<!-- 						class="form-control" placeholder="Filter"> -->
				<!-- 				</div> -->

				<div style="padding: 1rem; text-align: center;">


					<!-- 프로젝트 리스트 뽑아내는 부분 -->
					<div class="row"
						style="margin: 20px; padding-right: 22rem; padding-left: 22rem; text-align: left;"
						data-toggle="collapse" data-target="#demo">
						<div style="margin:7px;">
							<h3>
								<i class="fas fa-angle-right"></i> 내가 속한 프로젝트
							</h3>
						</div>
					</div>


					<div class="row"
						style="margin: 20px; padding-right: 22rem; padding-left: 22rem; text-align: left;"
						id="demo" class="collapse in">

						<c:forEach items="${projectList}" var="project">
							<div class="project"
								style="white-space: normal; width: 260px; height: 200px; margin: 10px; background-color: white;">
								<a style="color: black;"
									href="/project/wiki?num=${project.project_idx}">
									<div class="panel panel-primary"
										style="margin: 9px; padding: 4px; padding-top: 5px; display: inline-block; width: 170px; height: 150px; font-weight:100; font-size: 20px;">
										${project.project_title}</div>
										
								</a>

								<div id="mouseOver"
									style="text-align: center; vertical-align: middle; float: right; top: 50px; margin: 7px; padding: 7px;">
									<div id="myBtn${project.project_idx}" data-toggle="modal"
										data-target="#viewModal"
										style="border-bottom: 0.5px solid gray;">
										<i style="color: #cccccc;" class="fas fa-cog"></i>
									</div>

									<div id="delete${project.project_idx}" style="margin-top: 1px;">
										<i style="color: #cccccc" class="fas fa-trash-alt"></i>
									</div>
								</div>
							</div>



							<script type="text/javascript">
			                           $(function() {
			                              $("#myBtn${project.project_idx}").on("click",function(){
			                                 $.ajax({
			                                   url:"/project/view",
			                                   data : {"num" : ${project.project_idx}},
			                                   type:"get",
			                                   dataType:"json",
			                                   success : function(data){
			                                    
			                              		var memberTable = $("#project_member");
			                                      for(i in data){
			                                    	if(data[i].project_auth_idx != null){
				                                    	var tr = $("<tr>");
				                      						if(data[i].project_auth_idx === 1){
				                      							$("<td>").text("관리자").appendTo(tr);
				                      						}
				                      						if(data[i].project_auth_idx === 2){
				                      							$("<td>").text("팀원").appendTo(tr);
				                      						}
				                      						if(data[i].project_auth_idx === 3){
				                      							$("<td>").text("게스트").appendTo(tr);
				                      						}
				                      					$("<td>").text(data[i].user_name).appendTo(tr);
				                      					tr.appendTo(memberTable);
			                                    	}
			                                      }
			                                      for(i in data){       
			                                    	  $("#project_idx_view").val(data[i].project_idx);
			                                    	  $("#project_title_view").val(data[i].project_title);
			                                          $("#project_wiki_view").val(data[i].project_wiki);
			                                          $("#project_start_view").val(data[i].project_start);
			                                          $("#project_end_view").val(data[i].project_end);
			                                      }
			                                          $("#viewModal").show();                                   
			                                   } 
			                           		});
			                              });
			                              
			                              
			                              $("#delete${project.project_idx}").on("click", function() {
			                              const swalWithBootstrapButtons = Swal.mixin({
			                                   confirmButtonClass: 'btn btn-success',
			                                   cancelButtonClass: 'btn btn-danger',
			                                   buttonsStyling: false,
			                                 })
			                                    swalWithBootstrapButtons.fire({
			                                      title:'프로젝트를 삭제하시겠습니까?',
			                                      text: "삭제된 프로젝트는 복구할 수 없습니다.",
			                                      type: 'warning',
			                                      showCancelButton: true,
			                                      confirmButtonText: '삭제',
			                                      cancelButtonText: '취소',
			                                      reverseButtons: true
			                                    }).then((result) => {
			                                      if (result.value) {
			                                         $.ajax({
			                                             url:"/project/delete",
			                                             data:{"num" : ${project.project_idx}},
			                                             type:"get",
			                                             success : function(data){
			                                                 swalWithBootstrapButtons.fire(
			                                                   '삭제 완료',
			                                                   '프로젝트가 삭제되었습니다.',
			                                                   'success'
			                                                 )
			                                                 location.href = "/project/projectList";
			                                             }
			                                        });
			                                      } else if (
			                                        // Read more about handling dismissals
			                                        result.dismiss === Swal.DismissReason.cancel
			                                      ) {
			                                        swalWithBootstrapButtons.fire(
			                                          '취소',
			                                          '공지사항 상세보기로 돌아갑니다.',
			                                          'error'
			                                        )
			                                      }
			                                    })
			                              });
			          	                 });
                     			  </script>
						</c:forEach>

						<!--          프로젝트 생성하는 버튼         -->
						<div class="project-button-box"
							style="width: 260px; height: 200px; margin: 10px; padding: 1rem;">
							<div data-toggle="modal" data-target="#myModal"
								style="vertical-align: middle;">
								<div style="text-align: center; padding: 3.4rem; font-size: 20px;">
									<i class="fas fa-plus"></i><br> <span>새 프로젝트</span>
								</div>
							</div>
						</div>

					</div>

				</div>


				<!--          프로젝트 생성하는 모달         -->
				<div class="modal fade" id="myModal" style="vertical-align: middle;">
					<div class="modal-dialog">
						<form action="/project/create" id="createForm" name="createForm"
							method="post">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}">

							<div class="modal-content">
								<div style="float: right;">
									<button
										style="width: 10px; height: 10px; padding: 10px; float: right; display: inline-block; margin-right: 20px;"
										type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<h4 class="modal-title"
									style="text-align: center; display: inline-block;">새 프로젝트</h4>
								<div style="padding: 20px; margin: 20px; text-align: left;">
									<div class="form-group">
										<label for="project_title">프로젝트명:</label> <input type="text"
											class="form-control" id="project_title" name="project_title">
									</div>
									<div class="form-group">
										<label for="project_wiki">설명(선택 사항)</label> <input type="text"
											class="form-control" id="project_wiki"
											placeholder="예)웹사이트 디자인" name="project_wiki">
									</div>
									<div class="form-group">
										<label for="project_end">프로젝트 멤버(선택 사항)</label> <br> <i
											class="fas fa-plus-square"
											style="color: gray; padding-left: 5px; font-size: 23px;"
											data-toggle="modal" data-target="#memberInvite"></i> <span
											id="member"
											style="margin: 10px; background: #eeeeee; width: 70px; height: 25px; border-radius: 50px 50px 50px 50px;">
											<img src="/user/image?user_pic=${user_pic}"
											style="width: 25px; height: 25px; border-radius: 50px;">
											${user_name}
										</span>
									</div>
									<div class="form-group">
										<label for="project_start">시작</label> <input type="date"
											class="form-control" id="project_start" name="project_start" />
									</div>

									<div class="form-group">
										<label for="project_end">종료</label> <input type="date"
											class="form-control" id="project_end" name="project_end" />
									</div>
									<div style="text-align: center;">
										<input type="reset" class="btn btn-default" value="취소">
										<input type="submit" class="btn btn-default"
											id="createProject" value="프로젝트 생성">
									</div>
								</div>

							</div>
						</form>
						<script type="text/javascript">
							$(function() {      
					             $("#createForm").submit(function(){
					    
					                 var startDate = $('#project_start').val();
					                 var endDate = $('#project_end').val();
					                 var startArray = startDate.split('-');
					                 var endArray = endDate.split('-');   
					                 var start_date = new Date(startArray[0], startArray[1], startArray[2]);
					                 var end_date = new Date(endArray[0], endArray[1], endArray[2]);
					                 
										if(project_title.value == "") {
											alert("프로젝트명을 입력해 주세요.");
											project_title.focus();
											return false;
										}
						                 else if(start_date.getTime() > end_date.getTime()) {
						                     alert("프로젝트 종료일을 다시 선택해 주세요.");
						                     return false;
						                 }
					              });
							});
						</script>

					</div>
				</div>

				<!-- 초대 모달 -->
				<div class="modal fade" id="memberInvite" role="dialog">
					<div class="modal-dialog" style="width: 180px; height: 130px;">
						<div class="modal-content"
							style="left: 250px; top: 280px; width: 350px; margin: 10px; padding: 10px;">
							<div class="modal-head" style="margin: 10px; padding: 10px;">
								<h4 class="modal-title"
									style="text-align: center; display: inline-block;">멤버 초대하기</h4>
								<button type="button" class="close" data-dismiss="modal"
									style="display: inline-block;">&times;</button>
							</div>
							<div class="modal-body">
								<div style="display: inline-block; vertical-align: middle;">
									<img src="/img/friend.gif"
										style="border-radius: 50%; width: 120px; height: 100px;">
								</div>
								<div style="display: inline-block; vertical-align: middle;">
									<div
										style="font-size: 13px; margin: 10px; font-weight: 80%; vertical-align: middle; text-align: center;">함께
										스마트하게 협업하세요</div>
									<div
										style="font-size: 17px; margin: 10px; font-weight: 80%; vertical-align: middle; text-align: center;">
										<button id="inviteMember"
											style="color: #001133; border: 1px soild #001133">멤버
											초대하기</button>
									</div>

									<script type="text/javascript">
										$(function() {
											 $("#inviteMember").on("click", function() {
												Swal.fire({
										            title: '이메일을 입력하세요',
										            input: 'text',
										            inputAttributes: {
										              autocapitalize: 'off'
										            },
										          }).then((result) => {
										            if (result.value != null && result.value != '') {
										               
										             var emailVal = result.value;
										             var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
										             
										             if (emailVal.match(regExp) == null) {
										                
										                Swal.fire({
										                   text : '이메일 형식에 맞춰 입력해 주세요',
										                   type : 'warning'
										                })
										                
										                return false;
										             }  
									               
									              $.ajax({
									                 url : '/user/sendInvitationMail',
									                 type : 'POST',
									                 data : {'${_csrf.parameterName}' : '${_csrf.token}', 'toAddress' : result.value},
									                 success : function(result){
									                    if(result === 1){
									                       Swal.fire({
									                          text : '성공',
									                          type : 'success'
									                       })
									                    }else if(result === -1){
									                       Swal.fire({
									                          text : '실패',
									                          type : 'error'
									                       })
									                    }
									                 }
									                 
									              })
									            }
									          })
											 })
										})
										</script>
								</div>
							</div>
						</div>
					</div>
				</div>


				<!-- 프로젝트 상세 보기 모달 -->
				<div class="modal fade" id="viewModal"
					style="vertical-align: middle;">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div style="float: right;">
								<button
									style="width: 10px; height: 10px; padding: 10px; float: right; display: inline-block; margin-right: 20px;"
									type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							<h4 class="modal-title"
								style="text-align: center; display: inline-block;">프로젝트 상세
								보기</h4>
							<div style="padding: 20px; margin: 20px; text-align: left;">
								<form  id="modifyForm" name="modifyForm"
									method="post">
									<div class="form-group">
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}"> <input type="hidden"
											class="form-control" id="project_idx_view" name="project_idx">

										<label for="project_title">프로젝트명</label> <input type="text"
											class="form-control" id="project_title_view"
											name="project_title">
									</div>
									<div class="form-group">
										<label for="project_start">시작</label> <input type="date"
											class="form-control" id="project_start_view"
											name="project_start" />
									</div>
									<div class="form-group">
										<label for="project_end">종료</label> <input type="date"
											class="form-control" id="project_end_view" name="project_end" />
									</div>
									<div class="form-group">
										<label for="project_wiki">설명<</label>
										<textarea type="text" class="form-control"
											id="project_wiki_view" style="margin: 5px;"
											readonly="readonly"></textarea>
									</div>
									<div class="form-group">
										<label for="project_end">프로젝트 참여 멤버</label>
										<table id="project_member" class="table"
											style="font-weight: 100%; text-align: center; margin: 3px; text-align: center">
										</table>
										
									</div>

									<div style="text-align: center;">
										<input type="submit" id="iimodify" class="btn btn-info btn-sm"
											value="수정">
									</div>
								</form>

								<script type="text/javascript">
								 $(function() {
									 $("#iimodify").on("click", function() {
										 var data = $("#modifyForm").Serialize();
											$.ajax({
												   url:"/project/modify",
				                                   data : data,
				                                   type:"post",
													success : function(result){
														alert(result);
													if(result === 1){
														alert('1')
															location.href = "/project/projectList";												
														}
													}
											});
											return false;
										});
									});
								</script>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->

</body>
</html>