<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>


<title>Flex</title>

</head>
<body style = "overflow : hidden;">

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>
			<!-- End sidebar -->

<!-- 			<!-- Main --> 
<!-- 			<div class="main"> -->
				<!--             <main id="session02" class="col-12" > -->

				<div class="main" style="background: #f1f1f1; width : 100%; height : 100%;">
					<div class="rounded" style="background: white; width : 94%;margin-left: 3%; height : 94%; margin-top: 2%;">


<!-- 						<div class="row" -->
<!-- 							style="margin: 15px; padding-right: 0.5rem; padding-left: 0.5rem; text-align: left;"> -->

							<div style="margin: 5px; padding-right: 0.5rem; padding-left: 0.5rem; width: 95%">

								<div>
									<br> <br>
									<h2 style="width: 95%; margin-left: 5%; color: gray;">Project
										WIKI</h2>
									<hr style="width: 95%; margin-left: 5%; margin-right: 5%; color : black;">
								</div>


								<div style="width: 95%; margin-left: 5%;">
									<form action="/project/modifywiki" method="post">

										<div style="margin: 10px; padding: 10px; margin-top : 30px;">
											<h3
												style="width: 100%; margin-bottom: 10px; color: gray;">Project
												title</h3>
											<input type="text" style="width: 100%;"
												class="form-control" value="${project.project_title}"
												name="project_title">
										</div>

										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}"> <input type="hidden"
											name="project_idx" value="${project.project_idx}">

										<div style="margin: 10px; padding: 10px; ">
											<h3 style="width: 100%; color: gray;">wiki</h3>

											<div>
												<textarea name="project_wiki"
													class="summernote"> ${project.project_wiki}</textarea>												
											</div>
										</div>

										<script type="text/javascript">
											$(function() {
												$('.summernote').summernote({
													lang : 'ko-KR',
													height : 500,
													focus : true
												// 페이지가 열릴때 포커스를 지정
												});
											});
										</script>


										<div style="margin: 10px; padding: 10px;">
											<h3 style="width: 100%; color: gray;">Start
												Date</h3>
											<input type="date" style="width: 100%; "
												class=" form-control" value="${project.project_start}"
												name="project_start" />
										</div>

										<div style="margin: 10px; padding: 10px;">
											<h3 style="width: 100%;  color: gray;">End
												Date</h3>
											<input type="date" class=" form-control"
												style="width: 100%; "
												value="${project.project_end}" name="project_end" />
										</div>


										<div style="float: right; margin-right : 1%;">
											<input class="btn btn-info btn-m" type="submit" value="수정">
										</div>
									</form>
<!-- 								</div> -->
							</div>

						</div>
					</div>
				</div>

<!-- 			</div> -->
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->
</body>
</html>