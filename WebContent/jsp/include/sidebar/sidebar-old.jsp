<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
	.sidebar-sticky {                                      				/* sidebar 배경색 */
 		background : linear-gradient(to bottom, #00001a, #4d4d4d); 
		z-index : 1500; 
	} 
	.sidebar-heading span i{											/* sidebar 아이콘색*/
		color : #53cbcd; 
	}
</style>
<nav class="col-md-1 d-none d-md-block bg-light sidebar">
	<div class="sidebar-sticky">
    	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-2 mb-1 text-muted">
 			<span style = "font-size:1.5em;"><i class = "fa fa-calendar mr-5"></i></span>
		</h6>
    	<ul class="nav flex-column"> 
      		<li class="nav-item">
        		<a class="nav-link" href="/schedule/schedule">
          			<span data-feather="file"></span>
          			Gantt Chart
        		</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="/schedule/list">
          			<span data-feather="shopping-cart"></span>
          			Calendar
        		</a>
      		</li>
    	</ul>

    	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-3 mb-1 text-muted">
     		<span style = "font-size:1.5em;"><i class = "fa fa-tasks mr-5"></i></span>
   		</h6>
    	<ul class="nav flex-column mb-2">
      		<li class="nav-item">
        		<a class="nav-link" href="/user/chart">
          			<span data-feather="file-text"></span>
          			Analysis
        		</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="#">
          			<span data-feather="file-text"></span>
          			Task List
        		</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="#">
          			<span data-feather="file-text"></span>
          			Kanban Board
        		</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="todo/write">
          			<span data-feather="file-text"></span>
          			To-do List
        		</a>
      		</li>
   		</ul>
    	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-3 mb-1 text-muted">
     		<span style = "font-size:1.5em;"><i class = "fa fa-code-branch mr-5"></i></span>
   		</h6>
    	<ul class="nav flex-column mb-2">
      		<li class="nav-item">
        		<a class="nav-link" href="/version/commit">
          			<span data-feather="file-text"></span>
          			Commits
        		</a>
      		</li>
      		<li class="nav-item">
        		<a class="nav-link" href="/version/issue">
          			<span data-feather="file-text"></span>
          			Issues
        		</a>
      		</li>
    	</ul>
	    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-3 mb-1 text-muted">
     		<span style = "font-size:1.5em;"><i class = "fa fa-file mr-5"></i></span>
   		</h6>
    	<ul class="nav flex-column mb-2">
      		<li class="nav-item">
       			<a class="nav-link" href="/user/fileUpload?project_member_idx=3">
          			<span data-feather="file-text"></span>
          			File Sharing
        		</a>
      		</li>
     	</ul>
     	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-3 mb-1 text-muted">
     		<span style = "font-size:1.5em;"><i class = "fa fa-bug mr-5"></i></span>
   		</h6>
     	<ul class="nav flex-column mb-2">
      		<li class="nav-item">
        		<a class="nav-link" href="bugTracking/write">
          			<span data-feather="file-text"></span>
          			Bug Tracking
        		</a>  
      		</li>
     	</ul>
      	<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-3 mb-1 text-muted">
     		<span style = "font-size:1.5em;"><i class = "fab fa-wikipedia-w mr-5"></i></span>
   		</h6>
     	<ul class="nav flex-column mb-2">
      		<li class="nav-item">  
        		<a class="nav-link" href="#">
          			<span data-feather="file-text"></span>
          			WIKI
        		</a>
      		</li>
      		<li class="nav-item">
                 <a class="nav-link" href="projectMember/write">
                    Member
                 </a>
            </li> 
    	</ul>
    	</div>
    	
</nav>
