<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<nav id="nav" class="navbar navbar-expand-md navbar-dark bd-links">
	<button class="navbar-toggler d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

	<div class="collapse navbar-collapse accordion" id="collapsibleNavId">
		<ul class="nav flex-column mt-2 mt-lg-0">

			<li class="nav-item pb-3">
				<a class="nav-link" href="#workCollapse" data-toggle="collapse">
					<i class="fa fa-calendar mr-5"></i>Schedule
				</a> 
				<div class="collapse" id="workCollapse" data-parent="#collapsibleNavId">
					<ul style="list-style:none;">
						<li class="mb-3"> <a href="/schedule/schedule" class="nav-sub-item">Calendar </a></li>
						<li class="mb-3"> <a href="/schedule/gantt" class="nav-sub-item">Ganntt Chart </a></li>						
						<li> <a href="/user/chart" class="nav-sub-item">Analysis </a></li>
					</ul>
				</div>
			</li>
			
			<li class="nav-item pb-3">
				<a class="nav-link" href="#taskCollapse" data-toggle="collapse">  
					<i class="fa fa-tasks mr-5"></i>Task 
				</a>
				<div class="collapse" id="taskCollapse" data-parent="#collapsibleNavId">
					<ul style="list-style:none;">
						<li class="mb-3"> <a href="/task/write" class="nav-sub-item">List | Kanban </a></li>
						<li> <a href="/todo/write" class="nav-sub-item">ToDo </a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item pb-3">
				<a class="nav-link" href="#version" data-toggle="collapse">  
					<i class="fas fa-code-branch mr-5"></i>Version
				</a>
				<div class="collapse" id="version" data-parent="#collapsibleNavId">
					<ul style="list-style:none;">
						<li class="mb-3"> <a href="/version/commit" class="nav-sub-item"> Commit </a></li>
						<li> <a href="/version/issue" class="nav-sub-item"> Issue </a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item pb-3">
				<a class="nav-link" href="/user/fileUpload">
					<i class="fa fa-file mr-5"></i>File Sharing
				</a> 
			</li>
			<li class="nav-item pb-3">
				<a class="nav-link" href="/bugTracking/write"> 
					<i class="fa fa-bug mr-5"></i>Bug Tracking
				</a> 
			</li>
			<li class="nav-item pb-3">
				<a class="nav-link" href="/project/projectwiki">
					<i class="fab fa-wikipedia-w mr-5"></i>Wiki
				</a>
			</li>
			<li class="nav-item pb-3">
				<a class="nav-link" href="/projectMember/write">
					<i class="fas fa-user mr-5"></i>Member
				</a>
			</li>
		</ul>
	</div>
</nav>


