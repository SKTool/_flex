<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<style>
	.navbar {															/* header 배경색 */
 		background : rgba(0,0,0,0); 
		text-align : center; 
	}
 	.navbar-brand, .navbar-nav, .navbar, .text-nowrap a{  				/* header 글자색 */
		color : lightgray !important;     
	}   
	.navbar-brand { 
		background : none;
  		box-shadow : none !important;  
 		font-weight : 500;
	}
	.nav-item .nav-link {												
		color : lightgray;
		float : right;
		margin : 0 10px;
 		font-weight : 500;
	}
</style>
<nav class="navbar fixed-top flex-md-nowrap p-0"> 

	<a class="navbar-brand col-sm-3 col-md-1 mr-0" href="/index/main">FLEX</a>
	<ul class="navbar-nav px-3">
   		<li class="nav-item text-nowrap">
   			<a class="nav-link" href="/user/join">Join</a>
   			<a class="nav-link" href="/user/loginForm">Login</a>
   		</li>
	</ul> 
</nav>
