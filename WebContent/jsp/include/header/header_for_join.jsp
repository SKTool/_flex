<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<nav id="header" class="navbar navbar-expand navbar-dark  bd-navbar">
    <a class="navbar-brand" href="/index/main" style="margin-right: 3rem;">Flex</a>
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavBarId" aria-controls="collapsibleNavBarId"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavBarId">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="community-board" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">커뮤니티</a>
                <div class="dropdown-menu" aria-labelledby="community-board">
                    <a class="dropdown-item" href="/msgboard/msgboardList">자유게시판</a>
                    <a class="dropdown-item" href="/recruitment/recruitmentList">개발자모집</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="customer-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">고객센터</a>
                <div class="dropdown-menu" aria-labelledby="customer-center">
                    <a class="dropdown-item" href="#">Action 1</a>
                    <a class="dropdown-item" href="#">Action 2</a>
                </div>
            </li>
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <button class="btn-sm btn my-2 my-sm-0 mr-3 btn-light" type="button" onclick = "location.href='/user/loginForm'"> login </button>
            <button class="btn-sm btn my-2 my-sm-0 mr-3 btn-light" type="button" onclick = "location.href='/user/join'"> Join </button>
        </form>

    </div>
</nav>