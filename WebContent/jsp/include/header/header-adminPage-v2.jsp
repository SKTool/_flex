<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<security:authentication property="principal" var="username"/>

	<style>
/*  		#header-right-col a:active{  */
/*  			background-color: #FFFFFF;  */
/*  		}  */


		#header-row ul li div a:active{
   			background-color: whitesmoke;
   			border: 0.1rem solid lightgray;
		}

		#header-right-col ul li:active , #community-community:active, #customer-board:active{
			background-color: #3b475f;
		}
		
		#user-bell-alerts > div:active  {
   			background-color: whitesmoke;
		}		

 		#header-right-col > ul > li.profile.dropdown.show > div > label a:active{  
   			background-color: whitesmoke;
   			border: 0.1rem solid lightgray;
   		}   
		
/* 		#header-right-col > ul > li.profile.dropdown.show > div > label:active { */
/* 			background-color: #000000;   */
/* 		} */
/* 		#user-bell-alerts > div:nth-child(1) > div.header-right-col-bell-row */
		
/*    		#user-bell-alerts > div:nth-child(1) > div.header-right-col-bell-row:active { */
/*    			background-color: #000000; */
   			
/* 		}   */
		
				
	</style>

<script>
	
	function limitedRow() {
		$('#user-bell-alerts div:nth-child(5)').nextAll().remove();
		if ($("#user-bell-alerts").children().length >= 5) {

			$("#user-bell-alerts").append(
				'<div class="dropdown-item border px-1">'
				+ '<a class="text-center" href="#" style="font-weight: bold;background-color:lightgray">' + 'More Info' + '</a>'
				+ '</div>'
			);
		}
	}

	// var static_alert_bell_num = 0;

	function makeBellRow(_obj, inputType, day) {
	// 	limitedRow();
	// 	static_alert_bell_num+=1;

		console.log(_obj);
		console.log(_obj.user_bell_content);
		var obj = JSON.parse(_obj.user_bell_content);
		
		if ( day === 0 ) {
			var bgcolor = 'navy';
			
		} else if (day === -1 ) {
			var bgcolor = 'green';
			
		}

		var title;
		if (obj.title.length > 31) { 
			title = obj.title.substr(0, 31); 
		} else {
			title = obj.title;
		}

		var user_bell_alerts_row = 
				'<div class="dropdown-item bell-small-row border">'
// 				+ 		'<div align="center" style="margin-right: .1rem; display: inline-block; width:.5rem; height: .5rem; border: 1px solid orange; border-radius: .1rem;"></div>'
// 				+ 		'<strong style="font-size: .7rem;  float: left;">'+day+'</strong>'
				+	'<div class="header-right-col-bell-row"  style="position: relative; height: 1.5rem;">'
				+		'<div style="display: inline; position: absolute; left: 0;">'
				+ 			'<div class="mx-1" align="center" style="display: inline-block; width:.5rem; height: .5rem; border-radius: .1rem; background-color: '+bgcolor+'"></div>'
				+ 			'<strong style="font-size: 1rem;">'+obj.eventType+'</strong>'
				+		'</div>'
				+		'<div class="mx-2" style="display: inline; position: absolute; right: 0;">'
				+ 			'<small style="font-size: .8rem;">'+obj.email+'</small>'
				+		'</div>'
				+	'</div>'
				+ 	'<div style="clear: both; border-top: 2px solid navy;">'
				+ 	'<p class="mb-0 pb-4" style="font-size: .8rem; background-color: lightgray;">'+title+'</p>'
				+ 	'</div>'
				+ '</div>'
		
		
		if (inputType === 'webHook') {
			//TODO 데이터
			$("#user-bell-alerts").prepend( user_bell_alerts_row);
		} else {
			
			$("#user-bell-alerts").append( user_bell_alerts_row);
		}

		limitedRow();
		
	};

	function makeBellCount(obj) {
		if ( obj === '' ) {
			obj = 0;
		}
		var num = parseInt(obj);
		num += 1;
		$("#user-bell-count").text(num);
	}

	function dateToYYYYMMDD(date){
		function pad(num) {
			num = num + '';
			return num.length < 2 ? '0' + num : num;
		}
		return date.getFullYear() + pad(date.getMonth()+1) + pad(date.getDate());
	}

	

	<!-- connect -->
	<!-- -->
	<!-- -->
	<!-- -->
	<!-- -->
	<!-- -->
	<!-- -->
	<!-- -->
	function connect() {

		var sock = new SockJS("${contextPath}/chat");
		var stompClient = Stomp.over(sock);
		
		stompClient.connect({},function() {

			stompClient.subscribe("/alert/msg/${username}", function(message) {

				var bellCount = $("#user-bell-count").text();
				var body =  JSON.parse(message.body)[0];
				var body_user_bell_content =  JSON.parse(body.user_bell_content);


				if (body_user_bell_content['eventType'] === 'issue') {
						console.log(body);
						makeBellRow( body , 'webHook', 0);
						makeBellCount( bellCount );

				} else if (body_user_bell_content['eventType'] === 'pull_request') {
						console.log(body);
						makeBellRow( body , 'webHook', 0);
						makeBellCount(bellCount);

				} else if (body_user_bell_content['eventType'] === 'push') {
						console.log(body);
						makeBellRow( body , 'webHook', 0);
						makeBellCount( bellCount );

				}
			});
		});
	}
	
	
	$(function() {

		connect();
		
		
		
		$.ajax({
	    	url : '/user/getUser',
	    	type : 'post',
	    	data : {'${_csrf.parameterName}' : '${_csrf.token}'},
			success : function(user){
	    		$('#userIdA').text(user.user_id);
	    	}
	    })
		
		$('#myPage').on('click', function(){
			$('<form id = "getMyPage" action = "/user/userProfile" method = "post">').append($('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">')).appendTo($('body'));
			$('#getMyPage').submit();    	
		})
		
		
		

		$.ajax({
			url:'/userBell/alertSmallRow' ,
			method:'post' , 
			data: {'${_csrf.parameterName}' : '${_csrf.token}'}, 
			datatype: 'json' ,
			success:function(data){
				
// 				alert(data);
				for (i in data) {
// 					console.log(" alertSmallRow Success");
// 					console.log(data[i]);

// 					var subdate = (data[i].user_bell_occ_date.substr(0, 4)) +(data[i].user_bell_occ_date.substr(5, 2)) +(data[i].user_bell_occ_date.substr(8, 2));

					d = JSON.parse(data[i].user_bell_content);
					var subdate = (d.timestamp.substr(0, 4)) +(d.timestamp.substr(5, 2)) +(d.timestamp.substr(8, 2));

					var currentDate = dateToYYYYMMDD(new Date);
// 					alert(data[i].user_bell_type);
					if (data[i].user_bell_type === 1) { 
// 						alert(currentDate);
// 						alert(subdate);

						if (((currentDate) - (subdate)) === 0 ) {
// 							alert("당일");
							makeBellCount($("#user-bell-count").text());

							makeBellRow(data[i], 'db', 0);
						}
					} else {
// 						alert(currentDate);
// 						alert(subdate);

						if ( (((currentDate) - (subdate)) === 0)  ) {
// 							alert("당일");
							makeBellCount($("#user-bell-count").text());

							makeBellRow(data[i], 'db', 0);
						} else if ( (((currentDate) - (subdate)) === 1)  ) {
// 							alert("전날");
							makeBellCount($("#user-bell-count").text());

							makeBellRow(data[i], 'db', -1);
							
						}
					}
					
				};
			} ,
			error:function(data) {
// 				alert("alertSmallRow 접속 Error");
			}
		});
	})

</script>

          <nav class="navbar navbar-expand-sm" style="background-color: #001133;">
            <a class="navbar-brand col-sm-3 col-md-1 ml-5" style="color: white;" href="/adminPage/member">FLEX</a>
                <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#header-row" aria-controls="header-row"
                  aria-expanded="false" aria-label="Toggle navigation"></button>
                <div class="collapse navbar-collapse" id="header-row">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="community-community" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">커뮤니티</a>
                        <div class="dropdown-menu" aria-labelledby="community-community">
                             <a class="dropdown-item" href="/msgboard/msgboardList">자유게시판</a>
                   			 <a class="dropdown-item" href="/recruitment/recruitmentList">개발자모집</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="customer-board" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">notice</a>
                        <div class="dropdown-menu" aria-labelledby="customer-board">
                            <a class="dropdown-item" href="/adminBoard/adminBoardList">Notice</a>
                            <a class="dropdown-item" href="/qna/faqList">FAQ</a>
                            <a class="dropdown-item" href="/qna/faqList#QNA">Q&A</a>
                        </div>
                    </li>
                </ul>



                <div id="header-right-col" class="header-block header-block-nav">
                    <ul class="nav-profile mb-0" style="list-style: none;">
                        <li class="notifications dropdown new">

<!--                             <a href="#" class="dropdown-toggle" id="top-dropdown-alert-bell" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
<!--                                 <i class="fa fa-bell"  style="color: white;"></i> -->
<!--                                 <sup> -->
<!--                                 	user-bell-count -->
<!--                                     <span id="user-bell-count" class="counter ml-0" style="color: white;"></span> -->
<!--                                 </sup> -->
<!--                             </a> -->
<!--                             user-bell-alerts -->
<!--                             <div id="user-bell-alerts" class="dropdown-menu py-0" aria-labelledby="top-dropdown-alert-bell" style="width: 15rem; left: -50%; width: 18rem;"> -->
<!--                             		벨 row 추가 -->
<!--                             		벨 row 추가 -->
<!-- <!--                                <a class="dropdown-item" href="#">Action</a> --> -->
<!--                             </div> -->
                        </li>
                        <li class="profile dropdown">
                            <a class="nav-link dropdown-toggle" id="top-dropdown-profile" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
<!--                                 <div class="img" style="background-image: url('https://avatars3.githubusercontent.com/u/3959008?v=3&amp;s=40')"> </div> -->
                                <span class="name" style="color: white"> 
									${username} 
								</span>
                            </a>
                            
                            <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="top-dropdown-profile" x-placement="bottom-start">
                                <label id="myPage" style="width:100%"><a class="dropdown-item" href="#">
                                    <i class="fa fa-user"></i> Profile</a> </label> 
                                <label style="width:100%"><a class="dropdown-item" href="#">
                                    <i class="fa fa-bell"></i> Notifications </a></label>
                                <label style="width:100%"><a class="dropdown-item" href="#">
                                    <i class="fas fa-cog"></i> Settings </a></label>
                                <div class="dropdown-divider"></div>
                                <label style="width:100%"><a class="dropdown-item" href="/user/logout">
                                    <i class="fas fa-power-off"></i> Logout </a></label>
                            </div>
                        </li>
                    </ul>
                </div>

              </div>
          </nav>

<!-- <<<<<<< HEAD -->
<!-- function btn_bell_alert_row(num) { -->
<!-- // 	$('#alert-bell-modal-title').val( $('#alert-bell-title-'+num).text() ); -->
<!-- // 	$('#alert-bell-modal-idx').val( $('#alert-bell-idx-'+num).val() ); -->
<!-- // 	$('#alert-bell-modal-date').val( $('#alert-bell-date-'+num).text() ); -->
<!-- // 	$('#alert-bell-modal-content').val( $('#alert-bell-content-'+num).val() ); -->
<!-- // 	$('#alert-bell-modal-name').val( $('#alert-bell-id-'+num).text() ); -->
<!-- } -->
<!-- var static_alert_bell_num = 0; -->

<!-- function makeBellRow(_obj) { -->
<!-- // 	limitedRow(); -->
<!-- // 	static_alert_bell_num+=1; -->

<!-- // 	var obj = JSON.parse(_obj.user_bell_content); -->

<!-- // 	$("#user-bell-alerts").prepend( -->
<!-- // 		'<div class="media">' -->
<!-- // 			+ '<img src="/img/userBell/vim.png" alt="이미지" style="width: 4rem;">' -->
<!-- // 			+ '<div class="media-body"   data-toggle="modal" data-target="#alert-bell-modal"  onclick="btn_bell_alert_row('+static_alert_bell_num+')"   >' -->

<!-- // 				+ '<div style="pointer-events: none;">' -->
<!-- // 				+ '<a id="alert-bell-eventType-'+static_alert_bell_num+'" class="dropdown-item" href="#" style="font-size:1rem; font-width:bold;">'+ obj.eventType +'</a>' -->

<!-- // 				+ '<a id="alert-bell-title-'+static_alert_bell_num+'" class="dropdown-item" href="#" style="font-size:.3rem;">'+ obj.title +'</a>' -->
<!-- // 				+ '<div>'  -->
<!-- // 					+ '<div style="float:left;">' -->
<!-- // 						+ '<a  id="alert-bell-date-'+static_alert_bell_num+'" class="dropdown-item pr-0" href="#" style="font-size:.3rem;">'+ obj.timestamp +'</a>' -->
<!-- // 					+ '</div>' -->
<!-- // 					+ '<div  style="float: right;" >' -->
<!-- // 						+ '<a id="alert-bell-id-'+static_alert_bell_num+'" class="dropdown-item pl-0" href="#" style="font-size:.3rem;">'+ obj.email +'</a>' -->
<!-- // 					+ '</div>' -->
<!-- // 				+ '</div>'  -->
<!-- // 				+ '</div>' -->

<!-- // 				+ '<input type="hidden" id="alert-bell-content-'+static_alert_bell_num+'" value="'+obj.message+'" />' -->
<!-- // 				+ '<input type="hidden" id="alert-bell-idx-'+static_alert_bell_num+'" value="'+_obj.user_bell_idx+'" />' -->

<!-- // 			+ '</div>' -->
<!-- // 		+ '</div>' -->
<!-- // 		+ '<div class="dropdown-divider"></div>' -->


<!-- // 	); -->
	
<!-- }; -->

<!-- function makeBellCount(obj) { -->
<!-- // 	if ( obj === '' ) { -->
<!-- // 		obj = 0; -->
<!-- // 	} -->
<!-- // 	var num = parseInt(obj); -->

<!-- // 	num += 1; -->
<!-- // 	$("#user-bell-count").text(num); -->
<!-- } -->

<!-- function connect() { -->
<%-- // 	var sock = new SockJS("${contextPath}/chat"); --%>
<!-- // 	var stompClient = Stomp.over(sock); -->
	

<!-- // // 	<security:authentication property="principal"/> -->
<!-- // // 	alert("Fsdsfs"); -->
<%-- // // 	alert(${username}); --%>
	
<!-- //     stompClient.connect({},function() { -->
<%-- // //         stompClient.subscribe("/alert/msg/${param.project_member_idx}/${param.user_idx}", function(message) { --%>
<%-- // //         stompClient.subscribe("/alert/msg/${param.project_member_idx}/${username}", function(message) { --%>
<%-- //         stompClient.subscribe("/alert/msg/${username}", function(message) { --%>

<!-- // 			var bellCount = $("#user-bell-count").text(); -->
<!-- // 			var body =  JSON.parse(message.body)[0]; -->
<!-- // 			var body_user_bell_content =  JSON.parse(body.user_bell_content); -->


<!-- // 			if (body_user_bell_content['eventType'] === 'issue') { -->
<!-- // // 					console.log(body); -->
<!-- // // 					console.log(body.user_bell_idx); -->
<!-- // 					makeBellRow( body ); -->
<!-- // 					makeBellCount( bellCount ); -->

<!-- // 			} else if (body_user_bell_content['eventType'] === 'pull_request') { -->
<!-- // // 					console.log(body); -->
<!-- // // 					console.log(body.user_bell_idx); -->
<!-- // 					makeBellRow( body ); -->
<!-- // 					makeBellCount(bellCount); -->

<!-- // 			} else if (body_user_bell_content['eventType'] === 'push') { -->
<!-- // // 					console.log(body); -->
<!-- // // 					console.log(body.user_bell_idx); -->
<!-- // 					makeBellRow( body ); -->
<!-- // 					makeBellCount( bellCount ); -->

<!-- // 			} -->
<!-- // 		}); -->
<!-- // 	}); -->

<!-- }; -->

<!-- function dateToYYYYMMDD(date){ -->
<!-- //     function pad(num) { -->
<!-- //         num = num + ''; -->
<!-- //         return num.length < 2 ? '0' + num : num; -->
<!-- //     } -->
<!-- //     return date.getFullYear() + pad(date.getMonth()+1) + pad(date.getDate()); -->
<!-- } -->



<!-- $(function() { -->
<!-- // 	connect(); -->

<!-- //     $.ajax({ -->
<!-- //         url:'/userBell/alertSmallRow' , -->
<!-- //         method:'post' ,  -->
<%-- // 		data: {'${_csrf.parameterName}' : '${_csrf.token}'},  --%>
<!-- //         datatype: 'json' , -->
<!-- //         success:function(data){ -->
<!-- //         	console.log(data); -->
<!-- //         	for (i in data) { -->
<!-- // 				console.log(data[i]); -->
<!-- // 				var subdate = (data[i].user_bell_occ_date.substr(0, 4)) +(data[i].user_bell_occ_date.substr(5, 2)) +(data[i].user_bell_occ_date.substr(8, 3)); -->
<!-- // 				var currentDate = dateToYYYYMMDD(new Date); -->
<!-- //         		if (((currentDate) - (subdate)) === 0 ) { -->
<!-- // 					makeBellCount($("#user-bell-count").text()); -->

<!-- // 					makeBellRow(data[i]); -->
<!-- //         		} -->
        		
<!-- //         	}; -->
<!-- //         } , -->
<!-- //         error:function(data) { -->
<!-- //         	alert("alertSmallRow 접속 Error"); -->
<!-- //         } -->
<!-- //     }); -->
	
    
<!--     $.ajax({ -->
<!--     	url : '/user/getUser', -->
<!--     	type : 'post', -->
<%--     	data : {'${_csrf.parameterName}' : '${_csrf.token}'}, --%>
<!-- 		success : function(user){ -->
<!--     		$('#userIdA').text(user.user_id); -->
<!--     	} -->
<!--     }) -->
    
<!--     $('#myPage').on('click', function(){ -->
<%--     	$('<form id = "getMyPage" action = "/user/userProfile" method = "post">').append($('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">')).appendTo($('body')); --%>
<!-- 		$('#getMyPage').submit();    	 -->
<!--     }) -->
<!-- }); -->
	
<!-- </script> -->
<!-- <style> -->
<!-- /* 	.navbar {															/* header 배경색 */ */ -->
<!-- /* /* 		background : linear-gradient(to right, #53cbcd, #2eb1b4); */ */ -->
<!-- /*  		background : #001133;  */ -->
<!-- /* 		text-align : center;  */ -->
<!-- /* 		opacity : 0.5; */ -->
<!-- /* 	}  */ -->
<!-- /*  	.navbar-brand, .navbar-nav, .navbar, .text-nowrap a{  				/* header 글자색 */ */ -->
<!-- /* 		color : white !important;      */ -->
<!-- /* 	}  */ -->
<!-- /* 	.nav-item .nav-link {												 */ -->
<!-- /* 		color : lightgray; */ -->
<!-- /* 		float : right; */ -->
<!-- /* 		margin : 0 10px; */ -->
<!-- /*  		font-weight : 500; */ -->
<!-- /* 	} */ -->
<!-- /* 	.navbar-brand { */ -->
<!-- /* 		background : none; */ -->
<!-- /*  		box-shadow : none !important;  */ -->
<!-- /*  		font-weight : 500; */ -->
<!-- /* 	} */ -->
<!--  </style>  -->
<!-- <nav class="navbar fixed-top flex-md-nowrap p-0 shadow">  -->
<!-- 		<a class="navbar-brand col-sm-3 col-md-1 mr-0" href="/index/main">FLEX</a> -->
<!-- 		<ul class="navbar-nav px-3"> -->
<!-- 	   		<li class="nav-item text-nowrap"> -->
	   			
<!-- 	   		</li> -->
<!-- 	   		<li class="nav-item dropdown"> -->
<!--       			<a id = "userIdA" class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown"> -->
<!--      		 	</a> -->
<!--       			<div class="dropdown-menu" style = "position : absolute;">  -->
<!-- 			        <a id = "myPage" class="dropdown-item" href="#" style = "text-align : center;">MyPage</a> -->
<!-- 			        <a class="dropdown-item" href="/user/logout" style = "text-align : center;">Logout</a> -->
<!--       			</div> -->
<!--     		</li> -->
<!-- 		</ul> 	 -->
		
		
<!-- 		<!-- 대형이형 --> 
<!-- <!-- 		<ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex"> --> 
<!-- <!-- 			<li> --> 
<!-- <!-- 				<a class="nav-item nav-link mr-2 mb-0" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> --> 
<!-- <!-- 					<i class="fas fa-bell" style="font-size:1.5rem; position: relative; top: 0.3rem;"></i> --> 
<!-- <!-- 					<div style="position: relative; bottom:1.5rem; left:.8rem; font-size:.8rem;"> --> 
<%-- <%-- 						TODO 카운터 --%> 
<!-- <!-- 						<span id="user-bell-count" style="float: right;" class="badge badge-pill badge-danger"></span> --> 
<!-- <!-- 					</div> --> 
<!-- <!-- 				</a> --> 
<!-- <!-- 				<div id="user-bell-alerts" class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions" style="width:20rem;"> --> 
	
	 
<!-- <!-- 				</div> --> 
<!-- <!-- 			</li>  --> 
<!-- <!-- 		</ul> --> 

<%-- 		<%-- 모달 --%> 
<!-- <!-- 		<div id="alert-bell-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="alert-bell-modal-header" style="display: none;" aria-hidden="true"> --> 
<!-- <!-- 			<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document"> --> 
<!-- <!-- 				<div class="modal-content"> --> 

<!-- <!-- 					<div class="modal-header"> --> 
<!-- <!-- 					  <h5 class="modal-title text-center m-auto" id="alert-bell-modal-header">알림</h5> --> 
					  
<!-- <!-- 					  <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Close"> --> 
<!-- <!-- 						<span aria-hidden="true">×</span> --> 
<!-- <!-- 					  </button> --> 
<!-- <!-- 					</div> --> 

<!-- <!-- 					<div class="modal-body mb-5 ml-3 mr-3"> --> 
<!-- <!-- 						<form action="#" id="alert-bell-modal-form"> --> 
<%-- <%-- 							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">   --%> 
<!-- <!-- 							<input type="hidden" id="alert-bell-modal-idx" name="user_bell_idx"/> --> 
<!-- <!-- 							<div class="form-group"> --> 
<!-- <!-- 							  <label for="formGroupExampleInput">제목</label> --> 
<!-- <!-- 							  <input type="text" class="form-control" id="alert-bell-modal-title"  name="alert-bell-title" placeholder="Example input"> --> 
<!-- <!-- 							</div> --> 
<!-- <!-- 							<div class="form-row"> --> 
<!-- <!-- 							  <div class="form-group col-md-6"> --> 
<!-- <!-- 								<label for="alert-modal-date">날짜</label> --> 
<!-- <!-- 								<input type="text" class="form-control" id="alert-bell-modal-date" name="alert-bell-modal-date" placeholder="Date"> --> 
<!-- <!-- 							  </div> --> 
<!-- <!-- 							  <div class="form-group col-md-6"> --> 
<!-- <!-- 								<label for="alert-modal-name">이름</label> --> 
<!-- <!-- 								<input type="email" class="form-control" id="alert-bell-modal-name" name="alert-bell-modal-name"  placeholder="Email"> --> 
<!-- <!-- 							  </div> --> 
<!-- <!-- 							</div> --> 
<!-- <!-- 							<div class="form-group"> --> 
<!-- <!-- 							  <label for="exampleFormControlTextarea1">내용</label> --> 
<!-- <!-- 							  <textarea class="form-control" id="alert-bell-modal-content" name="alert-bell-modal-content"  rows="6"></textarea> --> 
<!-- <!-- 							</div> --> 
<!-- <!-- 						</form> --> 
<!-- <!-- 					</div> --> 
														
<!-- <!-- 					<div class="modal-footer"> --> 
<!-- <!-- 					  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --> 
<!-- <!-- 					  <button type="button" class="btn btn-danger" onclick="btn_bell_modal_delete()">Recovery</button> --> 
<!-- <!-- 					</div> --> 
<!-- <!-- 				</div> --> 
<!-- <!-- 			</div> --> 
<!-- <!-- 		</div> --> 
<!-- <!-- 		<script> --> 
<!-- <!-- 			function btn_bell_modal_delete() { --> 
<!-- <!-- 				var d = $("#alert-bell-modal-form").serialize(); --> 
<!-- <!-- 				$.ajax({ --> 
<!-- <!-- 					url:'/userBell/removeGitMsgUserBell' , --> 
<!-- <!-- 					data: d , --> 
<!-- <!-- 					type:'post' ,   --> 
<!-- <!-- 					dataType : "json" , --> 
<!-- <!-- 					success:function(data){ --> 
<!-- <!-- 						window.location.reload() --> 
<!-- <!-- 					} , --> 
<!-- <!-- 					error:function(data) { --> 
<!-- <!-- 						alert("removeGitMsgUserBell 접속 Error"); --> 
<!-- <!-- 					} --> 
<!-- <!-- 				}); --> 
<!-- <!-- 			} --> 
<!-- <!--  		</script> --> 
<!-- 		<!-- 대형이형끝 --> 
<!-- </nav> -->
<!-- ======= -->
<!-- >>>>>>> 3d7bb6e90dbe8fee8473a2927a45688dd0f0a767 -->
