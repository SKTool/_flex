<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- <!DOCTYPE html> -->
<!-- <html> -->
<!-- <head> -->
<!-- <meta charset="UTF-8"> -->
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

<%-- <jsp:include page="/jsp/include/top/top.jsp"></jsp:include> --%>


    <style>
        .list-group-item {
            font-size: 1rem;
            border: 0;
        }
        .list-group-item .numberCircle {
            display: inline-block;
            width: 1.5rem;
            line-height: 1.5rem;
            border-radius: 50%;
            text-align: center;
            font-size: 1rem;
            font-weight: bold;
            margin-right: .7rem;
        }
        #list-group-deadline .list-group-item .numberCircle {
            border: 1px solid tomato;
        }
        #list-group-before-deadline .list-group-item .numberCircle {
            border: 1px solid green;
        }
        #list-group-git-alert .list-group-item .numberCircle {
            border: 1px solid maroon;
        }


        .list-group-item .subNumberCircle {
            text-align: center;
            font-size: 0.5rem;
            font-weight: bold;
            margin-left: .8rem;
            margin-right: 1rem;
        }
        #list-group-deadline .list-group-item .subNumberCircle {
            color : tomato;
        }
        #list-group-before-deadline .list-group-item .subNumberCircle {
            color : green;
        }
        #list-group-git-alert .list-group-item .subNumberCircle {
            color : maroon;
        }



        /* alert-list-header */
        #alert-header div[class|=col] {
            padding-left: 0;
            padding-right: 0;
        }

        .alert-list-header {
            font-size: 25px;
        }
        .alert-list-header-data-icon {
            background-color: #1f5c87;
            display: inline-block;
            width: 45px;
            height: 40px;
            text-align: center;
            margin-right: 20px;
        }
        .alert-list-header-data-icon .alert-list-header-data-icon-mon {
            color: white;
            font-size: 10px;
        }
        .alert-list-header-data-icon .alert-list-header-data-icon-day {
            color: white;
            font-size: 15px;
        }
        .alert-list-header-filter {
            display: inline-block;
            float: right;
        }
        .alert-list-header-filter .btn {
            font-size: 5px;
        }
        /* End alert-list-header */


        .list-group-iterm {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .alert-session02-category-item-1 {
            display: inline-block;
            float: left;
        }
        .alert-session02-category-item-2 {
            display: inline-block;
            clear: both;
            font-size: 1.3rem;
        }
        .alert-session02-category-item-2-1 {
            font-size: .7rem;
        }
        #list-group-deadline .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-1 {
            color: tomato;
        }
        #list-group-deadline .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-2 {
            color: darkgray;
        }
        #list-group-before-deadline .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-1 {
            color: green;
        }
        #list-group-before-deadline .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-2 {
            color: darkgray;
        }
        #list-group-git-alert .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-1 {
            color: maroon;
        }
        #list-group-git-alert .alert-session02-category-item-2 .alert-session02-category-item-2-1 .alert-session02-category-item-2-1-2 {
            color: darkgray;
        }

    </style>


    <script>
        function remove_alert_btn_onclick() {
            var $i = $("<i class='fas fa-archive' style='color: white;'></i>")
            $('#alert-list-header-icon').empty();
            $('#alert-list-header-icon').append($i);

            $('#alert-list-header-icon').css('background-color', '#52b166')
            toggleScreen($('#alert-list-bottom-main-remove-alert'));
        }

        function review_alert_btn_onclick() {
            var $i = $("<i class='far fa-clock' style='color: white;'></i>");
            $('#alert-list-header-icon').empty();
            $('#alert-list-header-icon').append($i);

            $('#alert-list-header-icon').css('background-color', '#5f4db0');
// TODO 추가 ( 중요함으로 사용 생각중 )
//             toggleScreen($('#alert-list-bottom-main-review-alert'));
        };

        function toggleScreen(obj) {
            $('#alert-list-bottom-main-new-alert').prop('hidden', true);
//             $('#alert-list-bottom-main-review-alert').prop('hidden', true);
            $('#alert-list-bottom-main-remove-alert').prop('hidden', true);
            obj.prop('hidden', false);
        }
        function initScreen() {
            new_alert_btn_onclick();

            $('#alert-list-bottom-main-new-alert').prop('hidden', false);
//             $('#alert-list-bottom-main-review-alert').prop('hidden', true);
            $('#alert-list-bottom-main-remove-alert').prop('hidden', true);
        }
        function new_alert_btn_onclick() {
            var week = new Array('SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT');

            var currentDate = new Date();
            var date = currentDate.getDate();
            var todayLabel = week[currentDate.getDay()];

            var $div = '<div class="alert-list-header-data-icon-mon">'+todayLabel+'</div><div class="alert-list-header-data-icon-day">'+date+'</div>'

            $('#alert-list-header-icon').empty();
            $('#alert-list-header-icon').append($div);

            $('#alert-list-header-icon').css('background-color', '#1f5c87')
            toggleScreen($('#alert-list-bottom-main-new-alert'));
        }

        // 라인 색 
        function rgb2hex(rgb) {
            if (  rgb.search("rgb") == -1 ) {
                return rgb;
            } else {
                rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
                function hex(x) {
                    return ("0" + parseInt(x).toString(16)).slice(-2);
                }
                return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
            }
        }
        function createBtnLine(obj) {
            $('#btn-deadline').css('margin-bottom', '0');
            $('#btn-deadline').css('border-style', 'solid');
            $('#btn-deadline').css('border-bottom-width', '0');
            $('#btn-deadline').css('border-bottom-color', 'tomato');

            $('#btn-before-deadline').css('margin-bottom', '0');
            $('#btn-before-deadline').css('border-style', 'solid');
            $('#btn-before-deadline').css('border-bottom-width', '0');
            $('#btn-before-deadline').css('border-bottom-color', 'green');

            $('#btn-git-alert').css('margin-bottom', '0');
            $('#btn-git-alert').css('border-style', 'solid');
            $('#btn-git-alert').css('border-bottom-width', '0');
            $('#btn-git-alert').css('border-bottom-color', 'maroon');
            
            if ( $(obj).attr('aria-expanded') === 'true' ) {
                $(obj).css('margin-bottom', '0');
                $(obj).css('border-bottom-width', '0');
            } else {
                $(obj).css('margin-bottom', '.3rem');
                $(obj).css('border-bottom-width', '2px');
            }
        }

        $(function(){
            initScreen();


        });


    </script>

<!-- 	<title>Flex</title> -->
<!-- </head> -->
<!-- <body> -->

    <%-- navbar --%>
<%-- 	<jsp:include page="/jsp/include/header/header.jsp"></jsp:include> --%>
    <%-- end navbar --%>


    <%-- session --%>
<!--     <div id="session" class="container-fluid" style="padding:0;"> -->
<!--         <div class="row"> -->

<%-- 	        sidebar --%>
<!--             <div id="session01" class="col-sm-12 col-md-4 col-lg-3 col-xl-2 bd-sidebar"> -->
<%-- 				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include> --%>
<!--             </div> -->
<%-- 	        end sidebar --%>

<%-- 	         main --%>
<!--             <main id="session02" class="col-sm-12 col-md-8 col-lg-9 col-xl-10"> -->
            
    <div id="alert-list">
        <div id="alert-list-main">




            <%-- 새 알림 --%>
            <div id="alert-list-top-main">
                <div class="container">
                    <div id="alert-header" class="row my-4">
                        <div class="col-12 alert-list-header">
                            <div id="alert-list-header-icon" class="alert-list-header-data-icon">
                                <%-- <div class="alert-list-header-data-icon-mon">FEB</div>
                                <div class="alert-list-header-data-icon-day">12</div> --%>
                            </div>
                            <a> 업데이트 사항을 모두 확인하셨습니다. </a>
                            <div class="alert-list-header-filter">
                                <div class="btn-group">
                                    <button id="new-alert-btn" class="btn btn-light" onclick="new_alert_btn_onclick();">새 알림</button>
<!--                                     <button id="review-alert-btn" class="btn btn-light" onclick="review_alert_btn_onclick();">다시 알림</button> -->
                                    <button id="remove-alert-btn" class="btn btn-light" onclick="remove_alert_btn_onclick();">지움</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="alert-list-bottom-main-new-alert">
                <div class="container">
                    <div id="alert-session01" class="row mb-4" style="height: 6rem; background-color: #1f5c87;">
                        <div class="col-12 text-center my-auto" style="font-size: 1.3rem; color: white;">
                            <i class="far fa-bell"></i>
                            <a> 팀원들이 보낸 모든 알림을 받을 수있는 곳입니다. </a>
                        </div>
                    </div>
                    <div id="alert-session02" class="row">
                        <%-- <div class="col-1 text-center"> 오늘 </div>
                        <div class="col-11 text-center"> </div> --%>
                        <div id="alert-session02-category" class="col-12 px-0 accordion">
                            <%--마감일 지남 --%>
                            <ul id="list-group-deadline" class="list-group">
                                <li id="btn-deadline" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#list-group-item-collapse-deadline" aria-expanded="false" aria-controls="list-group-item-collapse-deadline" onclick="createBtnLine(this);">
                                    <div id="1-deadline-alert-numberCircle" class="numberCircle"> 0 </div> 마감 일 지남
                                </li>

                                <div id="list-group-item-collapse-deadline" class="collapse" data-parent="#alert-session02-category"> 

<!--                                     <li class="list-group-item list-group-item-action"> -->
<%--                                         TODO --%>
<!--                                         <div class="alert-session02-category-item-1"> -->
<!--                                             <i class="subNumberCircle fas fa-dot-circle"></i> -->
<!--                                         </div> -->
<!--                                         <div class="alert-session02-category-item-2"> -->
<!--                                             <div> -->
<!--                                                 <a> asdfasdf </a> -->
<!--                                             </div> -->
<!--                                             <div class="alert-session02-category-item-2-1"> -->
<%--                                                 TODO --%>
<!--                                                 <a class="alert-session02-category-item-2-1-1"> <i class="far fa-folder-open"></i> 마감일 13일 지남 </a> &nbsp &nbsp                                             -->
<!--                                                 <a class="alert-session02-category-item-2-1-2"> <i class="fas fa-calendar-check"></i>  다이렉트 업무내 업무함 </a> -->
<!--                                             </div> -->
<!--                                         </div> -->
<%--                                         END TODO --%>
<!--                                     </li> -->
 
                                </div>
                            </ul>

                            <ul  id="list-group-before-deadline"  class="list-group">
                                <li id="btn-before-deadline" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#list-group-item-collapse-before-deadline" aria-expanded="false" aria-controls="list-group-item-collapse-before-deadline" onclick="createBtnLine(this);">
                                    <div id="2-deadline-alert-numberCircle" class="numberCircle"> 0 </div> 마감 일 하루전
                                </li>
                                
                                <div id="list-group-item-collapse-before-deadline" class="collapse" data-parent="#alert-session02-category"> 
<!--                                     <li class="list-group-item list-group-item-action"> -->

<%--                                         todo --%>
<!--                                         <div class="alert-session02-category-item-1"> -->
<!--                                             <i class="subNumberCircle fas fa-dot-circle"></i> -->
<!--                                         </div> -->
<!--                                         <div class="alert-session02-category-item-2"> -->
<!--                                             <div> -->
<!--                                                 <a> asdfasdf </a> -->
<!--                                             </div> -->
<!--                                             <div class="alert-session02-category-item-2-1"> -->
<%--                                                 todo --%>
<!--                                                 <a class="alert-session02-category-item-2-1-1"> <i class="far fa-folder-open"></i> 마감일 13일 지남 </a> &nbsp &nbsp      -->
<!--                                                 <a class="alert-session02-category-item-2-1-2"> <i class="fas fa-calendar-check"></i>  다이렉트 업무내 업무함 </a> -->
<!--                                             </div> -->
<!--                                         </div> -->
<%--                                         end todo --%>

<!--                                     </li> -->

                                </div>
                            </ul>
						
                            
                            <ul id="list-group-git-alert" class="list-group">
                                <li id="btn-git-alert" class="list-group-item list-group-item-action" data-toggle="collapse" data-target="#list-group-item-collapse-before-deadline-git-alert" aria-expanded="false" aria-controls="list-group-item-collapse-before-deadline-git-alert" onclick="createBtnLine(this);">
                                    <div id="git-alert-numberCircle" class="numberCircle"> 0 </div> 깃 알림 
                                </li>

                                <div id="list-group-item-collapse-before-deadline-git-alert" class="collapse" data-parent="#alert-session02-category"> 
                                
									<script>
										function btn_git_alert_row(num) {

											console.log($('#git-alert-row-data-'+num).val());
											$('#alert-modal-idx').val($('#git-alert-row-idx-'+num).val());
											
											var data = JSON.parse($('#git-alert-row-data-'+num).val());
											
											$('#alert-modal-title').val(data.title);
											$('#alert-modal-date').val(data.timestamp);
											$('#alert-modal-content').val(data.message);
											$('#alert-modal-name').val(data.email);
										}

									</script>


									<!-- 페이지 이동으로 페이지 변경으로 ajax 로 변경 -->
									<script>
										$(function() {
											$.ajax({
												url:'/userBell/gitAlerts',
												data: {'${_csrf.parameterName}' : '${_csrf.token}'},
												method:'post',  
												datatype: 'json',
												success:function(data){
													
													console.log(data);
													
													for (i in data) {
														var time = JSON.parse(data[i].user_bell_content).timestamp;		
														
														var li = "<li class='list-group-item list-group-item-action'  data-toggle='modal' data-target='#exampleModalCenteredScrollable' onclick='btn_git_alert_row("+i+");'>"
														+"	<div class='alert-session02-category-item-1'>"
														+"		<i class='subNumberCircle fas fa-dot-circle'></i>"
														+"	</div>"
														+"	<div class='alert-session02-category-item-2'>"
														+"		<div> <a> "+data[i].project_title+"</a> </div>"

														+"		<input id='git-alert-row-idx-"+i+"' type='hidden' value='"+data[i].user_bell_idx+"'/>"
														+"		<input id='git-alert-row-data-"+i+"' type='hidden' value='"+data[i].user_bell_content+"'/>"

														+"		<div class='alert-session02-category-item-2-1'>"
														+"			<a class='alert-session02-category-item-2-1-1 mr-3'> <i class='far fa-calendar-check'></i> "+time+" </a>"
														+"			<a class='alert-session02-category-item-2-1-2'> <i class='fas fa-folder-open'></i> "+data[i].project_title+" </a>"
														+"		</div>"
														+"	</div>"
														+"</li>";
							
														if (data[i].user_bell_type === 1) {
															// 깃 알림	
	 														$('#list-group-item-collapse-before-deadline-git-alert').append(li);
	 														$('#git-alert-numberCircle').text($("#list-group-item-collapse-before-deadline-git-alert li").length);	
															
														} else {
															console.log("SDFS");
															console.log();
															var time = JSON.parse(data[i].user_bell_content).timestamp;															
															//TODO timestamp JSON 을 OBJECT로 변경
															
	 														var subdate = (time.substr(0, 4)) +(time.substr(5, 2)) +(time.substr(8, 2));
	 														var currentDate = dateToYYYYMMDD(new Date);
	 														
	 														
	 														
															if ( (((currentDate) - (subdate)) <= 0)  ) {		
																//  마감 일 지남
		 														$('#list-group-item-collapse-before-deadline').append(li);
		 														$('#2-deadline-alert-numberCircle').text($("#list-group-item-collapse-before-deadline li").length);														
															} else {
																// 마감 일 하루 전 and 마감 일 
		 														$('#list-group-item-collapse-deadline').append(li);
		 														$('#1-deadline-alert-numberCircle').text($("#list-group-item-collapse-deadline li").length);	
															}
	
														}
														// 깃 알림	
// 														$('#list-group-item-collapse-before-deadline-git-alert').append(li);
// 														$('#git-alert-numberCircle').text($("#list-group-item-collapse-before-deadline-git-alert li").length);
													
														// 마감 일 하루전
// 														$('#list-group-item-collapse-before-deadline').append(li);
// 														$('#2-deadline-alert-numberCircle').text($("#list-group-item-collapse-before-deadline li").length);
														
														//  마감 일 지남
// 														$('#list-group-item-collapse-deadline').append(li);
// 														$('#1-deadline-alert-numberCircle').text($("#list-group-item-collapse-deadline li").length);
														
													}
												},
												error:function(data) {
													alert("error");
												}
											});
										});
											
									</script>	
									
<!--
									   <c:forEach items="${gitAlerts}" var="gitAlert" varStatus="status">
										
											<li class="list-group-item list-group-item-action"  data-toggle="modal" data-target="#exampleModalCenteredScrollable" onclick="btn_git_alert_row(${status.count});">
												<div class="alert-session02-category-item-1">
													<i class="subNumberCircle fas fa-dot-circle"></i>
												</div>
												<div class="alert-session02-category-item-2">
													<div> <a> ${gitAlert.project_title}</a> </div>

													<input id="git-alert-row-idx-${status.count}" type="hidden" value='${gitAlert.user_bell_idx}'/>
													<input id="git-alert-row-data-${status.count}" type="hidden" value='${gitAlert.user_bell_content}'/>

													<div class="alert-session02-category-item-2-1">
														<a class="alert-session02-category-item-2-1-1 mr-3"> <i class="far fa-calendar-check"></i> ${gitAlert.user_bell_occ_date} </a>

														<a class="alert-session02-category-item-2-1-2"> <i class="fas fa-folder-open"></i> ${gitAlert.project_title} </a>
													</div>
												</div>
											</li>
										
										</c:forEach>
										<script>
											 $('#git-alert-numberCircle').text($("#list-group-item-collapse-before-deadline-git-alert li").length);
										</script>
-->
                                </div>
                                

								<%-- 깃 출력 모달 --%>
								<div id="exampleModalCenteredScrollable" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredScrollableTitle" style="display: none;" aria-hidden="true">
									<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
										<div class="modal-content">

											<div class="modal-header">
											  <h5 class="modal-title text-center m-auto" id="exampleModalCenteredScrollableTitle">알림</h5>
											  
											  <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">×</span>
											  </button>
											</div>

											<div class="modal-body mb-5 ml-3 mr-3">
												<form action="#" id="alert-modal-form">
													<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> 
													<input type="hidden" id="alert-modal-idx" name="user_bell_idx"/>
													<div class="form-group">
													  <label for="formGroupExampleInput">제목</label>
													  <input type="text" class="form-control" id="alert-modal-title" name="alert-modal-title" placeholder="Example input">
													</div>
													<div class="form-row">
													  <div class="form-group col-md-6">
														<label for="alert-modal-date">날짜</label>
														<input type="text" class="form-control" id="alert-modal-date" name="alert-modal-date" placeholder="Date">
													  </div>
													  <div class="form-group col-md-6">
														<label for="alert-modal-name">이름</label>
														<input type="email" class="form-control" id="alert-modal-name" name="alert-modal-name" placeholder="Email">
													  </div>
													</div>
													<div class="form-group">
													  <label for="exampleFormControlTextarea1">내용</label>
													  <textarea class="form-control" id="alert-modal-content" name="alert-modal-content" rows="6"></textarea>
													</div>
												</form>
											</div>
											<script>
												function btn_modal_delete() {

													var d = $("#alert-modal-form").serialize();
													$.ajax({
														url:'/userBell/removeGitMsgUserBell' ,
														data: d ,
														type:'post' ,  
														dataType : "json" ,
														success:function(data){
															window.location.reload()
														} ,
														error:function(data) {
															alert("error");
														}
													});
												}
											</script>
												
											<div class="modal-footer">
											  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
											  <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="btn_modal_delete();">Delete</button>
											</div>

										</div>
									</div>
								</div>
								<%-- end 깃 출력 모달  --%>
						
                                
                                
                                
                                
                                
                                
                            </ul>
                            

                        </div>
                    </div>
                </div>
            </div>




<%--             다시알림 --%>
<!--             <div id="alert-list-bottom-main-review-alert"> -->
<!--                 <div class="container"> -->
<!--                     <div id="alert-session01" class="row mb-4" style="height: 6rem; background-color:#5f4db0;"> -->
<!--                         <div class="col-12 text-center my-auto" style="font-size: 1.3rem; color: white;"> -->
<!--                             <i class="far fa-bell"></i> -->
<!--                             <a> 팀원들이 보낸 알림을 다시 볼 수있는 곳입니다. </a> -->
<!--                         </div> -->
<!--                     </div> -->
<!--                     <div id="alert-session02" class="row"> -->
<%--                         <div class="col-1 text-center"> 오늘 </div>  --%>
<%--                         <div class="col-11 text-center"> </div> --%> 
<!--                         <div class="col-12 px-0"> -->
<!--                             <div class="list-group"> -->
<!--                                 <a class="list-group-item"> -->
<!--                                     오늘 -->
<!--                                 </a> -->
<!--                                 <a class="list-group-item list-group-item-action"> -->
<!--                                     <div class="numberCircle"> 1 </div> -->
<!--                                     asdfasdf -->
<!--                                 </a> -->
<!--                                 <a class="list-group-item list-group-item-action"> -->
<!--                                     <div class="numberCircle"> 1 </div> -->
<!--                                     asdfasdf -->
<!--                                 </a> -->
<!--                                 <a class="list-group-item list-group-item-action"> -->
<!--                                     <div class="numberCircle"> 1 </div> -->
<!--                                     asdfasdf -->
<!--                                 </a> -->
<!--                                 <a class="list-group-item list-group-item-action"> -->
<!--                                     <div class="numberCircle"> 1 </div> -->
<!--                                     asdfasdf -->
<!--                                 </a> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->


            <%-- 지움 --%>
            <div id="alert-list-bottom-main-remove-alert">
                <div class="container">

                    <div id="alert-session01" class="row mb-4" style="height: 6rem; background-color: #52b166;">
                        <div class="col-12 text-center my-auto" style="font-size: 1.3rem; color: white;">
                            <i class="far fa-bell"></i>
                            <a> 지운 알림을 찾아볼 수 있는 곳입니다. </a>
                        </div>
                    </div>

                    <div id="alert-session02" class="row" style="border: 1px solid lightgray; background-color: white;">
                        
                        <div class="col-12 px-0">

						<!-- 페이지 합치면서 추가 -->
						<ul id="list-group-item-collapse-before-deadline-git-deleted-alert" >

						<%-- 모달 에 데이터 입력 --%>
							<script>
								function btn_git_deleted_alert_row(num) {

									console.log($('#git-deleted-alert-data-'+num).val());
									$('#deleted_alert-modal-idx').val($('#git-deleted-alert-idx-'+num).val());
									
									var data = JSON.parse($('#git-deleted-alert-data-'+num).val());
									
									$('#deleted_alert-modal-title').val(data.title);
									$('#deleted_alert-modal-date').val(data.timestamp);
									$('#deleted_alert-modal-content').val(data.message);
									$('#deleted_alert-modal-name').val(data.email);
								}
							</script>
							
							
							<!-- 페이지 을 합치면서 페이지 변경으로 ajax 로 변경 -->
							<script>
								$(function() {

									$.ajax({
										url:'/userBell/gitDeletedAlerts',
										data: {'${_csrf.parameterName}' : '${_csrf.token}'},
										method:'post',  
										datatype: 'json',
										success:function(data){

											console.log(data);
											for (i in data) {
												
												var li = "<li class='list-group-item list-group-item-action'  data-toggle='modal' data-target='#deleted_alter_modal' onclick='btn_git_deleted_alert_row("+i+");'>"
												+"	<div class='alert-session02-category-item-1'>"
												+"		<i class='subNumberCircle fas fa-dot-circle'></i>"
												+"	</div>"

												+"	<div class='alert-session02-category-item-2'>"
												+"		<div> <a> "+data[i].project_title+"</a> </div>"

												+"		<input id='git-deleted-alert-idx-"+i+"' type='hidden' value='"+data[i].user_bell_idx+"'/>"
												+"		<input id='git-deleted-alert-data-"+i+"' type='hidden' value='"+data[i].user_bell_content+"'/>"

												+"		<div class='alert-session02-category-item-2-1'>"
												+"			<a class='alert-session02-category-item-2-1-1 mr-3'> <i class='far fa-calendar-check'></i> "+data[i].user_bell_occ_date+" </a>"
												+"			<a class='alert-session02-category-item-2-1-2'> <i class='fas fa-folder-open'></i> "+data[i].project_title+" </a>"
												+"		</div>"
												+"	</div>"
												+"</li>";

												$('#list-group-item-collapse-before-deadline-git-deleted-alert').append(li);
// 												$('#git-alert-numberCircle').text($("#list-group-item-collapse-before-deadline-git-alert li").length);
											}
										},
										error:function(data) {
											alert("error");
										}
									});
								});
									
							</script>	
							
							
							
							
<%-- 						   <c:forEach items="${gitDeletedAlerts}" var="gitAlert" varStatus="status"> --%>
<%-- 								<li class="list-group-item list-group-item-action"  data-toggle="modal" data-target="#deleted_alter_modal" onclick="btn_git_deleted_alert_row(${status.count});"> --%>
<!-- 									<div class="alert-session02-category-item-1"> -->
<!-- 										<i class="subNumberCircle fas fa-dot-circle"></i> -->
<!-- 									</div> -->
<!-- 									<div class="alert-session02-category-item-2"> -->
<%-- 										<div> <a> ${gitAlert.project_title}</a> </div> --%>

<%-- 										<input id="git-deleted-alert-idx-${status.count}" type="hidden" value='${gitAlert.user_bell_idx}'/> --%>
<%-- 										<input id="git-deleted-alert-data-${status.count}" type="hidden" value='${gitAlert.user_bell_content}'/> --%>

<!-- 										<div class="alert-session02-category-item-2-1"> -->
<%-- 											<a class="alert-session02-category-item-2-1-1 mr-3"> <i class="far fa-calendar-check"></i> ${gitAlert.user_bell_occ_date} </a> --%>

<%-- 											<a class="alert-session02-category-item-2-1-2"> <i class="fas fa-folder-open"></i> ${gitAlert.project_title} </a> --%>
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</li> -->
<%-- 							</c:forEach> --%>

							</ul>


                        </div>
                    </div>
                    
                    
					<%-- 모달 시작 --%>
					 <div id="deleted_alter_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleted_alter_modal_title" style="display: none;" aria-hidden="true">
						<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
							<div class="modal-content">

								<div class="modal-header">
								  <h5 class="modal-title text-center m-auto" id="deleted_alter_modal_title">알림</h5>
								  
								  <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								  </button>
								</div>

								<div class="modal-body mb-5 ml-3 mr-3">
									<form action="#" id="deleted_alert-modal-form">
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> 
										<input type="hidden" id="deleted_alert-modal-idx" name="user_bell_idx"/>
										<div class="form-group">
										  <label for="formGroupExampleInput">제목</label>
										  <input type="text" class="form-control" id="deleted_alert-modal-title" name="deleted_alert-modal-title" placeholder="Example input">
										</div>
										<div class="form-row">
										  <div class="form-group col-md-6">
											<label for="deleted_alert-modal-date">날짜</label>
											<input type="text" class="form-control" id="deleted_alert-modal-date" name="deleted_alert-modal-date" placeholder="Date">
										  </div>
										  <div class="form-group col-md-6">
											<label for="deleted_alert-modal-name">이름</label>
											<input type="email" class="form-control" id="deleted_alert-modal-name" name="deleted_alert-modal-name" placeholder="Email">
										  </div>
										</div>
										<div class="form-group">
										  <label for="exampleFormControlTextarea1">내용</label>
										  <textarea class="form-control" id="deleted_alert-modal-content" name="deleted_alert-modal-content" rows="6"></textarea>
										</div>
									</form>
								</div>
								<script>
									function btn_modal_recovery() {

										var d = $("#deleted_alert-modal-form").serialize();
										$.ajax({
											url:'/userBell/recoveryGitMsgUserBell' ,
											data: d ,
											type:'post' ,  
											dataType : "json" ,
											success:function(data){
												window.location.reload()
											} ,
											error:function(data) {
												alert("error");
											}
										});
									}
								</script>
									
								<div class="modal-footer">
								  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								  <button type="button" class="btn btn-success" data-dismiss="modal" onclick="btn_modal_recovery();">Recovery</button>
								</div>

							</div>
						</div>
					</div>
					<%-- end 깃 출력 모달  --%>                   
                    
                    

                </div>
            </div>


        </div>
    </div>
    
    
            

<!--             </main> -->
<%-- 	         end main --%>

<!--         </div> -->
<!--     </div> -->
<%--     end session --%>

    
<%-- <%-- Footer --%> 
<!-- <footer class="page-footer font-small indigo"> -->

<%-- 	<jsp:include page="/jsp/include/footer/footer.jsp"></jsp:include> --%>
<!-- </footer> -->
<%-- <%-- Footer --%> 



<!-- </body> -->
<!-- </html> -->