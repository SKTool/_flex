<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<% request.setAttribute("user_idx", request.getSession().getAttribute("user_idx"));%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="UTF-8">
<title>FLEX</title>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
</head>  
<body>
<!-- 사이트 메인 -->
	<jsp:include page="${headerJSP}"/>            
	
	<div class="container-fluid">  
		<div class="row p-0">
			<c:if test="${sidebar ne null}">
		  		<jsp:include page="/jsp/include/sidebar/sidebar.jsp"/>
			</c:if>	  
	  
		    <main id = "mainDiv" role="main" class="container-fluid ml-sm-auto px-0 pt-0">
				<jsp:include page="${content}"/>            
	    	</main>
		</div>
	</div>
</body>
</html>