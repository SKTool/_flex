<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script type="text/javascript">
$(function() {
	  $(".expand").on( "click", function() {
	    $(this).next().slideToggle(200);
	    $expand = $(this).find(">:first-child");
	    
	    if($expand.text() == "+") {
	      $expand.text("-");
	    } else {
	      $expand.text("+");
	    }
	  });
	  
	  $(".expand1").on( "click", function() {
		    $(this).next().slideToggle(200);
		    $expand = $(this).find(">:first-child");
		    
		  });

//	버튼 클릭 시 모달창 띄우는 스크립트
		$("#qnaform").hide();
		$("#qnaWrite").on("click", function() {
			$("#qnaform").show("slow");
		});
		
	});
</script>
<style type="text/css">
*, *:before, *:after {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}

#integration-list {
	font-family: 'Open Sans', sans-serif;
	width: 100%;
	margin: 0 auto;
	display: table;
}

#integration-list ul {
	padding: 0;
	margin: 10px 0;
	color: #555;
}

#integration-list ul>li {
	list-style: none;
	border-top: 1px solid #ddd;
	display: block;
	padding: 15px;
	overflow: hidden;
}

#integration-list ul:last-child {
	border-bottom: 1px solid #ddd;
}

#integration-list ul>li:hover {
	background: #efefef;
}

.expand {
	display: block;
	text-decoration: none;
	color: #555;
	cursor: pointer;
}

.expand1 {
	display: block;
	text-decoration: none;
	color: #555;
	cursor: pointer;
}

h3, h4 {
	padding: 0;
	margin: 0;
	font-size: 17px;
	font-weight: 400;
}

span {
	font-size: 14px;
	margin: 10px;
}

#left, #right {
	display: table;
}

#sup {
	display: table-cell;
	vertical-align: middle;
	width: 90%;
}

.detail {
	margin: 20px;
	display: none;
	line-height: 15px;
}

.right-arrow {
	width: 10px;
	height: 100%;
	float: right;
	font-weight: bold;
	font-size: 20px;
}

.icon {
	height: 75px;
	width: 75px;
	float: left;
	margin: 0 15px 0 0;
}

.button {
	width: 20px;
	float: right;
	font-size: 10px;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<div id="main-sidebar" class="header" style="background: #4f5f6f;">
				<jsp:include page="/jsp/include/sidebar/sidebar-adminPage.jsp"></jsp:include>
			</div>
		</sec:authorize>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">


			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">
				<div
					style="background: white; width: 94%; height: 94%; margin-left: 3%; margin-top: 2%;">




					<!-- 권한 확인 -->

					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<sec:authentication property="principal" var="admin" />
					</sec:authorize>
					<sec:authorize access="hasRole('ROLE_USER')">
						<sec:authentication property="principal" var="user" />
					</sec:authorize>

					<!-- 문의글 작성자와 현재 로그인 된 회원 확인하기 위한 값  -->
					<sec:authentication property="name" var="login_user_ID" />

					<div class="container" style="padding: 20px; margin-top: 20px;">
						<h2 style="text-align: center;" id="Faq">FAQ</h2>
						<div class="pill-nav"
							style="text-align: center; margin: 10px; padding: 10px; text-decoration: none;">
							<a style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/adminBoard/adminBoardList">NOTICE</a> <a
								style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList">FAQ</a> <a style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList#QNA">QNA</a>
						</div>
						<!-- 관리자 권한 있으면 faq 등록 가능 -->
						<c:if test="${admin ne null }">
							<div style="display: inline;">
								<button class="btn btn-info btn-sm" style="float: right;"
									onclick="location.href='/adminBoard/write'">FAQ 작성</button>
							</div>
						</c:if>

						<!-- faq 시작 -->
						<div id="integration-list">
							<ul>
								<c:forEach items="${FaqList}" var="faq">
									<!-- 글 1개 -->
									<li>
										<!-- 제목 --> <a class="expand">
											<div class="right-arrow">+</div>
											<div>
												<h2>
													<img style="display: inline-block; margin-right: 20px"
														src="/img/Q.png">${faq.admin_board_title}</h2>
											</div>
									</a> <!-- 제목 끝 --> <!-- 내용 -->
										<div class="detail">
											<div id="right">
												<div id="sup">
													<div
														style="font-size: 10px; color: gray; text-align: right;">
														<!-- 관리자일 경우, 삭제 수정 가능 -->
														<c:if test="${admin ne null}">
															<button type="button"
																id="deleteButton${faq.admin_board_idx}">삭제</button>
															<button type="button" id="modifyButton"
																onclick="location.href='/adminBoard/modify?num=${faq.admin_board_idx}'">수정</button>
														</c:if>
													</div>
													<span><img
														style="display: inline-block; margin-right: 20px"
														src="/img/A.png">${faq.admin_board_content}</span>
												</div>
											</div>
										</div> <!-- 내용 끝-->

									</li>
									<!-- 글 1개 끝 -->


									<!-- 관리자일 경우, 삭제 모달-->
									<script type="text/javascript">
								$(function() {										
									$("#deleteButton${faq.admin_board_idx}").on("click", function() {
									const swalWithBootstrapButtons = Swal.mixin({
										  confirmButtonClass: 'btn btn-success',
										  cancelButtonClass: 'btn btn-danger',
										  buttonsStyling: false,
										})
											swalWithBootstrapButtons.fire({
											  title:'삭제',
											  text: "해당 게시글을 삭제하시겠습니까?",
											  type: 'warning',
											  showCancelButton: true,
											  confirmButtonText: '삭제',
											  cancelButtonText: '취소',
											  reverseButtons: true
											}).then((result) => {
											  if (result.value) {
												  $.ajax({
														url:"/adminBoard/delete",
														data:{"num" : ${faq.admin_board_idx}},
														type:"get",
														success : function(data){
														    swalWithBootstrapButtons.fire(
														      '삭제 완료',
														      '${faq.admin_board_title} 글이 삭제되었습니다. :) ',
														      'success'
														    )
														    location.href = "faqList";
														}
												 });
											  } else if (
											    // Read more about handling dismissals
											    result.dismiss === Swal.DismissReason.cancel
											  ) {
											    swalWithBootstrapButtons.fire(
											      '취소',
											      '현재 페이지로 돌아갑니다.',
											      'error'
											    )
											  }
											})
									});
								});
								</script>

								</c:forEach>
							</ul>
						</div>


						<!-- faq 끝 -->

						<h2 style="text-align: center; margin-top: 20px;" id="QNA">QNA</h2>
						<div class="pill-nav"
							style="text-align: center; margin: 10px; padding: 10px; text-decoration: none;">
							<a style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/adminBoard/adminBoardList">NOTICE</a> <a
								style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList">FAQ</a> <a style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList#QNA">QNA</a>
						</div>
						<div style="display: inline;">
							<button style="float: right;" id="qnaWrite"
								class="btn btn-info btn-sm">질문하기</button>
						</div>

						<br>


						<!-- 문의 시작 -->
						<div id="integration-list">

							<div style="float: right;">
								<form name="qnaWrite" action="/qna/write" method="post"
									id="qnaform">
									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}">
									<div style="margin: 10px; text-align: right;">
										<span>게시글 공개 여부 설정</span> <input type="radio" name="status"
											id="1" value="1"> <label for="1"><i
											class="fas fa-lock-open"></i></label> <input type="radio"
											name="status" id="2" value="2"> <label for="2"><i
											class="fas fa-lock"></i></label> <br>
									</div>
									<div class="container">
										<!-- Summernote -->
										<textarea name="qna_content" class="summernote"
											placeholder="관리자에게 궁금한 내용을 물어보세요 :) "> </textarea>
										<script type="text/javascript">
									$(function() {
										$('.summernote').summernote({
											lang : 'ko-KR',
											height : 180, // 기본 높이값
											focus : true
										// 페이지가 열릴때 포커스를 지정
										});
									});
								</script>

										<div style="padding: 15px; margin: 10px; text-align: right;">
											<input type="submit" class="btn btn-info btn-sm" value="저장">
											<input type="reset" class="btn btn-danger btn-sm" value="취소">
										</div>
									</div>
								</form>
							</div>

							<ul>
								<c:forEach items="${QnaList}" var="qna">
									<li><a class="expand1">
											<div class="button">
												<!-- 글쓴이 수정 삭제 버튼 보이기 -->
												<c:if
													test="${qna.user_id eq login_user_ID || admin ne null}">
													<i style="font-size: 13px;" class="fas fa-trash-alt"
														id="deleteQnaButton${qna.qna_idx}"></i>
												</c:if>
											</div> <!-- 삭제 모달--> <script type="text/javascript">
												$(function() {										
													$("#deleteQnaButton${qna.qna_idx}").on("click", function() {
													const swalWithBootstrapButtons = Swal.mixin({
														  confirmButtonClass: 'btn btn-success',
														  cancelButtonClass: 'btn btn-danger',
														  buttonsStyling: false,
														})
															swalWithBootstrapButtons.fire({
															  title:'삭제',
															  text: "문의글을 삭제하시겠습니까?",
															  type: 'warning',
															  showCancelButton: true,
															  confirmButtonText: '삭제',
															  cancelButtonText: '취소',
															  reverseButtons: true
															}).then((result) => {
															  if (result.value) {
																  $.ajax({
																		url:"/qna/delete",
																		data:{"num" : ${qna.qna_idx}},
																		type:"get",
																		success : function(data){
																		    swalWithBootstrapButtons.fire(
																		      '삭제 완료',
																		      '${qna.qna_content}가 삭제되었습니다. :) ',
																		      'success'
																		    )
																		    location.href = "faqList";
																		}
																 });
															  } else if (
															    // Read more about handling dismissals
															    result.dismiss === Swal.DismissReason.cancel
															  ) {
															    swalWithBootstrapButtons.fire(
															      '취소',
															      '현재 페이지로 돌아갑니다.',
															      'error'
															    )
															  }
															})
													});
												});
												</script>

											<div>
												<h4>
													<!-- 공개 문의는 바로 보일 수 있도록 처리 -->
													<c:if test="${qna.status eq 1}">
													${qna.qna_content}
												</c:if>

													<!-- 비공개 문의일 경우 -->
													<c:if test="${qna.status eq 2 }">
														<!-- 현재 로그인 유저&문의 유저 아이디가 같거나, 관리자인 경우 모두 보일 수 있도록 처리 -->
														<c:if
															test="${qna.user_id eq login_user_ID  || admin ne null}">
															<div>${qna.qna_content}</div>
														</c:if>

														<!-- 아이디 다르거나 관리자 아닐 경우 비밀글은 alert창 띄우기 -->
														<c:if
															test="${qna.user_id ne login_user_ID && admin eq null }">
															<span id="secret${qna.qna_idx}">비밀글입니다. </span>
															<i style="font-size: 13px;" class="fas fa-unlock"></i>
															<script type="text/javascript">
														$(function(){ 
															$("#secret"+${qna.qna_idx}).on( "click", function() {
																Swal.fire({
																	text : '비공개 문의 내역은 작성자 본인만 확인할 수 있습니다.',
																	type : 'error',
																	confirmButtonText : 'Cool'
																})
															});
														});
													</script>
														</c:if>
													</c:if>
												</h4>
											</div>
									</a> <!--공개 문의/로그인+글쓴이 아이디 같은 경우/관리자 로그인 상태인 경우 답변 보일 수 있도록 처리--> <c:if
											test="${qna.status eq 1 || qna.user_id eq login_user_ID || admin ne null}">
											<!-- 열리는 부분 -->
											<div class="detail">
												<div id="left"
													style="width: 15; float: left; padding-left: 20px;">
													<div id="sup">
														<h4> <c:if test="${qna.qna_answer_idx eq null}">
																<c:if test="${admin ne null }">
																	<button id="qnaAnsweWrite" class="btn btn-info btn-m"
																		onclick="location.href='/qna/writeAnswer?num=${qna.qna_idx}'">
																		답변 등록</button>
																</c:if>
															</c:if> <c:if test="${qna.qna_answer_idx ne null}">
																<img style="display: inline-block; margin-right: 20px"
																	src="/img/admin_a.png">
															</c:if>
														</h4>
													</div>
												</div>

												<div id="right"
													style="width: 88%; padding-left: 30px; dispaly: inline-block;">
													<div id="sup">
														<div style="padding: 8px;">
															<h4> <c:if test="${qna.qna_answer_idx ne null}">
														${qna.qna_answer_content}
														
															<c:if test="${admin ne null }">
																		<div class="button">
																			<i style="font-size: 13px;" class="fas fa-edit"
																				onclick="location.href='/qna/modifyAnswer?num=${qna.qna_idx}'"></i>
																		</div>
																		<div class="button">
																			<i style="font-size: 13px;" class="fas fa-trash-alt"
																				id="deleteQnaButton${qna.qna_answer_idx}"></i>
																		</div>
																	</c:if>
																	<script type="text/javascript">
															$(function() {										
																$("#deleteQnaButton${qna.qna_answer_idx}").on("click", function() {
																const swalWithBootstrapButtons = Swal.mixin({
																	  confirmButtonClass: 'btn btn-success',
																	  cancelButtonClass: 'btn btn-danger',
																	  buttonsStyling: false,
																	})
																		swalWithBootstrapButtons.fire({
																		  title:'삭제',
																		  text: "해당 답변을 삭제하시겠습니까?",
																		  type: 'warning',
																		  showCancelButton: true,
																		  confirmButtonText: '삭제',
																		  cancelButtonText: '취소',
																		  reverseButtons: true
																		}).then((result) => {
																		  if (result.value) {
																			  $.ajax({
																					url:"/qna/deleteAnswer",
																					data:{"num" : ${qna.qna_answer_idx}},
																					type:"get",
																					success : function(data){
																					    swalWithBootstrapButtons.fire(
																					      '삭제 완료',
																					      'success'
																					    )
																					    location.href = "faqList";
																					}
																			 });
																		  } else if (
																		    // Read more about handling dismissals
																		    result.dismiss === Swal.DismissReason.cancel
																		  ) {
																		    swalWithBootstrapButtons.fire(
																		      '취소',
																		      '현재 페이지로 돌아갑니다.',
																		      'error'
																		    )
																		  }
																		})
																});
															});
															</script>
																</c:if> <c:if test="${qna.qna_answer_idx eq null}">
																	답변이 등록되지 않았습니다. 조금만 기다려 주세요. :)
															</c:if>
															</h4>

														</div>
													</div>
												</div>

											</div>
										</c:if></li>


									<!-- 								 작성자 문의 삭제시 필요한 모달 -->
									<%-- 								<div class="modal fade" id="checkPass${qna.qna_idx}" --%>
									<!-- 									role="dialog"> -->
									<!-- 									<div class="modal-dialog modal-sm"> -->
									<!-- 										<div class="modal-content" -->
									<!-- 											style="margin: 10px; text-align: center; padding: 15px"> -->
									<!-- 											<form action="/qna/checkPass" method="post"> -->
									<!-- 												<div style="margin: 10px; text-align: center; padding: 2px"> -->
									<%-- 													<input type="hidden" name="${_csrf.parameterName}" --%>
									<%-- 														value="${_csrf.token}"> <input type="hidden" --%>
									<%-- 														name="qna_idx" value="${qna.qna_idx}"> <input --%>
									<%-- 														type="hidden" name="qna_idx" value="${qna.user_idx }"> --%>
									<!-- 													<input type="password" placeholder="비밀번호를 입력하세요." -->
									<!-- 														name="qna_pw"> -->
									<!-- 												</div> -->
									<!-- 												<div> -->
									<!-- 													<input type="submit" value="확인" class="btn btn-info btn-m"> -->
									<!-- 													<button type="button" class="btn btn-info btn-m" -->
									<!-- 														data-dismiss="modal">닫기</button> -->
									<!-- 												</div> -->
									<!-- 											</form> -->
									<!-- 										</div> -->
									<!-- 									</div> -->
									<!-- 								</div> -->
								</c:forEach>
							</ul>

							<!-- 페이징 처리 -->
							<div style="text-align: center;">
								<c:if test="${startPage !=1}">
									<a href="faqList?page=1">[처음]</a>
									<a href="faqList?page=${startPage-1}">[이전]</a>
								</c:if>
								<c:forEach var="pageNum" begin="${startPage}"
									end="${endPage < totalPage ? endPage : totalPage}">
									<c:choose>
										<c:when test="${pageNum == page}">
											<b>[${pageNum}]</b>
										</c:when>
										<c:otherwise>
											<a href="faqList?page=${pageNum}">[${pageNum}]</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<c:if test="${totalPage > endPage }">
									<a href="faqList?page=${endPage+1}">[다음]</a>
									<a href="faqList?page=${totalPage}">[마지막]</a>
								</c:if>
							</div>

						</div>
					</div>
				</div>
				<!-- 문의 끝 -->




			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->


	<!-- <!-- Footer -->
	<!-- <footer class="page-footer font-small indigo"> -->
	<%-- 	<jsp:include page="../include/footer/footer.jsp"></jsp:include> --%>
	<!-- </footer> -->
	<!-- <!-- Footer -->

</body>
</html>