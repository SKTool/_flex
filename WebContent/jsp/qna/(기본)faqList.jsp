<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../include/top/top.jsp"></jsp:include>
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<script type="text/javascript">
	$(function() {
// 		버튼 클릭 시 모달창 띄우는 스크립트
		$("#qnaform").hide();
		$("#qnaWrite").on("click", function() {
			$("#qnaform").show("slow");
		});
		
		
	});
</script>
<style type="text/css">
.card-header {
	background: none;
	border-top-left-radius: none;
	border-bottom-left-radius: none;
}

/* hover 부분은 수정 필요 */
.card-header:hover {
	background-color: #cccccc;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- navbar -->
	<jsp:include page="../include/header/header.jsp"></jsp:include>
	<!-- end navbar -->
	<!-- session -->
	<div id="session" class="container-fluid"
		style="padding: 0; background-color: #eeeeee">
		<div class="row">
			<!-- main -->
			<main id="session02" class="col-12">
			
			<!-- 권한 확인 --> 
			<sec:authorize
				access="hasRole('ROLE_ADMIN')">
				<sec:authentication property="principal" var="admin" />
			</sec:authorize> <sec:authorize access="hasRole('ROLE_USER')">
				<sec:authentication property="principal" var="user" />
			</sec:authorize> 
			
			<!-- 문의글 작성자와 현재 로그인 된 회원 확인하기 위한 값  --> <sec:authentication
				property="name" var="login_user_ID" />

			<div class="container">
				<h3 style="text-align: center;" id="Faq">FAQ</h3>
				<div class="pill-nav"
					style="text-align: center; text-decoration: none;">
					<a style="margin-left: 10px; margin-right: 10px; color: black;"
						href="/adminBoard/adminBoardList">NOTICE</a> <a
						style="color: gray;">|</a> <a
						style="margin-left: 10px; margin-right: 10px; color: black;"
						href="/qna/faqList">FAQ</a> <a style="color: gray;">|</a> <a
						style="margin-left: 10px; margin-right: 10px; color: black;"
						href="/qna/faqList#QNA">QNA</a>
				</div>
				<br>
				<br>

				<!-- 관리자 권한 있으면 faq 등록 가능 -->
				<div>
					<c:if test="${admin ne null }">
						<button onclick="location.href='/adminBoard/write'">Faq
							작성</button>
					</c:if>
				</div>

				<div>
					<!-- faq 리스트 시작 -->
					<div id="accordion">
						<div class="card">
							<c:forEach items="${FaqList}" var="faq">
								<div class="card-header">
									<a id="${faq.admin_board_idx}" class="card-link"
										data-toggle="collapse" href="#collapse${faq.admin_board_idx}"
										aria-expanded="false"> ${faq.admin_board_title} </a>
								</div>

								<!-- faq 내용 상세 보기 -->
								<div id="collapse${faq.admin_board_idx}" class="collapse"
									data-parent="#accordion">
									<div class="card-body" style="padding: 5px; margin: 3px;">
										<div style="font-size: 10px; color: gray; text-align: right;">
											<c:if test="${admin ne null}"> 
										작성자 : ${faq.user_name}
										<!-- 관리자일 경우, 삭제 수정 가능 -->
												<button type="button"
													id="deleteButton${faq.admin_board_idx}">삭제</button>
												<button type="button" id="modifyButton"
													onclick="location.href='/adminBoard/modify?num=${faq.admin_board_idx}'">수정</button>
											</c:if>
										</div>

										<div style="padding: 20px; margin: 6px;">
											${faq.admin_board_content}</div>
									</div>

									<!-- 관리자일 경우, 삭제 모달-->
									<script type="text/javascript">
								$(function() {										
									$("#deleteButton${faq.admin_board_idx}").on("click", function() {
									const swalWithBootstrapButtons = Swal.mixin({
										  confirmButtonClass: 'btn btn-success',
										  cancelButtonClass: 'btn btn-danger',
										  buttonsStyling: false,
										})
											swalWithBootstrapButtons.fire({
											  title:'삭제',
											  text: "해당 게시글을 삭제하시겠습니까?",
											  type: 'warning',
											  showCancelButton: true,
											  confirmButtonText: '삭제',
											  cancelButtonText: '취소',
											  reverseButtons: true
											}).then((result) => {
											  if (result.value) {
												  $.ajax({
														url:"/adminBoard/delete",
														data:{"num" : ${faq.admin_board_idx}},
														type:"get",
														success : function(data){
														    swalWithBootstrapButtons.fire(
														      '삭제 완료',
														      '${faq.admin_board_title} 글이 삭제되었습니다. :) ',
														      'success'
														    )
														    location.href = "faqList";
														}
												 });
											  } else if (
											    // Read more about handling dismissals
											    result.dismiss === Swal.DismissReason.cancel
											  ) {
											    swalWithBootstrapButtons.fire(
											      '취소',
											      '현재 페이지로 돌아갑니다.',
											      'error'
											    )
											  }
											})
									});
								});
								</script>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>

			<br>
			<h3 style="text-align: center;" id="QNA">QNA</h3>
			<div class="pill-nav"
				style="text-align: center; text-decoration: none;">
				<a style="margin-left: 10px; margin-right: 10px; color: black;"
					href="/adminBoard/adminBoardList">NOTICE</a> <a
					style="color: gray;">|</a> <a
					style="margin-left: 10px; margin-right: 10px; color: black;"
					href="/qna/faqList">FAQ</a> <a style="color: gray;">|</a> <a
					style="margin-left: 10px; margin-right: 10px; color: black;"
					href="/qna/faqList#QNA">QNA</a>
			</div>
			<br>

			<div class="container">
				

				<div id="accordion">
					<div class="card">

						
							<div class="card-header">
								<a class="card-link" data-toggle="collapse"
									href="#collapse${qna.qna_idx}" aria-expanded="false"> 
									<!-- 공개 문의는 바로 보일 수 있도록 처리 -->
									<c:if test="${qna.status eq 1}">
										
										${qna.qna_content}
									</c:if> 
								<!-- 비공개 문의일 경우 -->
								
								 <c:if test="${qna.status eq 2 }">
										<!-- 현재 로그인 유저&문의 유저 아이디가 같거나, 관리자인 경우 모두 보일 수 있도록 처리 -->
										<c:if test="${qna.user_id eq login_user_ID  || admin ne null}">
											<div>${qna.qna_content}</div>
										</c:if>

										<!-- 아이디 다르거나 관리자 아닐 경우 비밀글은 alert창 띄우기 -->
										<c:if test="${qna.user_id ne login_user_ID && admin eq null }">
											<span id="secret${qna.qna_idx}">비밀글입니다. </span>
											<i class="fas fa-unlock"></i>
											<script type="text/javascript">
											$(function(){ 
												$("#secret"+${qna.qna_idx}).on( "click", function() {
													Swal.fire({
														text : '비공개 문의 내역은 작성자 본인만 확인할 수 있습니다.',
														type : 'error',
														confirmButtonText : 'Cool'
													})
												});
											});
										</script>
										</c:if>
									</c:if>
								</a>
							</div>
							
							
								
						<!-- 삭제했음 -->
					</div>
				</div>


				

		

	
            </main>
	        <!-- end main -->

        </div>
    </div>
    <!-- end session -->

    
<!-- <!-- Footer --> -->
<!-- <footer class="page-footer font-small indigo"> -->
<%-- 	<jsp:include page="../include/footer/footer.jsp"></jsp:include> --%>
<!-- </footer> -->
<!-- <!-- Footer --> -->
</body>
</html>