<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<!--bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.css">
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- 한글 처리 -->
<script src="${contextPath}/js/summernote-ko-KR.js"></script>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<style type="text/css">
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<sec:authentication property="principal" var="admin" />
	</sec:authorize>


	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">
				<div
					style="background: white; width: 94%; height: 94%; margin-left: 3%; margin-top: 2%;">

					<div class="w3-container"
						style="height: 100%; width: 80%; margin-left: 10%; padding: 15px; margin:">
						
						<form name="modifyAnswer" action="/qna/modifyAnswer" method="post">

							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}"> <input type="hidden"
								name="qna_answer_idx" value="${qna.qna_answer_idx}">
								
							<table class="w3-table w3-bordered">
								<colgroup>
									<col style="width: 130px;">
									<col style="width: auto;">
								</colgroup>

								<tr>
									<th scope="row">문의 고객</th>
									<td>${qna.user_name}</td>
								</tr>
								<tr>
									<th scope="row">문의 내용</th>
									<td>${qna.qna_content}</td>
								</tr>
								<tr>
									<td colspan="2">

										<div class="detail" style="padding: 10px; margin: 10px;">
											<textarea name="qna_answer_content" class="summernote">
											${qna.qna_answer_content}
											</textarea>


											<script type="text/javascript">
												$(function() {
													$('.summernote')
															.summernote({
																lang : 'ko-KR',
																height : 300, // 기본 높이값
																focus : true
															// 페이지가 열릴때 포커스를 지정
															});
												});
											</script>

										</div>
									</td>
								</tr>

							</table>
						<div style="width: 100%; text-align: right;">
							<input type="submit" class="btn btn-info btn-m" id="submit"
								value="저장"> <input type="reset"
								class="btn btn-danger btn-m" value="취소"
								style="margin-left: 5px;">
						</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->





</body>
</html>