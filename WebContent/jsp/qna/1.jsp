<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.css">
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- 한글 처리 -->
<script src="${contextPath}/js/summernote-ko-KR.js"></script>

<script type="text/javascript">
	$(function() {
		$('.summernote').summernote({
			lang : 'ko-KR',
			height : 300, // 기본 높이값
			focus : true
		// 페이지가 열릴때 포커스를 지정
		});
	});
</script>
<style>
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}
</style>
</head>
<body>
<body>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">

				<div style="margin: 20px; padding: 20px;">
					<form name="writeAnswer" action="/qna/writeAnswer" method="post">
						<div class="container">
							<fieldset>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}">
								<div class="form-group">
									<label for="inputlg">문의 고객</label> 
									<span>${qna.user.name}</span>
								</div>
								<div class="form-group">
									<label for="inputlg">문의 내용</label> ${qna.qna_content}
								</div>
								<div class="form-group">
									<label for="inputlg"> 담장자</label> ${admin}
								</div>
								<div class="form-group">
									<label for="inputlg">문의 내용</label> ${qna.qna_content}
								</div>
								<div class="form-group">
									<label for="inputlg">답변 내용</label> 
									<textarea name="qna_answer_content"
												class="summernote"></textarea>
								</div>
							

								<div style="text-align: right; margin: 10px; padding: 10px;">
									<input
											type="submit" class="btn btn-info btn-m" id="submit"
											value="저장"><input type="reset"
											class="btn btn-danger btn-m" value="취소"
											style="margin-left: 5px;">
								</div>
							</fieldset>
						</div>
					</form>
				</div>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->

<!-- 	<!-- MainLayout_Wrap --> -->
<!-- 	<div id="mainLayout-wrap" class="wrap"> -->

<!-- 		<!-- Header --> -->
<!-- 		<div id="main-header" class="header"> -->
<%-- 			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include> --%>
<!-- 		</div> -->
<!-- 		<!-- End Header --> -->

<!-- 		<!-- Main_Wrap --> -->
<!-- 		<div id="main_wrap" class="main_wrap"> -->
<%-- 			<sec:authorize access="hasRole('ROLE_ADMIN')"> --%>
<%-- 				<sec:authentication property="principal" var="admin" /> --%>
<%-- 			</sec:authorize> --%>
<!-- 			<div class="main" style="background: #f1f1f1;"> -->
<!-- 				<div -->
<!-- 					style="background: white; width: 94%; height: 94%; margin-left: 3%; margin-top: 2%;"> -->

<!-- 					<div class="w3-container" -->
<!-- 						style="height: 100%; width: 80%; margin-left: 10%; padding: 15px; margin:"> -->

<!-- 						<fieldset> -->
<!-- 							<form name="writeAnswer" action="/qna/writeAnswer" method="post"> -->

<%-- 								<input type="hidden" name="${_csrf.parameterName}" --%>
<%-- 									value="${_csrf.token}"> <input type="hidden" --%>
<%-- 									name="qna_idx" value="${qna.qna_idx}"> --%>

<!-- 								<table> -->
<!-- 									<tr> -->
<!-- 										<th>문의 고객</th> -->
<%-- 										<th>${qna.user.name}</th> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<td colspan="2" style="text-align: center;">문의 내용</td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<%-- 										<td colspan="2" style="text-align: center;">${qna.qna_content}</td> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<th>담당자</th> -->
<%-- 										<th>${admin}</th> --%>
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<td colspan="2"><textarea name="qna_answer_content" -->
<!-- 												class="summernote"></textarea></td> -->
<!-- 									</tr> -->
<!-- 									<tr> -->
<!-- 										<th style="text-align: right;" colspan="2"><input -->
<!-- 											type="submit" class="btn btn-info btn-m" id="submit" -->
<!-- 											value="저장"><input type="reset" -->
<!-- 											class="btn btn-danger btn-m" value="취소" -->
<!-- 											style="margin-left: 5px;"></th> -->

<!-- 									</tr> -->
<!-- 								</table> -->
<!-- 							</form> -->
<!-- 						</fieldset> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
</body>


</html>