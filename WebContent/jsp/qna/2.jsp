<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% request.setAttribute("contextPath", request.getContextPath()); %>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<!--bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.css">
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<!-- 한글 처리 -->
<script src="${contextPath}/js/summernote-ko-KR.js"></script>

<script type="text/javascript">
	$(function() {
		$('.summernote').summernote({
			lang : 'ko-KR',
			height : 300, // 기본 높이값
			focus : true
		// 페이지가 열릴때 포커스를 지정
		});
	});
</script>
<style>
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}
</style>
</head>
<body>
<body>

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<sec:authentication property="principal" var="admin" />
	</sec:authorize>
	
	<div class="container">
		<fieldset>
			<legend>문의 답변 작성</legend>
			<form name="modifyAnswer" action="/qna/modifyAnswer" method="post">
				
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> 
				<input type="hidden" name="qna_answer_idx" value="${qna.qna_answer_idx}">

				<table>
					<tr>
						<th>문의 고객</th>
						<th>${qna.user.name}</th>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;">문의 내용</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;">${qna.qna_content}</td>
					</tr>
					<tr>
						<td colspan="2">
							<textarea name="qna_answer_content" class="summernote">
							${qna.qna_answer_content}
							</textarea>
						</td>
					</tr>
					<tr>
						<th><input type="submit" class="btn btn-primary" id="submit" value="저장"></th>
						<th><input type="reset" class="btn btn-primary" value="취소"></th>
					</tr>
				</table>
			</form>
		</fieldset>
	</div>
</body>


</html>