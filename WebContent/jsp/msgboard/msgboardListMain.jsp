<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% request.setAttribute("contextPath", request.getContextPath());%>
<!DOCTYPE html>
<script>
		function myFunction() { // 	제목으로 바로 검색하는 함수
			var input, filter, table, tr, td, i;
			input = document.getElementById("Searchtitle");
			filter = input.value.toUpperCase();
			table = document.getElementById("msgboard");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
</script> 
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%;  overflow: auto;">
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">FreeBoard</h3>
	<span style="padding-left: 5%; color: gray;">모든 사람들이 자유롭게 이용가능한 게시판</span>
<!-- 		<div style="text-align: center; padding:8%;"> -->
		<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">
			<hr>
			<br>
						<div>
						<input class="w3-input w3-border w3-padding" type="text"
							placeholder="Title 키워드로 검색" id="Searchtitle"
							onkeyup="myFunction()" style="width: 50%;">
						</div>
			<table id="msgboard" class="w3-table w3-bordered" style="text-align: center;">
				<tr>
					<td colspan="2" style="text-align: right;">
					</td>
					<td colspan="5" style="text-align: right;">
						<button class="btn btn-info" type="button" onclick="location.href='msgboardWrite'" style="margin-right: 20px;">작성</button>
					</td>
				</tr>
				<tr>
					<td style="width: 10%; text-align: center;">NickName</td>
					<td style="color: #8A0886; width: 50%; text-align: center;">Title</td>
					<td style="width: 25%; text-align: center;">Date Created</td>
					<td style="width: 15%; text-align: center;">Read Count</td>
				</tr>
	
				<c:forEach items="${msgboardList}" var="msgboard">
					<tr>
						<td style="text-align: center;">${msgboard.msgboard_nick}</td>
						<td style="color: #8A0886; text-align: center;"><a href="msgboardView?msgboard_idx=${msgboard.msgboard_idx}">
								${msgboard.msgboard_title}<c:if
									test="${msgboard.msgboard_rcount != 0}"></c:if>
						</a></td>
						<td style="text-align: center;">${msgboard.in_date}</td>
						<td style="text-align: center;">${msgboard.msgboard_read_count}</td>
					</tr>
				</c:forEach>
				<tr>
					<td colspan="5">
					<div style="text-align: center;">
						<c:if test="${startPage !=1}">
								<a href="/msgboard/msgboardList?page=1">[처음]</a>
								<a href="/msgboard/msgboardList?page=${startPage-1}">[이전]</a>
							</c:if> <c:forEach var="pageNum" begin="${startPage}"
								end="${endPage < totalPage ? endPage : totalPage}">
								<c:choose>
									<c:when test="${pageNum == page}">
										<b>[${pageNum}]</b>
									</c:when>
									<c:otherwise>
										<a href="/msgboard/msgboardList?page=${pageNum}">[${pageNum}]</a>
									</c:otherwise>
								</c:choose>
							</c:forEach> <c:if test="${totalPage > endPage }">
							<a href="/msgboard/msgboardList?page=${endPage+1}">[다음]</a>
							<a href="/msgboard/msgboardList?page=${totalPage}">[마지막]</a>
						</c:if>
					</div>
				</tr>
			</table>
		</div>
	</div>
</div>
