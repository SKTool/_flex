<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript" src="${contextPath}/js/msgboard/msgboardWrite.js"></script>
  <script type="text/javascript">
	//form 요소 선택해서 submit 이벤트가 발생하면
	//유효성검사 하기 :boardCheck()
	$(function(){		
		$("#msgboardWrite").on("submit",function(){
			if(!msgboardCheck("msgboardWrite")){
				return false;
			}
		});
	});
</script>
<meta charset="UTF-8">
<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%;  overflow: auto;">
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">FreeBoard Write</h3>
	<span style="padding-left: 5%; color: gray;">자유게시판 게시글 작성.</span>
<!-- 		<div style="text-align: center; padding:8%;"> -->
		<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">
			<hr>
			<h3 style="color: black; text-align: center; width: 50%; margin: 0 auto;">${msgboard.msgboard_title}</h3>
			<br>
			<form action="msgboardWrite" id="msgboardWrite" method="post"
				enctype="multipart/form-data" style="text-align: center; padding-left: 15%; padding-right: 15%;">
					<br>
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}"> <input type="hidden"
						name="msgboard_idx" value="${msgboard.msgboard_idx }">
					<div class="form-group">
						<label for="inputlg" class="w3-text w3-white w3-border">NickName</label>
						<input class="msgboard_nick input-lg" type="text"
							style="width: 20%; hight: 200px;" name="msgboard_nick"
							placeholder="사용할 닉네임을 입력하세요.">
					</div>
					<div class="form-group">
						<label for="inputlg" class="w3-text w3-white w3-border">Title</label>
						<input class="msgboard_title input-lg" type="text"
							style="width: 40%; hight: 200px;" name="msgboard_title"
							placeholder="제목을 입력하세요.">
					</div>

					<textarea name="msgboard_content" id="msgboard_content"
						class="msgboard_content"></textarea>
					<div class="container"
						style="text-align: center; margin-top: 15px;">
						<input class="btn btn-sm btn-info" type="submit" class="msgboard_content" id="submit" value="등록" style="margin-right: 10px; margin-bottom: 15px;"> 
							<input class="btn btn-sm btn-info" type="button" value="목록" onclick="location.href='msgboardList'" style="margin-right: 10px; margin-bottom: 15px;">
					</div>
			</form>
			<script>
				$(document).ready(function() {
					$('#msgboard_content').summernote();
				});
				$('#msgboard_content').summernote({
					lang : 'ko-KR',
					height : 300, // 기본 높이값
					focus : true
				});
			</script>
		</div>
	</div>
	</div>