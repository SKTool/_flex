<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript" src="${contextPath}/js/msgboard/msgboardWrite.js"></script>
  <script type="text/javascript">
	//form 요소 선택해서 submit 이벤트가 발생하면
	//유효성검사 하기 :boardCheck()
	$(function(){		
		$("#msgboardModify").on("submit",function(){
			if(!msgboardCheck("msgboardModify")){
				return false;
			}
		});
	});
</script>
<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%;  overflow: auto;">
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">FreeBoard Modify</h3>
	<span style="padding-left: 5%; color: gray;">자유게시판 게시글 수정</span>
		<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">
			<hr>
			<br>
			
		<form action="msgboardModify" id="msgboardModify" method="post"
			enctype="multipart/form-data" style="text-align: center; padding-left: 15%; padding-right: 15%;">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}"> <input type="hidden"
					name="msgboard_idx" value="${msgboard.msgboard_idx }">
				<div class="form-group">
					<label for="inputlg">NickName</label> <input
						class="msgboard_nick input-lg" type="text"
						style="width: 20%;" name="msgboard_nick"
						placeholder="사용할 닉네임을 입력하세요." value="${msgboard.msgboard_nick }">
				</div>

				<div class="form-group">
					<label for="inputlg">Title</label> <input
						class="msgboard_title input-lg" type="text"
						style="width: 40%; hight: 200px;" name="msgboard_title"
						placeholder="제목을 입력하세요." value="${msgboard.msgboard_title }">
				</div>

				<textarea name="msgboard_content" id="msgboard_content"
					class="msgboard_content">
				${msgboard.msgboard_content }
				</textarea>
				
				<div class="container" style="text-align: center; margin-top: 15px;">
					<input class="btn btn-sm btn-info" type="submit" class="msgboard_content" id="submit" value="등록" style="margin-right: 10px; margin-bottom: 15px;"> 
						<input class="btn btn-sm btn-info" type="button" value="목록" onclick="location.href='msgboardList'" style="margin-right: 10px; margin-bottom: 15px;">
				</div>
		</form>
		<script>
			$(document).ready(function() {
				$('#msgboard_content').summernote();
			});
			$('#msgboard_content').summernote({
				lang : 'ko-KR',
				height : 300, // 기본 높이값
				focus : true
			});
		</script>
	</div>
</div>
</div>
