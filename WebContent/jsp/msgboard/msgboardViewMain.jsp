<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript">
	$(function() {
		$('#deleteBtn').on('click', function(){
			Swal.fire({
				title : '정말 삭제하시겠습니까?',
				confirmButtonText: '삭제',
				showCancelButton: true
			}).then((result)=>{
				if(result.value){
					location.href='msgboardDelete?msgboard_idx=${msgboard.msgboard_idx}&type=delete'
				}else{
					return false;
				}
			})
		})
		
		
		
		$('#buttonDiv').css("width", $('#msgboardWriteForm').css('width'));
		$('#buttonDiv').css("margin-left",$('#msgboardWriteForm').css('margin-left'));
		
		$("#replyForm").on("submit", function() {
			var d = $(this).serialize();
			$.ajax({
				url : "${contextPath}/reply",
				data : d,
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data) {
						createReplyList();
					}
				}
			});
			return false;
		});
		createReplyList();
		$("#btnClose").on("click", function() {
			$("#modal-modify").hide("slow");
		});
		$("#btnModify").on("click", function() {
			var msgboard_reply_idx = $("#modify-msgboard_reply_idx").val();
			var d = $("#modal-form").serialize();
			$.ajax({
				url : "${contextPath}/reply/" + msgboard_reply_idx,
				data : d,
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data) {
						Swal.fire({
							text : '수정되었습니다.',
							type : 'success'
						})
						$("#modal-modify").hide("slow");
						createReplyList();
					} else {
						alert("수정실패");
					}
				}
			});
			return false;
		});
		$("#btnDelete").on("click", function() {
			var pass = $("modify-pass").val();
			var msgboard_reply_idx = $("#modify-msgboard_reply_idx").val();
			$.ajax({
				url : "${contextPath}/reply/msgboardReplyDelete",
				data : {
					"msgboard_idx" : '${param.msgboard_idx}',
// 					"msgboard_reply_pw" : msgboard_reply_pw,
					"msgboard_reply_idx" : msgboard_reply_idx,
					'${_csrf.parameterName}' : '${_csrf.token}'
				},
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data) {
						Swal.fire({
							text : '삭제되었습니다.',
							type : 'success'
						})
						$("#modal-modify").hide("slow");
						createReplyList();
					} else {
						alert("삭제실패");
					}
				}
			});
			return false;
		});
	});

	function createReplyList() {
		var replyTable = $("#reply");
		$("#reply tr:gt(0)").remove();
		$.ajax({
			url : "${contextPath}/reply/all/${msgboard.msgboard_idx}",
			type : "get",
			dataType : "json",
			success : function(data) {

				for ( var i in data) {
					var tr = $("<tr>");
					var btn = $("<button type='button' class='btn btn-sm btn-info'>수정</button>");
					$("<td>").text(data[i].msgboard_reply_content)
							.appendTo(tr);
					$("<td>").append(btn).appendTo(tr);
					tr.appendTo(replyTable);
					(function(m) {
						btn.on("click", function() {
							$("#modify-msgboard_reply_idx").val(
									data[m].msgboard_reply_idx);
							$("#modify-msgboard_reply_content").val(
									data[m].msgboard_reply_content);
							$("#modal-modify").show("slow");
						});
					})(i);
				}
			}
		});
	}
</script>
</head>
<body>
<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%; overflow: auto;" >
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">FreeBoard View</h3>
	<span style="padding-left: 5%; color: gray;">자유게시판 게시글 상세보기.</span>
<!-- 		<div style="text-align: center; padding:8%;"> -->
		<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">
			<hr>
			<h3 style="color: black; text-align: center; width: 50%; margin: 0 auto;">${msgboard.msgboard_title}</h3>
			<br>
			
			<div class="w3-container" id="buttonDiv" style="text-align: right; width: 80%;">
				<br>
				<button class="btn btn-sm btn-info" onclick="location.href='msgboardModify?msgboard_idx=${msgboard.msgboard_idx}&type=update'" style="margin-right: 10px; margin-bottom: 5px;">수정</button>
				<button id = "deleteBtn" class="btn btn-sm btn-danger" style="margin-bottom: 5px;">삭제</button>
			</div>
			
			<form action="msgboardWrite" id="msgboardWriteForm" method="post" enctype="multipart/form-data" style="margin: 0 auto; width: 80%;">
				<div style="margin-bottom: 20px; width: 100%; height: 60px;">
					<div style="padding: 20px; padding-bottom: 10px; width: 100%;">
						<span id="title" style="float: left;">${msgboard.msgboard_nick}</span>
						<span id="date" style="float: right; color: gray;">${msgboard.in_date }</span>
							<hr>
					</div>
				</div>
	
				<div style="margin-bottom: 150px;">
					<div style="padding-left: 30px; padding-right: 30px;">
						<span id="msgboardcontent"> ${msgboard.msgboard_content} </span>
					</div>
				</div>
	
				<div style="margin-bottom: 30px; text-align: left;">
					<div style="padding-left: 30px; padding-right: 30px">
						<span id="hits" style="font-weight: 900; font-size: 14px">ReadCount</span><span style="font-weight: 900; font-size: 14px; color: red; margin-left: 5px;">${msgboard.msgboard_read_count}</span>
					</div>
				</div>
			</form>

			<div class="w3-container">
				<table class="w3-table w3-striped" id="reply" style="width: 80%; margin-left: 10%; height: 100%;">
					<tr>
						<th style="background-color: #4682B4; color: white; text-align: center;">Content</th>
						<th style="width: 3%; background-color: #4682B4; color: white; text-align: center;">Modify</th>
					</tr>
				</table>
			</div>
			
			<div class="w3-container" style="width: 80%; margin-left: 10%;">
				<form name="replyForm" id="replyForm" style="text-align: center;">
					<input type="hidden" name="msgboard_reply_idx" id="modify-msgboard_reply_idx" value=""> 
					<input type="hidden" name="msgboard_idx" value="${msgboard.msgboard_idx}">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> <input type="hidden" name="user_idx" value="${userIdx}"> 
					<input type="hidden" name="msgboard__reply_idx" value="${msgboard.msgboard__reply_idx}">
					<table style="margin-bottom: 30px; width: 100%; height: 50px; text-align: center;">

					<tr >
						<td class="input-group-text" style="height: 80px;">Write Content</td>
						<td><textarea class="form-control" rows="3" cols="90" name="msgboard_reply_content" style="width: 100%;"></textarea></td>
						<td><input type="submit" class="btn btn-info" style="height: 80px;" value="등록"></td>
					</tr>
				</table>
			</form>
				<div class="w3-container" style="text-align: center;">
					<button class="btn btn-info" onclick="location.href='msgboardList'">목록</button>
				</div>
			</div>
		</div>
		
		<div class="modal" id="modal-modify">
			<div class="modal-dialog">
				<div class="modal-content" style="margin-top: 500px;">
					<form id="modal-form" class="w3-textarea w3-white w3-border w3-border-blue">
						<input type="hidden" name="msgboard_idx" value="${msgboard.msgboard_idx}"> 
						<input type="hidden" name="msgboard_reply_idx" value="${msgboard.msgboard_reply_idx}"> 
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
						<div class="modal-body">
							<span class="input-group-text">Write Content</span>
							<textarea class="form-control" rows="5" cols="50" name="msgboard_reply_content"
								id="modify-msgboard_reply_content"></textarea>
						</div>
						<div class="modal-footer">
							<button type="button" id="btnModify"
								class="btn btn-sm btn-info" data-dismiss="modal">등록</button>
							<button type="button" id="btnDelete"
								class="btn btn-sm btn-danger" data-dismiss="modal">삭제</button>
							<button type="button" id="btnClose"
								class="btn btn-sm btn-dark" data-dismiss="modal">닫기</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>