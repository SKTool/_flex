<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<link rel="stylesheet" type="text/css"
	href="/css/semantic.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
	integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
	crossorigin="anonymous"></script>
<script src="/js/semantic.min.js"></script>


<meta charset="UTF-8">


<title>Insert title here</title>
</head>
<body>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>
			<!-- End sidebar -->

			<!-- Main -->
			<div class="main">
				<div class="ui search">
									<div class="ui icon input">
										<input class="prompt" type="text"
											placeholder="Search countries..."> <i
											class="search icon"></i>
									</div>
									<div class="results"></div>
								</div>


				<script>
										$(function() {
											
										var content = [
											  { title: 'a' },
											  { title: 'b Arab Emirates' },
											  { title: 'c' },
											  { title: 'Antigua' },
											  { title: 'Anguilla' },
											  { title: 'Albania' },
											  { title: 'Armenia' },
											  { title: 'Netherlands Antilles' },
											  { title: 'Angola' },
											  { title: 'Argentina' },
											  { title: 'American Samoa' },
											  { title: 'Austria' },
											  { title: 'Australia' },
											  { title: 'Aruba' },
											  { title: 'Aland Islands' },
											  { title: 'Azerbaijan' },
											  { title: 'Bosnia' },
											  { title: 'Barbados' },
											  { title: 'Bangladesh' },
											  { title: 'Belgium' },
											  { title: 'Burkina Faso' },
											  { title: 'Bulgaria' },
											  { title: 'Bahrain' },
											  { title: 'Burundi' }
											  // etc
											];
										$('.ui.search')
										  .search({
										    source: content
										  })
										;
										})
									</script>




			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->





</body>
</html>