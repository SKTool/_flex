<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.js"></script>
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- bootstrap JS -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<!-- summernote JS CSS -->
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css"
	rel="stylesheet">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<script type="text/javascript">
	$(function() {
		$('.summernote').summernote({
			lang : 'ko-KR',
			height : 300, // 기본 높이값
			focus : true
		// 페이지가 열릴때 포커스를 지정
		});
	});
</script>
<style type="text/css">
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}
</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>


	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">


			<!-- Main -->
			<div class="main" style="background-color: #eeeeee">

				<div class="w3-container" style="width: 1140px; margin: auto;">
					<div style="margin: 20px; padding: 20px;">
						<form name="adModify" action="/adminBoard/modify" method="post">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}"> <input type="hidden"
								value="${adminBoard.admin_board_idx}" name="admin_board_idx">

							<fieldset>

								<div class="form-group">
									<label for="inputlg">제목</label> <input
										class="form-control input-lg" type="text" width="200px"
										name="admin_board_title"
										value="${adminBoard.admin_board_title}">
								</div>

								<div class="form-group">
									<label for="inputlg">카테고리</label> <select name="status">
										<option value="1">공지사항</option>
										<option value="2">FAQ</option>
									</select>
								</div>

								<!-- Summernote -->
								<textarea name="admin_board_content" class="summernote">
							${adminBoard.admin_board_content}
						</textarea>
							</fieldset>
						</form>
					</div>
					<div style="text-align: right; margin: 10px; padding: 10px;">

						<input type="submit" class="btn btn-info" value="수정"> 
						<button class="btn btn-danger"
							onclick="location.href='/adminBoard/adminBoardList'">취소</button>
					</div>
				</div>

			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->

</body>
</html>