<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet"
	href="https://www.w3schools.com/lib/w3-theme-black.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>


	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<div id="main-sidebar" class="header" style="background: #4f5f6f;">
				<jsp:include page="/jsp/include/sidebar/sidebar-adminPage.jsp"></jsp:include>
			</div>
		</sec:authorize>
		<!-- End Header -->
		

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">


			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">
				<div 
					style="background: white; width: 94%; height: 94%; margin-left: 3%; margin-top: 2%;">


					<sec:authorize access="hasRole('ROLE_ADMIN')">
						<sec:authentication property="principal" var="admin" />
					</sec:authorize>
					<sec:authorize access="hasRole('ROLE_USER')">
						<sec:authentication property="principal" var="user" />
					</sec:authorize>

					<div class="container" style="margin-top: 10px;">

						<h2 style="text-align: center; padding-top: 30px;" id="notice">공지사항</h2>
						<br>
						<div class="pill-nav"
							style="text-align: center; text-decoration: none;">
							<a style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/adminBoard/adminBoardList">NOTICE</a> <a
								style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList">FAQ</a> <a style="color: gray;">|</a> <a
								style="margin-left: 10px; margin-right: 10px; color: black;"
								href="/qna/faqList#QNA">QNA</a>
						</div>
						<div style="float: right; padding: 15px;">
							<c:if test="${admin ne null }">
								<button class="btn btn-info btn-m"
									onclick="location.href='/adminBoard/write'">게시글 작성</button>
							</c:if>
						</div>

						<br>
						<table class="w3-table w3-bordered" id="adminBoardTable"
							style="text-align: center;">
							<tr>
								<th style="text-align: center;">번호</th>
								<th style="text-align: center;">제목</th>
								<th style="text-align: center;">작성자</th>
								<th style="text-align: center;">작성일</th>
								<th style="text-align: center;">조회수</th>
							</tr>

							<c:forEach items="${adminBoardList}" var="board">
								<tr>
									<td style="text-align: center;">${board.admin_board_idx}</td>
									<td style="text-align: left;"><a
										style="text-decoration: none;"
										href="view?num=${board.admin_board_idx}">${board.admin_board_title}</a>
									</td>
									<td style="text-align: center;">${board.user_name}</td>
									<td style="text-align: center;">${board.in_date}</td>
									<td style="text-align: center;">${board.admin_board_read_count}</td>
								</tr>
							</c:forEach>

						</table>
						<div style="width: 350px;">
							<input class="w3-input w3-border w3-padding" type="text"
								placeholder="제목 키워드로 검색할 수 있습니다 :)" id="Searchtitle"
								onkeyup="myFunction()">
						</div>

						<div style="text-align: center;">
							<c:if test="${startPage !=1}">
								<a href="/adminBoard/adminBoardList?page=1">[처음]</a>
								<a href="/adminBoard/adminBoardList?page=${startPage-1}">[이전]</a>
							</c:if>
							<c:forEach var="pageNum" begin="${startPage}"
								end="${endPage < totalPage ? endPage : totalPage}">
								<c:choose>
									<c:when test="${pageNum == page}">
										<b>[${pageNum}]</b>
									</c:when>
									<c:otherwise>
										<a href="/adminBoard/adminBoardList?page=${pageNum}">[${pageNum}]</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:if test="${totalPage > endPage }">
								<a href="/adminBoard/adminBoardList?page=${endPage+1}">[다음]</a>
								<a href="/adminBoard/adminBoardList?page=${totalPage}">[마지막]</a>
							</c:if>
						</div>

					</div>
					<br>
				</div>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>

	<script>
		function myFunction() { // 	제목으로 바로 검색하는 함수
			var input, filter, table, tr, td, i;
			input = document.getElementById("Searchtitle");
			filter = input.value.toUpperCase();
			table = document.getElementById("adminBoardTable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				if (td) {
					txtValue = td.textContent || td.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	</script>
	<!-- End MainLayout_Wrap -->

</body>
</html>