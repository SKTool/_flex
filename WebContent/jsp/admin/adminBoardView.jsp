<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>


<style type="text/css">
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	padding: 8px;
	text-align: left;
	border-bottom: 1px solid #ddd;
}
</style>
<script type="text/javascript">
$(function() {
		
	$("#deleteButton").on("click", function() {
	const swalWithBootstrapButtons = Swal.mixin({
		  confirmButtonClass: 'btn btn-success',
		  cancelButtonClass: 'btn btn-danger',
		  buttonsStyling: false,
		})
			swalWithBootstrapButtons.fire({
			  title:'삭제',
			  text: "해당 게시글을 삭제하시겠습니까?",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: '삭제',
			  cancelButtonText: '취소',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {
				  $.ajax({
						url:"/adminBoard/delete?num=${adminBoard.admin_board_idx}",
						data:{"num" : ${adminBoard.admin_board_idx}},
						type:"get",
						success : function(data){
						    swalWithBootstrapButtons.fire(
						      '삭제 완료',
						      '${adminBoard.admin_board_title} 글이 삭제되었습니다. :) ',
						      'success'
						    )
						    location.href = "/adminBoard/adminBoardList";
						}
				 });
			  } else if (
			    // Read more about handling dismissals
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      '취소',
			      '공지사항 상세보기로 돌아갑니다.',
			      'error'
			    )
			  }
			})
	});
});

</script>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<sec:authentication property="principal" var="admin" />
	</sec:authorize>


	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- Main -->
			<div class="main" style="background: #f1f1f1;">
				<div
					style="background: white; width: 94%; height: 94%; margin-left: 3%; margin-top: 2%;">

					<div class="w3-container" style=" height: 100%; width : 80%; margin-left : 10%; padding: 15px; margin: ">

						<table class="w3-table w3-bordered" >
							<colgroup>
								<col style="width: 130px;">
								<col style="width: auto;">
							</colgroup>

							<tr>
								<th scope="row">제목</th>
								<td>${adminBoard.admin_board_title}</td>
							</tr>
							<tr>
								<th scope="row">작성자</th>
								<td>${adminBoard.user_name}</td>
							</tr>
							<tr>
								<td colspan="2">
									<ul class="w3-small">
										<li style="display: inline;"><strong>작성일</strong> <span>${adminBoard.in_date}</span></li>
										<li style="display: inline;"><strong>조회수</strong> <span>${adminBoard.admin_board_read_count}</span>
										</li>
									</ul>
									<div class="detail" style="padding: 10px; margin: 10px;">${adminBoard.admin_board_content}</div>
								</td>
							</tr>

						</table>

						<div style="width: 100%; text-align: right;">      
						<c:if test="${admin ne null}">
							<button class="btn btn-info btn-m" type="button"
								id="modifyButton"
								onclick="location.href='/adminBoard/modify?num=${adminBoard.admin_board_idx}'">수정</button>
							<button class="btn btn-danger btn-m" type="button"
								id="deleteButton">삭제</button>
						</c:if>
							<button class="btn btn-info btn-m"
								onclick="location.href='/adminBoard/adminBoardList'">목록</button>
						</div>
						
					</div>
				</div>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->





</body>
</html>