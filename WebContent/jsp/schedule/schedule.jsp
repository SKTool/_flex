<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.3.1.js"
	integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
	crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css"
	href="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css" />

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<title>Flex</title>



</head>
<body>
	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>
			<!-- End sidebar -->

			<!-- Main -->
			<div class="main" style="margin: 10px; padding: 10px;">

				<script type="text/javascript">
					$(function() {
						var month = new Date().getMonth() + 1;
					})
				</script>


				<div
					style="width: 94%; margin-left: 3%; margin-top: 3%; background: white;">
					<h1 style="color: gray;">Calendar</h1>
					<p>
						<span
							style="font-size: 0.8em; color: lightgray; font-weight: 100;">프로젝트
							전반의 일정을 관리할 수 있습니다.</span>
					</p>
					<hr>
					<div style="margin: 10px; padding: 10px;">
						<div id="menu" style="display: inline; margin: 3px;">
							<span id="menu-navi">
								<button type="button" class="btn btn-info btn-m"
									onclick="onClickTodayBtn()">
									<font style="vertical-align: inherit; border-radius: 50% 50%">today</font>
								</button>
								
								<button type="button" class="btn btn-info btn-m"
									onclick="movePrev()">
									<i class="fas fa-angle-left"></i>
								</button>
								<button type="button" class="btn btn-info btn-m"
									onclick="moveNext()">
									<i class="fas fa-angle-right"></i>
								</button>
							</span> 							
						</div>
						<div class="lnb-new-schedule" style="display: inline;">
							<button id="btn-new-schedule" type="button"
								class="btn btn-info btn-m" data-toggle="modal"
								style="float: right; margin-left: 65%;">New schedule</button>
						</div>
					</div>


					<div id="calendar" style="height: 1000px;"></div>
				</div>


				<script
					src="https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.js"></script>
				<script
					src="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.js"></script>
				<script type="text/javascript"
					src="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.min.js"></script>
				<script type="text/javascript"
					src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.min.js"></script>
				<script
					src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
				<script
					src="https://cdnjs.cloudflare.com/ajax/libs/chance/1.0.13/chance.min.js"></script>
				<script
					src="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.js"></script>
				<script src="/js/data/calendars.js"></script>
				<script src="/js/data/schedules.js"></script>
				<script src="/js/theme/dooray.js"></script>
				<script src="/js/default.js"></script>


				<script type="text/javascript">
					csrfParamName = '${_csrf.parameterName}'
					csrfToken = '${_csrf.token}'
					project_idx = '${project_idx}'
					scheduleList = '${scheduleList}'
				</script>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->


	<script type="text/javascript">
		var scheduleList = ('${scheduleList}');
		var sArray = JSON.parse(scheduleList);
		var dataList = [];
		for ( var i in sArray) {
			var data = {
				id : sArray[i].id,
				calendarId : sArray[i].calendarId,
				title : sArray[i].title,
				category : 'time',
				dueDateClass : '',
				start : sArray[i].start,
				end : sArray[i].end
			}
			console.log(data);
			dataList.push(data);
		}

		cal.clear();
		cal.createSchedules(dataList);
		cal.render();

		function onClickTodayBtn() {
			cal.today();
		}
		

		function moveNext(val) {
			cal.next();
		}
		function movePrev(val) {
			cal.prev();
		}
	</script>
</body>
</html>