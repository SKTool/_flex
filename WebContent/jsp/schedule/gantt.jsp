<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<title>Flex</title>


<style type="text/css">
#container {
	max-width: auto;
	margin: 1em 1em;
}

.hidden {
	display: none;
}

.main-container button {
	font-size: 12px;
	border-radius: 2px;
	border: 0;
	background-color: #ddd;
	padding: 13px 18px;
}

.main-container button[disabled] {
	color: silver;
}

.button-row button {
	display: inline-block;
	margin: 0;
}

.overlay {
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	background: rgba(0, 0, 0, 0.3);
	transition: opacity 500ms;
}

.popup {
	margin: 70px auto;
	padding: 20px;
	background: #fff;
	border-radius: 5px;
	width: 300px;
	position: relative;
}

.popup input, .popup select {
	width: 100%;
	margin: 5px 0 15px;
}

.popup button {
	float: right;
	margin-left: 0.2em;
}

.popup .clear {
	height: 50px;
}

.popup input[type=text], .popup select {
	height: 2em;
	font-size: 16px;
}
</style>
</head>
<body>
	<script src="https://code.highcharts.com/gantt/highcharts-gantt.js"></script>
	<script src="https://code.highcharts.com/stock/modules/stock.js"></script>
	<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap">

		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>
			<!-- End sidebar -->

			<!-- Main -->
			<div class="main">

				<div
					style="width: 94%; margin-left: 3%; margin-top: 3%; background: white;">
					<h1 style="color: gray;">Gantt Chart</h1>
					<p>
						<span
							style="font-size: 0.8em; color: lightgray; font-weight: 100; display: inline;">각
							업무별로 일정의 시작과 끝을 그래픽으로 표시하여 전체 일정을 한눈에 볼 수 있습니다.</span>
							<button id="pdf" class="btn btn-info btn-m"style="float: right;" >
							<i class="fa fa-download"></i> Download PDF
						</button>
					<hr>
					<div>
						<div id="container"></div>
					</div>
				</div>
				<br>
				<script>
					function convertMil(date) {
						var d = new Date(date), month = '' + (d.getMonth() + 1), day = ''
								+ d.getDate(), year = d.getFullYear();

						if (month.length < 2)
							month = '0' + month;
						if (day.length < 2)
							day = '0' + day;

						var date = [ year, month, day ].join('-');
						var y = date.substr(0, 4);
						var m = date.substr(5, 2);
						var s = date.substr(8, 2);
						return Date.UTC(y, m, s);
					}

					var ganttList = ('${ganttList}');
					var gArray = JSON.parse(ganttList);
					var dataList = [];
					for ( var i in gArray) {
						var name = gArray[i].title;
						var start = convertMil(gArray[i].start);
						var end = convertMil(gArray[i].end);

						var data = {
							name : name,
							start : start,
							end : end
						}
						dataList.push(data);
					}
					console.log(dataList);

					// THE CHART
					Highcharts.ganttChart('container', {
						title : {
							text : 'Project Timeline'
						},
						subtitle : {
							text : '일정 추가는 calendar에서 가능합니다.'
						},
						navigator : {
							enabled : true,
							liveRedraw : true,
							series : {
								type : 'gantt',
								pointPlacement : 3,
								pointPadding : 3
							},
							yAxis : {
								min : 3,
								max : 5,
								reversed : true,
								categories : []
							}
						},
						scrollbar : {
							enabled : true
						},
						rangeSelector : {
							enabled : true,
							selected : 0
						},
						series : [ {
							name : 'Project 1',
							data : dataList
						} ]
					});

					// Activate the custom button
					document.getElementById('pdf').addEventListener('click',
							function() {
								Highcharts.charts[0].exportChart({
									type : 'application/pdf'
								});
							});
				</script>
			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->
</body>
</html>