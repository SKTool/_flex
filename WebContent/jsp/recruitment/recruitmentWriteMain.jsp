<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
 <script type="text/javascript" src="${contextPath}/js/recruitment/recruitmentWrite.js"></script>
 <script type="text/javascript">
	//form 요소 선택해서 submit 이벤트가 발생하면
	//유효성검사 하기 :boardCheck()
	$(function(){		
		$("#recruitmentWrite").on("submit",function(){
			if(!recruitmentCheck("recruitmentWrite")){
				return false;
			}
		});
	});
</script>
<body>
		<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%;  overflow: auto;">
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">Recruitment Write</h3>
	<span style="padding-left: 5%; color: gray;">개발자 모집 게시글 작성.</span>
			<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">

	<hr>
	<br>
	<br>
			<form action="recruitmentWrite" id="recruitmentWrite"
				method="post" enctype="multipart/form-data"
				style="margin: 0 auto; width: 600px;">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}"> <input type="hidden"
					name="recruitment_idx" value="${recruitment.recruitment_idx }">
				<table class="w3-table w3-bordered" style="text-align: center;">
					<tr>
						<th style="text-align: center;">Title</th>
						<td><input type="text" id="recruitment_title"
							class="recruitment_title" name="recruitment_title"
							style="width: 430px;"></td>
					</tr>
					<tr>
						<th style="text-align: center;">Project summary</th>
						<td><input type="text" id="recruitment_content"
							class="recruitment_content" name="recruitment_content"
							style="width: 430px;"></td>
					</tr>
					<tr>
						<th style="text-align: center; display: inline-block;">Platform</th>
						<td>
						<div style="display: inline-block;">
								<div class="custom-control custom-checkbox" style="display: inline-block; margin-right: 15px;">
								  <input type="checkbox" class="custom-control-input" id="customCheck1" value="1" name="web">
								  <label class="custom-control-label" for="customCheck1">Web</label>
								</div>
								<div class="custom-control custom-checkbox" style="display: inline-block; margin-right: 16px;">
								  <input type="checkbox" class="custom-control-input" id="customCheck2" value="2" name="linux">
								  <label class="custom-control-label" for="customCheck2" >Linux</label>
								</div>
								<div class="custom-control custom-checkbox" style="display: inline-block; margin-right: 15px;">
								  <input type="checkbox" class="custom-control-input" id="customCheck3" value="3" name="window">
								  <label class="custom-control-label" for="customCheck3">Window</label>
								</div>					
								<div class="custom-control custom-checkbox" style="display: inline-block; margin-right: 16px;">
								  <input type="checkbox" class="custom-control-input" id="customCheck4" value="4" name="ios">
								  <label class="custom-control-label" for="customCheck4">IOS</label>
								</div>
								<div class="custom-control custom-checkbox" style="display: inline-block; margin-right: 15px;">
								  <input type="checkbox" class="custom-control-input" id="customCheck5" value="5" name="android">
								  <label class="custom-control-label" for="customCheck5">Android</label>
								</div>	
							</div>			
						</td>
					</tr>
					<tr>
						<th style="text-align: center;">Personnel</th>
						<td><select class="form-control" id="recruitment_personnel"
							name="recruitment_personnel">
								<option>0</option>
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>6</option>
								<option>7</option>
								<option>8</option>
								<option>9</option>
								<option>10</option>
						</select></td>
					</tr>
					<tr>
						<th style="text-align: center;">Project Start Date</th>
						<td><input type="date" class="input-sm form-control"
							id="recruitment_start" name="recruitment_start" /></td>
					</tr>
					<tr>
						<th style="text-align: center;">Project End Date</th>
						<td><input type="date" class="input-sm form-control"
							id="recruitment_end" name="recruitment_end" /></td>
					</tr>
				</table>
				<br>
				<div style="text-align: center;">
					<input class="btn btn-sm btn-info" type="submit" value="등록" style="background-color: #17a2b8;">
					<button class="btn btn-sm btn-info" type="button" onclick="location.href='recruitmentList'"  style="background-color: #17a2b8;">목록</button>
				</div>
			</form>
		</div>

			<br>
			<br>
				<h3 style="text-align: center; margin-bottom: 20px;">Recruitment Write</h3>
				<form action="recruitmentWrite" id="recruitmentWriteForm"
					method="post" enctype="multipart/form-data"
					style="margin: 0 auto; width: 600px;">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
					<input type="hidden" name= "recruitment_idx" value="${recruitment.recruitment_idx }">
					<table class="w3-table w3-bordered" style="text-align: center;">
						<tr>
							<th style="text-align: center;">Title</th>
							<td><input type="text" id="recruitment_title" class="recruitment_title" name="recruitment_title" style="width: 430px;"></td>
						</tr>
						<tr>
							<th style="text-align: center;">Project summary</th>
							<td><input type="text" id="recruitment_content" class="recruitment_content" name="recruitment_content" style="width: 430px;"></td>
						</tr>
						<tr>
							<th style="text-align: center;">Platform</th>
								<td><input type= "checkbox" name="web" value="1">Web
								<input type= "checkbox" name="linux" value="2" style="margin-left: 30px;">Linux
								<input type= "checkbox" name="window" value="3" style="margin-left: 30px;">Window
								<input type= "checkbox" name="ios" value="4" style="margin-left: 30px;">IOS
								<input type= "checkbox" name="android" value="5" style="margin-left: 30px;">Android</td>
						</tr>
						<tr>
							<th style="text-align: center;">Personnel</th>
							<td><select class="form-control" id="recruitment_personnel" name="recruitment_personnel">
								    <option>1</option>
								    <option>2</option>
								    <option>3</option>
								    <option>4</option>
								    <option>5</option>
								    <option>6</option>
								    <option>7</option>
								    <option>8</option>
								    <option>9</option>
								    <option>10</option>
								  </select></td>
						</tr>
						<tr>
							<th style="text-align: center;">Project Start Date</th>
							<td><input type="date" class="input-sm form-control" id="recruitment_start" name="recruitment_start" /></td>
						</tr>
						<tr>
							<th style="text-align: center;">Project End Date</th>
							<td><input type="date" class="input-sm form-control" id="recruitment_end" name="recruitment_end" /></td>
						</tr>
					</table>
					
					<br>
					<div style="text-align: center;">
						<input type="submit"  class="btn btn-info btn-sm" style="background: #fff !important; border: 1px solid #ccc; color: #666" value="Save">
						<button type="button" onclick="location.href='recruitmentList'" class="btn btn-info btn-sm">List</button>
					</div>
				</form>
			</div>
		</div>   

</body>
</html>