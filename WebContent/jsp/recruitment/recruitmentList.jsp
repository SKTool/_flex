<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>

<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<meta charset="UTF-8">


<title>Insert title here</title>
</head>
<body>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap" >
	
		<!-- Header -->
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<div id="main-sidebar" class="header" style="background: #4f5f6f;">
				<jsp:include page="/jsp/include/sidebar/sidebar-adminPage.jsp"></jsp:include>
			</div>
		</sec:authorize>
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">


			<!-- Main -->
			<div class="main">



				<jsp:include page="recruitmentListMain.jsp"></jsp:include>





			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->





</body>
</html>