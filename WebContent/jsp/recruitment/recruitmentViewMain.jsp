<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	request.setAttribute("contextPath", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script type="text/javascript">
	$(function() {
			$('#deleteBtn').on('click', function(){
				Swal.fire({
					title : '정말 삭제하시겠습니까?',
					confirmButtonText: '삭제',
					showCancelButton: true
				}).then((result)=>{
					if(result.value){
						location.href='recruitmentDelete?recruitment_idx=${recruitment.recruitment_idx}&type=delete'
					}else{
						return false;
					}
				})
			})
		
		
		
		
		
		
		$('#portfolioSendBtn').on('click', function(){
			$.ajax({
				url : '/user/sendMailWithFile',
				type : 'POST',
				data : {'${_csrf.parameterName}' : '${_csrf.token}', 
					    'toIdx' 	: '${recruitment.user_idx}',
					   },
				success : function(result){
					if(result === 1){
						Swal.fire({
							text : '지원 성공',
							type : 'success'
						})
					}else if(result === -1){
						Swal.fire({
							text : '지원 실패. 마이페이지에서 포트폴리오 파일을 업로드 해주세요.',
							type : 'success'
						})
					}
				}
			})	
		})
		
		$.ajax({
			url : '/recruitment/getPlatform',
			type : 'POST',
			data : {
				'${_csrf.parameterName}' : '${_csrf.token}',
				'recruitment_idx' : '${recruitment.recruitment_idx}'
			},
			success : function(platformList) {
				for (var i = 0; i < platformList.length; i++) {
					$('#platformTd').text(
							$('#platformTd').text()
									+ platformList[i].platform_name
									+ " ");
				}
			}
		})
	})
</script>
<title>Insert title here</title>
</head>
<body>
		<div style = "background : #eeeeee; height : 100%;">
	<div class="rounded" style="width: 80%; margin-left: 10%; background: white; height: 100%;  overflow: auto;">
	<h3 style="text-align: left; padding-left: 3%; padding-right: 3%; margin-top:3%; color: gray;">Recruitment View</h3>
	<span style="padding-left: 5%; color: gray;">개발자 모집 게시글 상세보기.</span>
			<div style="text-align: center; padding-bottom: 8%; padding-left: 8%; padding-right: 8%;">

	<hr>
	<br>
	<br>
			<div style="text-align: right;">
				<div class="w3-container" id="buttonDiv" style="text-align: right; width: 80%;">
					<div>
						<button class="btn btn-sm btn-info" onclick="location.href='recruitmentModify?recruitment_idx=${recruitment.recruitment_idx}&type=update'" style="margin-right: 10px; margin-bottom: 15px;">수정</button>
						<button id = "deleteBtn" class="btn btn-sm btn-danger" style="margin-right: 50px; margin-bottom: 15px;">삭제</button>
					</div>
				</div>
			</div>
			<br>
			<div class="w3-container">
				<form action="recruitmentWrite" id="recruitmentWriteForm" method="post" enctype="multipart/form-data" style="margin: 0 auto; width: 60%; ">
					<table class="w3-table w3-bordered">
						<tr>
							<th style="width: 15%; text-align: center;">Title</th>
							<td style="width: 40%; text-align: center;">${recruitment.recruitment_title }</td>
						</tr>
						<tr>
							<th style=" text-align: center;">Project summary</th>
							<td style="text-align: center;">${recruitment.recruitment_content}</td>
						</tr>
						<tr>
							<th style="text-align: center;">Platform</th>
							<td id="platformTd" style="text-align: center;"></td>
						</tr>
						<tr>
							<th style="text-align: center;">Personnel</th>
							<td style="text-align: center;">${recruitment.recruitment_personnel}</td>
						</tr>
						<tr>
							<th style="text-align: center;">project period</th>
							<td style="text-align: center;">${recruitment.recruitment_start}~${recruitment.recruitment_end}</td>
						</tr>
					</table>
				</form>
			</div>
			<br>
			<div class="w3-container" id="buttonDiv" style="text-align: right; width: 35%;"><span id="hits" style="font-weight: 900; font-size: 14px">Read Count</span> <span style="font-weight: 900; font-size: 14px; color: red;">${recruitment.recruitment_read_count}</span></div>
			
			<div style="text-align: center; padding: 25px">
				<input type="button" id = "portfolioSendBtn" class="btn btn-sm btn-info" value="모집공고 지원">
				<button class="btn btn-sm btn-info" type="button" onclick="location.href='recruitmentList'">목록</button>
			</div>
		</div>
	</div>
</div>	
</body>
</html>