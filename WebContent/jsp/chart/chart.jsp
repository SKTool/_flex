<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<title>Insert title here</title>
<script type="text/javascript">
	function getToday(){
		var today = new Date();
		var yyyy = today.getFullYear();
		var mm = today.getMonth()+1;
		if(mm.toString().length === 1){
			mm = "0" + mm;
		}
		var dd = today.getDate();
		if(dd.toString().length === 1){
			dd = "0" + dd;
		}
		var hh = today.getHours();
		if(hh.toString().length === 1){
			hh = "0" + hh;
		}
		var min = today.getMinutes();
		if(min.toString().length === 1){
			min = "0" + min;
		}
		var sec = today.getSeconds();
		if(sec.toString().length === 1){
			sec = "0" + sec;
		}
		today = yyyy + "-" + mm + "-" + dd + " " + hh + ":" + min + ":" + sec;
		return today;
	}
	
	function getNthWeek(paramDate){
		
		var today = paramDate;
		var year = today.getFullYear();
		var month = today.getMonth()+1;
		var dateCount = 0;
		 
		if(month === 1 || month === 3 || month === 5 || month === 7 || month === 8 || month === 10 || month === 12){
			dateCount = 31; 
		}else if(month === 4 || month === 6 || month === 9 || month === 11){
			dateCount = 30;
		}else if(month === 2){
		    if((year%4 === 0 && year%100 != 0)  || (year%400 === 0)){
		   		dateCount = 29;
		    }else{
		   		dateCount = 28;
		    }
		}
		 
		var day = today.getDay();
		
		if(day === 7){
			day = 0;
		}
		 
		var week1end = 7-day;
		var week2end = 14-day;
		var week3end = 21-day;
		var week4end = 28-day;
		var week5end = 35-day;
		if(week5end > dateCount){
			week5end = dateCount;
		}
		if(week4end === dateCount){
			week5end = 0;
		}
		 
		today = new Date();
		var thWeek = 0;
		 
		if(1 <= today.getDate() && today.getDate() <= week1end){
			thWeek = 1;  
		}else if(week1end+1 <= today.getDate() && today.getDate() <= week2end){
			thWeek = 2;  
		}else if(week2end+1 <= today.getDate() && today.getDate() <= week3end){
			thWeek = 3;
		}else if(week3end+1 <= today.getDate() && today.getDate() <= week4end){
			thWeek = 4;
		}else if(week4end+1 <= today.getDate() && today.getDate() <= week5end){
			if(week5end != 0){
				thWeek = 5;
			}
		} 
		  
		 
		if(week5end === 0){
			var result = [month, thWeek, week1end, week2end, week3end, week4end];
		}else{
			var result = [month, thWeek, week1end, week2end, week3end, week4end, week5end];
		}
	      
	    return result;
	}
	function getBugs(){
  	  
  		return new Promise(function (resolve, reject){
  			var a = [];
            
			$.ajax({
			 url : '/user/getBugCounts',
			 type : 'POST',
			 data : {'project_member_idx' : '${project_member_idx}', '${_csrf.parameterName}' : '${_csrf.token}'},
			 success : function(bugCountList){
			  resolve(bugCountList);
			 }
			});
  	  	})
    }
	
 	$(function(){
//  		alert('${project_member_idx}');
//  		Swal.fire({
//  			  title: '이메일을 입력하세요',
//  			  input: 'text',
//  			  inputAttributes: {
//  			    autocapitalize: 'off'
//  			  },
//  			}).then((result) => {
//  			  if (result.value != null && result.value != '') {
 				  
//  				var emailVal = result.value;
//  				var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
 				
//  				if (emailVal.match(regExp) == null) {
 					
//  					Swal.fire({
//  						text : '이메일 형식에 맞춰 입력해 주세요',
//  						type : 'warning'
//  					})
 					
//  					return false;
//  				}  
 				  
//  			    $.ajax({
//  			    	url : '/user/sendInvitationMail',
//  			    	type : 'POST',
//  			    	data : {'${_csrf.parameterName}' : '${_csrf.token}', 'toAddress' : result.value},
//  			    	success : function(result){
//  			    		if(result === 1){
//  			    			Swal.fire({
//  			    				text : '성공',
//  			    				type : 'success'
//  			    			})
//  			    		}else if(result === -1){
//  			    			Swal.fire({
//  			    				text : '실패',
//  			    				type : 'error'
//  			    			})
//  			    		}
//  			    	}
 			    	
//  			    })
//  			  }
//  			})
 		
 		
 		$.ajax({
 			url : '/user/dDay',
 			type : 'POST',
 			data : {'project_member_idx' : '${project_member_idx}', '${_csrf.parameterName}' : '${_csrf.token}'},
 			success : function(dDay){
	 			$('#dDayDiv').append($('<h2 style = "color : gray;">').html('프로젝트 완료까지 <span style = "color : #2da9d2; font-size : 2em;">' + 'D' + dDay + '</span>일'));
 			}
 		})
 		
 		function drawDonutChart() {
 			 
 			var today = getToday();
 			var completed = 0;
 			var overdue = 0;
 			var planned = 0;
 			 
 			$.ajax({
 				url 	: '/user/getAllTasks',
 				type 	: 'POST',
 				data 	: {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : '${project_member_idx}'},
 				success : function(taskList){
 					for(var i = 0; i < taskList.length; i++){
 						if(taskList[i].status === 1){
 							completed += 1;
 						}else if(taskList[i].task_end < today){
 							overdue += 1;
 						}else{
 							planned += 1;
 						}
 				 	}
 				 
 					var data = google.visualization.arrayToDataTable([
 						['Task', 'Hours per Day'],
 						['completed',     completed],
 						['overdue',      overdue],
 						['planned',  planned],
 					]);
 					
 		 		    var width = ($('.main').css('width')).replace('px','');
 		 		    
 				    var options = {
 				      title: 'Status of Task',
 				      pieHole: 0.9,
 				      width : width/2-120
 				    };
 			
 				    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
 				    chart.draw(data, options);
 				
 				    var overall = completed + overdue + planned;
 				    var progress = (completed/overall).toFixed(3)*100;
 				    
 				    $('#progressBar').css('width', progress + "%");
 				    $('#progressBar').text(progress + '%');
 		 		}
 			})
 		}
 		
 		function drawBarChart() {
			// DB에서 project_member_idx로 project_idx 뽑아서 project_member테이블에서 project_idx로 project_member_idx 뽑아냄
			// 각 project_member_idx로 git테이블에서 git_commit_count 뽑아서 전시
 			
 			$.ajax({
 				url : '/user/getGitCommitCounts',
 				type : 'post',
 				data : {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : '${project_member_idx}'},
 				success : function(resultList){
					var data = new google.visualization.DataTable();
					data.addColumn('string', 'member');
					data.addColumn('number', 'commits');
					
					for(var i = 0; i < resultList.length; i++){
						data.addRows([ [resultList[i].user_name, resultList[i].git_commit_count] ]);
					}
					
 		 			var width = ($('.main').css('width')).replace('px','');
 		 			
 		 			var options = {
 		 				chart	: {subtitle: '맴버별 커밋 수'},
 		 				bars	: 'horizontal',
 		 				width : width/2-120,
 		 				opacity: 0.2 
 		 			};

 		 			var chart = new google.charts.Bar(document.getElementById('barchart_material'));

 		 			chart.draw(data, google.charts.Bar.convertOptions(options));
 				}
 			})
 		}
 		
 		
 		function drawLineChart() {
 			
 		    var data = new google.visualization.DataTable();
 		    data.addColumn('string', 'Week');
 		    data.addColumn('number', 'Guardians of the Galaxy');
 		    
 		    var today = new Date();
 		    today.setDate(1);
 		    var weeks = getNthWeek(today);
 		    var weekOf = weeks[0] + "월 " + weeks[1] + "주차";
 		     
 		    getBugs().then(function(resolvedData){
 				data.addRows([ 
 					[" ",  Number(resolvedData[9])],
 					[" ",  Number(resolvedData[8])],
 					[" ",  Number(resolvedData[7])],
 					[" ",  Number(resolvedData[6])],
 					[" ",  Number(resolvedData[5])],
 					[" ",  Number(resolvedData[4])],
 					[" ",  Number(resolvedData[3])],
 					[" ",  Number(resolvedData[2])],
 					[" ",  Number(resolvedData[1])],
 					[weekOf, resolvedData[0]]
 	    		]);
 		    });
 			  
 		    var width = ($('.main').css('width')).replace('px','');
 		    var options = {
 		    		
 				chart: {  
 					title: 'Bug reports per week',
 					subtitle: 'from bugTracking data statistics'
 				},
 				
 				width: (width-40)*0.88, 
 				height: 400
 		    };
 		
 		    var chart = new google.charts.Line(document.getElementById('linechart_material'));
 		    
 		   	google.visualization.events.addListener(chart, 'error', function (err) {
	 		   	google.charts.load('current', {'packages':['line']});
	 		    google.charts.setOnLoadCallback(drawLineChart);
	    	});
 		   	
 		    chart.draw(data, google.charts.Line.convertOptions(options));
 		}
 		
	    google.charts.load("current", {'packages':["corechart"]});
	    google.charts.setOnLoadCallback(drawDonutChart);
	    
	    google.charts.load('current', {'packages':['bar']});
	    google.charts.setOnLoadCallback(drawBarChart);
	
	    google.charts.load('current', {'packages':['line']});
	    google.charts.setOnLoadCallback(drawLineChart);
	    
	    
 	});
</script>
</head>
<body>

	<div id="mainLayout-wrap" class="wrap" >
	
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>
		<div id="main_wrap" class="main_wrap">

			<div id="main-sidebar" class="sidebar">
				<jsp:include page="/jsp/include/sidebar/sidebar.jsp"></jsp:include>
			</div>

			<div class="main">
				<div style = "width : 94%; margin-left : 3%; margin-top : 3%; background : white;">
			            	<h1 style = "color : gray;">Analysis</h1>
			            	<p><span style = "font-size : 0.9em; color : lightgray; font-weight : 100;">&nbsp프로젝트 내의 업무, 깃, 버그트래킹 등의 통계를 나타냅니다</span></p>
			            	<hr>  
<!-- 			            	<hr style = "height : 80px; background : #e6f7ff; border : none; margin : 0;">  -->
			            	<br>   
			            	<br>   
			            	<br>   
			            	<div id = "dDayDiv" style = "height : 60px; width : 100%; margin : 0; padding : 0; text-align : center;">
			            	</div>
			            	<br>  
			            	<br>  
			            	<br>  
			            	<br>  
<!-- 			            	<hr style = "height : 80px; background : #e6f7ff; border : none; margin : 0;">  -->
			            	<hr>  
			            	<br>  
			            	<br>  
			            	<h4 style = "color : #2da9d2; text-align : center;">프로젝트 진행률(%)</h4>
			            	<p style = "color : lightgray; font-size : 0.8em; text-align : center;">업무 진행 정도를 퍼센트로 나타냅니다</p>
			            	<br>  
			            	<div class="progress" style = "width : 96%; margin-left : 2%;"> 
			 		 			<div id = "progressBar" class="progress-bar bg-success" style="width:20%"></div>
							</div>
			            	<br>    
			            	<br>    
			            	<hr>
			            	<br> 
			            	<br>
			            	<div style = "width : 50%; float:left; text-align : center;">
			            		<h4 style = "color : #2da9d2; text-align : center;">기한 별 업무(%)</h4>
				            	<p style = "color : lightgray; font-size : 0.8em; text-align : center;">날짜 기준 업무를 퍼센트로 나타냅니다 </p>
			            	</div>
			            	<div style = "width : 50%; float:left; text-align : center;">
			            		<h4 style = "color : #2da9d2; text-align : center;">Git Commit</h4>
				            	<p style = "color : lightgray; font-size : 0.8em; text-align : center;">깃 커밋 수를 월별로 나타냅니다</p>
			            	</div>
			            	<br>
			            	<br>
			            	<br>  
			            	<br>  
			    			<div id="donutchart" style="width: 50%; height: 500px; float : left;"></div>
			   				<div id="barchart_material" style="width: 50%; height: 500px; float : right;"></div>
			   				<div style = "width : 100%; float : left;">
			   					<br>
			   					<br>
			   					<br>
			   					<br>
			   					<hr>   
			   					<br>
			   					<br>
				   				<h4 style = "color : #2da9d2; text-align : center;">버그 리포트</h4>
			    	        	<p style = "color : lightgray; font-size : 0.8em; text-align : center;">최근 10개월간 기록된 오류를 선형 차트로 나타냅니다</p>
			   					<br>
			   					<br>
			   				</div>
			    			<div id="linechart_material" style="width: 88%; height: 500px; float : left; margin-left: 6%;"></div>
			    			<hr style = "width : 100%; float : left;">
			    </div> 
			</div>
		</div>
	</div>
</body>
</html>