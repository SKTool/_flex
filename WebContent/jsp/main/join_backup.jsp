<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script type="text/javascript">
	$(function(){
		$('#joinForm').on('submit', function(){
			
			var formInputs = $('#joinForm').find('input[name][type!="hidden"]');
			
			for(var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') != null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						
						swal.fire({
			 				text: "빈칸을 입력해 주세요",
			 				type: "warning",
			 			})
						$(formInputs[i]).focus();
						
						return false;
					}
				}
			} 
			
			if($.trim($('#id_temp').val()).length == 0 || $.trim($('#id_temp').val()) !== $.trim($('#user_id').val())){
				
				swal.fire({
	 				text: "아이디 중복확인이 필요합니다",
	 				type: "warning",
	 			})
	 			
				return false;
			}
			
			if($.trim($('#tel_temp').val()).length == 0 || $.trim($('#tel_temp').val()) !== $.trim($('#user_tel').val())){
				
				swal.fire({
	 				text: "전화번호 중복확인이 필요합니다",
	 				type: "warning",
	 			})
	 			
				return false;
			}
			
			if($.trim($("#user_pw").val()) !== $.trim($("#user_pw_c").val())) {
				
				swal.fire({
	 				text: "비밀번호가 일치하지 않습니다",
	 				type: "warning",
	 			})
	 			
				$('#user_pw').focus();
				return false;
			}
			
			if(!($('#agree').is(':checked'))){
				
				swal.fire({
					text : '이용약관 동의에 체크해주세요',
					type : 'warning'
				})
				return false;
			}
		});
		
		$('#user_id').on('blur', function(){
			
			$("#user_id").val($.trim($("#user_id").val()));
			
			var emailVal = $("#user_id").val();
			var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
			
			if (emailVal.match(regExp) == null) {
				
				$('#idCheckAlert').css('display', 'unset');
				$('#idCheckAlert').text(' 이메일 형식에 맞춰 입력해 주세요');
				
				return false;
			}
			
			$.ajax({
				url 	: '/user/userIdCheck',
				type 	: 'POST',
				data 	: {'user_id' : $("#user_id").val(), '${_csrf.parameterName}' : '${_csrf.token}'},
				success : function(result){
					
					if(result === 0){
						swal.fire({
			 				text: "아이디를 입력해 주세요",
			 				type: "warning",
			 			})
						$('#user_id').focus();
						
					}else if(result === 1){
						$('#idCheckAlert').css('display', 'unset');
						$('#idCheckAlert').text(' 사용 가능한 아이디 입니다');
						$('#id_temp').val($('#user_id').val());
						
					}else if(result === -1){
						$('#idCheckAlert').css('display', 'unset');
						$('#idCheckAlert').text(' 이미 사용중인 아이디 입니다');
 						$('#user_id').val('');
					}
				}
			});
		});
		
		$('#user_tel').on('blur', function(){
			
			$("#user_tel").val($.trim($("#user_tel").val()));
			
			var telVal = $("#user_tel").val();
			var regExp = /^\d{3}\d{3,4}\d{4}$/;
			
			if (telVal.match(regExp) == null) {
				
				$('#telCheckAlert').css('display', 'unset');
				$('#telCheckAlert').text(' 전화번호 형식에 맞춰서 입력해주세요.');
				
				return false;
			}
			  
			$.ajax({
				url 	: '/user/userTelCheck',
				type 	: 'POST',
				data 	: {'user_tel' : $("#user_tel").val(), '${_csrf.parameterName}' : '${_csrf.token}'},
				success : function(result){
					
					if(result === 0){
						$('#telCheckAlert').css('display', 'unset');
						$('#telCheckAlert').text(' 전화번호를 입력해주세요');
					}else if(result === 1){
						$('#telCheckAlert').css('display', 'unset');
						$('#telCheckAlert').text(' 사용 가능한 전화번호 입니다');
						$('#tel_temp').val($('#user_tel').val());
					}else if(result === -1){
						$('#telCheckAlert').css('display', 'unset');
						$('#telCheckAlert').text(' 이미 사용중인 전화번호 입니다');
						$('#user_tel').val('');
					}	
				}
			});
		});
	});
</script>
<style>
body {
	 	overflow : hidden;
	 }
</style>
    <div class="container-fluid" style="padding:0; overflow: hidden;">
        <div class="row">  
            <div class = "container col-xs-7 col-sm-7 col-md-7 col-xl-7" style = "padding : 0;">
            	<img src="/img/login.jpg" class="img-fluid">          
            </div>
			<div class = "container col-xs-5 col-sm-5 col-md-5 col-xl-5">
				<form action = "/user/join" id = "joinForm" method = "post" enctype = "multipart/form-data" style = "margin-left : 10%; margin-top : 8%; min-height : 100%;" >
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">  
					<h2> 회원 가입 </h2>
					<hr class = "bg-info">
					<br>
					<table style = "width : 100%;">
						<tr>
							<td>
								<hr>
							</td>
						</tr>                            
						<tr>
							<td width = "50%" >
								<br>
								<input class = "form-control" type = "email" name = "user_id" id = "user_id" placeholder = "이메일" required width = "100%">
								<input type = "hidden" id = "id_temp">       
							</td>    
							<td width = "50%">
							<br>
								<p id = "idCheckAlert" style = "display : none;"></p>
							</td>
						</tr>	
						<tr>
							<td>
								<br>
								<input class = "form-control"  type = "password" name = "user_pw" id = "user_pw" placeholder = "비밀번호" required>  
							</td>
						</tr>	
						<tr>
							<td>
								<br>
								<input class = "form-control"  type = "password" name = "user_pw_c" id = "user_pw_c" placeholder = "비밀번호 확인" required>
							</td>
						</tr>	
						<tr>
							<td>
								<br>
								<input class = "form-control"  type = "text" name = "user_name" id = "user_name" placeholder = "이름" required>
							</td>
						</tr>	
						<tr>
							<td width = "50%">
								<br>
								<input class = "form-control"  type = "tel" name = "user_tel" id = "user_tel" placeholder = "전화번호" required>
								<input type = "hidden" id = "tel_temp"> 
							</td>
							<td width = "50%">
							<br>
								<p id = "telCheckAlert" style = "display : none;"></p>
							</td>
						</tr>	
						<tr>
							<td align = "right">
								<br>
								<br>
								<br>
								<br>
								<br>
								<br> 
								<div class="form-check" align = "left">
					      			<label class="form-check-label" for="agree" style = "color : gray;">
					        			<input type="checkbox" class="form-check-input" id="agree">
					        			I agree to the felx’s Terms of service and Privacy policy.
					      			</label>
					    		</div>
								<input class = "btn btn-lg btn-outline-info btn-block" type = "submit" id = "joinBtn" value = "가입">
							</td>
						</tr>
						<tr>
							<td>
								<br>
								<hr class = "bg-info">	
							</td>
						</tr>
					</table>
				</form>    
			</div> 
        </div>
    </div>
<!-- <meta charset="UTF-8"> -->
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
<!-- <title>Insert title here</title> -->
<%-- <jsp:include page="/jsp/include/top.jsp"></jsp:include> --%>
<!-- <style type="text/css"> -->
<!-- /* 	@import url('https://fonts.googleapis.com/css?family=Noto+Serif+KR'); */ -->
<!-- /* 	body{ */ -->
<!-- /* 		font-family: 'Noto Serif KR', serif; */ -->
<!-- /* 	} */ -->
<!-- </style> -->
<!-- </head> -->
<!-- <body> -->
<%-- <jsp:include page="include/header/header_for_join.jsp"></jsp:include> --%>