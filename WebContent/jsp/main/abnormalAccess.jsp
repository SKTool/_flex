<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col" style="float: right; text-align: center;">
				<img src="/img/flex_logo.jpeg" width="250px" style="margin-top: 100px;">
			</div>
		</div>
		<br> <br>
		<div class="row">
			<div class="col-sm-2 col-md-5">
				<img src="/img/warning.png" style="float: right;" width="200px">
			</div>
			<div class="col-sm-10 col-md-7">
				<h2 style="margin-bottom: 0;">고객님, 불편을 드려 죄송합니다.</h2>
				<p style="color: #808080;">페이지 주소가 정확하지 않거나 페이지에 오류가 있어 요청하신 내용을 찾을 수 없습니다.</p>
				<p style="color: #808080;">입력하신 주소가 정확한지 확인 수 다시 시도해 주시기 바랍니다 .</p>
			</div>
		</div>
	</div>
</body>
</html>