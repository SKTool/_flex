<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<sec:authorize access="hasRole('ROLE_ADMIN')">
	<sec:authentication property="principal" var="admin" />
</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
	<sec:authentication property="principal" var="user" />
</sec:authorize>
<script>
	$(function() {
		$('#widgetDiv').css('width', $('#backgroundImgDiv').css('width')).css(
				'height', $('#backgroundImgDiv').css('height')).css('position',
				'relative');
		// 		$('#widgetDiv').css('padding-top', Number(($('#widgetDiv').css('height')).replace('px', ''))/6);
		// 		$('#mainDiv').css('overflow', 'hidden');
		var result = '${param.result}';
		if (result.length > 0) {
			result = Number(result);
			if (result === 1) {
				swal.fire({
					text : '계정이 삭제되었습니다.',
					type : 'success'
				})
			} else if (result === -1) {
				swal.fire({
					text : '계정 삭제에 실패하였습니다.',
					type : 'success'
				})
			}
		}
	})
</script>
<style>
#backgroundImgDiv {
	position: absolute;
	background: black;
	height: 100%;
	width: 100%;
	top: 0;
}

#backgroundImg {
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	opacity: 0.5;
}

#widget1 {
	background: rgba(0, 0, 0, 0.5);
	width: 18%;
	height: 60%;
	display: inline-block;
	margin-left: 18%;
	position: absolute;
	padding: 10px;
}

#widget2 {
	background: rgba(0, 0, 0, 0);
	width: 100%;
	height: 100%;
	display: inline-block;
	/*    		margin-left : 38%; */
	position: absolute;
	padding: 10px;
}

#widget3 {
	background: rgba(0, 0, 0, 0.5);
	width: 19%;
	height: 60%;
	display: inline-block;
	margin-left: 62%;
	position: absolute;
	padding: 10px;
}

.carousel-inner img {
	width: 100%;
	height: 100%;
}

#demo, .carousel-inner {
	width: 100%;
	height: 100%;
}

.carousel-caption {
	top: 0;
}

body {
	overflow: hidden;
}
</style>
<div id="backgroundImgDiv">
	<img id="backgroundImg" src="/img/main_background2.jpg">
</div>
<div id="widgetDiv">

	<!-- 	<div id = "widget1"> -->
	<!-- 		<div style = "position : relative; height : 80%;"> -->
	<!--    			<img id = "widget1Img" src = "/img/widget1.jpg" style = "height : 100%; width : 100%;"> -->
	<!-- 		</div> -->
	<!-- 		<h4 style = "color : #bfbfbf;">PROJECT FLEX</h4>	 -->
	<!--    		<h6 style = "color : #bfbfbf; margin-bottom : 0;">created by</h6> -->
	<!--    		<p style = "color : #bfbfbf;">Yoongun, Bomin, SolE, Soo, Daehyung</p> -->
	<!--    	</div> -->

	<div id="widget2">
		<div id="demo" class="carousel slide" data-ride="carousel">
			<ul class="carousel-indicators" style="bottom: 3%;">
				<li data-target="#demo" data-slide-to="0" class="active"></li>
				<li data-target="#demo" data-slide-to="1"></li>
				<li data-target="#demo" data-slide-to="2"></li>
			</ul>
			<div class="carousel-inner">
				<div class="carousel-item active" style="height: 100%;">
					<!-- 	      			<img src="/img/widget2_1.jpg" alt="Los Angeles" width="1100" height="500"> -->
					<div class="carousel-caption" style="height: 100%;">
						<h1
							style="color: lightgray; font-size: 17em; font-weight: 450; margin-top: 25%;">FLEX</h1>
					</div>
				</div>
				<div class="carousel-item" style="height: 100%;">
					<!-- 			    	<img src="/img/widget2_2.jpg" alt="Chicago" width="1100" height="500"> -->
					<div class="carousel-caption" style="margin-top: 10%;">
						<br> <br> <br><br><br>
						<h1 style="color: lightgray; font-size: 7em; font-weight: 300;">WORK
							SMARTER</h1>
						<h2 style="color: lightgray; font-size: 5em; font-weight: 200;">togeter</h2>
						<br>
						<p style="color: lightgray; font-size: 2em; font-weight: 100;">Introduce
							your team to a visual way of</p>
						<p style="color: lightgray; font-size: 2em; font-weight: 100;">tracking
							work sharing ideas and</p>
						<p style="color: lightgray; font-size: 2em; font-weight: 100;">measuring
							performance.</p>
					</div>
				</div>
				<div class="carousel-item" style="height: 100%;">
					<!-- 		    		<img src="/img/widget2_3.jpg" alt="New York" width="1100" height="500"> -->
					<div class="carousel-caption" style="margin-top: 15%;">
						<br> <br> <br>
						<h2 style="color: lightgray; font-size: 5em; font-weight: 200;">지금</h2>
						<h2 style="color: lightgray; font-size: 5em; font-weight: 200;">시작해보세요</h2>
						<!-- 				        <input type = "button" class = "btn btn-dark btn-block" value = "가입하기" style = "opacity : 0.8;">   	   -->
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#demo" data-slide="prev">
				<span class="carousel-control-prev-icon"></span>
			</a> <a class="carousel-control-next" href="#demo" data-slide="next">
				<span class="carousel-control-next-icon"></span>
			</a>
		</div>
	</div>

	<!-- 	<div id = "widget3"> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Gantt Chart</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspa chart shows the due-date of work</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Calendar</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspcalendar for team schedule management</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Analysis</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspcharts shows overall project statistics</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Task List</h6>   -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspa list shows tasks by project</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Kanban Board</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspshows task list as Kanban form</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Commits</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspshows the count of git commits</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Issues</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspshows the count of git issues</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">File Sharing</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspfile sharing in team</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">Bug Tracking</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspbug reports during developing a project</p> -->
	<!-- 		<h6 style = "color : #b3b3b3; margin-bottom : 0;">WIKI</h6> -->
	<!-- 		<p style = "color : #a6a6a6; margin-bottom : 0;">&nbsp&nbsp&nbspa manual that allows collaborative editing</p> -->
	<!-- 	</div> -->
</div>


<!-- <div style = "background : #d9d9d9; width : 100%; height : 150px;"></div> -->


<!-- <div style = "width : 100%; background : #d9d9d9; height : 400px;"> -->
<!--     <div id="sitemap" class="container-fluid text-center text-md-left"> -->

<!--         Grid row -->
<!--         <div class="row"> -->

<!--             Grid column -->
<!--             <div class="col-md-3 mx-auto"> -->

<!--                 Links -->
<!--                 <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links</h5> -->

<!--                 <ul class="list-unstyled"> -->
<!--                 <li> -->
<!--                     <a href="#!">Very long link 1</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Very long link 2</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Very long link 3</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Very long link 4</a> -->
<!--                 </li> -->
<!--                 </ul> -->

<!--             </div> -->
<!--             Grid column -->

<!--             <hr class="clearfix w-100 d-md-none"> -->

<!--             Grid column -->
<!--             <div class="col-md-3 mx-auto"> -->

<!--                 Links -->
<!--                 <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links</h5> -->

<!--                 <ul class="list-unstyled"> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 1</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 2</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 3</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 4</a> -->
<!--                 </li> -->
<!--                 </ul> -->

<!--             </div> -->
<!--             Grid column -->

<!--             <hr class="clearfix w-100 d-md-none"> -->

<!--             Grid column -->
<!--             <div class="col-md-3 mx-auto"> -->

<!--                 Links -->
<!--                 <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links</h5> -->

<!--                 <ul class="list-unstyled"> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 1</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 2</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 3</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 4</a> -->
<!--                 </li> -->
<!--                 </ul> -->

<!--             </div> -->
<!--             Grid column -->

<!--             <hr class="clearfix w-100 d-md-none"> -->

<!--             Grid column -->
<!--             <div class="col-md-3 mx-auto"> -->

<!--                 Links -->
<!--                 <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Links</h5> -->

<!--                 <ul class="list-unstyled"> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 1</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 2</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 3</a> -->
<!--                 </li> -->
<!--                 <li> -->
<!--                     <a href="#!">Link 4</a> -->
<!--                 </li> -->
<!--                 </ul> -->

<!--             </div> -->
<!--             Grid column -->

<!--         </div> -->
<!--         Grid row -->

<!--     </div> -->
<!-- </div> -->

<!-- <div style = "background : linear-gradient(to bottom, #4d4d4d, black); width : 100%; height : 150px;"></div> -->