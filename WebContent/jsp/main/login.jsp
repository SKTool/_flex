<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- <!DOCTYPE html> -->
<!-- <html> -->
<!-- <head> -->
<!-- <meta charset="UTF-8"> -->
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="674931549888-nfb6kriv0lrjpmrup2cgp2ktq2cs20ik.apps.googleusercontent.com">
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script> -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<!-- <title>Insert title here</title> -->
<script type="text/javascript">
	$(function(){
		$('#showResendMailModal').on('click', function(){
			$('#resendMailModal').modal('show');
		})
		$('#showidPwInquiryModal').on('click', function(){
			$('#idPwInquiryModal').modal('show');
		})
		
		
		
		
		$('#g-signin2').css('data-width', $('#loginBtn').css('width'));
		
		if(${param.error != null}){
			swal.fire({
				type : 'error',
				text : '아이디 또는 비밀번호를 확인하세요'
			})	
		}
		
		var error = "${error}";
		if(error.length > 0){
			if(error === 'login' || error === 'google_login'){
				swal.fire({
					text : '로그인에 실패하였습니다. 아이디와 비밀번호를 확인해주세요',
					type : 'error'
				})
			}
		}
		
		var result = '${result}';
		if(result.length > 0){
			
			result = Number(result);
			if(result === 1){
				swal.fire({
					text : '인증이 완료되었습니다',
					type : 'success'
				})
			}else if(result === 2){
				swal.fire({
					text : '이미 인증이 완료된 계정입니다',
					type : 'warning'
				})
			}else if(result === -1){
				swal.fire({
					text : '인증에 실패하였습니다',
					type : 'error'
				})
			}else if(result === -2){
				swal.fire({
					text : '정상적인 접근이 아닙니다',
					type : 'error'
				})
			}
		}
		
		var result_join = '${result_join}';
		if(result_join.length > 0){
			
			result_join = Number(result_join);
			if(result_join === 0){
				location.href='/index/abnormalAccess';
			}else if(result_join === 1){
				swal.fire({
					text : '인증 이메일을 발송하였습니다. 인증 후 로그인 해주세요',
					type : 'success'
				})
			}else if(result_join === -1){
				swal.fire({
					text : '회원 가입에 실패하였습니다. 다시 시도해 주세요',
					type : 'error'
				})
				location.href='/user/login';
			}
		}
		
		$('#loginBtn').on('submit', function(){ 
			
			var formInputs = $('#loginForm').find('input[name][type!="hidden"]');
			
			for(var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') != null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						swal.fire({
							text : '빈칸을 입력해주세요',
							type : 'warning'
						})						
						$(formInputs[i]).focus();
						
						return false;
					}
				}
			} 
		});
		
		$('#goBack').on('click', function(){
			location.href = '/index/main';
		});
		
		$('#showResendMail').on('click', function(){
			$('#resendMailDiv').css('display', 'unset');			
		})
		
		$('#resendMail').on('click', function(){
			
			var resendMailInputs = $('#resendMailForm').serialize();
			
			$.ajax({
				url 	: '/user/resendMail',
				type	: 'POST',
				data	: resendMailInputs,
				success : function(result){
					
					if(result === 1){
						swal.fire({
							text : '이메일 재발송이 완료되었습니다',
							type : 'success'
						})
					}else if(result === -1){
						swal.fire({
							text : '이메일 재발송에 실패하였습니다, 아이디와 비밀번호가 맞지 않거나 이미 인증된 계정입니다.',
							type : 'error'
						})
					}
				}
			})
		})
		
		$('#idInquiryDivBtn').on('click', function(){
			$('#idInquiryBtn').css('display', 'unset');
			$('#pwInquiryBtn').css('display', 'none');
		})
		
		$('#pwInquiryDivBtn').on('click', function(){
			$('#pwInquiryBtn').css('display', 'unset'); 
			$('#idInquiryBtn').css('display', 'none');
		})
		

		
		$('#idInquiryBtn').on('click', function(){
			
			var formInputs = $('#idInquiryForm').find('input[name][type!="hidden"]');
			
			for(var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') != null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						swal.fire({
							text : '빈칸을 입력해주세요',
							type : 'warning'
						})						
						$(formInputs[i]).focus();
						
						return false;
					}
				}
			}
			
			var idInquiryForm = $('#idInquiryForm').serialize();
			
			$.ajax({
				
				url 	: '/user/idInquiry',
				type 	: 'POST',
				data 	: idInquiryForm,
				success : function(resultMap){
					
					if(resultMap.result === -1){
						swal.fire({
							text : '빈칸을 입력해주세요',
							type : 'warning'
						})
					}else if(resultMap.result === 1){
						$('#idInquiryFormDiv').css('display', 'none');
						$('#idInquiryAlert').css('display', 'unset');
						$('#upperAlertHr').css('display', 'none');
						$('#belowAlertHr').css('display', 'unset');
						$('#idInquiryAlert').html("<br>" + "<p>" + resultMap.user_id + "</p>" + "<br>");
					}
				}
			});
		});
		 
		$('#pwInquiryBtn').on('click', function(){ 
			
			var formInputs = $('#pwInquiryForm').find('input[name][type != "hidden"]');
			
			for(var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') != null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						swal.fire({
							text : '빈칸을 입력해주세요',
							type : 'warning'
						})
						$(formInputs[i]).focus();
						
						return false;
					}
				}
			}
			
			var pwInquiryForm = $('#pwInquiryForm').serialize();
			
			$.ajax({
				
				url  	: '/user/pwInquiry',
				type 	: 'POST',
				data 	: pwInquiryForm,
				success : function(result){
					
					if(result === 1){
						swal.fire({
							text : '비밀번호 변경 메일을 발송하였습니다',
							type : 'success'
						})
					}else if(result === -1){
						swal.fire({
							text : '일치하는 계정이 없습니다. 다시 확인해 주세요',
							type : 'error'
						})
					}
				}
			});
		});
	});
		
	function onSignIn(googleUser) {
       	var profile = googleUser.getBasicProfile();
		
        $.ajax({
        	url		: '/google/googleUserCheck',
        	type	: 'POST',
        	data	: {'user_id' : profile.getId(),
      		  		   'user_name' : profile.getName(),
      		  		   'user_pic' : profile.getImageUrl(),
      		  		   'user_pw' : profile.getEmail(),
      		  		   '${_csrf.parameterName}' : '${_csrf.token}'},
      		success : function(result){
      			
      			$('#google_user_id').val(profile.getId());
      			$('#google_user_pw').val(profile.getEmail());
      			$('#googleLoginBtn').click();
      		}
        });

        var principal = '${principal}';
		if(!(principal.length > 0)){
			$('#logoutIframe').attr('src',"https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:9080/index/main?logout=true");
			
			$('#logoutIframe').on('load', function(){
				swal.fire({
					text : '계정 정보가 일치하지 않습니다',
					type : 'error'
				})
				location.href='/user/logout';
			})
		}
     }
</script>
<style type="text/css">
/* 	@import url('https://fonts.googleapis.com/css?family=Noto+Serif+KR'); */
	
/* 	body{ */
/* 		font-family: 'Noto Serif KR', serif; */
/* 	} */
/* 	body{ */
/* 		overflow : hidden; */
/* 	} */
	#inquiryToggle li a{
/* 		color : black; */
		color : gray;
		border : none;
	}
	#inquiryToggle li .active {
/* 		color : #008080 !important; */
		color : #17a1b7 !important;
		background : #31373d;
		border : none;
	}
	#backgroundDiv {
		position:absolute; 
		background : lightgray; 
		height : 100%; 
		width : 100%;
		top : 0;
	}
	#backgroundImg {
		top : 0; 
		left : 0; 
		width : 100%; 
		height : 100%;
	}
	#loginDiv {
		position : absolute;
		width : 100%; 
		height : 100%;
		background : rgba(0, 0, 0, 0.7); 
		padding : 0 42%;
		padding-top : 16%;
		padding-bottom : 20%;
	} 
	.abcRioButton {
		width : 100% !important;
	}
	body {
	 	overflow : hidden;
	 }
</style>
<!-- </head> --> 
<!-- <body> -->
<%-- 	<jsp:include page="include/header/header_for_join.jsp"></jsp:include> --%>

	<div id = "backgroundDiv">
		<img id = "backgroundImg" src = "/img/main_background2.jpg">
	</div>
	<div id = "loginDiv">
		<div style = "width : 100%; height : 100%; margin : 0;"> 
			<div class="card bg-dark text-white" style = "width : 100%; height : 100%;">
    			<div class="card-header" style = "text-align :center; font-size: 1.5em;">로그인</div>
    			<div class="card-body"  style = "text-align :center;">
	    			<form id = "loginForm" action = "/user/login" method = "post" style = "width : 100%; height : 100%;">
	 				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
						<br>
						
						
	    				<input class = "form-control" type = "email" name = "user_id" id = "user_id" placeholder = "이메일 주소" required>
						<br>
	    				<input class = "form-control" type = "password" name = "user_pw" id = "user_pw" placeholder = "비밀번호" required>
						
						
						<br>
						<br>
						<br>
	    				<input class = "btn btn-lg btn-secondary btn-block" type = "submit" id = "loginBtn" value = "로그인">
						<br>
	    				<div class="g-signin2" data-onsuccess="onSignIn" data-theme = "dark"></div>
						<br>
						<!--  -->
						<p class = "text-white" id = "showResendMailModal" data-toggle="modal" data-target="#resendMailModal" style = "font-size : 0.9em; cursor : pointer; margin : 0;">이메일 재발송하기</p>
						<p class = "text-white" id = "showidPwInquiryModal" data-toggle="modal" data-target="#idPwInquiryModal" style = "font-size : 0.9em; cursor : pointer; margin : 0;">아이디 / 비밀번호 찾기</p>
						<br>
					</form>
    			</div>
  			</div>
		</div>
	</div>           

<!-- 	<div class="container-fluid" style="padding:0; position : absolute;"> -->
<!--         <div class="row">   -->
<!--             <div class = "container col-xs-7 col-sm-7 col-md-7 col-xl-7" style = "padding : 0;"> -->
<!--             	<img src="/img/login.jpg" class="img-fluid">           -->
<!--             </div> -->
<!-- 			<div class = "container col-xs-5 col-sm-5 col-md-5 col-xl-5"> -->
<!-- 				<form id = "loginForm" action = "login" method = "post" style = "margin-left : 10%; margin-top : 8%; min-height : 100%;"> -->
<%-- 				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> --%>
					
<!-- 					<h2>로그인</h2> -->
<!-- 					<hr class = "bg-info">  -->
<!-- 					<br> -->
					
<!-- 					<table style = "width : 100%;"> -->
<!-- 						<tr> -->
<!-- 							<td>     -->
<!-- 								<br> -->
<!-- 								<br> -->
<!-- 								<br> -->
<!-- 								<hr> -->
<!-- 								<br> -->
<!-- 							</td> -->
<!-- 							<td> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<input class = "form-control" type = "email" name = "user_id" id = "user_id" placeholder = "이메일 주소" required width = "100%"> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<br> -->
<!-- 								<input class = "form-control" type = "password" name = "user_pw" placeholder = "비밀번호" required> -->
<!-- 							</td> -->
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<br> -->
<!-- 								<br> -->
<!-- 								<input class = "btn btn-lg btn-outline-info btn-block" type = "submit" id = "loginBtn" value = "로그인"> -->
<!-- 							</td> -->
<!-- 						</tr>		 -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<hr> -->
<!-- 								<p style = "font-size : 0.8em; color : gray; text-align : center;">또는</p> -->
<!-- 							</td> -->
<!-- 						</tr>		 -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<div class="g-signin2" data-onsuccess="onSignIn" data-theme = "dark" data-width = "352"></div> -->
<!-- 								<br> -->
<!-- 								<span style = "font-size : 0.9em; color : gray; margin-left : 2.5em;">  -->
<!-- 									이메일이 오지 않았나요? <span class = "text-primary" data-toggle="modal" data-target="#resendMailModal" style = "cursor : pointer;">이매일 재발송하기</span> -->
<!-- 								</span> -->
<!-- 								<span style = "font-size : 0.9em; color : gray; margin-left : 7em;"> -->
<!-- 									 <span data-toggle="modal" data-target="#idPwInquiryModal" style = "cursor : pointer;">아이디 / 비밀번호 찾기</span> -->
<!-- 								</span> -->
<!-- 							</td>   -->
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<td width = "50%"> -->
<!-- 								<br> -->
<%-- 								<c:if test="${param.error !=null }"> --%>
<!-- 									<div align = "center"> -->
<!-- 										<p style="color:red">아이디 또는 비밀번호를 확인하세요</p> -->
<!-- 									</div> -->
<%-- 								</c:if>				 --%>
<!-- 							</td> -->
<!-- 						</tr>	 -->
<!-- 					</table> -->
<!-- 				</form>			 -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<div align = "center" style = "display:none;">
		<form id = "googleLoginForm" action = "/google/google_login" method = "post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
			<table>
				<tr>
					<td>
						<input type = "text" name = "user_id" id = "google_user_id" required>
						<input type = "text" name = "user_pw" id = "google_user_pw" required>
						<input type = "submit" id = "googleLoginBtn" value = "로그인">
					</td>
				</tr>		
			</table>
		</form>			
	</div>
	<iframe id = "logoutIframe" style = "display : none;"></iframe>


	<div class="modal" id="resendMailModal" style = "margin-top : 10%;">
		<div class="modal-dialog rounded" style = "width : 15%;">
			<div class="modal-content rounded bg-dark" style = "width : 100%; height : 100%">
				
				<div class="modal-body rounded" style = "padding : 0; width : 100%; height : 100%;">
				
					<div class="card bg-dark text-white" style = "width : 100%; height : 100%;">
    					<div class="card-header" style = "text-align :center;">이메일 재발송하기</div>
   						<div class="card-body"  style = "text-align :center;">
   							<form id = "resendMailForm"> 
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
								<table width = "100%">
									<tr>
										<td> 
											<input class = "form-control" type="email" name="user_id" placeholder="이메일" required>
											<br>
										</td>
									</tr>
									<tr>
										<td>
											<input class = "form-control" type="password" name="user_pw" placeholder="비밀번호" required>
										</td>
									</tr>
								</table>
							</form>
   						</div>
   						<div class="card-footer"  style = "text-align :center;">
   							<button type="button" class="btn btn-info" id="resendMail" data-dismiss="modal">재발송</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">닫기</button>
   						</div>
   					</div>
				
<!-- 					<form id = "resendMailForm"> -->
<%-- 						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> --%>
<!-- 						<table> -->
<!-- 							<tr> -->
<!-- 								<td> -->
<!-- 									<input class = "form-control" type="email" name="user_id" placeholder="이메일" required> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 							<tr> -->
<!-- 								<td> -->
<!-- 									<input class = "form-control" type="password" name="user_pw" placeholder="비밀번호" required> -->
<!-- 								</td> -->
<!-- 							</tr> -->
<!-- 						</table> -->
<!-- 					</form> -->
				</div>

<!-- 				<div class="modal-footer"> -->
<!-- 					<button type="button" class="btn btn-info" id="resendMail" data-dismiss="modal">재발송</button> -->
<!-- 					<button type="button" class="btn btn-danger" data-dismiss="modal">닫기</button> -->
<!-- 				</div> -->

			</div>
		</div>
	</div>
	
	<div class="modal" id="idPwInquiryModal" style = "margin-top : 10%;">
		<div class="modal-dialog">
			<div class="modal-content" style = "width : 70%; margin-left : 15%;">

				<div class="modal-body bg-dark" align = "center" style = "width : 100%; padding : 0;">
				 
					<div class="card bg-dark text-white" style = "width : 100%; height : 100%;">
    					<div class="card-header" style = "text-align :center;">
    						<ul class="nav nav-tabs" id = "inquiryToggle" style = "border : none;">
						    	<li class="nav-item">
						      		<a class="nav-link active" data-toggle="tab" id = "idInquiryDivBtn" href="#idInquiryDiv">아이디찾기</a>
						    	</li>
						    	<li class="nav-item">
						      		<a class="nav-link" data-toggle="tab" id = "pwInquiryDivBtn" href="#pwInquiryDiv">비밀번호 찾기</a>
						    	</li>
						  	</ul>
    					</div>
   						<div class="card-body"  style = "text-align :center;">
   							<div class="tab-content" id = "toggleBody" style = "width : 100%;">
						
						    	<div id="idInquiryDiv" class="container tab-pane active"><br>
						      		<div id = "idInquiryFormDiv" style = "width : 100%;">
							      		<form id = "idInquiryForm" style = "width : 100%;">
										<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
											<table width = "100%"> 
												<tr>
													<td>
														<input type = "text" class = "form-control" name = "user_name" placeholder = "이름" required>
													</td>
												</tr>
												<tr>
													<td>
														<br>	
														<input type = "tel" class = "form-control" name = "user_tel" placeholder = "전화번호" required>
														<br>
													</td>
												</tr>
											</table>
										</form>
						      		</div>
<!-- 						      		<hr class = "bg-info" id = "upperAlertHr"> -->
									<div id = "idInquiryAlert" style = "display : none;">
						    		</div>
<!-- 						      		<hr class = "bg-info" id = "belowAlertHr" style = "display : none;"> -->
						    	</div>
						    	
						    	
						    	<div id="pwInquiryDiv" class="container tab-pane fade"><br>
						      		<form id = "pwInquiryForm">
									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
										<table width = "100%">
											<tr>
												<td>
													<input type = "email" class = "form-control" name = "user_id" placeholder = "아이디" required>
												</td>
											</tr>
											<tr>
												<td>
													<br>	
													<input type = "text" class = "form-control" name = "user_name"  placeholder = "이름" required>
												</td>
											</tr>
											<tr>
												<td>
													<br>	
													<input type = "tel" class = "form-control" name = "user_tel" placeholder = "전화번호" required>
													<br>
												</td>
											</tr>
										</table>
									</form>
						    	</div>
						  	</div>
   						</div>
   						<div class="card-footer"  style = "text-align :center;">
	   						<button type="button" id = "idInquiryBtn" class="btn btn-info">아이디 찾기</button>
							<button type="button" id = "pwInquiryBtn" class="btn btn-info" data-dismiss="modal" style = "display : none;">비밀번호 변경하기</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">닫기</button>
   						</div>
   						
   						
   						
<!-- 					<div class="container mt-3"> -->
<!-- 						<ul class="nav nav-tabs" id = "inquiryToggle"> -->
<!-- 					    	<li class="nav-item"> -->
<!-- 					      		<a class="nav-link active" data-toggle="tab" id = "idInquiryDivBtn" href="#idInquiryDiv">아이디찾기</a> -->
<!-- 					    	</li> -->
<!-- 					    	<li class="nav-item"> -->
<!-- 					      		<a class="nav-link" data-toggle="tab" id = "pwInquiryDivBtn" href="#pwInquiryDiv">비밀번호 찾기</a> -->
<!-- 					    	</li> -->
<!-- 					  	</ul> -->
					
<!-- 						<div class="tab-content" id = "toggleBody"> -->
						
<!-- 					    	<div id="idInquiryDiv" class="container tab-pane active"><br> -->
<!-- 					      		<h3 style = "color : gray;">아이디찾기</h3> -->
<!-- 					      		<hr class = "bg-info"> -->
<!-- 					      		<div id = "idInquiryFormDiv"> -->
<!-- 						      		<form id = "idInquiryForm"> -->
<%-- 									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> --%>
<!-- 										<table> -->
<!-- 											<tr> -->
<!-- 												<td> -->
<!-- 													<br>	 -->
<!-- 													<input type = "text" class = "form-control" name = "user_name" placeholder = "이름" required> -->
<!-- 												</td> -->
<!-- 											</tr> -->
<!-- 											<tr> -->
<!-- 												<td> -->
<!-- 													<br>	 -->
<!-- 													<input type = "tel" class = "form-control" name = "user_tel" placeholder = "전화번호" required> -->
<!-- 													<br> -->
<!-- 													<br> -->
<!-- 												</td> -->
<!-- 											</tr> -->
<!-- 										</table> -->
<!-- 									</form> -->
<!-- 					      		</div> -->
<!-- 					      		<hr class = "bg-info" id = "upperAlertHr"> -->
<!-- 								<div id = "idInquiryAlert" style = "display : none;"> -->
<!-- 					    		</div> -->
<!-- 					      		<hr class = "bg-info" id = "belowAlertHr" style = "display : none;"> -->
<!-- 					    	</div> -->
					    	
					    	
<!-- 					    	<div id="pwInquiryDiv" class="container tab-pane fade"><br> -->
<!-- 					      		<h3 style = "color : gray;">비밀번호 찾기</h3> -->
<!-- 					      		<hr class = "bg-info"> -->
<!-- 					      		<form id = "pwInquiryForm"> -->
<%-- 								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> --%>
<!-- 									<table> -->
<!-- 										<tr> -->
<!-- 											<td> -->
<!-- 												<br> -->
<!-- 												<input type = "email" class = "form-control" name = "user_id" placeholder = "아이디" required> -->
<!-- 											</td> -->
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<td> -->
<!-- 												<br>	 -->
<!-- 												<input type = "text" class = "form-control" name = "user_name"  placeholder = "이름" required> -->
<!-- 											</td> -->
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<td> -->
<!-- 												<br>	 -->
<!-- 												<input type = "tel" class = "form-control" name = "user_tel" placeholder = "전화번호" required> -->
<!-- 												<br> -->
<!-- 											</td> -->
<!-- 										</tr> -->
<!-- 									</table> -->
<!-- 								</form> -->
<!-- 								<hr class = "bg-info"> -->
<!-- 					    	</div> -->
<!-- 					  	</div> -->
					</div>
				</div>
<!-- 				<div class="modal-footer"> -->
<!-- 					<button type="button" id = "idInquiryBtn" class="btn btn-outline-primary">아이디 찾기</button> -->
<!-- 					<button type="button" id = "pwInquiryBtn" class="btn btn-outline-primary" data-dismiss="modal" style = "display : none;">비밀번호 변경하기</button> -->
<!-- 					<button type="button" class="btn btn-outline-danger" data-dismiss="modal">닫기</button> -->
<!-- 				</div> -->
			</div>
		</div>
	</div> 
	
	
	
	
<!-- 	<footer class="page-footer font-small indigo"> -->
<%-- 		<jsp:include page="include/footer/footer_for_join.jsp"></jsp:include> --%>
<!-- 	</footer> -->
<!-- </body> -->
<!-- </html> -->