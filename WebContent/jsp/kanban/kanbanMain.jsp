<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>칸반</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css"
	type="text/css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
	
<!-- sweetAlert2 CDN -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>	
<!-- 머티리얼 아이콘 CDN -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/icon?family=Material+Icons">
<!-- DatePicker CDN -->
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<!-- FASTSELECT CSS -->
<link rel="stylesheet" href="/css/fastselect.css" />
<!-- FASTSELECT JS  -->
<script src="/js/fastselect.standalone.js"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script type="text/javascript">
	$(function() {
		createKanbanList();
		//DatePicker
		$(".createDatePicker").flatpickr();
		$(".dueDatePicker").flatpickr();

		$("#select-taskMember").fastselect();
		$("#select-taskTag").fastselect();
		
		function createKanbanList() {
			var taskTitle = $("#taskTitle").val();
			var importance = $("#importance").val();
			var taskMember = $("#select-taskMember").val();
			var taskTag = $("#select-taskTag").val();
			
			var todoUl = $("#todo");
			$("#todo div:gt(0)").remove();
			
			var improgressUl = $("#inprogress");
			$("#inprogress div:gt(0)").remove();			
			var doneUl = $("#done");
			$("#done div:gt(0)").remove();

			$.ajax({
				url : "/task/selectTask",
				data : {"project_idx" : $("#projectIdx").val(),
						"${_csrf.parameterName}" : "${_csrf.token}"},
				type : "post",
				dataType : "json",
				success : function(data) {
					for ( var i in data) {
						var div = $("<div id='kanbanDiv"+i+"' data-toggle='modal' data-target='#kanbanViewModal' style='border: 1px solid #e8eaed; border-radius: 5px; background: #fff; cursor: pointer;padding: 10px 10px 10px 10px;'>");
						var subDiv = $("<div id='subDiv'>");
// 						$("<div id='titleDiv' style='max-width: 100%; width: auto; white-space: pre-wrap; word-break: break-word; caret-color: rgb(55, 53, 47); font-size: 14px; line-height: 1.5; min-height: 3px; font-weight: 500; pointer-events: none;'>").text(data[i].task_title).appendTo(div); //제목
						var title = $("<div id='subDiv'>").append($("<div id='titleDiv' style='max-width: 100%; width: auto; white-space: pre-wrap; word-break: break-word; caret-color: rgb(55, 53, 47); font-size: 14px; line-height: 1.5; min-height: 3px; font-weight: 500; pointer-events: none;'>").text(data[i].task_title));
						title.appendTo(div);
// 						$("<div id='importanceDiv' style='display: flex; align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; font-weight: bold; line-height: 120%; color: rgb(55, 53, 47); margin: 0px 6px 6px 0px; '>").text(data[i].importance_name).appendTo(div); //중요도
						var importance = $("<div id='subDiv'>").append($("<div id='importanceDiv' style='display: flex; align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; font-weight: bold; line-height: 120%; color: rgb(55, 53, 47); margin: 0px 6px 6px 0px; '>").text(data[i].importance_name));
						importance.appendTo(div);
						for ( var j = 0 ; j < data[i].members.length ; j++ ) {
// 						$("<div id='memberDiv' style='align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; line-height: 120%; color: rgb(55, 53, 47); background: rgba(206, 205, 202, 0.5); margin: 0px 6px 6px 0px;'>").text(data[i].members[j].user_name).appendTo(div); //함께하는 이
						$("<div id='memberDiv' style='display: inline; align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; line-height: 120%; color: rgb(55, 53, 47); background: rgba(206, 205, 202, 0.5); margin: 0px 6px 6px 0px;'>").text(data[i].members[j].user_name).appendTo(div); //함께하는 이
						}
						
						for ( var k = 0 ; k < data[i].tags.length ; k++ ) {
// 						$("<div id='tagDiv' style='align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; line-height: 120%; color: rgb(55, 53, 47); background: rgba(0, 120, 223, 0.2); margin: 0px 6px 6px 0px;'>").text(data[i].tags[k].task_tag_name).appendTo(div); //tagLis
						$("<div id='tagDiv' style='display: inline; align-items: center; flex-shrink: 1; min-width: 0px; height: 18px; border-radius: 3px; padding-left: 6px; padding-right: 6px; font-size: 12px; line-height: 120%; color: rgb(55, 53, 47); background: rgba(0, 120, 223, 0.2); margin: 0px 6px 6px 0px;'>").text(data[i].tags[k].task_tag_name).appendTo(div); //tagLis
						}
						
						if(data[i].status == -1){
							div.appendTo(todoUl);
						} else if (data[i].status == 0){
							div.appendTo(improgressUl);
						} else if (data[i].status == 1){
							div.appendTo(doneUl);
						}
						var detailKanban = $('#kanbanDiv' + i);
						(function(m) {
							detailKanban.on("click", function() {
								$.ajax({
									url : "/task/fastSelectViewTag",
									data : {"task_idx" : data[m].task_idx,
											"${_csrf.parameterName}" : "${_csrf.token}"},
									type : "get",
									dataType : "text",
									success : function(data) {
										var taskTag = $('<input type="text" multiple class="tagsInput" data-url="/task/fastSelectTag" data-user-option-allowed="true" name="taskTag" '
								                  +'  id = "selectView-taskTag" data-initial-value=\''+data+'\'>')
								            taskTag.appendTo($("#selectView-taskTag-div"));
											taskTag.fastselect(); 	
									}	
								});
								$.ajax({
									url : "/task/fastSelectViewMember",
									data : {"task_idx" : data[m].task_idx,
											"project_idx" : $("#projectIdx").val(),
											"${_csrf.parameterName}" : "${_csrf.token}"},
									type : "get",
									dataType : "text",
									success : function(data) {
										var taskMember = $('<input type="text" multiple class="tagsInput" data-url="/task/fastSelectMember/1" data-user-option-allowed="true" name="taskMember" '
								                  +'  id = "selectView-taskMember" data-initial-value=\''+data+'\'>')
								            taskMember.appendTo($("#selectView-taskMember-div"));
											taskMember.fastselect(); 	
									}	
								});								
								$("#selectView-taskTag-div").children().remove();
								$("#selectView-taskMember-div").children().remove();
								
								var title = data[m].task_title;
								var createDate = data[m].task_start;
								var dueDate = data[m].task_end;
								var status = data[m].status;
								var importance = data[m].importance_idx;
								var content = data[m].task_content;

								$("#taskViewTitle").val(title);
								$("#taskViewTitle").attr("disabled","true");
								$("#taskViewStart").val(createDate);
								$("#taskViewStart").attr("disabled","true");
								$("#taskViewEnd").val(dueDate);
								$("#taskViewEnd").attr("disabled","true");
								$("#viewStatus").val(status);
								$("#viewStatus").attr("value",status);
								$("#viewStatus").attr("disabled","true");
								$("#viewImportance").val(importance);
								$("#viewImportance").attr("value", importance);
								$("#viewImportance").attr("disabled","true");
								$("#taskViewContent").val(content);
								$("#taskViewContent").attr("readonly","disabled");
								
								$("#btnModify").toggle(function() {
									Swal.fire({
											title:'활성화됩니다',
											type: 'info',
											confirmButtonText: '확인'
											});
									$("#taskViewTitle").attr("disabled", false);
									$("#taskViewStart").attr("disabled", false);
									$("#taskViewEnd").attr("disabled", false);
									$("#viewStatus").attr("disabled", false);
									$("#viewImportance").attr("disabled", false);
									$("#taskViewContent").attr("readonly", false);
								}, function() {
									$.ajax({
										url : "/task/modify",
										data : {"task_idx" : data[m].task_idx,
												"task_title" : $("#taskViewTitle").val(),
												"task_member_idx" : $("#selectView-taskMember").val(),
												"task_start" : $("#taskViewStart").val(),
												"task_end" : $("#taskViewEnd").val(),
												"status" : $("#viewStatus").val(),
												"task_tag_idx" : $("#selectView-taskTag").val(),
												"importance_idx" : $("#viewImportance").val(),
												"task_content" : $("#taskViewContent").val(),
												"${_csrf.parameterName}" : "${_csrf.token}"},
										type : "post",
										success : function(result) {
											if (result) {
												Swal.fire({
													  type: 'success',
													  title: '정상적으로 수정되었습니다',
													  confirmButtonText: '확인',
													  timer: 1500
													})
												$("#kanbanViewForm").hide("slow");
												createKanbanList();
// 												location.reload();
											}
										}
									});
									return false;
								});
								
								$("#btnDelete").on("click",function(){
									Swal.fire({
										  title: '삭제하시겠습니까?',
										  type: 'warning',
										  showCancelButton: true,
										  confirmButtonColor: '#3085d6',
										  cancelButtonColor: '#d33',
										  confirmButtonText: '확인',
										  cancelButtonText: '취소'
										}).then((result) => {
											$.ajax({
												url : "/task/delete",
												data : {"task_idx" : data[m].task_idx,
														"${_csrf.parameterName}" : "${_csrf.token}"},
												type : "post",
												success : function(data) {
													  if (result.value) {
														    Swal.fire(
														      '삭제',
														      'success'
														    )
														  }
													createKanbanList();
// 													location.reload();
												}
											});
										})
								});
							});	
						})(i);			
					}
				}
			});
		}
			
		$("#btnWrite2").on("click", function() {
			//task 추가
			$.ajax({
				url : "/task/write",
				data : {
					"task_title" : $("#taskTitle").val(),
					"task_start" : $("#taskStart").val(),
					"task_end" : $("#taskEnd").val(),
					"status" : $("#status").val(),
					"task_content" : $("#taskContent").val(),
					"importance_idx" : $("#importance").val(),
					"task_tag_idx" : $("#select-taskTag").val(),
					"project_member_idx" : $("#select-taskMember").val(),
					"project_idx" : $("#projectIdx").val(),
					"${_csrf.parameterName}" : "${_csrf.token}"},
				type : "post",
				success : function(data) {
					Swal.fire({
						  type: 'success',
						  title: '정상적으로 등록되었습니다',
						  confirmButtonText: '확인',
						  showConfirmButton: false,
						  timer: 1500
						})
					createKanbanList();
// 					location.reload();
				}
			});
		});	

		//드래그
		$(".sortable").sortable({
			connectWith : ".sortable",
			cancel: "#titleHeader"
		});
	});
</script>
<style type="text/css">
#subDiv {
	display:inline;
}
/* input[type=text], select { */
/*  	-webkit-appearance: none;  */
/*  	width: 100%;  */
/*  	height: 50%;  */
/*  	border: 0;  */
/*  	font-family: inherit;  */
/*  	padding: 12px 0;  */
/*  	font-size: 12px;  */
/*  	font-weight: 100;  */
/*   	border-bottom: 2px solid #C8CCD4;  */
/*  	background: none;  */
/*  	border-radius: 0;  */
/*  	color: #223254;  */
/*  	transition: all .15s ease;  */
/*   	text-align: center;   */
/*  	align: center;  */
/*  	font-size: 0.8em;  */
/*  	font-weight: 100;  */
/*  	text-align-last:center;  */
/* 	display: inline-block;  */
/* 	margin-left: 10px;  */
/* 	margin-bottom: 15px;  */
/* 	position: relative;		  */
/* } */
body {
	background-color: #eeeeee;
}
div.kanban {
/* 	border-top: 5px solid #001133; */
	width: 300px;
	height: auto;
/* 	margin: 2%; */
	display: inline-block;
/* 	flex-direction: column; */
	z-index: 1;
	position: relative;
/* 	background: #fff; */
	background: #eeeeee;
/* 	padding: 1em; */
	border-radius: 2px;
}

button {
	margin-left: 5px;
	font-size: 0.8em;
	font-weight: 100;
}
.column {
  float: left;
  width: 25%;
  margin-left:8%;
}
#titleHeader{
	margin-top: 3%;
}
.row:after {
  content: "";
/*   display: table; */
  clear: both;
}
</style>
<script type="text/javascript">
	//Get the elements with class="column"
	var elements = document.getElementsByClassName("column");
	var a = document.getElementsByClassName("kanban");
	var i;

	// List View
	function listView() {
 	 for (i = 0; i < elements.length; i++) {
  	  elements[i].style.width = "100%"; 
  	  a[i].style.width = "90%";
  		}
	}
	// Grid View
	function gridView() {
 	 for (i = 0; i < elements.length; i++) {
   		elements[i].style.width = "25%";
  		}
	}
</script>
</head>

<body>
	<div style="width: 90%; margin-left: 5%; ">
			<div style="width: 100%;">
			<br> 
			<br> 
			<h2 id="todoTitle" style ="width: 95%;margin-left: 5%; color: gray; font-weight:100;">Kanban Board</h2>
			<p style = " margin-left : 5%;color : lightgray; font-size : 0.9em;">한눈에 확인 가능한 칸반 형식으로 일정을 확인하세요</p>
			<hr style = "width : 98%; margin-left : 1%;">
			<br>
		</div>
	</div>
	<div id="btnContainer" style="margin-left:7%;">
		<button class="btn" onclick="listView()"><i class="fa fa-bars"></i> List</button> 
		<button class="btn active" onclick="gridView()"><i class="fa fa-th-large"></i> Kanvan</button>
		<i class="fas fa-plus" id="btnWrite" data-toggle="modal" data-target="#kanbanWriteModal" style="cursor: pointer; position: absolute; margin:10px; right:150px;"></i>
	</div>
	<br>
	<div class="kanbanEntire">
		<!-- Todo -->
		<div class="row">
			<div class="column">
				<div class="kanban todo sortable" id="todo">
					<div id="titleHeader" style="background-color: #001133; color:white; padding: 15px;">No Status</div>
				</div>
		</div>

		<!-- InProgress -->
			<div class="column">
				<div class="kanban inprogress sortable" id="inprogress">
					<div id="titleHeader" style="background-color: #001133; color:white; padding: 15px;">In Progress</div>
				</div>
		</div>

		<!-- Done -->
			<div class="column">
				<div class="kanban done sortable" id="done">
					<div id="titleHeader" style="background-color: #001133; color:white; padding: 15px;">Completed</div>
				</div>
		</div>
		</div>
<!-- 		<button type="button" id="btnWrite" class="btn btn-primary" -->
<!-- 			data-toggle="modal" data-target="#kanbanWriteModal"> -->

			
<!-- 			</button> -->
<!-- 		<button id="btnList">리스트</button> -->
		
		
		
		
		
		
		<!-- 일정추가 모달 -->
		<div class="modal" id="kanbanWriteModal">
			<div class="modal-dialog modal-lg" style="width:600px; margin-left:35%;">
				<div class="modal-content">
					<div class="modal-header">
					<input type="hidden" id="projectIdx" value="${project_idx}">
						<h4 class="modal-title" style = "margin-top : 10px;">
							<label for="taskTitle">제목</label> <input  type="text" id="taskTitle" name="task_title" style="-webkit-appearance: none;border: 0; font-family: inherit;font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block;">
						</h4>
<!-- 						<br> -->
					
<!-- 						<h4 class="modal-title"> -->
<!-- 							<label for="taskTitle">제목</label> <input  type="text" id="taskTitle" name="task_title" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative; float :left;"> -->
<!-- 						</h4> -->
						<button type="button" class="close" data-dismiss="modal" style = "margin : 0;">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="">배정된 멤버</label>
							<div class="col-sm-9">
							<input type="text" multiple class="form-control" id="select-taskMember" 
									data-load-once="true" data-user-option-allowed="true"
									data-url="/task/fastSelectMember/1"
									name="projectMember" />
							</div>
						</div>
						<br> <br> createDate <input type="text"
							class="createDatePicker col-3" id="taskStart" name="task_start" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
						<br> <br> dueDate <input type="text"
							class="dueDatePicker col-3" id="taskEnd" name="task_end" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;"> <br>
						<br> status : <select class="col-3" name="status" id="status" name="status" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
							<option value="-1">No Status</option>
							<option value="0">In Progress</option>
							<option value="1">Completed</option>
						</select> <br> <br>

						<div class="form-group">
							<label class="col-sm-3 control-label" for="">직업군</label>
							<div class="col-sm-9">
								<input type="text" multiple class="form-control col-3" id="select-taskTag" 
									data-load-once="true" data-user-option-allowed="true"
									data-url="/task/fastSelectTag"
									name="taskTag" />
							</div>
						</div>
						
						중요도 : <select class="col-2" name="importance" id="importance" name="importance" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
							<option value="1">낮음</option>
							<option value="2">중간</option>
							<option value="3">높음</option>
						</select> <br> <br> <br> <br> 
						content : <textarea rows="10" cols="50" id="taskContent" name="task_content"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-info btn-sm" id="btnWrite2">등록</button>
					</div>
				</div>
			</div>
		</div>
		
		<!-- View 모달 -->
		<div class="modal" id="kanbanViewModal">
			<form id="kanbanViewForm">
			<div class="modal-dialog modal-lg" style="width:600px; margin-left:35%;">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">
							<label id="taskViewTitle">제목</label> <input type="text" id="taskViewTitle" name="task_title" style="-webkit-appearance: none;border: 0; font-family: inherit;font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block;">
						</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="">배정된 멤버</label>
							<div id="selectView-taskMember-div" class="col-sm-9">
							<!-- ajax를 통하여 input append -->	
							</div>
						</div>								
						<script>
						</script>		
						<br> createDate <input type="text"
							class="createDatePicker col-3" id="taskViewStart" name="task_start" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
						<br> dueDate <input type="text"
							class="dueDatePicker col-3" id="taskViewEnd" name="task_end" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;"> <br>
						<br> status : <select class="col-2" name="status" id="viewStatus"
							name="status" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
							<option value="-1">No Status</option>
							<option value="0">In Progress</option>
							<option value="1">Completed</option>
						</select> <br> <br>
						<div class="form-group">
							<label class="control-label" for="">직업군</label>
							<div id="selectView-taskTag-div" class="col-sm-9">
							<!-- ajax를 통하여 input append -->
							</div>
						</div>
						중요도 : <select class="col-2" name="importance" id="viewImportance"
							name="importance" style="-webkit-appearance: none; width: 100%; height: 50%; border: 0; font-family: inherit; padding: 12px 0; font-size: 12px; font-weight: 100; border-bottom: 2px solid #C8CCD4; background: none; border-radius: 0; color: #223254; transition: all .15s ease; text-align: center; align: center; font-size: 0.8em; font-weight: 100; text-align-last: center; display: inline-block; margin-left: 10px; margin-bottom: 15px; position: relative;">
							<option value="1">낮음</option>
							<option value="2">중간</option>
							<option value="3">높음</option>
						</select> <br> <br> <br> <br> content :
						<textarea rows="10" cols="50" id="taskViewContent"
							name="task_content"></textarea>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="submit" class="btn btn-info btn-sm" id="btnModify">수정</button>
						<button type="button" class="btn btn-danger btn-sm" id="btnDelete">삭제</button>
					</div>
				</div>
			</div>
			</form>	
		</div>
	</div>
</body>
</html>