<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
    <style>
        body {
            background-color: #eff2f6;
        }
        .stat-container {

        }
        .stat-col {
            border-bottom: 0.05rem solid #85CE36;
            float: left;
            white-space: nowrap;
            overflow: hidden;
            margin-bottom: 1rem;

            position: relative;
        }
        .stat-icon {
            color: #BDBDBD;
            display: inline-block;
            font-size: 1.5rem;
            text-align: center;
            vertical-align: middle;
            width: 2rem;

            /* margin-right: 4rem; */

            position: absolute;
            top: 0.3rem;
            left: 0;
        }
        .stat {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            display: inline-block;
            margin-right: 10px;

            margin-left: 1.5rem;
        }
        .stat .value {
            font-size: 1.3rem;
            line-height: 24px;
            overflow: hidden;
            text-overflow: ellipsis;
            font-weight: 500;
        }
        .stat .name {
            font-size: 1rem;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .stat-container .stat-row {
            margin-bottom: 1.5rem;
        }
        #bash-project-list > table > tbody * {
            padding: .5rem 0 .5rem .8rem;
        }
        .card-body-graph {
            padding: .5rem 0 .5rem 0;
            /* margin:  0 .5rem 0 .5rem 0; */

        }
        #nav-tab a {
            color: black;

        }
        #nav-tab .active {
            border-top: 0.1rem solid #9ed85f;

        }

    </style>

	<script>
	
		function createProjectList() {
			$('#project_table').find('*').remove();
			
			$.ajax({
				url: '/adminPage/projectListOnlyfive' ,
				data: {'${_csrf.parameterName}' : '${_csrf.token}'} , 
				type: 'post' ,
				dataType: 'json' ,
				success: function(data){

					for ( i in data ) {
						var $tr = 
							'<tr>'
							+ 	'<th scope="row">'+(parseInt(i)+1)+'<input type="hidden" id="projectIdx'+i+'"/></th> '
							+ 	'<td>'+data[i].project_title+'</td>'
							+ 	'<td>'+data[i].project_start+'</td>'
							+ 	'<td>'+data[i].project_end+'</td>'
							+ '</tr>'

						$('#project_table').append($tr);
						$('#projectIdx'+i).val(data[i].project_idx)
					}
					
				} ,
				error:function(data) {
					alert("project list error")
					
				}
			});
		
		}
		
		
$(function() {
	createProjectList();
	
})
							
	</script>
                                
    <div style="padding : 2% 5% 2% 5%;">
	<div class="px-5 pt-2" style="background-color: white;">
	
<!--     <div class="p-5"> -->

    <div class="container-fluid stat-container">
        <div class="row stat-row">
            <div class="col-md-5">

                <div class="card" style="height: 20rem;">
            
                    <div class="card-body p-3">
                        <blockquote class="blockquote">
                            <a style="font-weight: bold; font-size: 1rem;">Stats</a>
                            <p style="font-size: .5rem; color: #ccc;"> 사이트의 <a style="border-bottom: .01rem solid #ccc"> 상태를 표시하는 공간 </a> </p>
                            <!-- <footer class="card-blockquote">Footer<cite title="Source title">Source title</cite></footer> -->
                        </blockquote>

                        <div class="container stat-container">
                            <div class="row">
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                        <i class="fa fa-rocket"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ projectCount.projectCount } </div>
                                        <div class="name"> Project items </div>
                                    </div>
                                </div>
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ userCount.userCount } </div>
                                        <div class="name"> Member items </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                        <i class="fa fa-list-alt"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ recruitmentCount.recruitmentCount } </div>
                                        <div class="name"> Recruitment items </div>
                                    </div>
                                </div>
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                            <i class="fa fa-list-alt"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ msgBoardCount.msgBoardCount } </div>
                                        <div class="name"> MsgBoard items </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                        <i class="fa fa-list-alt"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ adminCount.adminBoardCount } </div>
                                        <div class="name"> Notice items </div>
                                    </div>
                                </div>
                                <div class="col-6 stat-col">
                                    <div class="stat-icon">
                                         <i class="fa fa-list-alt"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"> ${ qnaBoardCount.qnaCount } </div>
                                        <div class="name"> Q&A items </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>
 
            <div class="col-md-7">
                <div class="card" style="height: 20rem;">
                    <!-- <img class="card-img-top" src="holder.js/100x180/" alt=""> -->
                    <div class="card-body card-body-graph">

                        <nav>
                            <div class="nav nav-tabs justify-content-end" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link px-1 active" id="nav-project-tab" data-toggle="tab" href="#nav-project" role="tab" aria-controls="nav-project" aria-selected="true">Project</a>
                                <a class="nav-item nav-link px-1" id="nav-member-tab" data-toggle="tab" href="#nav-member" role="tab" aria-controls="nav-member" aria-selected="false">Member</a>
                                <a class="nav-item nav-link px-1" id="nav-board-tab" data-toggle="tab" href="#nav-board" role="tab" aria-controls="nav-board" aria-selected="false">Board</a>
                            </div>
                        </nav>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-project" role="tabpanel" aria-labelledby="nav-project-tab">
                                <canvas id="project_chart" width="200" height="80" 
                                    style="min-height: 270px;min-width: 100px; max-width: 1000px; max-height: 150px;"></canvas>

                            </div>
                            <div class="tab-pane fade" id="nav-member" role="tabpanel" aria-labelledby="nav-member-tab">
                                <canvas id="member_chart" width="200" height="80" 
                                    style="min-height: 270px;min-width: 100px; max-width: 1000px; max-height: 150px;"></canvas>

                            </div>
                            <div class="tab-pane fade" id="nav-board" role="tabpanel" aria-labelledby="nav-board-tab">
                                <canvas id="board_chart" width="200" height="80" 
                                    style="min-height: 270px;min-width: 100px; max-width: 1000px; max-height: 150px;"></canvas>

                            </div>
                        </div>



                        <!-- <canvas id="board_chart" width="200" height="80" 
                            style="min-height: 270px;min-width: 100px; max-width: 1000px; max-height: 150px;"></canvas> -->


                        <script>

                            var ctx = document.getElementById('project_chart').getContext('2d');
                            var chart = new Chart(ctx, {
                             // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                    data: {
                                        labels: ['1분기', '2분기', '3분기', '4분기'],
                                        datasets: [
                                            { 
                                            "label": "Member",
//                                             "data": [51, 30, 10, 50],
                                            "data": [ ${projectQuarter[0].quarterdata}, ${projectQuarter[1].quarterdata}, ${projectQuarter[2].quarterdata}, ${projectQuarter[3].quarterdata}],
                                            "borderColor": "rgb(196, 232, 167)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(196, 232, 167, 0.8)",
                                            "lineTension": 0.1
                                            }
                                        ], 
                                    },

                                // Configuration options go here
                                options: {
//                                     tooltips: {
//                                         backgroundColor: "rgb(255,255,255)",
//                                         bodyFontColor: "#858796",
//                                         borderColor: '#dddfeb',
//                                         borderWidth: 5,
//                                         caretSize: 15,
//                                         bodyFontSize: 15,
//                                         xPadding: 15,
//                                         yPadding: 15,
//                                         displayColors: false,
//                                         caretPadding: 10,
//                                     },
                                    legend: {
                                        display: true,
                                        position: 'top'
                                    },


                                }
                            });

                        </script>

                        <script>

                            var ctx = document.getElementById('member_chart').getContext('2d');
                            var chart = new Chart(ctx, {
                                // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                    data: {
                                        labels: ['1분기', '2분기', '3분기', '4분기'],
                                        datasets: [
                                            { 
                                            "label": "Project",
                                            "data": [ ${userQuarter[0].quarterdata}, ${userQuarter[1].quarterdata}, ${userQuarter[2].quarterdata}, ${userQuarter[3].quarterdata}],
                                            "borderColor": "rgb(196, 232, 167)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(196, 232, 167, 0.8)",
                                            "lineTension": 0.1
                                            }
                                        ], 
                                    },

                                // Configuration options go here
                                options: {
                                    legend: {
                                        display: true,
                                        position: 'top'
                                    },


                                }
                            });
                        </script>

                        <script>

                            var ctx = document.getElementById('board_chart').getContext('2d');
                            var chart = new Chart(ctx, {
                                // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                    data: {
                                        labels: ['1분기', '2분기', '3분기', '4분기'],
                                        datasets: [
                                            {
                                            "label": "recruitment",
                                            "data": [ ${recruitmentQuarter[0].quarterdata}, ${recruitmentQuarter[1].quarterdata}, ${recruitmentQuarter[2].quarterdata}, ${recruitmentQuarter[3].quarterdata}],
                                            "borderColor": "rgb(164, 215, 120)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(164, 215, 120, 0.8)",
                                            "lineTension": 0.1 
                                            },
                                            { 
                                            "label": "msgBoard",
                                            "data": [ ${msgBoardQuarter[0].quarterdata}, ${msgBoardQuarter[1].quarterdata}, ${msgBoardQuarter[2].quarterdata}, ${msgBoardQuarter[3].quarterdata}],
                                            "borderColor": "rgb(107, 188, 41)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(107, 188, 41. 0.8)",
                                            "lineTension": 0.1
                                            },
                                            { 
                                            "label": "qna",
                                            "data": [ ${qnaQuarter[0].quarterdata}, ${qnaQuarter[1].quarterdata}, ${qnaQuarter[2].quarterdata}, ${qnaQuarter[3].quarterdata}],
                                            "borderColor": "rgb(196, 232, 167)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(196, 232, 167, 0.8)",
                                            "lineTension": 0.1
                                            },
                                            { 
                                            "label": "admin",
                                            "data": [ ${adminQuarter[0].quarterdata}, ${adminQuarter[1].quarterdata}, ${adminQuarter[2].quarterdata}, ${adminQuarter[3].quarterdata}],
                                            "borderColor": "rgb(196, 255, 67)",
                                            "backgroundColor": "rgba(0, 0, 0, 0.01)",
                                            // "backgroundColor": "rgba(196, 232, 167, 0.8)",
                                            "lineTension": 0.1
                                            }
                                        ], 
                                    },

                                // Configuration options go here
                                options: {
                                    legend: {
                                        display: true,
                                        position: 'top'
                                    },


                                }
                            });

                        </script>

                    </div>

                </div>



            </div>

        </div>
        <div class="row stat-row">

            <div class="col-lg-12 col-xl-9">
                <div class="card">
                    <div class="card-header" style="background-color: white;">
                        <div style="display: inline-block; width: 45%">
                            <a style="font-size: 1.3rem; font-weight: bold;">
                                Newest Project List
                            </a>
                        </div>
                        <div class="text-right" style="display: inline-block; width: 54%">
                        	<div onclick="createProjectList();">
								<i class="fas fa-redo" style="color: #9ed85f;"></i>
                        	</div>
                        </div>

                    </div>
                    <div class="card-body p-0">

                        <div id="bash-project-list">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                    <th style="width: 5%" scope="col">Index</th>
                                    <th style="width: 65%;"  scope="col">Project Title</th>
                                    <th style="width: 15%;" scope="col">Project Start</th>
                                    <th style="width: 15%;" scope="col">Project End</th>
                                    </tr>
                                </thead>
                                <tbody id="project_table" >

                                </tbody>
                                
                                
                                
<!-- 									<tbody> -->
<%--                                 <c:forEach  items="${projectListOnlyfive}" var="project" varStatus="projectStatus" > --%>
<!-- 										<tr> -->
<%-- 										<th scope="row">${projectStatus.count}<input type="hidden"/></th>  --%>
<%-- 										<td>${project.project_title}</td> --%>
<%-- 										<td>${project.project_start}</td> --%>
<%-- 										<td>${project.project_end}</td> --%>
<!-- 										</tr> -->
<%--                                 </c:forEach> --%>
<!-- 									</tbody> -->
                            </table>
                        </div>


                </div>
            </div>



            </div>
            <div class="col-lg-12 col-xl-3">
                <!-- <div class="col-md-4 col-12"> -->
                    <div class="card">
                        <div class="card-header" style="background-color: white;">
                            <a style="font-size: 1rem; font-weight: bold;">
                                On Project, GIT Duty Cycle 
                            </a>
                        </div>
                        <div class="card-body">
                            <canvas id="myPieChart-1" width="500" height="250"></canvas>
                        </div>
                    </div>

                <script>

                    var ctx = document.getElementById("myPieChart-1");
                    var myPieChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            labels: ["Git", "Non Git"],
                            datasets: [{
                            data: [${onOffGitCount.projectMemberCount}, ${onOffGitCount.offGitCount}],
                            backgroundColor: ['#70b02b', '#9ed85f'],
                            hoverBackgroundColor: ['#70f02b', '#9ef85f'],
                            hoverBorderColor: "rgba(234, 236, 244, 1)",
                            }],
                        },
                        options: {
                            maintainAspectRatio: false,
                            tooltips: {
                                backgroundColor: "rgb(255,255,255)",
                                bodyFontColor: "#858796",
                                borderColor: '#dddfeb',
                                borderWidth: 3,
                                caretSize: 15,
                                bodyFontSize: 15,
                                xPadding: 15,
                                yPadding: 15,
                                displayColors: false,
                                caretPadding: 10,
                            },
                            legend: {
                                display: false,
                                position: 'right'
                            },
                            cutoutPercentage: 70
                        },
                    });

                </script>

                <!-- </div> -->

            </div>

        </div>
    </div>



<!--     </div> -->
</div>
</div>





	