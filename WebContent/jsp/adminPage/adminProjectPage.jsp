<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script>


<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>

<meta charset="UTF-8">

<style>

#admin-sidebar > li:nth-child(2) {
	background-color: #85CE36;
}
.filter {
    display: flex;
    z-index: 200;
    height: 70px;
    padding-left: 8px;
    box-shadow: 0 2px 2px 0 rgba(0,0,0,0.1);
    background-color: white;
}
</style>

<title>Insert title here</title>
</head>
<body>

	<!-- MainLayout_Wrap -->
	<div id="mainLayout-wrap" class="wrap" >
	
		<!-- Header -->
		<div id="main-header" class="header">
<%-- 			<jsp:include page="/jsp/include/header/header-adminPage.jsp"></jsp:include> --%>
			<jsp:include page="/jsp/include/header/header-adminPage-v2.jsp"></jsp:include>
		</div>
		
		
		<div id="main-sidebar" class="header" style="background: #4f5f6f;">
			<jsp:include page="/jsp/include/sidebar/sidebar-adminPage.jsp"></jsp:include>
		</div>
		
		
		<!-- End Header -->

		<!-- Main_Wrap -->
		<div id="main_wrap" class="main_wrap">

			<!-- sidebar -->
<!-- 			<div id="main-sidebar" class="sidebar"  style="background: #4f5f6f;"> -->
<%-- 				<jsp:include page="/jsp/include/sidebar/sidebar-adminPage.jsp"></jsp:include> --%>
<!-- 			</div> -->
			<!-- End sidebar -->

			<!-- Main -->
			<div class="main">



				<jsp:include page="/jsp/adminPage/adminProjectPageMain.jsp"></jsp:include>




			</div>
			<!-- End Main -->

		</div>
		<!-- End Main_Wrap -->

	</div>
	<!-- End MainLayout_Wrap -->





</body>
</html>