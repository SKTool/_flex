<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    

	<style>
        body {
            background-color: #eff2f6;

        }

        /* #admin-project-list.container-fluid .row:hover, #admin-project-list .row.project-selected { */
/*         #admin-project-list .row.project-selected { */
/*             background-color: #cccccc; */
/*         } */
/*         #admin-project-list .row.project-selected div {  */
/*         	color: #000000;  */
/*         } */

         #admin-project-list .row.project-selected div i {  
            color: #78bd2e;
         } 
        
        #admin-project-list div div .row {
            /* border: 1px solid black; */
/*             margin: .4rem 0 .4rem 0; */
	           font-size: 2.3rem; 

            /* border-bottom: .1rem solid lightgray;  */
        }

        #admin-project-list div div div:hover {
            color: #78bd2e;
        }

        /* .pagination .active {
            background-color: #78bd2e;
        } */

        /* .row.project-selected {
            background-color: tomato;

        } */

    </style>
    
    
    <style>
.avatar-container .avatar {
    border-radius: 0;
    border: 0;
    height: auto;
    width: 100%;
    margin: 0;
    align-self: center;
}
.identicon.bg4 {
    background-color: #e3f2fd;
}
.identicon.s48 {
    font-size: 20px;
    line-height: 46px;
}
.avatar-circle.s48, .s48.avatar, .s48.avatar-container {
    width: 48px;
    height: 48px;

    margin-left: 40px; 
} 
.identicon {
    text-align: center;
    vertical-align: top;
    color: #555;
    background-color: #eee;
}
</style>


    <div style="padding : 2% 5% 2% 5%;">
	<div class="px-5 pt-2" style="background-color: white;">
	
        <div class="container-fluid mb-2">
            <div class="row">
                    <div class="col-2 my-auto text-center" style="font-weight: bold;">
                        <h4>Member List</h4>
                    </div>
					<div class="col-8">
					</div>
					<div class="col-2 text-right">
						<button id="admin-project-delete" class="btn btn-danger"> 삭제 </button>
						<script>
							$('#admin-project-delete').on('click', function(){
								var valList = [];
								var values = $('#admin-project-list .row.project-selected');

								for ( var i = 0 ; i < values.length ; i++) {
									valList.push(values[i].children[0].value);
								}
								console.log(valList);
								
								$.ajax({
									url: '/adminPage/deleteMember',
									data: { 'list': JSON.stringify(valList),'${_csrf.parameterName}' : '${_csrf.token}'} , 
									type: 'post',
									dataType: 'json',
									success: function(data) {
										window.location.reload(true); 

									},
									error: function(data) {
										alert("delete error");
									}
								});
							
							});
						</script>
					</div>
            </div>
            <div style="color: #999">
               사이트내의 회원들을 관리하기 위한 페이지입니다.
            </div>
        </div>

        <div style="margin: .5rem 0 2.5rem 0; border-bottom: .1rem solid lightgray;"> </div>

        <script>
// 			function getRandomColor() {
// 				var colors = [ '#f3e5f5', '#e0f2f1', '#ffebee', '#fbe9e7', '#e3f2fd', '#eee' ];
// 				var color = colors[Math.floor(Math.random() * 6)];

// 				return color;
// 			}
			function getRandomColor() {
				var colors = [ '#f3e5f5', '#e0f2f1', '#ffebee', '#fbe9e7', '#e3f2fd', '#eee' ];
				var color = colors[Math.floor(Math.random() * 6)];

				return color;
			}
			function createList(page) {
				$('#admin-project-list > div > div').find('*').remove();
				$('#bottom-paging').find('*').remove();

				$.ajax({
					url: '/adminPage/memberList',
					data: {'page': page ,'${_csrf.parameterName}' : '${_csrf.token}'} , 
					type: 'post',
					dataType: 'json',
					success: function(data_map){
						console.log(data_map);
						data = data_map.projectList;
						console.log(data_map);
						console.log(data);
						
						for ( i in data ) {

							
							console.log(i);
							
							console.log(data[i]);
							var fon = getRandomColor();
							
							var fileCheck = '<a class="text-success"><i class="fas fa-file"></i></a>';
							var statusCheck = '<a class="p-2 mb-1 bg-danger text-white font-weight: bold;">인증 중</a>'; 

							if (data[i].user_file === undefined) {
								fileCheck = '<a class="text-danger"><i class="fas fa-file-excel"></i><a>'
							}
							if (data[i].status === 1) {
								var statusCheck = '<a class="p-2 mb-1 bg-info text-white font-weight: bold;">인증완료</a>';
							}
							
							var $tr = 
							'<div class="row py-2" onclick=" $(this).toggleClass(\'project-selected\') ">'
							+ '    <input type="hidden" id="user_idx_'+i+'" value="'+ data[i].user_idx +'"/>'
							+ '    <div class="px-0 col-1 text-right" style="padding: 0 0 .6rem 0;"><i class="fas fa-check-square" style="font-size: 1.5rem;"></i></div>'
		                    + '	   <div class="col-1">'
// 							+ '       <img id = "userPic" src = "/user/image?user_pic='+data[i].user_pic+'" width = "60%" style = "margin : 3%; border-radius : 5%; border : 2px solid '+fon+';">'
							+ '       <img id = "userPic" src = "/user/image?user_pic='+data[i].user_pic+'" width = "45%" style = "margin : 3%; margin-left: 25px; border-radius : 5%; border : 2px solid '+fon+';">'
		                    + '	   </div>'
							+ '    <div class="col-3 m-auto p-0" style="font-weight: bold; font-size: 1.1rem"><a>'+data[i].user_id   +'</a> </div>'
							+ '    <div class="col-2 m-auto p-0" style="font-weight: bold; font-size: 1.1rem"><a>'+data[i].user_name  +'</a> </div>'
							+ '    <div class="col-2 m-auto" style="font-size: 1rem;"><a>'+data[i].user_tel  +'</a></div>'
// 							+ '    <div class="col-1 m-auto" style="font-size: 1rem;">'+fileCheck +'</div>'
							+ '    <div class="col-1 m-auto" style="font-size: 1rem;">'+statusCheck+'</div>'
							+ '    <div class="col-2 m-auto" style="font-size: 1rem;"><a>'+data[i].in_date+'</a></div>'
							+ '</div>'


							$('#admin-project-list > div > div').append($tr);
							$('#projectIdx'+i).val(data[i].project_idx)
						}
// 							startPage=0, totalPage=2, endPage=1
						var startPage = data_map.startPage;
						var totalPage = data_map.totalPage;
						var endPage = data_map.endPage;
						console.log(startPage);
						console.log(totalPage);
						console.log(endPage);
						
						var end; 
						
						if (endPage > totalPage ) {
							end = totalPage;
						} else {
							end = endPage;
						}
						
						if (page != 1) {
							$('#bottom-paging').append('<li class="page-item"><a style="color: #85CE36;" class="page-link" href="#" onclick="createList('+(parseInt(page)-1)+')" >Previous</a></li>')
						}
						for ( var i = startPage ; i < end ; i++ ) {
							$('#bottom-paging').append(
								 '<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#" onclick="createList('+(parseInt(i)+1)+')">'+(parseInt(i)+1)+'</a></li>'
							);
						}
						
						if (page != totalPage) {
							$('#bottom-paging').append('<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#" onclick="createList('+(parseInt(page)+1)+')"  >Next</a></li>')
						}
					}, 
					error: function(data) {
						alert("error");
					}
				});

			}

            $(function() {
            	createList(1);
            });

            



        </script>


        <div id="admin-project-list" class="container-fluid px-3"  style="background-color: white;color: #999">
            <div class="m-2">
                <div id="admin-project-list-header">

                </div>

            </div>
        </div>

	</div>
                <!--#85CE36-->
        <div class="my-3" style="text-align: right;" >
            <nav style="display: inline-block;" aria-label="Page navigation example">
                <ul id="bottom-paging" class="pagination" style="font-weight: bold" >

                </ul>
            </nav>
        </div>

    </div>
    

    