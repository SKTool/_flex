<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    

	<style>
        body {
            background-color: #eff2f6;

        }

        /* #admin-project-list.container-fluid .row:hover, #admin-project-list .row.project-selected { */
/*         #admin-project-list .row.project-selected { */
/*             background-color: #cccccc; */
/*         } */
/*         #admin-project-list .row.project-selected div {  */
/*         	color: #000000;  */
/*         } */

         #admin-project-list .row.project-selected div i {  
            color: #78bd2e;
         } 
        
        #admin-project-list div div .row {
            /* border: 1px solid black; */
/*             margin: .4rem 0 .4rem 0; */
	           font-size: 2.3rem; 

            /* border-bottom: .1rem solid lightgray;  */
        }

        #admin-project-list div div div:hover {
            color: #78bd2e;
        }

        /* .pagination .active {
            background-color: #78bd2e;
        } */

        /* .row.project-selected {
            background-color: tomato;

        } */

    </style>
    
    
    <style>
.avatar-container .avatar {
    border-radius: 0;
    border: 0;
    height: auto;
    width: 100%;
    margin: 0;
    align-self: center;
}
.identicon.bg4 {
    background-color: #e3f2fd;
}
.identicon.s48 {
    font-size: 20px;
    line-height: 46px;
}
.avatar-circle.s48, .s48.avatar, .s48.avatar-container {
    width: 48px;
    height: 48px;

    margin-left: 40px; 
} 
.identicon {
    text-align: center;
    vertical-align: top;
    color: #555;
    background-color: #eee;
}
</style>


    <div style="padding : 2% 5% 2% 5%;">
	<div class="px-5 pt-2" style="background-color: white;">
	
        <div class="container-fluid mb-2">
            <div class="row">
                    <div class="col-2 my-auto text-center" style="font-weight: bold;">
                        <h4>Project List</h4>
                    </div>
<!--                     <div class="col-2"> -->
<!--                         <select class="form-control form-control-sm"> -->
<!--                             <option>More Actions...</option> -->
<!--                             <option>Delete</option> -->
<!--                         </select> -->
<!--                     </div> -->
					<div class="col-8">
					</div>
					<div class="col-2 text-right">
						<button id="admin-project-delete" class="btn btn-danger"> 삭제 </button>
						<script>
							$('#admin-project-delete').on('click', function(){
								var valList = [];
								var values = $('#admin-project-list .row.project-selected');

								for ( var i = 0 ; i < values.length ; i++) {
// 									console.log(values[i].children[0].value);
									valList.push(values[i].children[0].value);
								}
								console.log(valList);
								
								$.ajax({
									url: '/adminPage/deleteProject',
									data: { 'list': JSON.stringify(valList),'${_csrf.parameterName}' : '${_csrf.token}'} , 
									type: 'post',
									dataType: 'json',
									success: function(data) {
										window.location.reload(true); 

									},
									error: function(data) {
										alert("delete error");
									}
								});
							
							});
						</script>
					</div>
            </div>
            <div style="color: #999">
                사이트내의 프로젝트의 리스트들을 표시하는 페이지입니다.
            </div>
        </div>

        <div style="margin: .5rem 0 2.5rem 0; border-bottom: .1rem solid lightgray;"> </div>

        <script>
			function getRandomColor() {
				var colors = [ '#f3e5f5', '#e0f2f1', '#ffebee', '#fbe9e7', '#e3f2fd', '#eee' ];
				var color = colors[Math.floor(Math.random() * 6)];

				return color;
			}
			function createList(page) {
				$('#admin-project-list > div > div').find('*').remove();
				$('#bottom-paging').find('*').remove();

				$.ajax({
					url: '/adminPage/projectList',
					data: {'page': page ,'${_csrf.parameterName}' : '${_csrf.token}'} , 
					type: 'post',
					dataType: 'json',
					success: function(data_map){
						console.log(data_map);
// 						data_map = JSON.parse(data_json); 
						data = data_map.projectList;
// 						console.log(data_json);
						console.log(data_map);
						console.log(data);
						
						
						for ( i in data ) {

							console.log(i);
							console.log(data[i]);
							var cha = data[i].project_title.substr(0,1);
							var fon = getRandomColor();

							var $tr = 
							'<div class="row py-2" onclick=" $(this).toggleClass(\'project-selected\') ">'
							+ '    <input type="hidden" id="project_idx_'+i+'" value="'+ data[i].project_idx +'"/>'
							+ '    <div class="px-0 col-1 text-right" style="padding: 0 0 .6rem 0" ><i class="fas fa-check-square"  style="font-size: 1.5rem;"></i></div>'
							+ '    <div class="px-0 col-1 m-auto avatar-container flex-grow-0 flex-shrink-0 rect-avatar s48" style="font-size: 1rem;"> <a>'
							+ '    		<div class="avatar project-avatar s48 identicon bg4" style="text-transform: uppercase; background-color: '+fon+'">'+cha+'</div>'
							+ '    </a></div>'
							+ '    <div class="col-6 m-auto p-0" style="font-weight: bold; font-size: 1.1rem"><a>'+data[i].project_title+'</a> </div>'
							+ '    <div class="col-2 m-auto" style="font-size: 1rem;"><a>'+data[i].project_start+'</a></div>'
							+ '    <div class="col-2 m-auto" style="font-size: 1rem;"><a>'+data[i].project_end+'</a></div>'
							+ '</div>'
					
							$('#admin-project-list > div > div').append($tr);
							$('#projectIdx'+i).val(data[i].project_idx)
						}
// 							startPage=0, totalPage=2, endPage=1
						var startPage = data_map.startPage;
						var totalPage = data_map.totalPage;
						var endPage = data_map.endPage;
						console.log(startPage);
						console.log(totalPage);
						console.log(endPage);
						
						var end; 
						
						if (endPage > totalPage ) {
							end = totalPage;
						} else {
							end = endPage;
						}
						
						if (page != 1) {
							$('#bottom-paging').append('<li class="page-item"><a style="color: #85CE36;" class="page-link" href="#" onclick="createList('+(parseInt(page)-1)+')" >Previous</a></li>')
						}
						for ( var i = startPage ; i < end ; i++ ) {
							$('#bottom-paging').append(
								 '<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#" onclick="createList('+(parseInt(i)+1)+')">'+(parseInt(i)+1)+'</a></li>'
							);
						}
						
						if (page != totalPage) {
							$('#bottom-paging').append('<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#" onclick="createList('+(parseInt(page)+1)+')"  >Next</a></li>')
						}
					}, 
					error: function(data) {
						alert("error");
					}
				});

			}

            $(function() {
            	createList(1);
            });

            



        </script>


        <div id="admin-project-list" class="container-fluid px-3"  style="background-color: white;color: #999">
            <div class="m-2">
                <div id="admin-project-list-header">
<!--                     <div class="row" style="font-size: medium"> -->
<!--                         <div class="col-2">Index</div> -->
<!--                         <div class="col-6">Project Title</div>  -->
<!--                         <div class="col-2">Project Start</div> -->
<!--                         <div class="col-2">Project End</div> -->
<!--                     </div> -->
                </div>

            </div>
        </div>

	</div>
                <!--#85CE36-->
        <div class="my-3" style="text-align: right;" >
            <nav style="display: inline-block;" aria-label="Page navigation example">
                <ul id="bottom-paging" class="pagination" style="font-weight: bold" >
<!-- 					<li class="page-item"><a style="color: #85CE36;" class="page-link" href="#">Previous</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">1</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">2</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">3</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">4</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">5</a></li> -->
<!-- 					<li class="page-item"><a style="color: #85CE36;"class="page-link" href="#">Next</a></li> -->
                </ul>
            </nav>
        </div>

<!--  -->	
    </div>
    

    