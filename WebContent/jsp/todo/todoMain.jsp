<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="/css/todo/todo.css">

<script>
	window.onload = function() {
		createTodoList();
		var id = 0;
		var btnNew = document.getElementById("btnAdd");
		var donelist = document.getElementById("donelist");
		inputText.focus();
		
		//새로운 list 추가
		btnNew.onclick = function() {
			var itemText = $("#inputText").val();
			if (!itemText || itemText === "" || itemText === " ") { //blank 방지
				Swal.fire({
					  type: 'info',
					  title: '내용을 입력하세요',
					  timer: 1000
					})
				return false;
			}
				$.ajax({
						url : "/todo/write",
						data : {
							"to_do_content" : itemText,
							"to_do_status" : 0,
							"${_csrf.parameterName}" : "${_csrf.token}"
							},
						type : "post",
						success : function(result) {
							if (result) {
								Swal.fire({
									  type: 'success',
									  title: '정상적으로 등록되었습니다',
									  showConfirmButton: false,
									  timer: 1000
									})
								addNewItem(document.getElementById("todolist"),result);
								$("#inputText").val("");
							}
						}
					});
		};
		inputText.onkeyup = function(event) {
			//Event.which(13) == ENTER
			if (event.which === 13) {
				var itemText = $("#inputText").val();
				if (!itemText || itemText === "" || itemText === " ") { //blank 방지
					Swal.fire({
						  type: 'info',
						  title: '내용을 입력하세요',
						  timer: 1000
						})
					return false;
				}
				$.ajax({
					url : "/todo/write",
					data : {
						"to_do_content" : itemText,
						"to_do_status" : 0,
						"${_csrf.parameterName}" : "${_csrf.token}"
						},
					type : "post",
					success : function(result) {
						if (result) {
							Swal.fire({
								  type: 'success',
								  title: '정상적으로 등록되었습니다',
								  showConfirmButton: false,
								  timer: 1000
								})
							addNewItem(document.getElementById("todolist"),result);
							$("#inputText").val("");
							var inputText = document.getElementById("inputText");
							inputText.focus();
						}
					}
				});
			}
		};
		//check 표시하면 상태 변경
		function updateItemStatus() {
			var chId = this.id.replace("cb_", "");
			var itemText = document.getElementById("item_" + chId);
			var listItem = document.getElementById("li_" + chId);
			var listItemParentId = listItem.parentElement;
 			if (this.checked) {
 				$.ajax({
 					url : "/todo/checked",
 					data : {"to_do_status" : 1,
 							"to_do_idx" : $("#li_"+chId).attr('value'),
 							"${_csrf.parameterName}" : "${_csrf.token}"
 							},
 					type : "get",
 					success : function(result){
 		 				itemText.className = "checked";
 		 				donelist.appendChild(listItem); 
 					}
 				});		
 			} else {
 				$.ajax({
 					url : "/todo/checked",
 					data : {"to_do_status" : 0,
 							"to_do_idx" : $("#li_"+chId).attr('value'),
 							"${_csrf.parameterName}" : "${_csrf.token}"
 							},
 					type : "get",
 					success : function(result){
 		 				itemText.className = "";
 		 				todolist.appendChild(listItem);
 					}	
 				});
 			}
		}

		function renameItem() {
			//this : pencilIcon
			//var newText = prompt("수정하시겠습니까?");	
			var spanId = this.id.replace("pencilIcon_", "");
			var span = document.getElementById("item_" + spanId);
			Swal.fire({
				title: "수정하시겠습니까?",
				input: 'text',
				//inputAttributes: {}
				}).then((text) => {
			if (!text.value || text.value === "" || text.value === " ") { //blank 방지
				Swal.fire({
					  type: 'info',
					  title: '수정 할 내용을 입력하세요',
					  timer: 1000
					})
				return false;
			}
				$.ajax({
						url : "/todo/modify",
						data : {
							"to_do_content" : text.value,
							"to_do_idx" : $("#li_" + this.id.replace("pencilIcon_", "")).attr("value"),
							"${_csrf.parameterName}" : "${_csrf.token}"
							},
						type : "post",
						success : function(result) {
							if (result) {
								Swal.fire({
									  type: 'success',
									  title: '수정 완료되었습니다',
									  showConfirmButton: false,
									  timer: 1000
									})
								span.innerHTML = result.to_do_content;
							}
						}
					});
				});
		}

		function removeItem() {
			//this : span
			var listItemId = this.id.replace("eraserIcon_", "");
			Swal.fire({
			  title: '삭제하시겠습니까?',
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonColor: '#3085d6',
			  cancelButtonColor: '#d33',
			  confirmButtonText: '삭제',
			  cancelButtonText: '취소',	  
			}).then((result) => {
				$.ajax({
						url : "/todo/delete",
						data : {"to_do_idx" : $("#li_" + this.id.replace("eraserIcon_", "")).attr('value'),
								"${_csrf.parameterName}" : "${_csrf.token}"
								},
						type : "post",
						success : function(data) {
							document.getElementById("li_" + listItemId).style.display = "none";
							if (result.value) {
							    Swal.fire({
										  type: 'success',
										  title: '정상적으로 삭제되었습니다',
							    })
							}
						}
					})
				});
		}
		function mouseover() {
			//this === li
			var pencilIconId = this.id.replace("li_", "");
			var pencilIcon = document.getElementById("pencilIcon_"
					+ pencilIconId);
			var eraserIcon = document.getElementById("eraserIcon_"
					+ pencilIconId);
			pencilIcon.style.visibility = "visible";
			eraserIcon.style.visibility = "visible";
		}
		function mouseout() {
			//this === li
			var pencilIconId = this.id.replace("li_", "");
			var pencilIcon = document.getElementById("pencilIcon_"
					+ pencilIconId);
			var eraserIcon = document.getElementById("eraserIcon_"
					+ pencilIconId);
			pencilIcon.style.visibility = "hidden";
			eraserIcon.style.visibility = "hidden";
		}

		function addNewItem(list, itemText) {
			id++;
// 			var date = new Date();
// 			var id = "" + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

			var listItem = document.createElement("li");
			listItem.id = "li_" + id;
			listItem.value = itemText.to_do_idx;
			listItem.addEventListener("mouseover", mouseover);
			listItem.addEventListener("mouseout", mouseout);

			var checkBox = document.createElement("input");
			checkBox.type = "checkBox";
			checkBox.id = "cb_" + id;
			checkBox.onclick = updateItemStatus;

			var span = document.createElement("span");
			span.id = "item_" + id;
			span.innerHTML = itemText.to_do_content;

			var pencilIcon = document.createElement("i");
			pencilIcon.id = "pencilIcon_" + id;
			pencilIcon.className = "fas fa-pencil-alt";
			pencilIcon.onclick = renameItem;

			var eraserIcon = document.createElement("i");
			eraserIcon.id = "eraserIcon_" + id;
			eraserIcon.className = "fas fa-eraser";
			eraserIcon.onclick = removeItem;
					
			var list = document.getElementById("todolist");
			var list2 = document.getElementById("donelist");
			listItem.appendChild(checkBox);
			listItem.appendChild(span);
			listItem.appendChild(pencilIcon);
			listItem.appendChild(eraserIcon);
				list.appendChild(listItem);
		}
		function addNewItem2(list, itemText) {
			id++;
// 			var date = new Date();
// 			var id = "" + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

			var listItem = document.createElement("li");
			listItem.id = "li_" + id;
			listItem.value = itemText.to_do_idx;
			listItem.addEventListener("mouseover", mouseover);
			listItem.addEventListener("mouseout", mouseout);

			var checkBox = document.createElement("input");
			checkBox.type = "checkBox";
			checkBox.id = "cb_" + id;
			checkBox.checked="checked";
			checkBox.onclick = updateItemStatus;

			var span = document.createElement("span");
			span.id = "item_" + id;
			span.innerHTML = itemText.to_do_content;

			var pencilIcon = document.createElement("i");
			pencilIcon.id = "pencilIcon_" + id;
			pencilIcon.className = "fas fa-pencil-alt";
			pencilIcon.onclick = renameItem;

			var eraserIcon = document.createElement("i");
			eraserIcon.id = "eraserIcon_" + id;
			eraserIcon.className = "fas fa-eraser";
			eraserIcon.onclick = removeItem;

			var list2 = document.getElementById("donelist");
			listItem.appendChild(checkBox);
			listItem.appendChild(span);
			listItem.appendChild(pencilIcon);
			listItem.appendChild(eraserIcon);
			list2.appendChild(listItem);
		}
		
		function createTodoList() {
			var date = new Date();
			var id = "" + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

			var listItem = document.createElement("li");
			listItem.id = "li_" + id;

			listItem.addEventListener("mouseover", mouseover);
			listItem.addEventListener("mouseout", mouseout);

			var checkBox = document.createElement("input");
			checkBox.type = "checkBox";
			checkBox.id = "cb_" + id;
			checkBox.onclick = updateItemStatus;

			var span = document.createElement("span");
			span.id = "item_" + id;

			var pencilIcon = document.createElement("i");
			pencilIcon.id = "pencilIcon_" + id;
			pencilIcon.className = "fas fa-pencil-alt";
			pencilIcon.onclick = renameItem;

			var eraserIcon = document.createElement("i");
			eraserIcon.id = "eraserIcon_" + id;
			eraserIcon.className = "fas fa-eraser";
			eraserIcon.onclick = removeItem;

			var list = document.getElementById("todolist");
			var list2 = document.getElementById("donelist");
			listItem.appendChild(checkBox);
			listItem.appendChild(span);
			listItem.appendChild(pencilIcon);
			listItem.appendChild(eraserIcon);

			$.ajax({
				url : "/todo/all",
				type : "post",
				data : {"${_csrf.parameterName}" : "${_csrf.token}"},
				success : function(data) {
					console.log(data);
					for ( var i in data) {
						if(data[i].to_do_status == 0){
							addNewItem(list, data[i]);
						} else if(data[i].to_do_status == 1){
							addNewItem2(list2, data[i]);
						}
					}
				}
			});
		}
	};
</script>
	<div style="width: 90%; margin-left: 5%;">
			<div style="width: 100%;">
			<br> 
			<br> 
			<h2 id="todoTitle" style ="width: 95%;margin-left: 5%; color: gray; font-weight:100;">Lightweight To-do's</h2>
			<p style = " margin-left : 5%;color : lightgray; font-size : 0.9em;">간단한 작성으로 나만의 할일을 기록하세요</p>
			<hr style = "width : 98%; margin-left : 1%;">
			<br>
		</div>
	</div>
	
<div class="container" style="float: right; margin:40px;">
<!-- 	<div class="row"> -->
<!-- 		<h2 id="todolistHeader">Lightweight To-do's</h2> -->
<!-- 	</div> -->
	<div class="row">
		<div class="col-sm-4">
			<hr>
			<p id="enter">To-Do's</p>
			<div class="form-group" id="form-group">
				<input class="form-control input-lg" type="text" id="inputText"
					placeholder="what to do"
					style="margin-bottom: 5px; display: inline; width: 300px; height: 35px;">
				<button class="btn btn-info btn-sm" id="btnAdd"
					style="display: inline; float: right;">추가</button>
				<hr>
				<!-- 					<ul id="todolist"></ul> -->
				<ul id="todolist" style="list-style: none; padding: 0; margin: 0;"></ul>
			</div>

		</div>

		<div class="col-sm-4">
			<hr>
			<p id="enter">Done</p>
			<div>
				<br>
				<hr style="margin-top: 32px;">
				<!-- 					<ul id="donelist"></ul> -->
				<ul id="donelist" style="list-style: none; padding: 0; margin: 0;"></ul>
			</div>
		</div>
	</div>
</div>
