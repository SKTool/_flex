<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<meta charset="UTF-8">
<title>Insert title here</title>
<script>
	$(function(){
		
		$('#user_pic').on('change', showImg);
		
		var userPicWidth = $('#userPic').css('width');
		
		$('#userPic').attr('height', userPicWidth);
		
		var picHeight = Number(userPicWidth.replace('px', ''));
		var fullHeight = Number(($('#userPicDiv').css('height')).replace('px', ''));
		$('#userPic').css('margin-top', ((fullHeight-picHeight)/2)+'px');
		 
		$('#modifyUserPic').on('click', function(){
			$('#modifyUserPicModal').modal('show');
		});
		$('#modifyPwGet2').on('click', function(){
			$('#modifyPwModal').modal('show');
		});
		$('#removeAccount2').on('click', function(){
			$('#removeAccountModal').modal('show');
		});
		
		$('#modifyUserPicCancel').on('click', function(){
			
				$('#modifyUserPicDiv').css('display', 'none');		
			
		});
		
		$('#defaultPic').on('click', function(){
			$.ajax({
				url : '/user/defaultPic',
				type : 'POST',
				data : {'user_id' : '${user.user_id}', '${_csrf.parameterName}' : '${_csrf.token}', 'user_idx' : '${user.user_idx}'},
				success : function(result){
					if(result === 1){
						swal.fire({
							text : '기본이미지로 변경되었습니다.',
							type : 'success'
						}).then(()=> {
							location.reload();
						})
					}else if(result === -1){
						alert('fail');
					}
				}
			})
		})
		
		$('#modifyUserPicBtn').on('click', function(){
			
			var user_pic = $('#user_pic').val();
			
			if(user_pic == null || user_pic == ''){
				
				swal.fire({ 
					text :'이미지 파일을 첨부해주세요.',
					type : 'warning'
				})
				return false;
			}
			
			var form_temp = document.getElementById('modifyUserPicForm');
			form_temp.method  = 'POST';
			form_temp.enctype = 'multipart/form-data';
			var formData = new FormData(form_temp);
			
			$.ajax({
				
				url 		: '/user/modifyPic',
				type 		: 'POST',
				data 		: formData,
				cache 		: false,
				processData : false,
				contentType : false,
				success : function(result){
					
					if(result === 0){
						swal.fire({ 
							type : 'error' 
						})
					}else if (result === 1){
						swal.fire({
							text : '수정 성공',
							type : 'success'
						}).then(()=>{
							var form = $('<form action = "/user/modifyProfile" method = "post">');
							var input = $('<input type = "hidden" name = "user_idx" value = "${user.user_idx}">');
							var submit = $('<input type = "submit" id = "submitIt" style = "display : none;">');
							var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
							input.appendTo(form);
							submit.appendTo(form);
							token.appendTo(form);
							form.appendTo($("body"));
							
							$('#submitIt').click();
						})
					}else if(result === -1){
						swal.fire({
							text : '수정 실패',
							type : 'error'
						}).then(()=>{
							location.reload();
						})
					}
				}
			});
		});
		
		$('#modifyUserProfile').on('click', function(){
			
			var formInputs = $('#modifyProfileForm').find('input[name][type != "hidden"]');
			
			for(var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') !== null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						swal.fire({
							text : '빈칸을 입력해 주세요',
							type : 'warning'
						})
						$(formInputs[i]).focus();
						
						
						return false;
					}
				}
			}
			
			
			
			if($.trim($('#tel_temp').val()).length == 0 || $.trim($('#tel_temp').val()) !== $.trim($('#user_tel').val())){
				
				if($.trim($('#user_tel').val()) != '${user.user_tel}'){
					
					swal.fire({
						text : '사용할 수 없는 전화번호 입니다.',
						type : 'warning'
					})
					
					return false;
				}
			}
			
			var formInputs = $('#modifyProfileForm').serialize();
			
			$.ajax({
				url 	: '/user/modifyUser',
				type 	: 'POST',
				data 	: formInputs,
				success : function(result){
					
					if(result === 1){
						swal.fire({
							text : '수정 성공',
							type : 'success'
						}).then(() => {
							
							var form = $('<form action = "/user/userProfile" method = "post">');
							var input = $('<input type = "hidden" name = "user_idx" value = "${user.user_idx}">');
							var submit = $('<input type = "submit" id = "submitIt" style = "display : none;">');
							var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
							input.appendTo(form);
							submit.appendTo(form);
							token.appendTo(form);
							form.appendTo($("body"));
							
							$('#submitIt').click();
						})
						
					}else if(result === -1){
						swal.fire({
							text : '수정 실패',
							type : 'error'
						})
					}
				}
			})
		})
		$('#removeAccountBtn').on('click', function(){

			var removeAccountForm = $('#removeAccountForm').find('input[name][type != "hidden"]');
			
			for(var i = 0; i < removeAccountForm.length; i++){
				if($(removeAccountForm[i]).attr('required') != null){
					if($.trim($(removeAccountForm[i]).val()) == ''){
						$(removeAccountForm[i]).val("");
						$(removeAccountForm[i]).focus();
						alert('빈칸을 입력해주세요.');
						return false;
					}
				}
			}
			
			if($('#user_pw').val() != $('#user_pw_c').val()){
				
				alert('비밀번호를 확인해 주세요');
				$('#user_pw').focus();
				
				return false;
			}
			
			
			$.ajax({
				url : '/user/userPwCheck',
				type : 'POST',
				data : {'user_pw' : $('#user_pw').val(), 'user_idx' : '${user.user_idx}', '${_csrf.parameterName}' : '${_csrf.token}'},
				success : function(result){
					if(result === 1){
						swal.fire({
							title : '계정 삭제',
							text : '계정을 정말 삭제하시겠습니까?',
							showCancelButton: true
						}).then((result) => {
							if(result.value){
								var removeAccountForm = $('#removeAccountForm').serialize();
								
								$.ajax({
									url 	: '/user/removeAccount',
									type	: 'POST',
									data	: removeAccountForm,
									error	:function(){
										alert('네트워크 오류');
									},
									success : function(result){
										
										location.href='/index/main?result=' + result
									}
								
								})
							}else{
								location.reload();
							}
						})
					}else if(result === -1){
						swal.fire({
							text : '비밀번호가 맞지 않습니다',
							type : 'error'
						})
						return false;
					}
				}
			})
		});
		$('#cancel').on('click', function(){
			
			var form = $('<form action = "/user/userProfile" method = "post">');
			var input = $('<input type = "hidden" name = "user_idx" value = "${user.user_idx}">');
			var submit = $('<input type = "submit" id = "submitIt" style = "display : none;">');
			var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
			input.appendTo(form);
			submit.appendTo(form);
			token.appendTo(form);
			form.appendTo($("body"));
			
			$('#submitIt').click();
			
		})
		
		$('#modifyIdGet').on('click', function(){
			$('#user_id').removeAttr('readonly');
			$('#modifyIdGet').css('display', 'none');
			$('#modifyId').css('display', 'unset');
			$('#modifyIdCancel').css('display', 'unset');
			$('#idCheckBtn').css('display', 'unset');
		})
		
		$('#modifyTelGet').on('click', function(){
			$('#user_tel').removeAttr('readonly');
			$('#modifyTelGet').css('display', 'none');
			$('#modifyTel').css('display', 'unset');
			$('#modifyTelCancel').css('display', 'unset');
			$('#telCheckBtn').css('display', 'unset');
		})
		
		$('#modifyIdCancel').on('click', function(){
			$('#user_id').val('${user.user_id}');
			$('#user_id').attr('readonly', 'readonly');
			$('#modifyId').css('display', 'none');
			$('#modifyIdCancel').css('display', 'none');
			$('#idCheckBtn').css('display', 'none');
			$('#modifyIdGet').css('display', 'unset');
		})
		
		$('#modifyTelCancel').on('click', function(){
			$('#user_tel').val('${user.user_tel}');
			$('#user_tel').attr('readonly', 'readonly');
			$('#modifyTel').css('display', 'none');
			$('#modifyTelCancel').css('display', 'none');
			$('#telCheckBtn').css('display', 'none');
			$('#modifyTelGet').css('display', 'unset');
		})
		
		$('#modifyId').on('click', function(){
			
			if($.trim($('#id_temp').val()).length == 0 || $.trim($('#id_temp').val()) !== $.trim($('#user_id').val())){
				swal.fire({
					text : '아이디 중복 확인이 필요합니다',
					type : 'warning'
				})
				
				return false;
			}
			
			var user_id = $('#user_id').val();
			
			if(user_id === '${user.user_id}'){
				
				$('#modifyIdCancel').click();
				
				return false;
			}
			
			var conf = confirm('확인을 누르시면 인증 메일이 발송되고 이전 아이디는 사용하지 못합니다. 진행하시겠습니까?');
			if(conf){
				
				$.ajax({
					url 	: "/user/modifyUserId",
					type 	: "POST",
					data 	: {"user_id" : $('#user_id').val(), "user_idx" : '${user.user_idx}', '${_csrf.parameterName}' : '${_csrf.token}'}, 
					success : function(result){
						
						if(result === 1){
							
							$.ajax({
								url		: '/user/logout',
								type	: 'GET',
								success : function(){
									swal.fire({
										text : '이메일 발송이 완료되었습니다.',
										type : 'success'
									})					
									location.href='/index/main';
								}
							})
						}else if(result === -1){
							swal.fire({
								text : '수정 실패',
								type : 'success'
							})					
						}
					}
				})
			}
		})
		
		$('#modifyTel').on('click', function(){
			
			if($.trim($('#tel_temp').val()).length == 0 || $.trim($('#tel_temp').val()) !== $.trim($('#user_tel').val())){
				
				swal.fire({
					text : '전화번호 중복 확인이 필요합니다',
					type : 'warning'
				})
				
				return false;
			}
			
			var user_tel = $('#user_tel').val();
			
			if(user_tel === '${user.user_tel}'){
				
				$('#modifyTelCancel').click();
				
				return false;
			}
			
			$.ajax({
				url 	: "/user/modifyUserTel",
				type 	: "POST",
				data 	: {"user_tel" : $('#user_tel').val(), "user_idx" : '${user.user_idx}', '${_csrf.parameterName}' : '${_csrf.token}'}, 
				success : function(result){
					
					if(result === 1){
						swal.fire({
							text : '수정 완료',
							type : 'success'
						})
						location.reload();
						
					}else if(result === -1){
						swal.fire({
							text : '수정 실패',
							type : 'error'
						})
					}
				}
			})
		})
		
		$('#idCheckBtn').on('click', function(){
			
			$("#user_id").val($.trim($("#user_id").val()));
			  
			var emailVal = $("#user_id").val();
			var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
			
			if (emailVal.match(regExp) == null) {
				swal.fire({
					text : '이메일 형식에 맞춰서 입력해주세요.',
					type : 'warning'
				})
				$("#user_id").focus();
				
				return false;
			}
			
			$.ajax({
				url 	: '/user/userIdCheck',
				type 	: 'POST',
				data 	: {'user_id' : $("#user_id").val(), '${_csrf.parameterName}' : '${_csrf.token}'},
				success : function(result){
					
					if(result === 0){
						swal.fire({
							text : '아이디를 입력해주세요',
							type : 'warning'
						})
						$('#user_id').focus();
						
					}else if(result === 1){
						
						swal.fire({
							text : '사용 가능한 아이디 입니다',
							type : 'warning'
						})
						$('#id_temp').val($('#user_id').val());
						
					}else if(result === -1){
						
						swal.fire({
							text : '이미 사용중인 아이디 입니다',
							type : 'warning'
						})
 						$('#user_id').val('');
					}
				}
			});
		});
		
		$('#user_tel').on('blur', function(){
			
			$("#user_tel").val($.trim($("#user_tel").val()));

			if($('#user_tel').val() === '${user.user_tel}'){
				$('#telTd').css('padding-top', '');
				$('#telP').css('display', 'none');
				return false;
			}
			
			var telVal = $("#user_tel").val();
			var regExp = /^\d{3}\d{3,4}\d{4}$/;
			
			if (telVal.match(regExp) == null) {
				$('#telTd').css('padding-top', '27px');
				$('#telP').css('display', 'unset');
				$('#telP').text('전화번호 형식에 맞춰서 입력해주세요.');
				return false;
			}
			
			$.ajax({
				url 	: '/user/userTelCheck',
				type 	: 'POST',
				data 	: {'user_tel' : $("#user_tel").val(), '${_csrf.parameterName}' : '${_csrf.token}'},
				success : function(result){
					
					if(result === 0){
						$('#telTd').css('padding-top', '27px');
						$('#telP').css('display', 'unset');
						$('#telP').text('전화번호를 입력해주세요');
					}else if(result === 1){
						$('#telTd').css('padding-top', '27px');
						$('#telP').css('display', 'unset');
						$('#telP').text('사용 가능한 전화번호 입니다');
						$('#tel_temp').val($('#user_tel').val());
					}else if(result === -1){
						$('#telTd').css('padding-top', '27px');
						$('#telP').css('display', 'unset');
						$('#telP').text('이미 사용중인 전화번호 입니다');
					}	
				}
			});
		});
		
		$('#modifyPwGet').on('click', function(){
			
			$('#modifyPwDiv').css('display', 'unset');
		});
		$('#modifyPw').on('click', function(){
			
			var temp_modifyPwForm = $('#modifyPwForm').find('input[name][type!="hidden"]');
			
			for(var i = 0; i < temp_modifyPwForm.length; i++){
				
				if($(temp_modifyPwForm[i]).attr('required') != null){
					
					if($.trim($(temp_modifyPwForm[i]).val()) == ""){
						swal.fire({
							text : '빈칸을 입력해 주세요',
							type : 'warning'
						})
						$(temp_modifyPwForm[i]).focus();
						
						return false;
					}					
				}
			}
			
			if($('#user_pw_new').val() != $('#user_pw_new_c').val()){
				swal.fire({
					text : '비밀번호가 맞지 않습니다',
					type : 'error'
				})
				$('#user_pw_new').focus();
				
				return false;
			}
			
			var modifyPwForm = $('#modifyPwForm').serialize();
			
			$.ajax({
				url 	 : '/user/modifyUserPw',
				type	 : 'POST',
				data	 : modifyPwForm,
				success  : function(result){
					
					if(result === 0){
						swal.fire({
							text : '기존의 비밀번호와 일치하지 않습니다',
							type : 'error'
						})
						
					}else if(result === 1){
						swal.fire({
							text : '수정 성공. 다시 로그인해주세요',
							type : 'success'
						}).then(()=> {
							location.href='/user/logout';
						})
					}else if(result === -1){
						swal.fire({
							text : '수정 실패',
							type : 'error'
						}).then(()=> {
							location.reload();
						})
					}
				}  
			})
		});
		
		function showImg(e){
			var sel_file;
			
			var files = e.target.files;
			var filesArr = Array.prototype.slice.call(files);
			
			filesArr.forEach(function(f){
				if(!f.type.match('image.*')){
					alert('이미지 파일을 점부해주세요');
					return false;
				}
				
				sel_file = f;
				
				var reader = new FileReader();
				reader.onload = function(e){
					$('#modalImg').attr('src', e.target.result);
				}
				reader.readAsDataURL(f);
			});
		}
		
		$('#user_file').on('change', function(){
			var user_file = $('#user_file').val();
			
			if(user_file == null || user_file == ''){
				
				swal.fire({ 
					text : '파일을 첨부해주세요.',
					type : 'warning'
				})
				return false;
			}
			
			var form_temp = document.getElementById('userFileForm');
			form_temp.method  = 'POST';
			form_temp.enctype = 'multipart/form-data';
			var formData = new FormData(form_temp);
			
			$.ajax({
				url : '/user/modifyFile',
				type : 'POST',
				data : formData,
				cache 		: false,
				processData : false,
				contentType : false,
				success : function(result){
					if(result === 1){
						location.reload();
					}else if(result === -1){
						swal.fire({
							type : 'error',
							text : '업데이트 실패. 파일 크기를 확인해주세요'
						})
					}
				}
			})
		})
	});

</script>
<style>
 	#userProfileTable tr th, #userProfileTable tr td {
 		height : 4em;
 		font-size: large;
 	}
	#modifyUserPicDiv{
		display:none;
	}
</style>
</head>
<body>
	<div id="mainLayout-wrap" class="wrap" >
	
		<div id="main-header" class="header"> 
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>

		<div id="main_wrap" class="main_wrap">
  			
  			
  			
  			
  			
			<div class="main" style = "background : white;">
			
				<div id = "navigator" style = "height : 30px; width : 95%; margin-left : 5%; margin-top : 1%;">
			 		<a href = "/index/main" style = "color : gray;">Home</a>&nbsp&nbsp<span style = "color : gray;">/</span>&nbsp&nbsp<a href = "/user/userProfile" style = "color : gray;">MyPage</a>&nbsp&nbsp<span style = "color : gray;">-</span>&nbsp&nbsp<a href = "#" style = "color : gray;">Modify</a>
		 		</div>
		 		
				<hr class = "bg-secondary" style = "width : 98%; margin-left : 1%;">
				<br> 
				<br>
				
 					
 				<!--  -->
 				<div style = "background : #f5f0f0; width : 100%; height : 80%; padding : 0; margin : 0;">
 				
 				
				<div class = "rounded" style = "background : white; border : 3px solid #f1dada;  width : 70%;height : 100%; margin-left : 15%; padding : 0;">
 					
 					<p style = "color : gray; width : 98%; margin-left : 3%; font-size : 3em; font-weight : 100; margin-top : 3%;">Profile</p>
 					<p style = "color : lightgray; width : 98%; margin-left : 4%; font-size : 1em; font-weight : 100;">계정 정보 수정</p>
 					<hr style = "color : gray; width : 98%; margin-left : 1%;">
 					<br> 
	 				<br>
	 				<br>
 					<div class = "container" id = "projectListDiv" style = "padding : 0;">
 						<div class = "row">
 						
							<div id = "userPicDiv" class = "container col-md-4" style = "padding : 0;">
								<c:if test = "${not empty user.user_pic}">
									<img id = "userPic" src = "/user/image?user_pic=${user.user_pic}" width = "84%" style = "margin : 7%; border-radius : 50%; border : 5px solid #b3d1ff;">
								</c:if>
							</div>
	  								
							<div class = "container col-md-8" style = "padding : 0;">
								<form id = "modifyProfileForm" method = "post" style = "width : 100%;"> 
  									<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
  									<input type="hidden" name = "user_idx" value = "${user.user_idx}">
									<br>
									
	    							<table style = "margin-top : 10%; margin-left : 5%; width : 90%;" id = "userProfileTable">   
										<tr style = "width : 100%;"> 
											<th style = "width : 4em;">아이디</th>
											<td style = "width : 80%; padding-right : 10%;">
												<input type = "email" class = "form-control" id = "user_id" name = "user_id" value = "${user.user_id}" readonly="readonly">
											</td>
										</tr>
										<tr> 
											<th>이름</th>
											<td style = "width : 80%; padding-right : 10%;">
												<input type = "text" class = "form-control" id = "user_name" name = "user_name" value = "${user.user_name}" required>
											</td>
										</tr>
										<tr>
											<th>
												전화번호
											</th>
											<td id = "telTd" style = "width : 80%; padding-right : 10%;">
												<input type = "tel" class = "form-control" id = "user_tel" name = "user_tel" value = "${user.user_tel}">
												<p id = "telP" style = "margin : 0; margin-left : 1em; display : none; color : gray;"></p>
												<input type = "hidden" id = "tel_temp">
											</td>
										</tr>
										<tr>
											<th>가입일</th>
											<td>${user.in_date}</td>
										</tr> 
										<tr> 
											<td colspan = "2"  style = "height : 10px;"><hr></td>
										</tr> 
									</table> 
								</form> 
								<table style = "margin-left : 5%; width : 90%;">
									<tr>
										<th style = "width : 20%; font-size : 1.1em;">포트폴리오</th> 
										<td style = "width : 40%;">
											${user.user_file}
										</td>
										<td style = "width : 40%;">
											<form id = "userFileForm">
												<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
												<input type="hidden" name = "user_idx" value = "${user.user_idx}">
												<input type = "file" name = "user_file" id = "user_file">
											</form>
										</td>
									</tr>
								</table> 
								<br>  
								<br>  
								<div align = "right" style = "margin-right : 20%; margin-top : 10%; width : 100%;"> 
									<c:if test = "${user != null}">
										<input type = "button" class = "btn btn-secondary" id = "modifyUserProfile" value = "회원 정보 수정">
										<input type = "button" class = "btn btn-secondary" data-toggle="modal" data-target="#modifyUserPicModal" id = "modifyUserPic" value = "사진 변경">
										
										<c:if test = "${empty google}">
											<input type = "button" class = "btn btn-secondary" data-toggle="modal" data-target="#modifyPwModal" id = "modifyPwGet2" value = "비밀번호변경">
											<input type = "button" class = "btn btn-danger" data-toggle="modal" data-target="#removeAccountModal" value = "탈퇴" id = "removeAccount2">
										</c:if>
										<c:if test = "${not empty google}">
											<input type = "button" class = "btn btn-danger" value = "계정 연결 해제" id = "showRemoveGoogleAccount">
										</c:if>  
									</c:if>
								</div>
								<br>
			 				</div> 
			 			</div>
					</div>
					<br>
					<hr style = "color : gray; width : 98%; margin-left : 1%;">
					<br>
					
	 			</div>
	 			
	 			</div>
	 			<!--  -->
	 			 
	 			<br>
	 			<br>
	 			<hr class = "bg-secondary" style = "width : 98%; margin-left : 1%;">
	 			<br>
	 		</div>
			
			
			
			
			
			
			<div id = "removeGoogleAccountDiv" align = "center" style = "display:none;">
				<form id = "removeGoogleAccountForm">
					<table>
						<tr>
							<td>
								메일 주소를 입력해주세요<input type = "text" id = "user_email_remove" name = "user_email" required>
							</td>
						</tr>
						<tr>
							<td align = "right">
								<input type = "button" id = "removeGoogleAccount" value = "해제">
							</td>
						</tr>
					</table>
				</form>
			</div>
			<iframe id = "logoutIframe" style = "display : none;"></iframe>
		</div>
	</div>
	<div class="modal" id="modifyUserPicModal" style = "margin-top : 10%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">프로필 사진 변경</h4>
				</div>
				
				<div class="modal-body">
				  	<form id = "modifyUserPicForm">
				  		<img src = "/user/image?user_pic=${user.user_pic}" id = "modalImg" class="rounded" width = "80%" style = "margin-left : 10%; margin-top : 2%;">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
						<br>
						<br> 
						<input type = "file" id = "user_pic" name = "user_pic">
						<input type = "hidden" name = "user_idx" value = "${user.user_idx}">
					</form>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" id = "defaultPic">기본이미지로 변경</button>
					<input type = "button" class = "btn btn-primary" id = "modifyUserPicBtn" value = "변경">
					<button type="button" class="btn btn-danger" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal" id="modifyPwModal" style = "margin-top : 15%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">비밀번호 변경</h4>
				</div>
				
				<div class="modal-body">
				  	<form id = "modifyPwForm">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
						<input type = "hidden" name = "user_idx" value = "${user.user_idx}">
						<br>
						<input type = "password" class = "form-control" id = "user_pw_old" name = "user_pw_old" placeholder = "기존 비밀번호 " required>
						<br>
						<input type = "password" class = "form-control" id = "user_pw_new" name = "user_pw_new" placeholder = "새 비밀번호" required>
						<br>
						<input type = "password" class = "form-control" id = "user_pw_new_c" name = "user_pw_new_c" placeholder = "비밀번호 확인" required>
						<br>
					</form>
				</div>
				
				<div class="modal-footer">
					<input type = "button" class = "btn btn-outline-primary" id = "modifyPw" value = "변경">
					<button type="button" class="btn btn-outline-danger" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="removeAccountModal" style = "margin-top : 10%;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">계정 삭제</h4>
				</div>
				
				<div class="modal-body" align = "center">
					<form id = "removeAccountForm" action = "removeAccount" method = "post" style = "width : 80%;">
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
						<input type = "password" class = "form-control" name = "user_pw" id = "user_pw" placeholder = "비밀번호" required>
						<input type = "hidden" name = "user_idx" value = "${user.user_idx}">
						<br>
						<input type = "password" class = "form-control" name = "user_pw_c" id = "user_pw_c" placeholder = "비밀번호 확인" required>
					</form>
				</div>
				
				<div class="modal-footer">
					<input type = "button" class = "btn btn-outline-primary" value = "탈퇴"  id = "removeAccountBtn">
					<button type="button" class="btn btn-outline-danger" data-dismiss="modal">닫기</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>