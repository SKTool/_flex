<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<script type="text/javascript">
	function getProjectList(){
		$('#projectList div:gt(0)').remove();
		$.ajax({
			url : '/user/getProjectsForMyPage',
			type : 'POST',
			data : { '${_csrf.parameterName}' : '${_csrf.token}', 'user_idx' : '${user.user_idx}'},
			success : function(projectList){
				
				for(var i = 0; i < projectList.length; i++){
					var div = $('<div class = "card bg-light" style = "width : 90%; margin-left : 5%;">');
					var divBody = $('<div class = "card-body">');
					var table = $('<table>'); 
					var tr = $('<tr>');
					 
					$('<td>').text(projectList[i].project_title).appendTo(tr);
					$('<td>').text((projectList[i].project_start).substr(0, 11)).appendTo(tr);
					$('<td>').text(projectList[i].admin_user_id).appendTo(tr);
					if(projectList[i].bell_active == 1){
						$('<td>').append('<div class="custom-control custom-switch"><input type="checkbox" checked="checked" class="custom-control-input" id="switch' + i + '" data-toggle="toggle"><label class="custom-control-label" for="switch'+ i +'">알림</label><input type = "hidden" name = "bell_active" value = "'+ projectList[i].bell_active +'"><input type = "hidden" name = "project_member_idx" value = "'+ projectList[i].my_project_member_idx +'"></div>').appendTo(tr);
					}else if(projectList[i].bell_active == -1){
						$('<td>').append('<div class="custom-control custom-switch"><input type="checkbox" class="custom-control-input" id="switch' + i + '"><label class="custom-control-label" for="switch'+ i +'">알림</label><input type = "hidden" name = "bell_active" value = "'+ projectList[i].bell_active +'"><input type = "hidden" name = "project_member_idx" value = "'+ projectList[i].my_project_member_idx +'"></div>').appendTo(tr);
					}
					$('<td>').append('<input type = "button" id = "leaveBtn' + i + '" class = "btn btn-outline-danger" value = "나가기">').appendTo(tr);
					
					tr.appendTo(table);
					table.appendTo(divBody);
					divBody.appendTo(div);
					div.appendTo($('#projectListDiv'));
					$('<br>').appendTo($('#projectListDiv'));
					
					(function(i){
						$('#switch' + i).on('click', function(){
							
							var bell_active_val = 0;
							if($('#switch'+i).is(':checked')){
								bell_active_val = 1;
							}else{
								bell_active_val = -1;
							}
							
							$.ajax({
								url : '/user/alterBellActive',
								type : 'POST',
								data : {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : projectList[i].my_project_member_idx, 'bell_active' : bell_active_val },
								success : function(result){
								}
							})
						});
					})(i);
					
					
					(function(i){
						$('#leaveBtn' + i).on('click', function(){
							Swal.fire({
								  text: '정말 나가시겠습니까?',
								  type: 'warning',
								  confirmButtonText: '나가기',
								  showCancelButton : true,
								  cancelButtonText : '취소',
							}).then((result)=>{
								if(result.value){
									$.ajax({
										url : '/user/leaveProject',
										type : 'POST',
										data : {'${_csrf.parameterName}' : '${_csrf.token}', 'project_member_idx' : projectList[i].my_project_member_idx},
										success : function(result){
											if(result === 1){
												alert('success');
												location.reload();
											}else if(result === -1){
												Swal.fire({
													  text: '서버가 불안정합니다, 네트워크 연결을 확인해주세요',
													  type: 'warning',
												}).then(()=>{
													location.reload();
												})
											}
										}
									})
								}
							})
						});
					})(i);
				}
			} 
		}) 
	}
	$(function(){
		getProjectList();
		
		$('#modifyModalBtn').on('click', function(){
			$('#modifyComment').css('display', 'unset');
		});
		
		$('#modifyModalCancelBtn').on('click', function(){
			$('#modifyComment').css('display', 'none');
		});
		
		$('body').on('change', function(){
			var userPicWidth = $('#userPic').css('width');
			$('#userPic').attr('height', userPicWidth);
		})
		$('#modifyCommentBtn').on('click', function(){
			
			if($('#user_comment').val() == null || $('#user_comment').val().trim() == ""){
				
				swal.fire({
					text : '빈 칸을 입력해 주세요',
					type : 'warning'
				})
			}
			
			$.ajax({
				
				url 	: '/user/modifyUser',
				type 	: 'POST',
				data 	: {"user_comment" : $('#user_comment').val(), "user_id" : '${user.user_id}'},
				success : function(){
					
					var form = $('<form action = "/user/userProfile" method = "post">');
					var input = $('<input type = "hidden" name = "user_idx" value = "${user_idx}">');
					var submit = $('<input type = "submit" id = "submitIt"  style = "display:none;">');
					var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
					input.appendTo(form);
					submit.appendTo(form);
					token.appendTo(form);
					form.appendTo($("body"));
					
					$('#submitIt').click();
				}
			});
		});
		
		$('#modifyProfile').on('click', function(){
		
			var form = $('<form action = "/user/modifyProfile" method = "post">');
			var input = $('<input type = "hidden" name = "user_id" value = "${user.user_id}">');
			var submit = $('<input type = "submit" id = "submitIt"  style = "display:none;">');
			var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
			input.appendTo(form);
			submit.appendTo(form);
			token.appendTo(form);
			form.appendTo($("body"));
			
			$('#submitIt').click();
		});
		
		$('#showRemoveGoogleAccount').on('click', function(){
			$('#removeGoogleAccountDiv').css('display', 'unset');
		})
		
		$('#removeGoogleAccount').on('click', function(){
			
			var user_email_remove = $('#user_email_remove').val().trim();
			
			if(user_email_remove.length <= 0 || user_email_remove == ''){
				swal.fire({
					text : '메일 주소를 입력해주세요',
					type : 'warning'
				})
				
				return false;
			}
			
			$.ajax({
				url		: '/user/pwCheck',
				type	: 'POST',
				data	: {'user_idx' : '${user.user_idx}', 'user_pw' : $('#user_email_remove').val(), '${_csrf.parameterName}' : '${_csrf.token}' },
				success : function(result){
					
					if(result === 1){
						
						var conf = confirm('연결을 해제하면 이 사이트를 사용하는 동안 입력한 정보를 더이상 사용할 수 없습니다. 연결을 해제하시겠습니까?');
						
						if(conf){
							
							$('#logoutIframe').attr('src',"https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:9080/index/main?logout=true");
							$('#logoutIframe').on('load', function(){

								var form = $('<form action = "/user/removeGoogleAccount" method = "post">');
								var input = $('<input type = "hidden" name = "user_idx" value = "${user.user_idx}">');
								var pw = $('<input type = "hidden" name = "user_pw" value = "'+$('#user_email_remove').val()+'">');
								var submit = $('<input type = "submit" id = "submitIt" style = "display:none;">');
								var token = $('<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">');
				
								input.appendTo(form);
								pw.appendTo(form);
								submit.appendTo(form);
								token.appendTo(form);
								form.appendTo($("body"));
								
								$('#submitIt').click();
							});
						}
					}else if(result === -1){
						swal.fire({
							text : '이메일이 맞지 않습니다',
							type : 'warning'
						})
					}
				}
			})
		})
		
		$('#projectList').on('click', function(){
			location.href='/user/projectList';			
		})
		
		$('#openNotice').on('click', function(){
			
			$('.active').removeClass('active');
			$('#openNoticeLi').addClass('active');
			$('#userProfileDiv').css('display', 'none');
			$('#noticeDiv').css('display', 'unset');
		})
		
		$('#openProfile').on('click', function(){
			
			$('.active').removeClass('active');
			$('#openProfileLi').addClass('active');
			$('#userProfileDiv').css('display', 'unset');
			$('#noticeDiv').css('display', 'none');
		})
	});
</script>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Noto+Serif+KR');
	
	body{
 		font-family: 'Noto Serif KR', serif; 
	}
	#modifyComment{
		display:none;
	}
	.nav nav-tabs li a:hover{ 
 		background-color : lightgray;
 	}
 	a.active {
 		color : #009999 !important; 
 	}
 	
 	#userProfileTable tr th, #userProfileTable tr td {
 		height : 5em;
 		font-size: large;
 	}
 	
 	#projectListDiv div table tr td{
 		padding : 10px;
 	}
</style>                                                                 
</head>
<body>
<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
	<div class="container-fluid" style="padding:0; margin : 0; width : 100%;">
		<div class="row" style = " width : 100%; background : white; margin : 0;">
		 	<div class = "container-fluid" style = 'padding : 0; margin : 0;'>
		 	
		 			 		
		 		<div id = "navigator" style = "height : 30px; width : 95%; margin-left : 5%;margin-top : 100px;">
			 		<a href = "/index/main" style = "color : gray;">Home</a>&nbsp&nbsp<span style = "color : gray;">/</span>&nbsp&nbsp<a href = "/user/userProfile" style = "color : gray;">MyPage</a>
		 		</div>
		 		
		 		<div id = "myPageMain" style = "width : 100%; height : 1000px;background : lightgray;">
		 		
		 			<div id = "simpleProfile" style = "width : 20%; height : 100%; margin : 0; float : left;">
		 				<div class = "rounded" style = "width : 18%; height : 60%; margin-left : 1%; margin-top : 1%; background : #f8ecec; position : fixed;">
<!-- 		 					<div style = "background : black; border-radius : 50%; width : 86%; height : 60%; margin-left : 7%; margin-top : 7%;"> -->
		 						<img id = "userPic" src = "/user/image?user_pic=${user.user_pic}" width = "90%" style = "margin : 10% 5%; border-radius : 50%; border : 5px solid #ccddff">
<!-- 		 					</div> -->
<!-- 모양은 다 만들었고 값 넣어주면 됨. -->
		 					<div style = "width : 100%; height : 40%; background : white; padding : 10% 10%;">
		 						<p><i class="fas fa-phone" style = "font-size : 2em;"></i></p>
		 						<p><i class="far fa-calendar-check" style = "font-size : 2em;"></i></p> 
		 						<p><i class="fas fa-id-card" style = "font-size : 2em;"></i></p>
		 						<p><i class="far fa-file" style = "font-size : 2em;"></i></p>
		 					</div>  
		 				</div>
		 			</div> 
		 			
		 			<div id = "simpleProfile" style = "width : 80%; height : 100%;background :blue; margin : 0;float : right;">
		 				<div class = "rounded" style = "width : 90%; height : 97%; margin-left : 5%; margin-top : 1%; background : red;">
		 					<br>
		 					<p style = "color : white; width : 98%; margin-left : 1%; font-size : 2em; font-weight : 100;">Project List</p>
		 					<hr style = "color : white; width : 98%; margin-left : 1%;">
		 					<br>
		 				</div> 
		 			</div> 
		 			
		 		</div>
		 		 
		 		
		 		<!-- header 아래  header -->
<!-- 		 		<div class = "row" style = " width : 100%; height : 83px; background : white; margin : 0;"> -->
<!-- 						<h2 style = "margin-left : 5%; margin-top : 1%;">계정</h2> -->
<!-- 						<hr class = "bg-info" style = "width : 100%;"> -->
<!-- 				</div> -->
				
				
				
				
				
				<!-- toogle ㅡ -->
<!-- 	  			<div class = "row" style = "width : 95%; height : 30px; margin : 0; margin-left : 5%;">  -->
<!-- 					<ul class="nav nav-tabs" role="tablist" style = "border : none;">     -->
<!-- 						<li class="nav-item" id = "openProfileLi" style = "border :none;">  -->
<!--       						<a class="nav-link active" id = "openProfile" data-toggle="tab" href="#userProfileTab" style = "color : gray; border : none;">내 계정</a> -->
<!--     					</li> -->
<!-- 						<li class="nav-item" id = "openNoticeLi" style = "border :none;"> -->
<!--       						<a class="nav-link" id = "openNotice" data-toggle="tab" href="#bellTab" style = "color : gray; border : none;">알람</a> -->
<!--     					</li> -->
<!--   					</ul>                                                                            -->
<!-- 	  			</div>    						 -->




				<!-- menu -->
<!-- 				<div class = "row" id = "mainRow" style = "width : 100%; margin : 0;">	 		 	 -->
<!-- 					<div class="tab-content" style = "width : 100%; height : 100%;">  -->
					
<!--    						<div id="userProfileTab" class="container tab-pane active" style = "min-width : 100%; height : 100%; margin : 0; padding : 0;"> -->
<!--    							<div class = "row" style = "width : 100%; height : 100%; margin : 0;"> -->
<!--    								<div class = "container col-md-3" style = "padding : 0; height : 100%;"> -->
<%--    									<img id = "userPic" src = "/user/image?user_pic=${user.user_pic}" width = "80%" style = "margin : 20% 10%; border-radius : 50%; border : 5px solid #b3d1ff"> --%>
<!--    					 			</div>                       -->
   								
<!--    								<div class = "container col-md-3" style = "padding : 0; padding-bottom : 20%; height : 100%;">  -->
<!--     								<table style = "margin-top : 10%; margin-left : 5%; width : 30%;" id = "userProfileTable">    -->
<!-- 										<tr>  -->
<%-- 											<td colspan = "2"  style = "font-size : 1.5em;"><b>${user.user_id}</b></td> --%>
<!-- 										</tr> -->
<!-- 										<tr>  -->
<!-- 											<th>이름</th> -->
<%-- 											<td>${user.user_name}</td> --%>
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<th>전화번호</th> -->
<%-- 											<td>${user.user_tel}</td> --%>
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<th>가입일</th> -->
<%-- 											<td>${user.in_date}</td> --%>
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<td colspan = "2"  style = "height : 10px;"><hr></td> -->
<!-- 										</tr> -->
<!-- 										<tr> -->
<!-- 											<th>포트폴리오</th> -->
<%-- 											<td>${user.user_file}</td> --%>
<!-- 										</tr> -->
<!-- 									</table> -->
<!-- 									<br>   -->
<!-- 									<div align = "right" style = "margin-right : 15%; margin-top : 10%;">  -->
<%-- 										<c:if test = "${user != null}"> --%>
<!-- 											<input type = "button" class = "btn btn-outline-primary" value = "정보수정" id = "modifyProfile"> -->
<%-- 										</c:if>  --%>
<!-- 									</div> -->
<!--    								</div> -->
<!--    								<div class = "container col-md-6" id = "projectListDiv" style = "padding : 0; border-left : 1px solid lightgray;"> -->
<!--    									<table width = "70%" style = "margin-left : 8%; text-align : center;">   -->
<!--    										<tr> -->
<!--    											<th style = "width : 18%;">이름</th>   		 -->
<!--    											<th style = "width : 25%;">시작일</th> -->
<!--    											<th style = "width : 32%;">관리자</th> -->
<!--    											<th style = "width : 25%;">알림</th>                 -->
<!--    										</tr> -->
<!--    										<tr><th><br></th></tr> -->
<!--    									</table> -->
<!--    								</div> -->
<!--    							</div> -->
<!--    						</div> -->
   				
<!-- 	    				<div id="bellTab" class="container tab-pane fade" style = "width : 100%; height : 100%; margin : 0; padding : 0;"><br> -->
<%-- 							<jsp:include page="/jsp/userBell/userBell.jsp"></jsp:include> --%>
<!-- 					    </div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- 			</div> -->
				
		<div id = "removeGoogleAccountDiv" align = "center" style = "display:none;">
			<form id = "removeGoogleAccountForm">
				<table>
					<tr>
						<td>
							메일 주소를 입력해주세요<input type = "text" id = "user_email_remove" name = "user_email" required>
						</td>
					</tr>
					<tr>
						<td align = "right">
							<input type = "button" id = "removeGoogleAccount" value = "해제">
						</td>
					</tr>
				</table>
			</form>
		</div>
		<iframe id = "logoutIframe" style = "display : none;"></iframe>
	</div>
</div> 
<br>
</body>
</html>