<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="674931549888-nfb6kriv0lrjpmrup2cgp2ktq2cs20ik.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js" async defer></script>
<jsp:include page="/jsp/include/top/top.jsp"></jsp:include>
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
	$(function(){
		
		var screenHeight = document.body.clientHeight-40;
		
		$('#mainDiv').attr('style', 'padding:0; overflow: hidden; height : ' + screenHeight + 'px;');
		  
		$('#modifyBtn').on('click', function(){ 
			 
			var formInputs = $('#modifyForm').find('input[name][type!="hidden"]'); 
			
			for( var i = 0; i < formInputs.length; i++){
				
				if($(formInputs[i]).attr('required') != null){
					
					if($.trim($(formInputs[i]).val()) == ''){
						swal.fire({
							text : '빈칸을 입력해 주세요',
							type : 'warning'
						})
						$(formInputs[i]).focus();
						
						return false;
					}	
				};
			};
			
			if($('#user_pw').val() !== $('#user_pw_c').val()){
						swal.fire({
							text : '비밀번호가 맞지 않습니다',
							type : 'error'
						})
				
				$('#user_pw_c').focus();
				
				return false;
			}
			
			var modifyForm = $('#modifyForm').serialize();
			
			$.ajax({
				url  	: '/user/modifyUserPwFromMail',
				type 	: 'POST',
				data 	: modifyForm,
				success : function(result){
					
					if(result === 1){
						
						swal.fire({
							text : '비밀번호 변경 완료',
							type : 'success'
						}).then(()=>{
							location.href='/index/main';
						})
						
					}else if(result === -1){
						
						swal.fire({
							text : '비밀번호 변경 실패',
							type : 'error'
						})
					}
				}
			});
		});
	});
</script>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Noto+Serif+KR');
	
	body{
		font-family: 'Noto Serif KR', serif;
		min-height : 100vh;
	}  
</style>
</head>
<body style = "overflow :hidden;">

	<div id="mainLayout-wrap" class="wrap" >
	
		<div id="main-header" class="header">
			<jsp:include page="/jsp/include/header/header.jsp"></jsp:include>
		</div>

		<div id="main_wrap" class="main_wrap">

			<div class="main">

				<div class="container-fluid" id = "mainDiv"> <!-- #e0ebeb -->
			    	<div class="row" style = "min-height: 100%; position : relative;">
			    		<div class = "container" style = "min-height : 100%; min-width : 100%; position : absolute; background : linear-gradient( to bottom, #e0ebeb, #ffe6e6); z-index : -1;">
			    		</div>  
						<div class = "container" style = "min-height : 100%; min-width : 70%; margin-left : 15%;position : absolute; background : white; z-index : 0;">
							<form id = "modifyForm" style = "margin-top : 10%; margin-left : 35%; width : 30%;">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
								<input type = "hidden" name = "user_id" value = "${param.user_id}">
								<input type = "hidden" name = "user_idx" value = "${param.user_idx}"> 
								<input type = "hidden" name = "uuid" value = "${param.uuid}"> 
								<br>
								<h2 align = "center">비밀번호 변경</h2>  
								<br>
								<hr class = "bg-info"> 
								<br>
								<br>
								<br> 
									<input type = "password" class = "form-control" name = "user_pw" id = "user_pw" placeholder = "비밀번호" required>
								<br> 
									<input type = "password" class = "form-control" name = "user_pw_c" id = "user_pw_c" placeholder = "비밀번호 확인" required>
								<br> 
								<br> 
								<br> 
								<br> 
								<br> 
								<input type = "button" class = "btn btn-info btn-block" id = "modifyBtn" value = "변경">
								<br> 
								<br> 
								<hr class = "bg-info">
								<br>
								<br>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>