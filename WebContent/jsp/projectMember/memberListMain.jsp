<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="../include/top/top.jsp"></jsp:include>

<title>멤버</title>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>	
<style type="text/css">
	.cp_ipradio {
		width: 90%;
/* 		margin: 2em auto; */
		text-align: left;
	}
	.cp_ipradio .option-input {
		position: relative;
		position: relative;
		top: 5.33333px;
		right: 4px;
		bottom: 0;
		left: 0;
		width: 25px;
		height: 25px;
		margin-right: 0.5rem;
		cursor: pointer;
		transition: all 0.15s ease-out 0s;
		color: #ffffff;
		border: none;
		outline: none;
		background: #d7cbcb;
		-webkit-appearance: none;
		        appearance: none;
	}
	.cp_ipradio .option-input:hover {
		background: #3ca9c9;
	}
	.cp_ipradio .option-input:checked {
		background: #5bc0de;
	}
	.cp_ipradio .option-input:checked::before {
		font-size: 20px;
		line-height: 30px;
		position: absolute;
		display: inline-block;
		width: 25px;
		height: 25px;
		content: '✔';
		text-align: center;
	}
	.cp_ipradio .option-input:checked::after {
		position: relative;
		display: block;
		content: '';
		-webkit-animation: click-wave 0.65s;
		        animation: click-wave 0.65s;
		background: #68dbdd;
	}
	.cp_ipradio .option-input.radio {
		border-radius: 50%;
	}
	.cp_ipradio .option-input.radio::after {
		border-radius: 50%;
	}
	.cp_ipradio label {
		line-height: 40px;
		display: block;
	}
	.cp_ipradio .option-input:disabled {
		cursor: not-allowed;
		background: #5bc0de;
	}
	.cp_ipradio .option-input:disabled::before {
		font-size: 20px;
		line-height: 30px;
		position: absolute;
		display: inline-block;
		width: 30px;
		height: 30px;
		content: '✖︎';
		text-align: center;
	}
	.cp_ipradio .disabled {
		color: #9e9e9e;
	}	
</style>	
<script type="text/javascript">
	$(function() {
		createMemberList();
		$("#btnInvite").on("click", function() {
			var email = $("#emailCheck").val();
			var invite = $("input:radio[name=invite]:checked").val();
			var projectAuthIdx;
			if (invite === "팀원") {
				projectAuthIdx = 2;
			} else if (invite === "게스트") {
				projectAuthIdx = 3;
			}
			alert(email);
			alert(invite);
			$.ajax({
				url : "/projectMember/register",
				data : {
					"user_id" : email,
					"project_auth_idx" : projectAuthIdx,
					"${_csrf.parameterName}" : "${_csrf.token}"
				},
				type : "get",
				success : function(data) {
					Swal.fire({
						title: '정상적으로 초대되었습니다',
						type: 'success',
						confirmButtonText: '확인',
					})
					createMemberList();
				}
			});
		});
	});
	function createMemberList() {
		var memberList = $("#memberList");
		$("#memberList tr:gt(0)").remove();
		$.ajax({
			url : "/projectMember/all",
			data : {"${_csrf.parameterName}" : "${_csrf.token}"},
			type : "post",
			success : function(data) {
				for ( var i in data) {
					var tr = $("<tr>");
					var divDropdown = $('<div class="dropdown dropright"></div>');
					var divDropdownMenu = $('<div class="dropdown-menu"><a class="dropdown-item" href="#" id="modifyManager'+i+'">관리자로 변경</a><a class="dropdown-item" href="#" id="deleteProject'+i+'">프로젝트에서 삭제</a></div>');
					var dropdown = $('<a class="dropdown-item" href="#"></a>');
					var btnCog = $('<i class="fas fa-cog" data-toggle="dropdown"></i>');
					var assignWrite = $("#assignWrite");
					$("<td>").text(data[i].user_name).appendTo(tr);
					$("<td>").text(data[i].user_id).appendTo(tr);
					$("<td>").text(data[i].auth_name).appendTo(tr);
					$("<td style='padding-left:25px;'>").append(btnCog).append(divDropdownMenu).appendTo(divDropdown).appendTo(tr);
					tr.appendTo(memberList);					
					
					(function(m) { 

						$("#modifyManager"+m).on("click",function(){
							Swal.fire({
						        title: '관리자로 위임하시겠습니까?',
						        type: 'question',
						        showCancelButton: true,
						        confirmButtonColor: '#3085d6',
						        confirmButtonText: '확인',
						        cancelButtonColor: '#d33',
						        cancelButtonText: '취소'
						    }).then((result) => {
						    	if (result.value) {
							$.ajax({
								url : "/projectMember/modify",
								data : {"project_member_idx" : data[m].project_member_idx,
// 									    "project_auth_idx" : data[m].project_auth_idx,
									    "${_csrf.parameterName}" : "${_csrf.token}"
									    },
							    type : "post",
							    success : function(data){
							    	if(data == 1){
							    		Swal.fire({
							    			title: '관리자로 변경되었습니다',
							    			type: 'success',
							    		}).then(()=>{
							    			location.reload();
							    		})
							    	} else if(data == -1) {
							              Swal.fire({
							                  title : '변경이 실패되었습니다',
							                  type: 'error'
							               })
							    		}	
							   	 	}
								});
//								return false;	
								}
							})
						});
						
						$("#deleteProject"+m).on("click",function(){
							alert("프로젝트에서 삭제하시겠습니까?");
							$.ajax({
								url : "/projectMember/delete",
								data : {"project_member_idx" : data[m].project_member_idx,
									    "${_csrf.parameterName}" : "${_csrf.token}",
									},
								type : "post",
								success : function(data){
									alert("정상적으로 삭제되었습니다");
									createMemberList();	
								}
							});
//								return false;
						});						
					})(i);
				}
			}
		});
	}
</script>
</head>
<body>
	<!-- 멤버 리스트 -->
	<div style="width: 90%; margin-left: 5%;">
		<div style="width: 100%;">
			<br> 
			<br> 
			<h2 id="memberTitle" style ="width: 95%;margin-left: 5%; color: gray; font-weight:100;">Member Authority</h2>
			<p style = " margin-left : 5%;color : lightgray; font-size : 0.9em;">소속된 멤버들 간 권한관리 및 리스트 확인</p>
			<hr style = "width : 98%; margin-left : 1%;">
			<br>
		</div>
		<button class="btn btn-info btn-sm" data-toggle="modal" data-target="#inviteModal" style="float: right; margin: 5px;">멤버초대</button>
			<div>
				<table class="table" id="memberList" style="width:80%; margin-left:10%; margin-top: 5%;">
					<tr>
						<th>이름</th>
						<th>이메일</th>
						<th>권한</th>
						<th>아이콘</th>
					</tr>
				</table>
			</div>	
	</div>
	<div class="modal" id="inviteModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<h4 class="modal-title" style="	font-size: 1.5em; font-weight: 100;">
						멤버 초대하기 <br> 초대 유형을 선택해주세요
					</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
				<div class="cp_ipradio">
					<input type="radio" name="invite" value="팀원" checked="checked" id="invite" class="option-input radio">팀원
					<input type="radio" name="invite" value="게스트" id="invite" class="option-input radio">게스트
				</div>
					<div id="inviteComment"></div>
					<script type="text/javascript">
						$(function() {
							$("input:radio[name='invite']")
									.click(
											function() {
												var inviteComment = $("#inviteComment");

												$("#inviteComment div:gt(0)").remove();
												var inviteValue = $("input:radio[name=invite]:checked").val();
												var div = $("<div>");
												if (inviteValue === "팀원") {
													$("<div style='margin:7px;'>").text("팀원은 프로젝트의 공개된 모든 정보에 엑세스할 수 있으며, 팀원이 프로젝트 멤버로 추가되면 의 읽기/쓰기 권한이 부여됩니다").appendTo(div);
												} else if (inviteValue === "게스트") {
													$("<div style='margin:7px;'>").text("게스트는 프로젝트의 정보에 제한적으로 엑세스가 필요한 외부 협력자 입니다. 게스트가 프로젝트 멤버로 추가되면 읽기권한만 부여됩니다").appendTo(div);
												}
												div.appendTo(inviteComment);
											});
						});
					</script>
					<div style="margin:7px;">이메일로 초대하기</div>
					<div>
						<input type="text" id="emailCheck" placeholder="email@flex.com">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-info btn-sm" id="btnInvite">멤버 초대하기</button>
				</div>

			</div>
		</div>
	</div>

<script type="text/javascript">
	$("#assignWrite").on("click",function(){
		var assignInput = $("#assignInput").val();
		alert(assignInput);
	});
</script>
</body>
</html>