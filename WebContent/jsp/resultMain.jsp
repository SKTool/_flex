
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">

		Swal.fire({
			text : "${msg}",
			type : 'success',
		}).then(()=>{
			location.href="${url}";
		})
		
</script>
