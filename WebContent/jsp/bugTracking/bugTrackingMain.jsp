<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="../include/top/top.jsp"></jsp:include>

<title>버그 추적 게시판</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/todo/common.css">	
<link rel="stylesheet" href="/css/todo/style.css">	
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
	
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css"
	type="text/css" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
	integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
	crossorigin="anonymous">
	
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>	
<style type="text/css">
a:link {
	color: black;
	text-decoration: none;
}

a:visited {
	color: black;
	text-decoration: none;
}

a:hover {
	color: blue;
	text-decoration: underline;
}

div {
	float: left;
}
#trash{
	margin-left:330px;
	display: inline;
	cursor: pointer;
	font-size: 15px;
/* 	vertical-align: middle; */
}
#name{
	float:none;
	font-weight: bold;
	
	display: inline;
}
#bugTitle {
/* 	text-align: center; */
	font-size: 2em;
	font-weight: 100;
}

#appendBugList {
/* 	text-align: center; */
	font-size: 0.8em;
	font-weight: 100;
}

#thBugList {
/* 	text-align: center; */
	font-size: 1em;
	font-weight: bolder;
}

#modalView {
	text-align: center;
	font-size: 0.9em;
	font-weight: 100;
}

input[type=text], select {
 	-webkit-appearance: none; 
 	width: 100%; 
 	height: 50%; 
 	border: 0; 
 	font-family: inherit; 
 	padding: 12px 0; 
 	font-size: 12px; 
 	font-weight: 100; 
  	border-bottom: 2px solid #C8CCD4; 
 	background: none; 
 	border-radius: 0; 
 	color: #223254; 
 	transition: all .15s ease; 
  	text-align: center;  
 	align: center; 
 	font-size: 0.8em; 
 	font-weight: 100; 
 	text-align-last:center; 
display: inline-block; 
margin-left: 10px; 
margin-bottom: 15px; 
position: relative;		 


/*     padding:5px;  */
/*     border:2px solid #ccc;  */
/*     -webkit-border-radius: 5px; */
/*     border-radius: 5px; */
}

textarea {
    padding:5px; 
    border:2px solid #ccc; 
    -webkit-border-radius: 5px;
    border-radius: 5px;
	color: #223254;
	width: 100%;
	border: 1;
	background: none;
	font-size: 0.8em;
	font-weight: 100;
	
	display: inline-block;
  	margin-left: 10px;
  	margin-bottom: 15px;
 	 position: relative;
}
#replyDivDetail {
	float:none;
	font-size: 0.8em;
	font-weight: 100;
	border: 1px solid #C8CCD4;
	width: 400px;
}
</style>
<script type="text/javascript">
	$(function() {
		createBugList();
// 		$("#write-occdate").datepicker({
// 			dateFormat : 'yy-mm-dd'
// 		});
		$("#write-occdate").flatpickr();
		$("#bugViewModalOccdate").flatpickr();
// 		$("#bugViewModalOccdate").datepicker({
// 			dateFormat : 'yy-mm-dd'
// 		});

		$("#bugViewModal").on("bugModal", function(e) {
			var d = $(e.bug_title).val();
		});

		$("#btnWrite").on("click", function() {
			var d = $("#modal-form").serialize();
			$.ajax({
				url : "/bugTracking/write",
				data : d,
				type : "post",
				success : function(result) {
					if (result === 1) {
						Swal.fire({
							title: '정상적으로 등록되었습니다',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: '확인'
						}).then(()=>{
							location.reload();
						})
// 						$("#modal-form").hide("slow");
// 						createBugList();
						
					}
				}
			});
		});

	});

	function createBugList() {
		var bugTable = $("#bugList");
		$("#bugList tr:gt(0)").remove();
				$.ajax({
					url : "/bugTracking/all",
					type : "post",
					data : {"${_csrf.parameterName}" : "${_csrf.token}"},
					dataType : "json",
					success : function(data) {
						var iNum = 0;
						for ( var i in data) {
							var tr = $("<tr>");
							iNum++
							$("<td id='appendBugList'>").text(iNum).appendTo(tr);
							$("<td id='appendBugList'>").append($("<a id='titleBtn"+i+"' href='#' data-toggle='modal' data-target='#bugViewModal'>").text(data[i].bug_title)).appendTo(tr);
							var critical;
							if(data[i].bug_critical == 1){
								critical = "낮음";
								$("<td id='appendBugList'>").text(critical).appendTo(tr);
							} else if (data[i].bug_critical == 2){
								critical = "중간";
									$("<td id='appendBugList'>").text(critical).appendTo(tr);
							} else if (data[i].bug_critical == 3){
								critical = "높음";
								$("<td id='appendBugList'>").text(critical).appendTo(tr);
							}
							var status;
							if(data[i].status == 1){
								status = "신규";
								$("<td id='appendBugList'>").text(status).appendTo(tr);
							} else if (data[i].status == 2){
								status = "테스트";
								$("<td id='appendBugList'>").text(status).appendTo(tr);
							} else if (data[i].status == 3){
								status = "확인";
								$("<td id='appendBugList'>").text(status).appendTo(tr);
							}
							$("<td id='appendBugList'>").text(data[i].user_name).appendTo(tr);
							$("<td id='appendBugList'>").text(data[i].bug_where).appendTo(tr);
							tr.appendTo(bugTable);

							//console.log(btn);
							var btn = $('#titleBtn' + i);
							(function(m) {
								btn.on("click", function() {
									var idx = data[m].bug_idx;
									var title = data[m].bug_title;
									var critical = data[m].bug_critical;
									var status = data[m].status;
									var where = data[m].bug_where;
									var occdate = data[m].occ_date;
									var content = data[m].bug_content;

									//현재 데이터 정보를 포함해서 화면을 보이기
									$("#bugViewModalTitle").val(title);
									$("#bugViewModalTitle")
											.attr("value", title);
									$("#bugIdx").attr("value", idx);
									$("#bugViewModalTitle").attr("disabled",
											"true");

									$("#bugViewModalCritical").val(critical);
									$("#bugViewModalCritical").attr("value",critical);
									$("#bugViewModalCritical").attr("disabled","true");

									$("#bugViewModalStatus").val(status);
									$("#bugViewModalStatus").attr("value",status);
									$("#bugViewModalStatus").attr("disabled","true");

									$("#bugViewModalWhere").val(where);
									$("#bugViewModalWhere").attr("value", where);
									$("#bugViewModalWhere").attr("disabled","true");
									$("#bugViewModalOccdate").val(occdate);
									$("#bugViewModalOccdate").attr("value",occdate);
									$("#bugViewModalOccdate").attr("disabled","true");

									$("#bugViewModalContent").text(content);
									$("#bugViewModalContent").attr("value",content);
									$("#bugViewModalContent").attr("readonly",
											"disabled");

									$("#replyDiv").children().remove();
									replyBugList();

								});
							})(i);
						}
					}
				});
	}

	function replyBugList() {
		var bugReplyList = $("#replyBugList");
		$("#replyBugList div:gt(0)").remove();
		$.ajax({
			url : "/bugReply/all",
			data : {"${_csrf.parameterName}" : "${_csrf.token}",
					"bug_idx" : $("#bugIdx").val()},
			type : "post",
			dataType : "json",
			success : function(data) {
				for ( var i in data) {
					var div = $("#replyDiv");
					var detailDiv = $("<div id='replyDivDetail' style='padding: 6px; margin: 3px;'>");
					var btnDelete = $("<i class='far fa-trash-alt fa-lg' id='trash'></i>");
// 					var btnDelete = $("<i class='fas fa-trash-alt'></i>");
// 					$("<div style='float:none;'>").append(btnDelete).appendTo(detailDiv);
					$("<div id='name'>").text(data[i].user_name).append(btnDelete).appendTo(detailDiv);
					$("<div style='float:none;'>").text(data[i].bug_reply_content).appendTo(detailDiv);
					$("<div style='float:none;'>").text(data[i].in_date).appendTo(detailDiv);
// 					$("<div style='float:none;'>").append(btnDelete).appendTo(detailDiv);
					
					detailDiv.appendTo(div);
					div.appendTo(bugReplyList);
					(function(m) {
						btnDelete.on("click", function() {
							Swal.fire({
								title: '삭제하시겠습니까?',
								type: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								confirmButtonText: '삭제',
								cancelButtonColor: '#d33',
								cancelButtonText: '취소'
							}).then((result) => {
								if (result.value) {
									$.ajax({
										url : "${contextPath}/bugReply/delete",
										data : {"${_csrf.parameterName}" : "${_csrf.token}",
												"bug_reply_idx" : data[m].bug_reply_idx},
										type : "post",
										success : function(data) {
											if (data) {
											    Swal.fire({
											      title : '댓글이 삭제되었습니다',
											      type: 'success'
											      
									   		 	})
											} else {
												Swal.fire({
													 title : '삭제가 실패되었습니다',
													 type: 'error'
										    	})
										  	}
											replyBugList();
										}
									});
									
								}
								return false;
							})
						});
					})(i);
				}
			}
		});
	}
</script>
</head>
<body>
	<!-- 게시물 등록 -->
	<div style="width: 90%; margin-left: 5%;">
		<div style="width: 100%;">
			<br> 
			<br> 
			<h2 id="bugTitle" style ="width: 95%;margin-left : 5%; color : gray;">Bug Tracking</h2>
			<p style = " margin-left : 5%;color : lightgray; font-size : 0.9em;">신속한 이슈 추적 및 팀 내의 명확한 의사 소통</p>
			<hr style = "width : 98%; margin-left : 1%;">
			<br>
		</div> 
		<!-- Button to Open the Modal -->
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal"
			data-target="#bugModal" style="float: right; margin: 5px;">추가</button>
			
	<div style="width: 100%; text-align: center;">
		<table class="table" id="bugList" style="width: 80%; margin-left:10%;">
			<tr>
				<th id="thBugList">번호</th>
				<th id="thBugList">제목</th>
				<th id="thBugList">중요도</th>
				<th id="thBugList">상태</th>
				<th id="thBugList">작성자</th>
				<th id="thBugList">버전</th>
			</tr>
		</table>
	</div>
			
		<!-- The Modal -->
		<div class="container">
<div class="modal" id="bugModal">
			<form class="modal-dialog modal-m modal-dialog-centered" id="modal-form">
			    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<input type="hidden" name="project_member_idx" id="write-member-idx" value=""> 
				<table class="modal-content">
					<tr class="modal-header">
						<th class="modal-title" style="margin-top:15px; margin-left:10px;">제목</th>
						<td><input class="form-input shutter-in" type="text" name="bug_title" id="write-title" style="width:300px; margin-left:-10px; "></td>
						<td><button type="button" class="close" data-dismiss="modal">&times;</button></td>
					</tr>
					<tr class="modal-body">
						<th id="modalView"><div style="margin-left: 10px;">중요도</div></th>
						<td>
							<select class="form-input shutter-in" name="bug_critical" id="write-critical" style="width: 60px;">
								<option value="1" >낮음</option>
								<option value="2">중간</option>
								<option value="3">높음</option>
							</select>
						</td>
					</tr>

					<tr class="modal-body">
						<th id="modalView" style="margin-left: 10px;">상태</th>
						<td>
							<select class="form-input shutter-in" name="status" id="write-status" style="width: 75px">
								<option value="1">신규</option>	
								<option value="2">테스트</option>
								<option value="3">확인</option>
							</select>
						</td>
					</tr>

					<tr class="modal-body">
						<th id="modalView">버전</th>
						<td><input class="form-input shutter-in" type="text" name="bug_where" id="write-where" style="width:100px;"></td>
					</tr>

					<tr class="modal-body">
						<th id="modalView">발생일</th>
						<td>
							<input class="form-input shutter-in" type="text" name="occ_date" id="write-occdate" style="width:150px;">
						</td>
					</tr>

					<tr class="modal-body">
						<th id="modalView">내용</th>
						<td><textarea rows="10" cols="100" name="bug_content"
							id="write-content" style="width:400px;"></textarea></td>
					</tr>

					<tr class="modal-footer">
						<th><button type="button" class="btn btn-info btn-sm" id="btnWrite">등록</button></th>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- 하나의 게시글 확인 -->		
	<div class="container">
		<div class="modal" id="bugViewModal">
			<form class="modal-dialog" id="modal-form-2">
				 <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
				<input type="hidden" name="project_member_idx" id="write-member-idx" value="1">
					<input type="hidden" name="bug_idx" id="bugIdx">
				<div>
				<table class="modal-content">
					<tr class="modal-header">
						<th class="modal-title">제목</th>
						<td><input class="form-input shutter-in col-10" type="text" name="bug_title" id="bugViewModalTitle" style="width:300px;"></td>
						<td><button type="button" class="close" data-dismiss="modal">&times;</button></td>
					</tr>
					<tr class="modal-body">
						<th id="modalView">중요도</th>
						<td>
							<select class="form-input shutter-in" name="bug_critical" id="bugViewModalCritical"
								style="width: 60px">
								<option value="1">낮음</option>
								<option value="2">중간</option>
								<option value="3">높음</option>
							</select>
						</td>
					</tr>
					<tr class="modal-body">
						<th id="modalView">상태</th>
						<td>
							<select class="form-input shutter-in" name="status" id="bugViewModalStatus"
								style="width: 75px">
								<option value="1">신규</option>
								<option value="2">테스트</option>
								<option value="3">확인</option>
							</select>
						</td>
					</tr>
					<tr class="modal-body">
						<th id="modalView">버전</th>
						<td>
							<input class="form-input shutter-in" type="text" name="bug_where" id="bugViewModalWhere" style="width:100px;">
						</td>
					</tr>
					<tr class="modal-body">
						<th id="modalView">발생일</th>
						<td>
							<input class="form-input shutter-in col-3" type="text" name="occ_date" id="bugViewModalOccdate">
						</td>
					</tr>
					<tr class="modal-body">
						<th id="modalView">내용</th>
						<td>
							<textarea rows="10" cols="100" name="bug_content"
								id="bugViewModalContent" style="width:400px;"></textarea>
						</td>
					</tr>
					<tr class="modal-footer">
						<th>
							<button type="submit" class="btn btn-info btn-sm" id="btnModify">수정</button>
						</th>
						<th>
							<button type="button" class="btn btn-danger btn-sm" id="btnDelete">삭제</button>
						</th>
					</tr>
					<tr style="margin-left: 20%;">
						<th>
							<div style="margin-left: 30px;"	>
							<textarea rows="1" cols="80" name="bug_reply_content"
								id="bugReplyContent" placeholder="댓글을 작성해주세요" style="width:70%;"></textarea>
							<button type="button" class="btn btn-info btn-sm"
								id="btnReplyWrite" style="margin-left:1px; margin-top: -45px;">등록</button>
						</div>
						</th>
					</tr>
					
				<tr >
					<th id="reply2" style="margin: 0 5% 5% 5%;">
						<div id="replyBugList" style="margin-left: 30px;">
							<div id="replyDiv">
								<div id="replyDivDetail">
								</div>
							</div>
						</div>
					</th>
				</tr>
					</table>
				</div>	
			</form>
		</div>
	</div>				
	</div>
	<script>
		$("#btnModify").toggle(function() {
			Swal.fire({
				title: '활성화됩니다',
				type: 'info',
				confirmButtonColor: '#3085d6',
				confirmButtonText: '확인'
			})
			$("#bugViewModalTitle").attr("disabled", false);
			$("#bugViewModalCritical").attr("disabled", false);
			$("#bugViewModalStatus").attr("disabled", false);
			$("#bugViewModalWhere").attr("disabled", false);
			$("#bugViewModalOccdate").attr("disabled", false);
			$("#bugViewModalContent").attr("disabled", false);
			$("#bugViewModalContent").attr("readonly",false);
			$("#modal-form-2").show();
		}, function() {
			var d = $("#modal-form-2").serialize();
			$.ajax({
				url : "/bugTracking/modify",
				data : d,
				type : "post",
				success : function(result) {
					if (result === 1) {
						Swal.fire({
							title: '정상적으로 수정되었습니다',
							type: 'success',
						}).then(()=>{
							location.reload();
						})
					}
				}
			});
			return false;
		});
		$("#btnDelete").on("click", function() { //btnDelete
			var bugNum = $("#bugIdx").val();		
			  Swal.fire({
				    title: '삭제하시겠습니까?',
				    type: 'warning',
				    showCancelButton: true,
				    confirmButtonColor: '#3085d6',
				    confirmButtonText: '삭제',
				    cancelButtonColor: '#d33',
				    cancelButtonText: '취소'
				  }).then((result) => {
					  if (result.value) {
						$.ajax({
							url : "/bugTracking/delete",
							data : {"bug_idx" : bugNum,
									"${_csrf.parameterName}" : "${_csrf.token}"},
							type : "post",
							success : function(result) {
								if (result) {
						              Swal.fire({
						                  title : '정상적으로 삭제되었습니다',
						                  type: 'success'
						                  
						                }).then(()=>{
											location.reload();
										})
								} else {
						            Swal.fire({
						                title : '삭제가 실패되었습니다',
						                type: 'error'
						               })
								}
							}
						});
				 	 }
				})
			}); //btnDelete
		
		
		
		
		
		
		
		$("#btnReplyWrite").on("click", function() {
			$.ajax({
				url : "/bugReply/write",
				data : {
					"bug_reply_content" : $("#bugReplyContent").val(),
					"bug_idx" : $("#bugIdx").val(),
					"${_csrf.parameterName}" : "${_csrf.token}"
				},
				type : "post",
				success : function(result) {
					Swal.fire({
						title: '댓글이 정삭적으로 등록되었습니다',
						type: 'success',
						
					})
					$("#bugReplyContent").val("");
					replyBugList();
				}
			});
		});
	</script>
	<!-- 버그 게시판 리스트 -->
	
</body>
</html>



