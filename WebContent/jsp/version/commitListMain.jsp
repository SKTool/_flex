<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<security:authentication property="principal" var="username"/>

<style>
#session02-container .row:hover {
  background: rgb(242, 246, 248);
}
</style>
  
<script>
	function clipboardCopy(val) {
	  var dummy = document.createElement("textarea");
	  document.body.appendChild(dummy);
	  dummy.value = val;
	  dummy.select();
	  document.execCommand("copy");
	  document.body.removeChild(dummy);
	}
</script>
  
  
  <div  style="background-color: white;">
  
  


		<div>
      		<br> 
      		<br> 
	      		<h2 style="width : 95%;margin-left : 5%; color : gray;"> Commit </h2>
	      		<p style=" margin-left : 5%;color : lightgray; font-size : 0.9em;"> GitHub 에서 프로젝트의 Commit 정보를 가져오세요.</p>
	      		<hr style="width : 98%; margin-left : 1%;">
      		<br>
      	</div>




  
  <div id="top-btn" class="text-right" style="padding: 0 15% 0 15%;">
  
  <script>
  	$('#top-btn').append(
  		$('<button/>', {
  			class: "btn btn-info btn-sm mr-2",
  			text: "WebHook 주소",
  			click: function() {

  				Swal.fire({
  				  title: '당신의 WebHook 주소입니다.',
  				  input: 'text',
//   				  inputValue: 'http://www.flex.com?project_member_idx=...user_id=...',
  				  inputValue: 'http://52.79.235.120:8080/userBell/alertUserBell?project_member_idx=${ sessionScope.project_member_idx }&user_id=${username}',
  				  showCancelButton: false
//   				  inputValidator: (value) => {
//   				    return !value && 'You need to write something!'
//   				  }
  				});	
  			}
  		})
	);
  	$('#top-btn').append(
  		$('<button/>', {
  			class: "btn btn-danger btn-sm ml-2",
  			text: "깃허브 저장소 삭제",
  			click: function() {
  				
  				Swal.fire({
  				  title: '삭제하시겠습니까?',
  				  text: "삭제할 경우 복구할 수 없습니다",
  				  type: 'warning',
  				  showCancelButton: true,
  				  cancelButtonText: '취소',
  				  confirmButtonColor: '#3085d6',
  				  cancelButtonColor: '#d33',
  				  confirmButtonText: '삭제하겠습니다!'
  				}).then((result) => {
  				  if (result.value) {
  					$.ajax({
  						url: '/version/commit/removeGitBygitIdAndProjectMemberIdx',
  						type: 'POST',
						data: {'${_csrf.parameterName}' : '${_csrf.token}'},
  						success: function(data) {
							if ( data ){
							  Swal.fire({
								title: '삭제되었습니다!',
// 								text: '깃 계정이 삭제되었습니다.',
								type: 'success'
							  }).then((result)  => {
								  if (result.value) {
							  		window.location.reload();
									  
								  }
							  });
							} else {
								alert("delete false")
							}
  						},
  						error: function() {
  							alert("error");
  						}
  					});
  				  }
  				});
  				
  			}
  		})
	);

  </script>
  
  </div>
  
  <div style="margin: 1% 80% 0 10%">
    <div class="btn-group">
      <button id="selected-branch" type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false" style="font-size: .8rem;">
      	<c:choose>
      		<c:when test="${ selectedBranch ne null }">
				  <a>Branch :</a><a style="font-weight: bold;"> ${ selectedBranch }</a>
      		</c:when>
      		<c:otherwise>
				  <a>Branch : </a>
      		</c:otherwise>
      	</c:choose>
      
      </button>

      <!-- 검색 DIV -->
      <div class="dropdown-menu dropdown-menu-left">
      <!-- <div class="dropdown-menu dropun-menu-right dropdown-menu-lg-left"> -->
      <!-- <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg-left pb-0 show"> -->

        <div class="px-3" style="font-weight: bold; font-size: .8rem;">
          <a> Switch branche/tag </a>
        </div>

        <div class="dropdown-divider"></div>

        <div class="p-2">
          <input type="text" class="form-control" id="exampleDropdownFormEmail1" placeholder="Filter Branches/Tags">
          <script>
          	$('#exampleDropdownFormEmail1').on("keyup", function(data){
          		console.log($(this).val());
          		$.ajax({
          			url: '/version/commit/searchBranch',
          			data: {"branch":$(this).val(),'${_csrf.parameterName}' : '${_csrf.token}'},
          			type: 'post',
          			dataType: 'json',
          			success: function(date) {
          				console.log(date);
          				$('#branch-search-modal').children().remove();
          				for (i in date) {
          					$('#branch-search-modal').append('<a href="/version/commit?branch='+date[i]+'" class="list-group-item list-group-item-action" onclick="btn();">'+date[i]+'</a>')
//           					$('#branch-search-modal').append('<a href="/version/commit?branch='+date[i]+'" class="list-group-item list-group-item-action">'+date[i]+'</a>')
          				}
          			}
          			
          		});
          	});
          
          </script>
        </div>

        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link py-1 active" style="font-size: .8rem;" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Branch</a>
            <a class="nav-item nav-link py-1" style="font-size: .8rem;" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Tag</a>
          </div>
        </nav>
        <script>
            $('#nav-home-tab').on("click", function(e){
              $('#nav-home-tab').tab('show')
              e.stopPropagation();
            });
            $('#nav-profile-tab').on("click", function(e){
              $('#nav-profile-tab').tab('show')
              e.stopPropagation();
            });
        </script>
        <div class="tab-content" id="nav-tabContent">

          <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

            <div style="overflow:auto; width: 20rem; height: 20rem;">
              <div id="branch-search-modal" class="list-group list-group-flush px-3" style="font-size: 1rem;">
<!--                 <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a> -->

				<c:if test="${ branches ne null }">
				
					<c:forEach items="${branches}" var="branch" >
						<a href="/version/commit?branch=${ branch }" class="list-group-item list-group-item-action" onclick="btn();">${branch}</a>
					</c:forEach>

				</c:if>
				
				
				<script>
				
					function btn() {
						
						var timerInterval
						Swal.fire({
// 						  title: 'Auto close alert!',
						  title: '정보를 로딩하고 있습니다.',
// 						  html: 'I will close in <strong></strong> seconds.',
						  timer: 3000,
						  onBeforeOpen: () => {
							Swal.showLoading()
// 							location.href="/version/commit?branch=master";
							timerInterval = setInterval(() => {
							  Swal.getContent().querySelector('strong')
								.textContent = Swal.getTimerLeft()
							}, 200)
						  },
						  onClose: () => {
							clearInterval(timerInterval)
						  }
						}).then((result) => {
						  if (
							// Read more about handling dismissals
							result.dismiss === Swal.DismissReason.timer
						  ) {
							console.log('I was closed by the timer')
						  }
						})
							
					}

				</script>
				
				

              </div>
            </div>
            
          </div>

          <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

            <div style="overflow:auto; width: 20rem; height: 20rem;">
              <div class="list-group list-group-flush p-3" style="font-size: .7rem; border-top: 0;">
                <a href="#" class="list-group-item list-group-item-action" style="border-top: 0;">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
              </div>
            </div>

          </div>

        </div>

      </div>
      <!-- END 검색 -->


    </div>
  </div>

  <script>
    $(function() {
      // alert("sdf");
      // $("#aaaa").text("sdfsdf");
      // $("#idid > a").text("sdfsdf");
      
//     	loadData();
    });
  </script>
    
  <div id="session02-container" class="mx-5 p-3">
    <div id="date">

    
		<div class="p-3" style="border-left: 1px solid lightgray; margin: 0 10% 0 10%;">
			<div class="container-fluid">
	<c:if test="${ commits ne null }">
	<c:forEach items="${commits}" var="commit" varStatus="status">
		<c:if test="${commit.date ne null }">
			
			<div class="row">
				<div class="col-12" >
				  <!-- git-commit-date-line -->
				  <div id="git-commit-date-line" style="margin: 1% 0 1% 0; position: relative; right:2.2rem;" >
					<i class="fas fa-mars-stroke-h p-1" style="position: relative; right:0.2rem; background-color: white;"></i>
					<!-- 날짜  -->
					<a> ${commit.date} </a>
				  </div>
				</div>
			</div>
		</c:if>


				  <!-- git-commit-row -->
				  <div class="row" style="border: 1px solid whitesmoke;">
					<div class="col-8">
					  <div>
						<!-- 예비 id -->
						<!-- git-commit-row-left-col-title -->
						<div id="idid"> 
						<!-- 내용...? 제목  -->
						  <a style="font-weight: bold;"> ${ commit.title } </a> 
						</div>

						<!-- git-commit-row-left-col-datail-date -->
						<div> 
		<!--                   <img src="./img/images.png" style=" border: 0.1 rem solid black;width: 1.5rem;"/>  -->
						<!-- 작성자 + 시간 -->
						  <a style="font-size: 0.9rem;"> ${ commit.name } 님이 ${ commit.time } 에 커밋하였습니다. </a> 
						</div>
					  </div>

					</div>
					<div class="col-4 py-1 text-right">
					  <div>
						<div class="btn-toolbar" style="display: inline-block;" role="toolbar" aria-label="toolbar with button groups">
						  <div class="btn-group mr-2" role="group" aria-label="first group">
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="clipboardCopy($('#sha-${status.count}').attr('value'));">
							  <i class="far fa-clipboard"></i>
							</button>
							<!-- git-commit-row-right-col-btn-group-1-commit -->
							<button type="button" class="btn btn-outline-primary btn-sm" style="width: 5rem;" onclick="window.open('${commit.commit_url}');">
							  <!-- 예비 id -->
								<!-- url -->
							  <a id="sha-${status.count}" value="${ commit.sha }">${ commit.short_sha }</a>
							</button>
						  </div>
						  <div class="btn-group mr-2" role="group" aria-label="second group">
							<!-- git-commit-row-right-col-btn-group-2-diff -->
								<!-- url -->
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="window.open('${commit.tree_url}');"><i class="fas fa-code"></i></button>
						  </div>
						</div>
					  </div>
					</div>
				  </div>					  

	</c:forEach> 
	</c:if>
			</div>
		  </div>


  </div>
</div>




  
  
  
  
  
</div>