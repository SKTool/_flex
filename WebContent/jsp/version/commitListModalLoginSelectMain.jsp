<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    
    

<c:if test="${ hasGitInProjectMember ne true }">

	<script>

		<!-- 임시 코드 -->
		$(function() {
				$("#exampleModalCenter").modal('show');
				// $("#list-git-repo").modal('show'); 
		})
	</script>

</c:if>

<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"> -->
<!-- 	Git 로그인  -->
<!-- </button> -->
<!-- 저장소 선택 모달 -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#list-git-repo"> -->
<!--     Git 저장소 -->
<!-- </button> -->

<div class="modal-open">


    <!-- 깃 로그인 모달 -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header border-0">

            <h5 class="modal-title m-auto" id="exampleModalCenterTitle">Github 로그인 계정</h5>
            <button type="button" class="close ml-0" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>

        </div>
        <div class="modal-body">

            <!-- <form id="git-modal"> -->
                <div class="form-group">
                  <label for="git-username" style="font-weight: bold">사용자이름</label>
                  <input type="email" class="form-control" id="git-username" name="git-username" aria-describedby="emailHelp" placeholder="예) Username">
                  <small id="emailHelp" class="form-text text-muted">우리는 너의 정보를 절대로 공유하지 않습니다.</small>
                </div>
                <div class="form-group">
                  <label for="git-pw" style="font-weight: bold">패스워드</label> 
                  <input type="password" class="form-control" id="git-pw"  name="git-pw" placeholder="Password">
                </div>
                <!-- <div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div> -->
                <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
              <!-- </form> -->

        </div>
        <div class="modal-footer">
            <button id="git-cancel" type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
            <button id="git-login" type="button" class="btn btn-primary" onclick=" $('#exampleModalCenter').modal('hide'); $('#list-git-repo').modal('show'); ">로그인</button>
        </div>



        <script>
            $('#git-login').on('click', function() {
                var username = $('#git-username').val();
                var password = $('#git-pw').val();
                var base64Account = btoa(username+':'+password);

                var url = 'https://api.github.com/user/repos';

                $.ajax({
                    url: url, 
                    dataType: 'json',
                    type: 'get', 
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Authorization","Basic "+base64Account);
                    },
                    success: function(date) {
                        //console.log(date);
                        $('#list-git-repo-git-id').val($('#git-username').val());
                        $('#list-git-repo-git-pw').val($('#git-pw').val());
                        for (i in date) {
                            $('#list-git-repo-select').append(
                                '<option value="'+date[i].full_name+'">'+date[i].full_name+'</option>'
                            );
                            console.log(date[i].full_name);
                        }

                        // project-memeber-idx 를 가져온다
//                         $.ajax({
//                             url: '#',
//                             dataType: 'json',
//                             type: 'post',
//                             success: function(date) {
//                                 $('#list-git-project-member-select').append();
//                                 // alert("성공");
//                             }, 
//                             error: function() {
//                                 // alert("서버와 연결에 실패하였습니다");
//                             }
//                         });
                    }, 
                    error: function() {
                        alert("Git 서버와 연결에 실패하였습니다");
                    }
                });

            });
        </script>
        </div>
    </div>
    </div>

</div>

<!-- 저장소 선택 모달 -->

<!-- <div class="modal fade" id="list-git-repo" style="background-color: rgba(100,100,100,0.8);" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"> -->
<div class="modal fade" id="list-git-repo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
        <div class="modal-header border-0">
            <h5 class="modal-title" id="exampleModalCenterTitle">Github 저장소</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <!-- <div style="overflow-y: auto;"> -->
            <form id="list-git-repo-form" action="/version/commit/saveGitIdPw" method="POST">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">  
                <input type="hidden" id="list-git-repo-git-id" name="git-id"/>
                <input type="hidden" id="list-git-repo-git-pw" name="git-pw"/>

                <div class="form-group">
                    <label for="list-git-repo-select" style="font-weight: bold"> 저장소 </label>
                    <select class="custom-select" name="repository" id="list-git-repo-select">
                        <!-- <option selected>저장소를 선택하세요.</option> -->
                    </select>
                </div>
                
<!--                 <div class="form-group"> -->
<!--                     <label for="list-git-project-member-select" style="font-weight: bold"> 프로젝트 </label> -->
<!--                     <select class="custom-select" name="project-member-idx" id="list-git-project-member-select"> -->
<!--                         <option value='' selected>프로젝트를 선택하세요.</option> -->
<!--                     </select> -->
<!--                 </div> -->

                <!-- <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action py-1">Porta ac consectetur ac</a>
                    <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1" aria-disabled="true">Vestibulum at eros</a>
                </div> -->
            <!-- </div>          -->

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">취소</button>
            <button type="button" class="btn btn-primary" onclick="selectRepo()">선택</button>
        </div>
        </div>
    </div>
</div>

    <script>
        function selectRepo() {
            Swal.fire({
                type: 'success',
                title: '로딩 완료',
                text: '저장소를 읽어오는데 성공하였습니다.',
                showConfirmButton: true 
            }).then((result) => {
                // $('#exampleModalCenter').modal('hide'); 
                $('#list-git-repo').modal('hide');
                // $('#list-git-repo-form').
                $('#list-git-repo-form').submit();

                // var params = $('#list-git-repo-form').serialize();
                // $.ajax({
                //     url: '#',
                //     type: 'post',
                //     data: params,
                //     async: false,
                //     success: function() {
                //         alert("페이지 표시 에 성공");
                //     }, 
                //     error: function() {
                //         alert("페이지 표시 에 실패");
                //     }
                // });

            });
        }
    </script>
    