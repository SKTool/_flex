<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>


<style>
	#nav-tabContent > div div:hover {
	  background: rgb(242, 246, 248);
	}
</style>
    
<script>
var selectState; 


var $icon = 
	'<div>'
	+	'<i class="fas fa-comment-dots m-2"></i> Comment'
	+ '</div>'

var $commentBody = $('<div/>',{
	class: "mt-3 p-2 gb-light" ,
	style: "font-weight: bold;"
});
	
function send_comment(number, obj) {
	console.log(number);
	console.log(obj);
	$.ajax({
		url: '/version/issue/sendComment',
		type: 'POST',
		dataType: 'json',
		data: {"number": number , 
				"comment": obj , 
				'${_csrf.parameterName}' : '${_csrf.token}'},
		success: function(data) {
			if (data === 1) {
// 				alert("success");
				$('#issueModal').modal('toggle');
// 				$('#issueModal').modal('show');
// 				$('#issueModal').modal('handleUpdate');
			}
		}, 
		error: function() {
			alert("send Comment error");
		}
		
	});
}	

function issue_btn(obj) {
	removeRow($('#issue-modal-body-content'));
//		$('#issue-modal-title').text( $('#issue-open-row-state-'+obj).val() + $('#issue-open-row-out-span-'+obj).text()  + ' #' +  $('#issue-open-row-number-'+i).val() );
	removeRow($('#issue-modal-title'));
	$('#issue-modal-title').append( '<span class="badge badge-success mr-2">'+$('#issue-open-row-state-'+obj).val()+'</spen>');
	$('#issue-modal-title').append( '<a>'+$('#issue-open-row-out-span-'+obj).text()+'</a>' );

	$('#issue-modal-title').append( '<a id="issue-number" style="color: lightgray;font-size: 1.5rem;">' +  $('#issue-open-row-number-'+obj).val() + '</a>' );
	
	
	
	$('#issue-modal-body-header').text($('#issue-open-row-out-a-'+obj).text());
	
	$('#issue-modal-card-body').text($('#issue-open-row-body-'+obj).attr('value'));
	var number = $('#issue-open-row-number-'+obj).attr('value');

	$.ajax({
		url: '/version/issue/comment',
		type: 'POST',
		dataType: 'json',
		data: {"number": number , '${_csrf.parameterName}' : '${_csrf.token}'},
		success: function(date) {
//				removeRow($('#issue-modal-body-content'));

			if (date.length !== 0 ) {
				$commentBody.append($icon);
				for (i in date) {
// 					console.log(date[i]);
					var $card = 	
							 '<div class="card m-2">'
						+		'<div class="card-header">' 
						+			'<p class="card-text" style="font-weight: 5">'
						+				date[i].login + ' ' + date[i].updated_at
						+			'</p>' 
						+		'</div>'
						+		'<div class="card-body">'
						+			'<p class="card-text" style="font-weight: 5">'
						+				date[i].body
						+			'</p>' 
						+		'</div>'
						+	'</div>';
					
					$commentBody.append($card);
				}
				$('#issue-modal-body-content').append($commentBody);

			}

		}, 
		error: function() {
			alert("error");
			
		}
	});
	
}

function issue_closed_btn(obj) {
	removeRow($('#issue-modal-body-content'));
	removeRow($('#issue-modal-title'));
	$('#issue-modal-title').append( '<span class="badge badge-danger mr-2">'+$('#issue-closed-row-state-'+obj).val()+'</spen>');
	$('#issue-modal-title').append( '<a>'+$('#issue-closed-row-out-span-'+obj).text()+'</a>' );
// 	$('#issue-modal-title').append( '<a>'+$('#issue-closed-row-number-'+obj).val() + '</a>' );

	$('#issue-modal-title').append( '<a id="issue-number" style="color: lightgray;font-size: 1.5rem;">' +  $('#issue-closed-row-number-'+obj).val() + '</a>' );

	$('#issue-modal-body-header').text($('#issue-closed-row-out-a-'+obj).text());
	$('#issue-modal-card-body').text($('#issue-closed-row-body-'+obj).attr('value'));
	var number = $('#issue-closed-row-number-'+obj).attr('value');

	$.ajax({
		url: '/version/issue/comment',
		type: 'POST',
		dataType: 'json',
		data: {"number": number , '${_csrf.parameterName}' : '${_csrf.token}'},
		success: function(date) {
			//removeRow($('#issue-modal-body-content'));

			if (date.length !== 0 ) {
				$commentBody.append($icon);
				for (i in date) {
// 					console.log(date[i]);

					var $card = 	
							 '<div class="card m-2">'
						+		'<div class="card-header">' 
						+			'<p class="card-text" style="font-weight: 5">'
						+			date[i].login + ' ' + date[i].updated_at
						+			'</p>'
						+		'</div>'
						+		'<div class="card-body">'
						+			'<p class="card-text" style="font-weight: 5">'
						+				date[i].body
						+			'</p>' 
						+		'</div>'
						+	'</div>';
					
					$commentBody.append($card);
				}
				$('#issue-modal-body-content').append($commentBody);

			}
		}, 
		error: function() {
			alert("error");
			
		}
	});
	
}



function removeRow(obj) {
	obj.find('*').remove();
}


function post_open_issue(param) {
	$.ajax({
		url: '/version/issue/getIssues/open',
//			url: url,
//			data: {'${_csrf.parameterName}' : '${_csrf.token}'},
		data: param,
		type: 'post',
		dataType: 'json',
		success: function(date) {
			removeRow($('#nav-open'));
// 			console.log(date)
			
			for (i in date) {
				$('#nav-open').append( 
				  '<div id="issue-open-row-'+i+'" class="row m-2" data-toggle="modal" data-target="#issueModal" onclick="console.log(issue_btn); issue_btn('+i+');">'
				  + '<div class="col-12 border px-3 py-1">'
				  +		'<input type="hidden" id="issue-open-row-body-'+i+'" value="'+date[i].body+'" />'
				  +		'<input type="hidden" id="issue-open-row-number-'+i+'" value="'+date[i].number+'" />'
				  +		'<input type="hidden" id="issue-open-row-state-'+i+'" value="'+date[i].state+'" />'
				  + '<div>'
				  +   '<i class="fas fa-exclamation-circle text-success mr-2"></i>'
				  +   '<a id="issue-open-row-out-span-'+i+'" style="font-size: 1.2rem; font-weight: blod;">'
				  +     date[i].title  
				  +   ' &ensp; </a>'
				  +   '<div style="float: right; width: 3rem;"> <i class="far fa-comment-alt"></i> '+date[i].comments+' </div>'
				  + '</div>'
				  + '<div>'
				  +   '<a id="issue-open-row-out-a-'+i+'" class="pl-4" style="font-size: .8rem;">'
				  +     date[i].updated_at + ' ' + date[i].login
				  +	   '</a>'
				  + '</div>'

				  + '</div>'
				  +'</div>'
				);
				if (date[i].labels.length !== 0) {
					for (j in date[i].labels) {
						
						console.log(date[i].labels);
						
						console.log(date[i].labels[j].color);
						
						console.log(date[i].labels[j].name);
						
						$('#issue-open-row-out-span-'+i).after(
							  '<span class="badge badge-pill mr-1" style="color: whitesmoke; font-size: .7rem; background-color: #'+date[i].labels[j].color+';">'+date[i].labels[j].name+'</span>'
						);
					}
				}
			}
			
		}, 
		error: function() {
			alert("error");	
		}
	});	
}

function post_closed_issue(param) {
// 	console.log(param);

	$.ajax({
		url: '/version/issue/getIssues/closed',
//			url: url,
//			data: {'${_csrf.parameterName}' : '${_csrf.token}'},
		data: param,
		type: 'post',
		dataType: 'json',
		success: function(date) {
			removeRow($('#nav-closed'));
// 			console.log(date)
			for (i in date) {
				$('#nav-closed').append( 
				  '<div id="issue-closed-row-'+i+'" class="row m-2" data-toggle="modal" data-target="#issueModal" onclick="issue_closed_btn('+i+');">'
				  + '<div class="col-12 border px-3 py-1">'
				  +		'<input type="hidden" id="issue-closed-row-body-'+i+'" value="'+date[i].body+'" />'
				  +		'<input type="hidden" id="issue-closed-row-number-'+i+'" value="'+date[i].number+'" />'
				  +		'<input type="hidden" id="issue-closed-row-state-'+i+'" value="'+date[i].state+'" />'
				  + '<div>'
				  +   '<i class="fas fa-exclamation-circle text-danger mr-2"></i>'
				  +   '<a id="issue-closed-row-out-span-'+i+'" style="font-size: 1.2rem; font-weight: blod;">'
				  +     date[i].title
				  +   '&ensp; </a>'
				  +   '<div style="float: right; width: 3rem;"> <i class="far fa-comment-alt"></i> '+date[i].comments+' </div>'
				  + '</div>'
				  + '<div>'
				  +   '<a id="issue-closed-row-out-a-'+i+'" class="pl-4" style="font-size: .8rem;">'
				  +     date[i].updated_at + ' ' + date[i].login
				  +	   '</a>'
				  + '</div>'

				  + '</div>'
				  +'</div>'
				);
				if (date[i].labels.length !== 0) {
					for (j in date[i].labels) {
						$('#issue-closed-row-out-span-'+i).after(
							  '<span class="badge badge-pill mr-1" style="color: whitesmoke; font-size: .7rem; background-color: #'+date[i].labels[j].color+';">'+date[i].labels[j].name+'</span>'
						);
					}
				}

			}
			
		}, 
		error: function() {
			alert("error");	
		}
	});
}





$(function() {
	
	$.ajax({
		url: '/version/issue/selectLabels',
		data: {'${_csrf.parameterName}' : '${_csrf.token}'},
		dataType: 'json',
		  type: 'post',
		  success: function(data) {
			for(i in data) {
				$('#top-labels').append(
					$('<a/>', {
						class: "dropdown-item",
						html: '<span class="badge badge-pill" style="background-color: #'+data[i].color+';">'+data[i].name+'</span>',
						click: function() {
							var name = $(this).children().text();
						
							var param = {'labels': name, '${_csrf.parameterName}' : '${_csrf.token}'}
							
							if ( selectState === 'open') {
								post_open_issue(param);
							} else {
								post_closed_issue(param);
								
							}
						}
					})
				);
			  }
		  },
		error: function() {
			alert("error");
			  
		}
	});

	
	$('#nav-open-tab').on('click', function() {
		selectState = 'open';
		console.log('nav-open-tab');
		var param = {'${_csrf.parameterName}' : '${_csrf.token}'}
		post_open_issue(param);

	});

	$('#nav-closed-tab').on('click', function() {
		selectState = 'closed';
		console.log('nav-closed-tab');
		var param = {'${_csrf.parameterName}' : '${_csrf.token}'}
		post_closed_issue(param);

	});
	
	$( "#nav-open-tab" ).trigger( "click" );
	
});

</script>
   
    
    
  <div  style="background-color: white;">
  
  




		<div>
      		<br> 
      		<br> 
	      		<h2 style="width : 95%;margin-left : 5%; color : gray;"> Issue </h2>
	      		<p style=" margin-left : 5%;color : lightgray; font-size : 0.9em;"> 프로젝트에서 발생하는 이슈을 보여주고 댓글을 쓸 수 있습니다.</p>
	      		<hr style="width : 98%; margin-left : 1%;">
      		<br>
      	</div>






      <div class="container" style="margin-left: 200px; margin-right: 200px;">

<!-- 		<div class="row mb-4"> -->
<!-- 			<div class="col-12"> -->

<!-- 				<div class="input-group" style="width: 30%;"> -->
<!-- 				  <div class="input-group-prepend"> -->
<!-- 					<span class="input-group-text" id="basic-addon3">Filter</span> -->
<!-- 				  </div> -->
<!-- 				  <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3"> -->
<!-- 				</div> -->

<!-- 			</div> -->
<!-- 		</div> -->


        <div class="row">
          <div class="col">

            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-open-tab" data-toggle="tab" href="#nav-open" role="tab" aria-controls="nav-open" aria-selected="true">Open</a>
					<a class="nav-item nav-link" id="nav-closed-tab" data-toggle="tab" href="#nav-closed" role="tab" aria-controls="nav-profile" aria-selected="false">Closed</a>

                <div style="margin-left: 70%;">
                  <div class="text-right">
<!--                     <div class="btn-group" role="group"> -->
<!--                       <button id="btnGroupDrop1" type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
<!--                       Author  -->
<!--                       </button> -->
<!--                       <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"> -->
<!--                         <a class="dropdown-item" href="#"> User 1 </a> -->
<!--                         <a class="dropdown-item" href="#"> User 2 </a> -->
<!--                       </div> -->
<!--                     </div> -->
<!--                     <div class="btn-group" role="group"> -->
<!--                       <button id="btnGroupDrop2" type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
<!--                         Projects   -->
<!--                       </button> -->
<!--                       <div class="dropdown-menu" aria-labelledby="btnGroupDrop2"> -->
<!--                         <a class="dropdown-item" href="#"> User 1 </a> -->
<!--                         <a class="dropdown-item" href="#"> User 2 </a> -->
<!--                       </div> -->
<!--                     </div> -->

                    <div class="btn-group" role="group">
                      <button id="btnGroupDrop3" type="button" class="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Labels
                      </button>
                      <div id="top-labels" class="dropdown-menu" aria-labelledby="btnGroupDrop3">
						<!-- ajax -->
	
                      </div>
                    </div>
                  </div>

                </div>

              </div>
            </nav>

          </div>
          <!-- col -->
        </div>
        <!-- row -->

          <div class="tab-content" id="nav-tabContent">

		  	<!-- OPEN -->
            <div class="tab-pane fade show active" id="nav-open" role="tabpanel" aria-labelledby="nav-open-tab">


            </div>
		  	<!-- CLOSE -->
            <div class="tab-pane fade" id="nav-closed" role="tabpanel" aria-labelledby="nav-closed-tab">

            </div>

          </div>

      </div>
      <!-- container -->

  </div>

	<!-- Modal -->
	<div class="modal fade" id="issueModal" tabindex="-1" role="dialog" aria-labelledby="issueModalTitle" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title m-3" id="issue-modal-title">  </h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>

		  </div>

		  <div id="issue-modal-body" class="modal-body">


			<div class="card m-2">
			  <div id="issue-modal-body-header" class="card-header"></div>
			  <div id="issue-modal-card-body" class="card-body"> <p class="card-text"></p> </div>
			</div>
			<div id="issue-modal-body-content">

			</div>


		  </div>
		  	<div class="modal-footer" style="flex-direction: column;">

	<!-- 			<form action="#" method="post" style="width: 100%;"> -->
	<%-- 				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"> --%>
				<div class="input-group">
				  <div class="input-group-prepend">
					<span class="input-group-text">With textarea</span>
				  </div>
				  <textarea id="issue-modal-comment" name="issue-modal-comment" class="form-control" aria-label="With textarea" rows="4"></textarea>
				</div>
	<!-- 			</form> -->
				<div class="my-3 text-right" style="width: 100%;">
					<button type="submit" class="btn btn-info" onclick="console.log( $('#issue-number').text() ); send_comment( $('#issue-number').text(), $('#issue-modal-comment').val() ); "> 보내기 </button>
				</div>

		  </div>
		</div>
	  </div>
	</div>
