
function msgboardCheck(formname){
	var form = $("#"+formname);
	if(!(form.find('input[name="msgboard_nick"]').val().trim())){
//		alert("제목을 작성하세요");
		Swal.fire({
			text : '닉네임을 작성하세요. * 필수',
			type : 'info'
		})
		return false;
	}
	if(!(form.find('input[name="msgboard_title"]').val().trim())){
//		alert("프로젝트 개요를 작성하세요");
		Swal.fire({
			text : '제목을 작성하세요. * 필수',
			type : 'info'
		})
		return false;
	}
	if(!(form.find('textarea[name="msgboard_content"]').val().trim())){
//		alert("모집 인원 수를 입력하세요.");
		Swal.fire({
			text : '내용을 작성하세요. * 필수',
			type : 'info'
		})
		return false;
	}
	return true;
}
