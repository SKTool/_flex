
function recruitmentCheck(formname){
	var form = $("#"+formname);
	if(!(form.find('input[name="recruitment_title"]').val().trim())){
//		alert("제목을 작성하세요");
		Swal.fire({
			text : 'Title을 작성하세요. * 필수',
			type : 'info'
		})
		return false;
	}
	if(!(form.find('input[name="recruitment_content"]').val().trim())){
//		alert("프로젝트 개요를 작성하세요");
		Swal.fire({
			text : 'Project Summary를 작성하세요. * 필수',
			type : 'info'
		})
		return false;
	}
	if(!(form.find('input[name="recruitment_start"]').val().trim())){
//		alert("시작 날짜를 입력하세요.");
		Swal.fire({
			text : 'Project Start Date를 입력하세요. * 필수',
			type : 'info'
		})
		return false;
	}
//	if(!(form.find('input[name="recruitment_end"]').val().trim())){
////		alert("끝나는 날짜를 입력하세요.");
//		Swal.fire({
//			text : '끝나는 날짜를 입력하세요.',
//			type : 'info'
//		})
//		return false;
//	}
	return true;
}

