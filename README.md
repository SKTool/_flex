# Flex ( 프로젝트 관리 사이트 )

## 목차
[ 시연 동영상 ](#시연-동영상)  
* [1. 이력관리-(Github-연동 - Commit-Issue)](#이력관리)  
    * [1-1. Github-로그인-(인증) - 3:21초](#github-로그인)  
    * [1-2. Commit-정보 - 3:30초](#commit-정보)    
    * [1-3. Issue-정보-표시및-댓글-추가 - 4.11초](#issue-정보-표시및-댓글-추가)  
* [2. 알람 (websocket, 알람창)](#알람)  
    * [2-1. 알람창 - 5:09초](#알람창)  
    * [2-2. 알림 저장소 - 7:17초](#알림-저장소)    
* [3. 관리자 페이지](#관리자 페이지)  
    * [2-1. 관리자 대시보드 - 9:47초](#관리자-대시보드)  
    * [2-2. 프로젝트와 멤버 리스트 - 9:53초](#프로젝트와-멤버-리스트)    

## 파일
[공유파일-Flex](https://drive.google.com/drive/folders/1OzHi4BmJ58_9-iDIQWcZhrJfUS00C2QT)
<br/>
    **PDF 파일 + 동영상** 
<br/>
## 프로젝트 목적
    협업을 더욱 스마트하게   
    더 이상 이메일, 엑셀, 메모장 등으로 업무를 관리하지 마세요.  
    Flex는 업무 및 프로젝트 관리, 파일공유 등이 모든 기능을 하나의 탄탄한 웹에 담았습니다.  

## 개발 기간
    19년 02월 1일 ~ 19년 03월 29일

## 인원 수 
    5명

## 담당 파트
* **설대형** 
<br/>**버전관리(Commit, Issue), 알람, 관리자 페이지**
* 김수 
<br/> 로그인, 회원가입, 파일 공유  
* 유윤건
<br/> 개발자 모집 게시판, 채팅
* 김보민 
<br/> 작업일정, TODO 리스트, 버그 트레킹 게시판
* 김솔이 
<br/> 프로젝트 일정, 공지사항, QnA 

## 시연 동영상 
  
[![Video Label](./blog/flex_main.PNG)](https://r3---sn-i3beln7z.c.drive.google.com/videoplayback?expire=1559199032&ei=-ETvXI_1E9P9uQXq2ZGYDg&ip=210.96.218.100&cp=QVNKQUJfWFBRQ1hOOlhOTXdwbTZ6SkpsMFQ4YWJ3bGN1N0dHbC1NN3FYZXlMWm5FNGt1T2J6Nng&id=e96c1ded3b9c497c&itag=22&source=webdrive&requiressl=yes&mm=30&mn=sn-i3beln7z&ms=nxu&mv=u&pl=21&ttl=transient&susc=dr&driveid=1SDgflzJp3Jlwqz9WLcbClhhI1FFJ1zxB&app=explorer&mime=video/mp4&dur=619.578&lmt=1555937186332507&mt=1559183731&sparams=expire,ei,ip,cp,id,itag,source,requiressl,ttl,susc,driveid,app,mime,dur,lmt&sig=ALgxI2wwRQIgcnUz_qQcjzjg-6X_sHFkubEZlBg4oG8F3jZ4tF-jjMwCIQDvNf1avOLjP3ug3zcNok9GaHWU5SNgmhK_Z7hqz48CEQ==&lsparams=mm,mn,ms,mv,pl&lsig=AHylml4wRgIhAOxsf52hWLNC0_A-D2IjNS7cV_Bp_3zDEbLeGDvA85P-AiEA5Oma8jPrs_WqHX-yaxSLg1Hz_X65LtAo66b0zJKxPmc=&cpn=1K8Rl4O_dYDjX3AS&c=WEB_EMBEDDED_PLAYER&cver=20190529)  
  
## 이력관리 
##### (Github 연동 Commit Issue)  

### Github 로그인
##### 3:21초

Github안에 존재하는 해당 프로젝트 저장소와 브랜치를 선택한 후 **Commit& Issue 정보를 표시** 하여 프로젝트의 버전을 보다 편하게 관리할 수 있게 도와준다. <br/>
**Github RestAPI** 을 사용할 때 필요한 Github ID 와 **OAuth token** 정보 입력하여 데이터베이스에 저장 <br/>
Github 계정에 포함된 저장소의 이름을 Github Rest API 를 사용하여 가져온 후 화면에 표시 및 데이터베이스에 저장 <br/>

<img src="./blog/GithubRESTApi-commit-01-gitlogin.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-commit-02-repository.png"  width="70%" height="50%">

### Commit 정보
##### 3:30초

1. **Github RestAPI** 을 사용해서 Commit 의 정보를 데이터베이스가 아닌 Github 에서 직접 **JSON** 형태로 으로 가져온다.  <br/>
2. **Jackson 라이브러리** 를 사용하여 자바 객체로 변환 후 필요한 데이터를 페이지에 보여준다. <br/>

<img src="./blog/GithubRESTApi-commit-03-gitBranch.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-commit-04-newBranch.png"  width="70%" height="50%">

### Issue 정보 표시및 댓글 추가 
##### 4.11초

1. **Github RestAPI** 을 사용해서 Issue 의 정보를 데이터베이스가 아닌 Github 에서 직접 **JSON** 형태로 으로 가져온다.  <br/>
2. **Jackson 라이브러리** 를 사용하여 자바 객체로 변환 후 필요한 데이터를 페이지에 보여준다  <br/>
3. **Github RestAPI** 를 사용해서 Issue 글에 댓글을 작성한 후 **Github** 에 Issue 로 보낼 수 있다.  <br/>

<img src="./blog/GithubRESTApi-issue-01-Open-List.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-issue-02-Closed-List.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-issue-03-Open-Write.png"  width="70%" height="50%">

-----

## 알람 
##### (websocket, 알람창)

### 알람창 
##### 5:09초

프로젝트는 진행할때 **발생하는 이벤트** 를 **실시간으로 확인** 할 수 있게 도움을 주기 위한 알림창 <br/>
**프로젝트 일정**, **작업 일정** 그리고 **Github의 이벤트** 가 발생하면 **sockjs, stomp 라이브러리** 를 활용하여 구현한 **WebSocket** 를  <br/> 
사용하여 실시간으로 데이터(간단한 내용, 알림 카운트) 받을 수 있게 구현 <br/><br/>

**Git Webhook** 은 외부에서 웹 서버로 데이터가 보내진다. (공인 아이피 필요) <br/>
공인 IP 주소를 가지고 있는 외부 서버를 사용하기 위해서 클라우드 서비스를 지원하는 **아마존 웹 서비스(AWS) 서버** 에 war 파일을 올려서 사용 <br/>

<img src="./blog/GithubRESTApi-WebHook-01Alert.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-WebHook-02-Alert-Commit.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-WebHook-06-Alert-Commit-push.png"  width="70%" height="50%">

## 알림 저장소 
##### 7:17초

일정 양을 넘어가는 알림과 일정이 지나간 알림을 차후에도 다시 확인할 수 있도록 **알림을 모아서 확인할 수 있는 페이지** 를 구현 <br/><br/>
알림은 프로젝트 일정과 작업 일정은 마감 일정과 미감 전 일정으로 두 분류로 나누어서 저장하고 GitEvent 는 깃 알림으로 저장 <br/>

<img src="./blog/GithubRESTApi-WebHook-05-Alert-Commit-list.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-WebHook-07-Alert-List.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-WebHook-08-Alert-List-Delete.png"  width="70%" height="50%">
<img src="./blog/GithubRESTApi-WebHook-09-Alert-List-Delete.png"  width="70%" height="50%">

-----

## 관리자 페이지

### 관리자 대시보드 
##### 9:47초

관리자가 간편하게 사이트에 프로젝트, 회원, 게시판 등의 정보를 통합한 통계를 한 눈에 확인할 수 있는 **관리자 대쉬보드 페이지** 이다. <br/><br/>
관리자만이 접속할 수 있게 **Spring Security** 를 사용하여 일반 사용자 접근을 제한 <br/>
관리자 페이지 그래프는 **chart.js** 사용하여 구현

<img src="./blog/AdminPage-01-DashBoard.png"  width="70%" height="50%">

### 프로젝트와 멤버 리스트
##### 9:53초

관리자가 프로젝트와 멤버의 상태를 확인할 수 있고 여러개를 선택해서 즉시 삭제할 수 있는 페이지 <br/><br/>
각 페이지는 10개씩 표시되고 나머지는 하단에 존재하는 **페이징 바** 를 클릭하여 다음 정보를 확인할 수 있습니다. <br/>

<img src="./blog/AdminPage-04-MemberListe.png"  width="70%" height="50%">
<img src="./blog/AdminPage-05-MemberListe-Delete.png"  width="70%" height="50%">

